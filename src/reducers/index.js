import { configureStore } from "@reduxjs/toolkit";
import auth from "../components/authorization/store";
import studentReducer from "../components/layout/Students/store";
import departmentReducer from "../components/layout/Departments/store";
import batchReducer from "../components/layout/Batch/store";
import classReducer from "../components/layout/Class/store";
import scheduleReducer from "../components/layout/Schedule/store";
import subjectReducer from "../components/layout/Subjects/store";
import locationReducer from "../components/layout/Locations/store";
import resultReducer from "../components/layout/Result/store";
import professorReducer from "../components/layout/Professors/store";
import contactsReducer from "../components/layout/Contacts/store";
import profileReducer from "../components/layout/Profile/store";
import helpReducer from "../components/layout/Help/store";

const store = configureStore({
  reducer: {
    auth: auth.reducer,
    students: studentReducer.reducer,
    departments: departmentReducer.reducer,
    batch: batchReducer.reducer,
    class: classReducer.reducer,
    schedule: scheduleReducer.reducer,
    subjects: subjectReducer.reducer,
    locations: locationReducer.reducer,
    results: resultReducer.reducer,
    professors: professorReducer.reducer,
    contacts: contactsReducer.reducer,
    profile: profileReducer.reducer,
    help: helpReducer.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});
export default store;
