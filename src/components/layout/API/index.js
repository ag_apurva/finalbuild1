export const LOAD_STUDENTS =
  "https://l12w9l17oj.execute-api.us-east-2.amazonaws.com/build/students/getstudents";
export const CREATE_STUDENTS =
  "https://l12w9l17oj.execute-api.us-east-2.amazonaws.com/build/students/createstudent";
export const EDIT_STUDENTS =
  "https://l12w9l17oj.execute-api.us-east-2.amazonaws.com/build/students/editstudent";
export const DELETE_STUDENTS =
  "https://l12w9l17oj.execute-api.us-east-2.amazonaws.com/build/students/deletestudent";
export const CREATE_CONTACT_URL =
  "https://l12w9l17oj.execute-api.us-east-2.amazonaws.com/build/contacts/createcontact";
export const UPDATE_CONTACT_URL =
  "https://l12w9l17oj.execute-api.us-east-2.amazonaws.com/build/contacts/updatecontact";
export const LOAD_CONTACTS =
  "https://l12w9l17oj.execute-api.us-east-2.amazonaws.com/build/contacts/getcontacts";

export const DELETE_CONTACTS =
  "https://l12w9l17oj.execute-api.us-east-2.amazonaws.com/build/contacts/deletecontact";

export const GET_FAQ_URL =
  "https://l12w9l17oj.execute-api.us-east-2.amazonaws.com/build/helps/gethelp";
export const CREATE_FAQ =
  "https://l12w9l17oj.execute-api.us-east-2.amazonaws.com/build/helps/createhelp";
export const UPDATE_FAQ =
  "https://l12w9l17oj.execute-api.us-east-2.amazonaws.com/build/helps/updatehelp";
export const DELETE_FAQ =
  "https://l12w9l17oj.execute-api.us-east-2.amazonaws.com/build/helps/deletehelp";

export const GET_DEPARTMENTS =
  "https://l12w9l17oj.execute-api.us-east-2.amazonaws.com/build/departments/getdepartments";
export const CREATE_DEPARTMENTS =
  "https://l12w9l17oj.execute-api.us-east-2.amazonaws.com/build/departments/createdepartment";

export const GET_BATCH =
  "https://l12w9l17oj.execute-api.us-east-2.amazonaws.com/build/admissions/getadmissions";
export const CREATE_BATCH =
  "https://l12w9l17oj.execute-api.us-east-2.amazonaws.com/build/admissions/createadmissions";

export const GET_CLASS =
  "https://l12w9l17oj.execute-api.us-east-2.amazonaws.com/build/classes/getclasses";
export const CREATE_CLASS =
  "https://l12w9l17oj.execute-api.us-east-2.amazonaws.com/build/classes/createclasses";

export const GET_LOCATION =
  "https://l12w9l17oj.execute-api.us-east-2.amazonaws.com/build/locations/getlocations";
export const CREATE_LOCATION =
  "https://l12w9l17oj.execute-api.us-east-2.amazonaws.com/build/locations/createlocation";

export const SAVE_PROFILE =
  "https://l12w9l17oj.execute-api.us-east-2.amazonaws.com/build/authentication/adminsaveprofile";

export const GET_SUBJECT =
  "https://l12w9l17oj.execute-api.us-east-2.amazonaws.com/build/subjects/getsubjects";
export const CREATE_SUBJECT =
  "https://l12w9l17oj.execute-api.us-east-2.amazonaws.com/build/subjects/createsubjects";
export const GET_RESULT =
  "https://l12w9l17oj.execute-api.us-east-2.amazonaws.com/build/results/getresults";
export const GET_SCHEDULE =
  "https://l12w9l17oj.execute-api.us-east-2.amazonaws.com/build/schedule/getclassdata";
export const ADMIN_PROFILE =
  "https://l12w9l17oj.execute-api.us-east-2.amazonaws.com/build/authentication/adminmyprofile?universityCode=TR021&userEmail=vishalg48519941994@gmail.com";
// export const SAVE_PROFILE = "";
//  const universityCode = useSelector((state) => state.auth.universityCode);
export const CREATE_SCHEDULE =
  "https://l12w9l17oj.execute-api.us-east-2.amazonaws.com/build/schedule/createschedule";
export const GET_PROFESSORS =
  "https://l12w9l17oj.execute-api.us-east-2.amazonaws.com/build/professors/getprofessors";
export const CREATE_PROFESSORS =
  "https://l12w9l17oj.execute-api.us-east-2.amazonaws.com/build/professors/createprofessor";
export const DELETE_PROFESSOR =
  "https://l12w9l17oj.execute-api.us-east-2.amazonaws.com/build/professors/deleteprofessor";

export const DELETE_SCHEDULE =
  "https://l12w9l17oj.execute-api.us-east-2.amazonaws.com/build/schedule/deleteschedule";
export const CHECK_FOR_CONFLICT =
  "https://l12w9l17oj.execute-api.us-east-2.amazonaws.com/build/schedule/checkforconflict";

export const SCHEDULE_DATA_PROFESSOR =
  "https://l12w9l17oj.execute-api.us-east-2.amazonaws.com/build/schedule/getschedule";
export const SCHEDULE_DATA_LOCATION =
  "https://l12w9l17oj.execute-api.us-east-2.amazonaws.com/build/schedule/loadlocationschedule";
