import { Modal } from "semantic-ui-react";
import React, { useState } from "react";
import { DeleteContacts } from "./DeleteContacts";
import { useSelector } from "react-redux";
import { EditContacts } from "./EditContacts";
import { Button } from "semantic-ui-react";
export const ContactsProfile = function (props) {
  const [editContact, setEditContact] = useState(true);
  const [deleteContact, setDeleteContact] = useState(false);
  const errorCreateContact = useSelector(
    (state) => state.contacts.errorCreateContact
  );
  const submitSuccessful = useSelector(
    (state) => state.contacts.submitSuccessful
  );
  if (submitSuccessful) {
    props.onClose(false);
  }
  return (
    <Modal open={props.open} onClose={props.onClose} dimmer={"inverted"} closeIcon>
      <Modal.Header>
        <Button
          className={deleteContact === true ? "item active" : "item"}
          onClick={() => {
            setEditContact(false);
            setDeleteContact(true);
          }}
        >
          Delete Contact
        </Button>
        <Button
          className={editContact === true ? "item active" : "item"}
          onClick={() => {
            setEditContact(true);
            setDeleteContact(false);
          }}
        >
          Edit Contact
        </Button>
        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
          <span>&times;</span>
        </button>
      </Modal.Header>
      <Modal.Content>
        {deleteContact === true ? (
          <DeleteContacts
            contactName={props.contactName}
            idContact={props.idContact}
          />
        ) : (
          <React.Fragment />
        )}
        {editContact === true ? (
          <EditContacts idContact={props.idContact} />
        ) : (
          <React.Fragment />
        )}
      </Modal.Content>
      <Modal.Actions>
        {errorCreateContact ? <p>{errorCreateContact}</p> : <React.Fragment />}
      </Modal.Actions>
    </Modal>
  );
};
