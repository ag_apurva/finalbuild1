import React, { useState } from "react";
import { contactActions } from "./store/index";
import { useSelector, useDispatch } from "react-redux";
import "../css/student.css";
import { ContactsProfile } from "./ContactsProfile";
/*eslint eqeqeq: "off"*/
import { CreateContacts } from "./CreateContacts";
import { MyProfile } from "../Profile/Profile";
import logo from "../image/attendance.png";
export const Contacts = function () {
  const userName = useSelector((state) => state.auth.myProfile.userName);
  const userRole = useSelector((state) => state.auth.myProfile.userRole);
  const [myProfile, setMyProfile] = useState(false);
  const [editContactPopUp, editContactsAddPopUp] = useState(false);
  const [contactId, setContactId] = useState("");
  const [createContact, setCreateContact] = useState(false);
  const contactsData = useSelector((state) => state.contacts.contactsData);
  const dispatch = useDispatch();
  const renderContacts = () => {
    return contactsData.map((item, index) => {
      const idUniContacts = item.idUniContacts;
      const name = item.name || " ";
      const designation = item.designation || " ";
      const category = item.category || " ";
      const location = item.location || " ";
      const phoneNumber1 = item.phoneNumber1 || " ";
      const phoneNumber2 = item.phoneNumber2 || " ";
      //idUniContacts
      let contactObject = {
        emailId: item.emailId,
        name,
        designation,
        location,
        category,
        phoneNumber1,
        phoneNumber2,
      };
      return (
        <tr key={index}>
          {editContactPopUp && contactId === index ? (
            <ContactsProfile
              open={editContactPopUp}
              onClose={() => editContactsAddPopUp(false)}
              contactName={name}
              idContact={idUniContacts} // this is basically id of the UniContacts Table.
            />
          ) : (
            <React.Fragment />
          )}
          <td>{name}</td>
          <td>{designation}</td>
          <td>{location}</td>
          <td>{category}</td>
          <td>{phoneNumber1}</td>
          <td>{phoneNumber2}</td>
          <td className="action-cell">
            <button
              onClick={() => {
                editContactsAddPopUp(true);
                setContactId(index);
                dispatch(
                  contactActions.setEditContactObject({
                    editContact: contactObject,
                  })
                );
              }}
            >
              <i className="fa fa-edit" aria-hidden="true"></i>
            </button>
            <button
              onClick={() => {
                editContactsAddPopUp(true);
                setContactId(index);
                dispatch(
                  contactActions.setEditContactObject({
                    editContact: contactObject,
                  })
                );
              }}
            >
              <i className="fa fa-trash" aria-hidden="true"></i>
            </button>
          </td>
        </tr>
      );
    });
  };
  let profileImage = useSelector((state) => state.auth.profileImage);
  if (profileImage == "") {
    profileImage = logo;
  }
  if (myProfile) {
    return <MyProfile open={myProfile} onClose={() => setMyProfile(false)} />;
  } else if (createContact) {
    return (
      <CreateContacts
        open={createContact}
        onClose={() => setCreateContact(false)}
      />
    );
  } else {
    return (
      <div className="main-content bg-light">
        <header>
          <h4>
            <label htmlFor="nav-toggel" className="hamburgerIcon">
              <i className="fa fa-bars" aria-hidden="true"></i>
            </label>
            <span className="name">Contacts</span>
          </h4>

          <div className="user-wrapper" onClick={() => setMyProfile(true)}>
            <img src={profileImage} width="50px" height="50px" alt="" />
            <div>
              <h6>{userName}</h6>
              <small>{userRole}</small>
            </div>
          </div>
        </header>
        <main>
          <div className="table-content tableGridUi">
            <table className="table table-hover">
              <thead>
                <tr>
                  <th scope="col">Name</th>
                  <th scope="col">​Designation</th>
                  <th scope="col">​Location</th>
                  <th scope="col">Category</th>
                  <th scope="col">​PhoneNumber1</th>
                  <th scope="col">​PhoneNumber2</th>
                  <th className="text-center">Action</th>
                </tr>
              </thead>
              <tbody>{renderContacts()}</tbody>
            </table>
          </div>
          <div className="mt-4">
            <button
              className="btn btn-primary"
              onClick={() => setCreateContact(true)}
            >
              Add Contact
            </button>
          </div>
        </main>
      </div>
    );
  }
};
