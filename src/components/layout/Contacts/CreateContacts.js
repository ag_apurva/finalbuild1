import React from "react";
import { Formik, Field, ErrorMessage } from "formik";
import { Modal } from "semantic-ui-react";
import { useSelector, useDispatch } from "react-redux";
import { Form } from "formik-semantic-ui-react";
import * as Yup from "yup";
import validator from "validator";
import { createContacts } from "./store/actions";
import { Loader, Dimmer } from "semantic-ui-react";
export const CreateContacts = function (props) {
  const dispatch = useDispatch();
  const validate = (formValues) => {
    let error = {};
    if (formValues.email && !validator.isEmail(formValues.email)) {
      error.email = "* Please provide a valid email";
    }
    if (formValues.phoneNumber1 && formValues.phoneNumber1.length !== 10) {
      error.phoneNumber1 = "* Please provide a 10 digit Phone Name";
    }

    if (!formValues.category) {
      error.category = "* Please provide a Category";
    }

    if (formValues.phoneNumber2 && formValues.phoneNumber2.length !== 10) {
      error.phoneNumber2 = "* Please provide a 10 digit Phone Name";
    }
    if (formValues.phoneNumber1) {
      let filter = /^[1-9]{1}[0-9]{2}[0-9]{7}$/;
      if (!filter.test(formValues.phoneNumber1)) {
        error.phoneNumber1 =
          "*Please provide a 10 digit Phone Number that does not start with 0";
      }
    }

    if (formValues.phoneNumber2) {
      let filter = /^[1-9]{1}[0-9]{2}[0-9]{7}$/;
      if (!filter.test(formValues.phoneNumber2)) {
        error.phoneNumber2 =
          "*Please provide a 10 digit Phone Number that does not start with 0";
      }
    }
    return error;
  };

  const validationSchema = Yup.object({
    name: Yup.string().required("Please provide a Name").nullable(),
    //location: Yup.string().required("Please provide a Location").nullable(),
    designation: Yup.string()
      .required("Please provide a Designation")
      .nullable(),
  });
  const initialValues = {
    name: "",
    email: "",
    location: "",
    category: "",
    designation: "",
    phoneNumber1: "",
    phoneNumber2: "",
  };

  const spinner = useSelector((state) => state.contacts.spinner);
  const errorCreateContact = useSelector(
    (state) => state.contacts.errorCreateContact
  );
  const submitSuccessful = useSelector(
    (state) => state.contacts.submitSuccessful
  );
  if (submitSuccessful) {
    props.onClose(false);
  }
  let contactsData = useSelector((state) => state.contacts.contactsData);
  const universityCode = useSelector((state) => state.auth.universityCode);
  const onSubmit = async (formValues) => {
    let createContactObject = {
      name: formValues.name,
      location: formValues.location,
      emailId: formValues.email,
      category: formValues.category,
      designation: formValues.designation,
      phoneNumber1: formValues.phoneNumber1
        ? "+" + formValues.phoneNumber1
        : "",
      phoneNumber2: formValues.phoneNumber2
        ? "+" + formValues.phoneNumber2
        : "",
      universityCode: universityCode,
    };
    dispatch(createContacts(createContactObject, contactsData));
  };

  return (
    <Modal open={props.open} onClose={props.onClose} dimmer={"inverted"} closeIcon>
      <Modal.Header>
        <h3>Create Contacts</h3>
        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
          <span>&times;</span>
        </button>
      </Modal.Header>
      {spinner ? (
        <React.Fragment>
          <Dimmer active>
            <Loader size="big">Loading</Loader>
          </Dimmer>
        </React.Fragment>
      ) : (
        <Modal.Content>
          <Formik
            initialValues={initialValues}
            validate={validate}
            onSubmit={onSubmit}
            validationSchema={validationSchema}
          >
            <Form>
            <div className="row modelFormUi">
              <div className="field col-4">
                <label>Name</label>
                <Field type="text" name="name" />
                <div className="errorMessage">
                <ErrorMessage name="name">
                  {(errorMsg) => {
                    return <p>{errorMsg}</p>;
                  }}
                </ErrorMessage>
                </div>
              </div>
              <div className="field col-4">
                <label>Email</label>
                <Field type="text" name="email" />
                <div className="errorMessage">
                <ErrorMessage name="email">
                  {(errorMsg) => {
                    return <p>{errorMsg}</p>;
                  }}
                </ErrorMessage>
                </div>
              </div>
              <div className="field col-4">
                <label>Location</label>
                <Field type="text" name="location" />
                <div className="errorMessage">
                <ErrorMessage name="location">
                  {(errorMsg) => {
                    return <p>{errorMsg}</p>;
                  }}
                </ErrorMessage>
                </div>
              </div>
              <div className="field col-4">
                <label>Designation</label>
                <Field type="text" name="designation" />
                <div className="errorMessage">
                <ErrorMessage name="designation">
                  {(errorMsg) => {
                    return <p>{errorMsg}</p>;
                  }}
                </ErrorMessage>
                </div>
              </div>
              <div className="field col-4">
                <label>Category</label>
                <Field type="text" name="category">
                  {(props) => {
                    return (
                      <select {...props.field}>
                        <option value="">None</option>
                        <option value="Student">Student</option>
                        <option value="Professor">Professor</option>
                        <option value="Alumni">Alumni</option>
                        <option value="Management">Management</option>
                      </select>
                    );
                  }}
                </Field>
                <div className="errorMessage">
                <ErrorMessage name="category">
                  {(errorMsg) => {
                    return <p>{errorMsg}</p>;
                  }}
                </ErrorMessage>
                </div>
              </div>
              <div className="field col-4">
                <label>Phone Number1</label>
                <Field type="text" name="phoneNumber1" />
                <div className="errorMessage">
                <ErrorMessage name="phoneNumber1">
                  {(errorMsg) => {
                    return <p>{errorMsg}</p>;
                  }}
                </ErrorMessage>
                </div>
              </div>
              <div className="field col-4">
                <label>Phone Number2</label>
                <Field type="text" name="phoneNumber2" />
                <div className="errorMessage">
                <ErrorMessage name="phoneNumber2">
                  {(errorMsg) => {
                    return <p>{errorMsg}</p>;
                  }}
                </ErrorMessage>
                </div>
              </div>

              <div className="col-12 mt-4">
              <button
                className="btn btn-primary"
                type="submit"
              >
                Submit
              </button>
              </div>
              </div>
            </Form>
          </Formik>
        </Modal.Content>
      )}
      <Modal.Actions>
        {errorCreateContact ? <p>{errorCreateContact}</p> : <React.Fragment />}
      </Modal.Actions>
    </Modal>
  );
};
