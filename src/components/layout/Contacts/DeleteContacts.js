import React from "react";
import { deleteContacts } from "./store/actions";
import { Loader, Dimmer } from "semantic-ui-react";
import { useSelector, useDispatch } from "react-redux";
export const DeleteContacts = function (props) {
  const spinner = useSelector((state) => state.contacts.spinner);
  const contactsData = useSelector((state) => state.contacts.contactsData);
  const dispatch = useDispatch();
  const universityCode = useSelector((state) => state.auth.universityCode);
  const onSubmit = async () => {
    dispatch(
      deleteContacts(
        {
          universityCode: universityCode,
          idContact: props.idContact,
        },
        contactsData
      )
    );
  };
  return (
    <React.Fragment>
      {spinner ? (
        <React.Fragment>
          <Dimmer active>
            <Loader size="big">Loading</Loader>
          </Dimmer>
        </React.Fragment>
      ) : (
        <React.Fragment>
          <div className="deleteBlock">
          <p>Are you sure you want to delete Contact {props.contactName} ? </p>
          <button
            onClick={() => onSubmit()}
            className="btn btn-primary mt-2"
          >
            Delete Contact
          </button>
          </div>
        </React.Fragment>
      )}
    </React.Fragment>
  );
};
