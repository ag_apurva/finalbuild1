/*eslint eqeqeq: "off"*/
import { contactActions } from "./index";
import {
  LOAD_CONTACTS,
  CREATE_CONTACT_URL,
  UPDATE_CONTACT_URL,
  DELETE_CONTACTS,
} from "../../API";
import axios from "axios";

export const getContacts = function (code) {
  return async function (dispatch) {
    var config = {
      method: "get",
      url: LOAD_CONTACTS,
      headers: {},
      params: {
        universityCode: code,
      },
    };
    try {
      let data = await axios(config);
      dispatch(contactActions.contactsData({ contactsData: data.data }));
    } catch (e) {
      dispatch(
        contactActions.setContactError({ contactError: JSON.stringify(e) })
      );
    }
  };
};

export const createContacts = function (contact, contactData) {
  return async function (dispatch) {
    dispatch(contactActions.setSpinner({ spinner: true }));
    try {
      let dataInput = JSON.stringify(contact);
      let config = {
        method: "post",
        url: CREATE_CONTACT_URL,
        headers: {
          "x-api-key": "ZU3QgpqZK773dcNoDRFfK42hZJ9IMzCM2TOXmMYQ",
        },
        data: dataInput,
      };

      let createContact = await axios(config);
      console.log("createContact", createContact);

      if (!createContact.data.hasOwnProperty("error")) {
        contact["idUniContacts"] = createContact.data.data;
        let newContacts = [...contactData, contact];
        dispatch(contactActions.contactsData({ contactsData: newContacts }));

        dispatch(
          contactActions.setSubmitSuccessfull({
            submitSuccessful: true,
          })
        );
        setTimeout(() => {
          dispatch(
            contactActions.setSubmitSuccessfull({ submitSuccessful: false })
          );
        }, 200);
      } else {
        dispatch(
          contactActions.setContactError({
            errorCreateContact: createContact.data.error,
          })
        );
        setTimeout(() => {
          dispatch(
            contactActions.setContactError({
              errorCreateContact: "",
            })
          );
        }, 20000);
        setTimeout(() => {
          dispatch(
            contactActions.setSubmitSuccessfull({ submitSuccessful: true })
          );
        }, 20000);
        setTimeout(() => {
          dispatch(
            contactActions.setSubmitSuccessfull({ submitSuccessful: false })
          );
        }, 20200);
      }
    } catch (e) {
      console.log("error creating contact", e);
      dispatch(
        contactActions.setContactError({
          errorCreateContact: JSON.stringify(e),
        })
      );
      setTimeout(() => {
        dispatch(
          contactActions.setContactError({
            errorCreateContact: "",
          })
        );
      }, 20000);
      setTimeout(() => {
        dispatch(
          contactActions.setSubmitSuccessfull({ submitSuccessful: true })
        );
      }, 20000);
      setTimeout(() => {
        dispatch(
          contactActions.setSubmitSuccessfull({ submitSuccessful: false })
        );
      }, 20200);
    }
    dispatch(contactActions.setSpinner({ spinner: false }));
  };
};

export const deleteContacts = function (deleteObject, contactsData) {
  return async function (dispatch) {
    dispatch(contactActions.setSpinner({ spinner: true }));
    try {
      const config = {
        method: "delete",
        url: DELETE_CONTACTS,
        headers: {},
        params: {
          universityCode: deleteObject.universityCode,
          idContact: deleteObject.idContact,
        },
      };

      let deleteContact = await axios(config);
      if (!deleteContact.data.hasOwnProperty("error")) {
        const newContactsData = contactsData.filter((contact) => {
          return contact.idUniContacts !== deleteObject.idContact;
        });
        dispatch(
          contactActions.setSubmitSuccessfull({
            submitSuccessful: true,
          })
        );
        dispatch(
          contactActions.contactsData({ contactsData: newContactsData })
        );
        setTimeout(() => {
          dispatch(
            contactActions.setSubmitSuccessfull({
              submitSuccessful: false,
            })
          );
        }, 200);
      } else {
        dispatch(
          contactActions.setContactError({
            errorCreateContact: deleteContact.data.error,
          })
        );
        setTimeout(() => {
          dispatch(
            contactActions.setContactError({
              errorCreateContact: "",
            })
          );
        }, 20000);
        setTimeout(() => {
          dispatch(
            contactActions.setSubmitSuccessfull({ submitSuccessful: true })
          );
        }, 20000);
        setTimeout(() => {
          dispatch(
            contactActions.setSubmitSuccessfull({ submitSuccessful: false })
          );
        }, 20200);
      }
    } catch (e) {
      dispatch(
        contactActions.setContactError({
          errorCreateContact: "createSubject.data.error",
        })
      );
      setTimeout(() => {
        dispatch(
          contactActions.setContactError({
            errorCreateContact: "",
          })
        );
      }, 20000);
      setTimeout(() => {
        dispatch(
          contactActions.setSubmitSuccessfull({ submitSuccessful: true })
        );
      }, 20000);
      setTimeout(() => {
        dispatch(
          contactActions.setSubmitSuccessfull({ submitSuccessful: false })
        );
      }, 20200);
    }
    dispatch(contactActions.setSpinner({ spinner: false }));
  };
};

export const editContacts = function (updateContact, contactsData) {
  return async function (dispatch) {
    dispatch(contactActions.setSpinner({ spinner: true }));
    try {
      let dataInput = JSON.stringify(updateContact);
      let config = {
        method: "put",
        url: UPDATE_CONTACT_URL,
        headers: {
          "x-api-key": "ZU3QgpqZK773dcNoDRFfK42hZJ9IMzCM2TOXmMYQ",
        },
        data: dataInput,
      };
      let contactIndex = contactsData.findIndex((item) => {
        return item.idUniContacts == updateContact.idUniContact;
      });
      let newContacts = contactsData.filter((item) => {
        return item.idUniContacts != updateContact.idUniContact;
      });
      let newContactObject = Object.assign(
        { ...contactsData[contactIndex] },
        { ...updateContact }
      );
      newContacts.push({ ...newContactObject });

      let updatedContact = await axios(config);
      console.log("updatedContact", updatedContact);
      if (!updatedContact.data.hasOwnProperty("error")) {
        dispatch(
          contactActions.setSubmitSuccessfull({
            submitSuccessful: true,
          })
        );
        setTimeout(() => {
          dispatch(
            contactActions.setSubmitSuccessfull({
              submitSuccessful: false,
            })
          );
        }, 200);
        dispatch(contactActions.contactsData({ contactsData: newContacts }));
      } else {
        dispatch(
          contactActions.setContactError({
            errorCreateContact: updatedContact.data.error,
          })
        );
        setTimeout(() => {
          dispatch(
            contactActions.setContactError({
              errorCreateContact: updatedContact.data.error,
            })
          );
        }, 20000);
        setTimeout(() => {
          dispatch(
            contactActions.setSubmitSuccessfull({ submitSuccessful: true })
          );
        }, 20000);
        setTimeout(() => {
          dispatch(
            contactActions.setSubmitSuccessfull({ submitSuccessful: false })
          );
        }, 20200);
      }
    } catch (e) {
      console.log("update contact error2", e);
      dispatch(
        contactActions.setContactError({
          errorCreateContact: JSON.stringify(e),
        })
      );
      setTimeout(() => {
        dispatch(
          contactActions.setContactError({
            errorCreateContact: "",
          })
        );
      }, 20000);
      setTimeout(() => {
        dispatch(
          contactActions.setSubmitSuccessfull({ submitSuccessful: true })
        );
      }, 20000);
      setTimeout(() => {
        dispatch(
          contactActions.setSubmitSuccessfull({ submitSuccessful: false })
        );
      }, 20200);
    }
    dispatch(contactActions.setSpinner({ spinner: false }));
  };
};
