import { createSlice } from "@reduxjs/toolkit";

const contactSlice = createSlice({
  name: "contactReducer",
  initialState: {
    contactsData: [],
    editContact: {},
    submitSuccessful: false,
    spinner: false,
    errorCreateContact: "",
  },
  reducers: {
    contactsData(state, action) {
      state.contactsData = action.payload.contactsData;
    },
    setEditContactObject(state, action) {
      state.editContact = action.payload.editContact;
    },
    setSpinner(state, action) {
      state.spinner = action.payload.spinner;
    },
    setContactError(state, action) {
      state.errorCreateContact = action.payload.errorCreateContact;
    },
    setSubmitSuccessfull(state, action) {
      state.submitSuccessful = action.payload.submitSuccessful;
    },
  },
});

export const contactActions = contactSlice.actions;

export default contactSlice;
