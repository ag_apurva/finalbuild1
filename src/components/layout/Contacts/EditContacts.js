import React from "react";
import { Formik, Field, ErrorMessage } from "formik";
import { useSelector, useDispatch } from "react-redux";
import { Form } from "formik-semantic-ui-react";
import * as Yup from "yup";
import validator from "validator";
import { editContacts } from "./store/actions";
import { Loader, Dimmer } from "semantic-ui-react";
const validate = (formValues) => {
  let error = {};
  if (formValues.email && !validator.isEmail(formValues.email)) {
    error.email = "* Please provide a valid email";
  }
  if (formValues.phoneNumber1 && formValues.phoneNumber1.length !== 10) {
    error.phoneNumber1 = "* Please provide a 10 digit Phone Name";
  }
  if (formValues.phoneNumber2 && formValues.phoneNumber2.length !== 10) {
    error.phoneNumber2 = "* Please provide a 10 digit Phone Name";
  }
  if (formValues.phoneNumber1) {
    let filter = /^[1-9]{1}[0-9]{2}[0-9]{7}$/;
    if (!filter.test(formValues.phoneNumber1)) {
      error.phoneNumber1 =
        "*Please provide a 10 digit Phone Number that does not start with 0";
    }
  }
  if (formValues.phoneNumber2) {
    let filter = /^[1-9]{1}[0-9]{2}[0-9]{7}$/;
    if (!filter.test(formValues.phoneNumber2)) {
      error.phoneNumber2 =
        "*Please provide a 10 digit Phone Number that does not start with 0";
    }
  }
  return error;
};

const validationSchema = Yup.object({
  name: Yup.string().required("Please provide a Name").nullable(),
  designation: Yup.string().required("Please provide a Designation").nullable(),
});

export const EditContacts = function (props) {
  const editContact = useSelector((state) => state.contacts.editContact);
  const spinner = useSelector((state) => state.contacts.spinner);
  const dispatch = useDispatch();
  const initialValues = {
    name: editContact.name,
    email: editContact.emailId,
    designation: editContact.designation,
    location: editContact.location,
    category: editContact.category,
    phoneNumber1: editContact.phoneNumber1
      ? editContact.phoneNumber1.toString().slice(1, 11)
      : "",
    phoneNumber2: editContact.phoneNumber2
      ? editContact.phoneNumber2.toString().slice(1, 11)
      : "",
  };
  const universityCode = useSelector((state) => state.auth.universityCode);
  const contactsData = useSelector((state) => state.contacts.contactsData);
  const onSubmit = async (formValues) => {
    let updateContactObject = {
      name: formValues.name,
      emailId: formValues.email,
      location: formValues.location,
      designation: formValues.designation,
      phoneNumber1: formValues.phoneNumber1
        ? "+" + formValues.phoneNumber1
        : "",
      phoneNumber2: formValues.phoneNumber2
        ? "+" + formValues.phoneNumber2
        : "",
      idUniContact: props.idContact,
      universityCode: universityCode,
    };
    dispatch(editContacts({ ...updateContactObject }, contactsData));
  };

  if (spinner) {
    return (
      <React.Fragment>
        <Dimmer active>
          <Loader size="big">Loading</Loader>
        </Dimmer>
      </React.Fragment>
    );
  } else {
    return (
      <React.Fragment>
        <Formik
          className="ui segment"
          initialValues={initialValues}
          validate={validate}
          onSubmit={onSubmit}
          validationSchema={validationSchema}
        >
          <Form>
            <div className="row modelFormUi">
            <div className="field col-4">
              <label>Name</label>
              <Field type="text" name="name" />
              <div className="errorMessage">
              <ErrorMessage name="name">
                {(errorMsg) => {
                  return <p>{errorMsg}</p>;
                }}
              </ErrorMessage>
              </div>
            </div>
            <div className="field col-4">
              <label>Email</label>
              <Field type="text" name="email" />
              <div className="errorMessage">
              <ErrorMessage name="email">
                {(errorMsg) => {
                  return <p>{errorMsg}</p>;
                }}
              </ErrorMessage>
              </div>
            </div>
            <div className="field col-4">
              <label>Location</label>
              <Field type="text" name="location" />
              <div className="errorMessage">
              <ErrorMessage name="location">
                {(errorMsg) => {
                  return <p>{errorMsg}</p>;
                }}
              </ErrorMessage>
              </div>
            </div>
            <div className="field col-4">
              <label>Designation</label>
              <Field type="text" name="designation" />
              <div className="errorMessage">
              <ErrorMessage name="designation">
                {(errorMsg) => {
                  return <p>{errorMsg}</p>;
                }}
              </ErrorMessage>
              </div>
            </div>
            <div className="field col-4">
              <label>Phone Number1</label>
              <Field type="text" name="phoneNumber1" />
              <div className="errorMessage">
              <ErrorMessage name="phoneNumber1">
                {(errorMsg) => {
                  return <p>{errorMsg}</p>;
                }}
              </ErrorMessage>
              </div>
            </div>
            <div className="field col-4">
              <label>Phone Number2</label>
              <Field type="text" name="phoneNumber2" />
              <div className="errorMessage">
              <ErrorMessage name="phoneNumber2">
                {(errorMsg) => {
                  return <p>{errorMsg}</p>;
                }}
              </ErrorMessage>
            </div>
            </div>

            <div className="col-12 mt-4">
            <button
              className="btn btn-primary"
              type="submit"
            >
              Submit
            </button>
            </div>
            </div>
          </Form>
        </Formik>
      </React.Fragment>
    );
  }
};
