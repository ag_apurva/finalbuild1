import { GET_BATCH, CREATE_BATCH } from "../../API";
import { batchActions } from "./index";
import axios from "axios";
export const getAdmissions = (code) => {
  return async function (dispatch) {
    const config = {
      method: "get",
      url: GET_BATCH,
      headers: {},
      params: {
        universityCode: code,
      },
    };

    try {
      let admissionsData = await axios(config);
      if (!admissionsData.data.hasOwnProperty("error")) {
        dispatch(batchActions.admissionsData(admissionsData.data));
      }
    } catch (e) {
      console.log("error retriving Admissions", e);
    }
  };
};

export const createAdmissions = (formValues) => {
  return async function (dispatch) {
    dispatch(batchActions.setSpinner({ spinner: true }));
    var axios = require("axios");
    var data = JSON.stringify(formValues);

    var config = {
      method: "post",
      url: CREATE_BATCH,
      headers: {
        "Content-Type": "application/json",
      },
      data: data,
    };

    try {
      let createAdmission = await axios(config);
      dispatch(batchActions.setSpinner({ spinner: false }));
      if (!createAdmission.data.hasOwnProperty("error")) {
        let AdmCode = createAdmission.data.data; //since create Admission was supposed to return ADM Code
        let admssionData = {
          admissionCode: AdmCode,
          admissionYear: formValues["admissionYear"],
          departmentCode: formValues["departmentCode"],
          departmentName: formValues["departmentName"],
          academicStartData: formValues["academicStartData"],
          academicEndData: formValues["academicEndData"],
        };
        dispatch(batchActions.addAdmissions({ admssionData: admssionData }));
        dispatch(batchActions.setSubmitSuccessfull({ submitSuccessful: true }));
        setTimeout(() => {
          dispatch(
            batchActions.setSubmitSuccessfull({ submitSuccessful: false })
          );
        }, 200);
      } else {
        dispatch(
          batchActions.setAdmissionError({
            errorCreateAdmission: createAdmission.data.error,
          })
        );
        setTimeout(() => {
          dispatch(
            batchActions.setAdmissionError({
              errorCreateAdmission: "",
            })
          );
        }, 20000);
        setTimeout(() => {
          dispatch(
            batchActions.setSubmitSuccessfull({ submitSuccessful: true })
          );
        }, 20000);
        setTimeout(() => {
          dispatch(
            batchActions.setSubmitSuccessfull({ submitSuccessful: false })
          );
        }, 20200);
      }
    } catch (e) {
      console.log("error creating locations", e);
      dispatch(
        batchActions.setAdmissionError({
          errorCreateAdmission: JSON.stringify(e),
        })
      );
      setTimeout(() => {
        dispatch(
          batchActions.setAdmissionError({
            errorCreateAdmission: "",
          })
        );
      }, 20000);
      setTimeout(() => {
        dispatch(batchActions.setSubmitSuccessfull({ submitSuccessful: true }));
      }, 20000);
      setTimeout(() => {
        dispatch(
          batchActions.setSubmitSuccessfull({ submitSuccessful: false })
        );
      }, 20200);
    }
  };
};
