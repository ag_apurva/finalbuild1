import { createSlice } from "@reduxjs/toolkit";

const batchSlice = createSlice({
  name: "batchReducer",
  initialState: {
    admissionsData: [],
    spinner: false,
    submitSuccessful: false,
    errorCreateAdmission: "",
  }, //{ counter: 0, showCounter: false },
  reducers: {
    admissionsData(state, action) {
      state.admissionsData = [...action.payload];
    },
    addAdmissions(state, action) {
      state.admissionsData.push(action.payload.admssionData);
    },
    setSpinner(state, action) {
      state.spinner = action.payload.spinner;
    },
    setAdmissionError(state, action) {
      state.errorCreateAdmission = action.payload.errorCreateAdmission;
    },
    setSubmitSuccessfull(state, action) {
      state.submitSuccessful = action.payload.submitSuccessful;
    },
  },
});

export const batchActions = batchSlice.actions;
export default batchSlice;
