/*eslint eqeqeq: "off"*/
import React, { useState } from "react";
import { useSelector } from "react-redux";
import { CreateBatch } from "./CreateBatch";
import { MyProfile } from "../Profile/Profile";
import logo from "../image/attendance.png";
export const Batch = function (props) {
  const userName = useSelector((state) => state.auth.myProfile.userName);
  const userRole = useSelector((state) => state.auth.myProfile.userRole);
  const [createBatchPopUp, setCreateBatchPopUp] = useState(false);
  const [myProfile, setMyProfile] = useState(false);
  const admissionsData = useSelector((state) => state.batch.admissionsData);
  let profileImage = useSelector((state) => state.auth.profileImage);
  if (profileImage == "") {
    profileImage = logo;
  }
  const renderBatch = () => {
    return admissionsData.map((item, index) => {
      let { admissionCode, admissionYear, departmentCode } = item;
      let departmentName = item.departmentName || "";
      return (
        <tr key={index}>
          <td>{admissionCode}</td>
          <td>{admissionYear}</td>
          <td>{departmentCode}</td>
          <td>{departmentName}</td>
        </tr>
      );
    });
  };

  if (myProfile) {
    return <MyProfile open={myProfile} onClose={() => setMyProfile(false)} />;
  } else if (createBatchPopUp) {
    return (
      <CreateBatch
        open={createBatchPopUp}
        onClose={() => setCreateBatchPopUp(false)}
      />
    );
  } else {
    return (
      <div className="main-content bg-light">
        <header>
          <h4>
            <label htmlFor="nav-toggel" className="hamburgerIcon">
              <i className="fa fa-bars" aria-hidden="true"></i>
            </label>
            <span className="name">Create Batch</span>
          </h4>

          <div className="user-wrapper" onClick={() => setMyProfile(true)}>
            <img src={profileImage} width="50px" height="50px" alt="" />
            <div>
              <h6>{userName}</h6>
              <small>{userRole}</small>
            </div>
          </div>
        </header>
        <main>
          <div className="table-content tableGridUi">
            <table className="table table-hover">
              <thead>
                <tr>
                  <th scope="col">Admission Code</th>
                  <th scope="col">Admission Year</th>
                  <th scope="col">Dept Code</th>
                  <th scope="col">Dept Name</th>
                </tr>
              </thead>
              <tbody>{renderBatch()}</tbody>
            </table>
          </div>
          <div className="mt-4">
          <button
            className="btn btn-primary"
            onClick={() => setCreateBatchPopUp(true)}
          >
            Add Batch
          </button>
          </div>
        </main>
      </div>
    );
  }
};
