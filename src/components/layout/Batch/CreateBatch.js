/*eslint eqeqeq: "off"*/
import { useSelector, useDispatch } from "react-redux";
import { Formik, Field, ErrorMessage } from "formik";
import { Modal, Button, FormField } from "semantic-ui-react";
import moment from "moment";
import "react-datetime/css/react-datetime.css";
import Datetime from "react-datetime";
import React from "react";
import { Form } from "formik-semantic-ui-react";
import { createAdmissions } from "./store/actions";
//import { Loader, Dimmer } from "semantic-ui-react";
export const CreateBatch = function (props) {
  const departmentsData = useSelector(
    (state) => state.departments.departmentsData
  );
  const spinner = useSelector((state) => state.batch.spinner);
  const errorCreateAdmission = useSelector(
    (state) => state.batch.errorCreateAdmission
  );
  const admissionsData = useSelector((state) => state.batch.admissionsData);
  const submitSuccessful = useSelector((state) => state.batch.submitSuccessful);
  const dispatch = useDispatch();
  const validate = (formValues) => {
    // console.log("fromValues", formValues);
    let error = {};
    if (!formValues.admissionYear) {
      error.admissionYear = "* Please provide an Admission Year";
    }
    if (!formValues.departmentCode) {
      error.departmentCode = "* Please provide an Department Name";
    }

    if (formValues.admissionYear && formValues.departmentCode) {
      admissionsData.forEach((item) => {
        if (
          item.admissionYear == formValues.admissionYear &&
          item.departmentCode == formValues.departmentCode
        ) {
          error.departmentCode =
            "* This combination of year and department exists";
        }
      });
    }

    if (!formValues.degreeOffered) {
      error.degreeOffered = "* Please provide an Degree Offered";
    }
    if (!formValues.academicStartData) {
      error.academicStartData = "* Please provide an Academic Start Date";
    }
    if (!formValues.academicEndData) {
      error.academicEndData = "* Please provide an Academic End Date";
    }
    if (
      moment(formValues.academicStartData) >= moment(formValues.academicEndData)
    ) {
      error.academicEndData =
        "* Academic End Date should be greater than Academic Start Date ";
    }
    // console.log("Errors", error);
    return error;
  };
  const universityCode = useSelector((state) => state.auth.universityCode);
  const onSubmit = async (formValues) => {
    departmentsData.forEach((item) => {
      if (item.departmentCode == formValues.departmentCode) {
        formValues["departmentName"] = item.departmentName;
      }
    });
    formValues["academicStartData"] = moment(
      formValues["academicStartData"]
    ).format("YYYY-MM-DD");
    formValues["academicEndData"] = moment(
      formValues["academicEndData"]
    ).format("YYYY-MM-DD");
    formValues["universityCode"] = universityCode;
    console.log("formValues", formValues);
    dispatch(createAdmissions(formValues));
  };
  if (submitSuccessful) {
    props.onClose();
  }
  return (
    <Modal
      open={props.open}
      onClose={props.onClose}
      style={{ overflow: "auto", maxHeight: 600 }}
      dimmer={"inverted"}
      closeIcon
    >
      <Modal.Header>
        <h3>Batch Creation</h3>
        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
          <span>&times;</span>
        </button>
      </Modal.Header>
      {spinner ? (
        <Modal.Content>
          <div className="ui active inverted dimmer">
            <div className="ui loader"></div>
          </div>
        </Modal.Content>
      ) : (
        <Modal.Content>
          <Formik
            initialValues={{
              admissionYear: "",
              departmentCode: "",
              degreeOffered: "",
              academicStartData: new Date(),
              academicEndData: new Date(),
            }}
            validate={validate}
            onSubmit={onSubmit}
          >
            <Form>
              <div className="row modelFormUi">
              <FormField className="col-4">
                <label htmlFor="AdmissionYear">AdmissionYear</label>
                <Field id="AdmissionYear" name="admissionYear">
                  {(props) => {
                    return (
                      <select {...props.field}>
                        <option value="">None</option>
                        {[
                          { item: 2019 },
                          { item: 2020 },
                          { item: 2021 },
                          { item: 2022 },
                          { item: 2023 },
                          { item: 2024 },
                          { item: 2025 },
                        ].map((item, index) => (
                          <option key={index} value={item.item}>
                            {item.item}
                          </option>
                        ))}
                      </select>
                    );
                  }}
                </Field>
                <div className="errorMessage">
                <ErrorMessage name="admissionYear" />
                </div>
              </FormField>
              <FormField className="col-4">
                <label htmlFor="DepartmentCode">Departments</label>
                <Field id="DepartmentCode" name="departmentCode">
                  {(props) => {
                    return (
                      <select {...props.field}>
                        <option value="">None</option>
                        {departmentsData.map((item, index) => (
                          <option key={index} value={item.departmentCode}>
                            {item.departmentName}
                          </option>
                        ))}
                      </select>
                    );
                  }}
                </Field>
                <div className="errorMessage">
                <ErrorMessage name="departmentCode" />
                </div>
              </FormField>
              <FormField className="col-4">
                <label>Degree Offered</label>
                <Field name="degreeOffered" />
                <div className="errorMessage">
                <ErrorMessage name="degreeOffered" />
                </div>
              </FormField>
              <FormField className="col-4">
                <label htmlFor="startDate">Academic Start Data</label>
                <Field name="academicStartData" id="startDate">
                  {(props) => {
                    const { setFieldValue } = props.form;
                    return (
                      <Datetime
                        {...props.field}
                        timeFormat={false}
                        dateFormat="YYYY-MM-DD"
                        onChange={(event) => {
                          setFieldValue(props.field.name, event._d);
                        }}
                      />
                    );
                  }}
                </Field>
                <div className="errorMessage">
                <ErrorMessage name="academicStartData" />
                </div>
              </FormField>
              <FormField className="col-4">
                <label htmlFor="endDate">Academic End Data</label>
                <Field name="academicEndData" id="endDate">
                  {(props) => {
                    const { setFieldValue } = props.form;
                    return (
                      <Datetime
                        {...props.field}
                        timeFormat={false}
                        dateFormat="YYYY-MM-DD"
                        onChange={(event) => {
                          setFieldValue(props.field.name, event._d);
                        }}
                      />
                    );
                  }}
                </Field>
                <div className="errorMessage">
                <ErrorMessage name="academicEndData" />
                </div>
              </FormField>
              <div className="col-12 mt-4">
                <Button type="submit" className="btn btn-primary">Submit</Button>
              </div>
              </div>
            </Form>
          </Formik>
        </Modal.Content>
      )}
      <Modal.Actions>
        {errorCreateAdmission ? (
          <p>{errorCreateAdmission}</p>
        ) : (
          <React.Fragment />
        )}
      </Modal.Actions>
    </Modal>
  );
};

export default CreateBatch;
