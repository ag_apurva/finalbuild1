import React, { useState, useEffect } from "react";
import { Formik, Field, ErrorMessage } from "formik";
import { useSelector, useDispatch } from "react-redux";
import { Form } from "formik-semantic-ui-react";
import { createHelpContent } from "./store/actions";
import { Loader, Dimmer } from "semantic-ui-react";
import { Modal } from "semantic-ui-react";
import { TextareaAutosize } from "@material-ui/core";
export const CreateHelp = function (props) {
  let helpData = useSelector((state) => state.help.helpData);
  const [helpCategories, setHelpCategories] = useState([]);
  const spinner = useSelector((state) => state.help.spinner);
  const dispatch = useDispatch();
  useEffect(() => {
    let categories = [];
    helpData.forEach((item) => {
      categories.push(item.category);
    });
    categories = new Set([...categories]);
    setHelpCategories([...categories]);
  }, [helpData]);

  const validate = (formValues) => {
    const error = {};
    if (formValues.newcategoryCheckBox && !formValues.newCategory) {
      error.newCategory = "Please add a Category";
    }
    if (!formValues.newcategoryCheckBox && !formValues.category) {
      error.category = "Please select a Category";
    }
    if (!formValues.question) {
      error.question = "Please write an Question";
    }
    if (!formValues.answer) {
      error.answer = "Please write an Answer";
    }
    return error;
  };

  const submitSuccessful = useSelector((state) => state.help.submitSuccessful);
  if (submitSuccessful) {
    props.onClose(false);
  }
  const initialValues = {
    newcategoryCheckBox: false,
    newCategory: "",
    category: "",
    question: "",
    answer: "",
  };
  const universityCode = useSelector((state) => state.auth.universityCode);
  const onSubmit = (formValues) => {
    let submitCategory = {};
    if (formValues.newcategoryCheckBox) {
      submitCategory.category = formValues.newCategory;
    } else {
      submitCategory.category = formValues.category;
    }
    submitCategory.question = formValues.question;
    submitCategory.answer = formValues.answer;
    submitCategory.universityCode = universityCode;
    dispatch(createHelpContent(submitCategory, helpData));
  };

  if (spinner) {
    return (
      <React.Fragment>
        <Dimmer active>
          <Loader size="big">Loading</Loader>
        </Dimmer>
      </React.Fragment>
    );
  } else {
    return (
      <Modal open={props.open} onClose={props.onClose} dimmer={"inverted"} closeIcon>
        <Modal.Header>
          <h3>Create Help</h3>
          <button type="button" className="close" data-dismiss="modal" aria-label="Close">
          <span>&times;</span>
        </button>
        </Modal.Header>
        <Modal.Content>
          <Formik
            className="ui segment"
            initialValues={initialValues}
            validate={validate}
            onSubmit={onSubmit}
          >
            {(formik) => {
              return (
                <Form>
                  <div className="row modelFormUi">
                  <div className="field col-4">
                    <label>Create New Category</label>
                    <Field type="checkbox" name="newcategoryCheckBox" />
                    <div className="errorMessage">
                    <ErrorMessage name="newcategoryCheckBox">
                      {(errorMsg) => {
                        return <p>{errorMsg}</p>;
                      }}
                    </ErrorMessage>
                    </div>
                  </div>
                  <div className="field col-4">
                    <label>New Category</label>
                    <Field
                      type="text"
                      name="newCategory"
                      disabled={!formik.values.newcategoryCheckBox}
                    />
                    <div className="errorMessage">
                    <ErrorMessage name="newCategory">
                      {(errorMsg) => {
                        return <p>{errorMsg}</p>;
                      }}
                    </ErrorMessage>
                    </div>
                  </div>
                  <div className="field col-4">
                    <label>Category</label>
                    <Field type="text" name="category">
                      {(props) => {
                        return (
                          <select
                            className="form-control"
                            disabled={formik.values.newcategoryCheckBox}
                            name={props.field.name}
                            onBlur={props.field.onBlur}
                            onChange={(e) => {
                              props.field.onChange(e);
                            }}
                          >
                            <option value="">Select</option>
                            {helpCategories.map((year, index) => {
                              return (
                                <option key={index} value={year}>
                                  {year}
                                </option>
                              );
                            })}
                          </select>
                        );
                      }}
                    </Field>
                    <div className="errorMessage">
                    <ErrorMessage name="category">
                      {(errorMsg) => {
                        return <p>{errorMsg}</p>;
                      }}
                    </ErrorMessage>
                    </div>
                  </div>
                  <div className="field col-12">
                    <label>Question</label>
                    <TextareaAutosize name="question"></TextareaAutosize>
                    {/* <Field type="text" name="question" /> */}
                    <div className="errorMessage">
                    <ErrorMessage name="question">
                      {(errorMsg) => {
                        return <p>{errorMsg}</p>;
                      }}
                    </ErrorMessage>
                    </div>
                  </div>
                  <div className="field col-12">
                    <label>Answer</label>
                    <TextareaAutosize name="answer"></TextareaAutosize>
                    {/* <Field type="text" name="answer" /> */}
                    <div className="errorMessage">
                    <ErrorMessage name="answer">
                      {(errorMsg) => {
                        return <p>{errorMsg}</p>;
                      }}
                    </ErrorMessage>
                    </div>
                  </div>
                  <div className="col-12 mt-4">
                  <button
                    className="btn btn-primary"
                    type="submit"
                  >
                    Submit
                  </button>
                  </div>
                  </div>
                </Form>
              );
            }}
          </Formik>
        </Modal.Content>
        <Modal.Actions></Modal.Actions>
      </Modal>
    );
  }
};

// import { Modal } from "semantic-ui-react";

// export const CreateHelp = function (props) {
//   return (
//     <Modal open={props.open} onClose={props.onClose} dimmer={"inverted"}>
//       <Modal.Header>Create Help</Modal.Header>
//       <Modal.Content>
//         <Formik
//           className="ui segment"
//           initialValues={initialValues}
//           validate={validate}
//           onSubmit={onSubmit}
//         >
//           <Form className="ui form">
//             <div className="field">
//               <label>Category</label>
//               <Field type="text" name="category" />
//               <ErrorMessage name="category">
//                 {(errorMsg) => {
//                   return <p style={{ color: "red" }}>{errorMsg}</p>;
//                 }}
//               </ErrorMessage>
//             </div>
//             <div className="field">
//               <label>Question</label>
//               <Field type="text" name="question" />
//               <ErrorMessage name="question">
//                 {(errorMsg) => {
//                   return <p style={{ color: "red" }}>{errorMsg}</p>;
//                 }}
//               </ErrorMessage>
//             </div>
//             <div className="field">
//               <label>Answer</label>
//               <Field type="text" name="answer" />
//               <ErrorMessage name="answer">
//                 {(errorMsg) => {
//                   return <p style={{ color: "red" }}>{errorMsg}</p>;
//                 }}
//               </ErrorMessage>
//             </div>
//             <button
//               className="btn btn-primary mb-2"
//               style={{ backgroundColor: "#4cadad" }}
//               type="submit"
//             >
//               Submit
//             </button>
//           </Form>
//         </Formik>
//       </Modal.Content>
//       ;<Modal.Actions></Modal.Actions>
//     </Modal>
//   );
// };
