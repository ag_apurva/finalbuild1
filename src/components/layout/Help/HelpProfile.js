import { Modal } from "semantic-ui-react";
import React, { useState } from "react";
import { DeleteHelp } from "./DeleteHelp";
import { useSelector } from "react-redux";
import { EditHelp } from "./EditHelp";
import { Button } from "semantic-ui-react";
export const HelpProfile = function (props) {
  const [editHelp, setEditHelp] = useState(true);
  const [deleteHelp, setDeleteHelp] = useState(false);
  const submitSuccessful = useSelector((state) => state.help.submitSuccessful);
  if (submitSuccessful) {
    props.onClose();
  }
  const errorCreateHelp = useSelector((state) => state.help.errorCreateHelp);
  return (
    <Modal open={props.open} onClose={props.onClose} dimmer={"inverted"} closeIcon>
      <Modal.Header>
        <Button
          className={deleteHelp === true ? "item active" : "item"}
          onClick={() => {
            setEditHelp(false);
            setDeleteHelp(true);
          }}
        >
          Delete Help
        </Button>
        <Button
          className={editHelp === true ? "item active" : "item"}
          onClick={() => {
            setEditHelp(true);
            setDeleteHelp(false);
          }}
        >
          Edit Help
        </Button>
        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
          <span>&times;</span>
        </button>
      </Modal.Header>
      <Modal.Content>
        {deleteHelp === true ? <DeleteHelp /> : <React.Fragment />}
        {editHelp === true ? <EditHelp /> : <React.Fragment />}
      </Modal.Content>
      <Modal.Actions>
        {errorCreateHelp ? <p>{errorCreateHelp}</p> : <React.Fragment />}
      </Modal.Actions>
    </Modal>
  );
};
