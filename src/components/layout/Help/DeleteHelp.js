import React from "react";
import { deleteHelpContent } from "./store/actions";
import { useSelector, useDispatch } from "react-redux";
import { Loader, Dimmer } from "semantic-ui-react";
export const DeleteHelp = function () {
  const editHelp = useSelector((state) => state.help.editHelp);
  let universityCode = useSelector((state) => state.auth.universityCode);
  let helpData = useSelector((state) => state.help.helpData);
  const onSubmit = async () => {
    let deleteObject = {
      idFaq: editHelp.idFaq,
      universityCode: universityCode,
    };
    dispatch(deleteHelpContent(deleteObject, helpData));
  };
  const spinner = useSelector((state) => state.help.spinner);
  const dispatch = useDispatch();
  if (spinner) {
    return (
      <React.Fragment>
        <Dimmer active>
          <Loader size="big">Loading</Loader>
        </Dimmer>
      </React.Fragment>
    );
  } else {
    return (
      <React.Fragment>
        <p>
          <button
            onClick={() => onSubmit()}
            className="btn btn-primary mb-2"
            style={{ backgroundColor: "#4cadad" }}
          >
            Delete HElp
          </button>
        </p>
      </React.Fragment>
    );
  }
};
