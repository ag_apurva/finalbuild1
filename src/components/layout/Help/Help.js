/*eslint eqeqeq: "off"*/
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import "../css/student.css";
import logo from "../image/attendance.png";
import { HelpProfile } from "./HelpProfile";
import { CreateHelp } from "./CreateHelp";
import { MyProfile } from "../Profile/Profile";
import { helpActions } from "./store/index";
export const Help = function () {
  const userName = useSelector((state) => state.auth.myProfile.userName);
  const userRole = useSelector((state) => state.auth.myProfile.userRole);
  const [myProfile, setMyProfile] = useState(false);
  const [editHelpPopUp, editHelpAddPopUp] = useState(false);
  const [idFaqValue, setIdUniFaqValue] = useState("");
  const [createHelp, setCreateHelp] = useState(false);
  const helpData = useSelector((state) => state.help.helpData);
  const dispatch = useDispatch();
  let profileImage = useSelector((state) => state.auth.profileImage);
  if (profileImage == "") {
    profileImage = logo;
  }
  const renderHelp = () => {
    return helpData.map((item, index) => {
      let { idFaq, category, answer, question } = item;
      const helpObject = { idFaq, category, answer, question };
      return (
        <tr key={index}>
          {editHelpPopUp && idFaqValue == idFaq ? (
            <HelpProfile
              open={editHelpPopUp}
              onClose={() => editHelpAddPopUp(false)}
            />
          ) : (
            <React.Fragment />
          )}
          <td>{category}</td>
          <td>{question}</td>
          <td>{answer}</td>
          <td className="action-cell">
            <button
              onClick={() => {
                editHelpAddPopUp(true);
                setIdUniFaqValue(idFaq);
                dispatch(
                  helpActions.setEditHelpObject({
                    editHelp: helpObject,
                  })
                );
              }}
            >
              <i className="fa fa-edit" aria-hidden="true"></i>
            </button>
            <button
              onClick={() => {
                editHelpAddPopUp(true);
                setIdUniFaqValue(idFaq);
                dispatch(
                  helpActions.setEditHelpObject({
                    editHelp: helpObject,
                  })
                );
              }}
            >
              <i className="fa fa-trash" aria-hidden="true"></i>
            </button>
          </td>
        </tr>
      );
    });
  };

  // const renderHelp = () => {
  //   return helpData.map((item, index) => {
  //     let { idUniFAQ, category, answer, question } = item;
  //     console.log("item", item);
  //     let UserImage = logo;

  //     return (
  //       <tr key={index}>
  //         <td>
  //           <button onClick={() => this.editFAQAddPopUp(idUniFAQ)}>
  //             <i className="fa fa-ellipsis-h" aria-hidden="true"></i>
  //           </button>
  //         </td>
  //       </tr>
  //     );
  //   });
  // };
  if (myProfile) {
    return <MyProfile open={myProfile} onClose={() => setMyProfile(false)} />;
  } else if (createHelp) {
    return (
      <CreateHelp open={createHelp} onClose={() => setCreateHelp(false)} />
    );
  } else {
    return (
      <div className="main-content bg-light">
        <header>
          <h4>
            <label htmlFor="nav-toggel" className="hamburgerIcon">
              <i className="fa fa-bars" aria-hidden="true"></i>
            </label>
            <span className="name">Help</span>
          </h4>

          <div className="user-wrapper" onClick={() => setMyProfile(true)}>
            <img src={profileImage} width="50px" height="50px" alt="" />
            <div>
              <h6>{userName}</h6>
              <small>{userRole}</small>
            </div>
          </div>
        </header>
        <main>
          <div className="table-content tableGridUi">
            <table className="table table-hover">
              <thead>
                <tr>
                  <th scope="col">Category</th>
                  <th scope="col">Question</th>
                  <th scope="col">Answer</th>
                  <th className="text-center">Action</th>
                </tr>
              </thead>
              <tbody>{renderHelp()}</tbody>
            </table>
          </div>
          <div className="mt-4">
            <button
              className="btn btn-primary"
              onClick={() => setCreateHelp(true)}
            >
              Add Help
            </button>
            <button
              className="btn btn-primary ml-2"
              onClick={() => setCreateHelp(true)}
            >
              Add Help
            </button>
          </div>
        </main>
      </div>
    );
  }
};
