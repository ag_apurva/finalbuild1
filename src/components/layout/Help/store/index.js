import { createSlice } from "@reduxjs/toolkit";

const helpSlice = createSlice({
  name: "HelpReducer",
  initialState: {
    helpData: [],
    editHelp: {},
    submitSuccessful: false,
    spinner: false,
    errorCreateHelp: "",
  },
  reducers: {
    helpData(state, action) {
      state.helpData = action.payload.helpData;
    },
    setEditHelpObject(state, action) {
      state.editHelp = action.payload.editHelp;
    },
    setSpinner(state, action) {
      state.spinner = action.payload.spinner;
    },
    setHelpError(state, action) {
      state.errorCreateHelp = action.payload.errorCreateHelp;
    },
    setSubmitSuccessfull(state, action) {
      state.submitSuccessful = action.payload.submitSuccessful;
    },
  },
});

export const helpActions = helpSlice.actions;

export default helpSlice;
