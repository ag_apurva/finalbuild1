/*eslint eqeqeq: "off"*/
import { GET_FAQ_URL, CREATE_FAQ, UPDATE_FAQ, DELETE_FAQ } from "../../API";
import { helpActions } from "./index";
import axios from "axios";
export const editHelpContent = function (updateHelpFaq, helpFaQData) {
  return async function (dispatch) {
    dispatch(helpActions.setSpinner({ spinner: true }));
    try {
      const data = JSON.stringify({
        universityCode: updateHelpFaq.universityCode,
        idFaq: updateHelpFaq.idFaq,
        question: updateHelpFaq.question,
        answer: updateHelpFaq.answer,
      });

      const config = {
        method: "put",
        url: UPDATE_FAQ,
        headers: {
          "Content-Type": "application/json",
        },
        data: data,
      };
      let helpFindIndex = helpFaQData.findIndex((help) => {
        return help.idFaq == updateHelpFaq.idFaq;
      });
      let helpObject = helpFaQData[helpFindIndex];
      helpObject = { ...helpObject };
      helpObject = Object.assign(helpObject, { ...updateHelpFaq });
      let newHelpFaQData = helpFaQData.filter((help) => {
        return help.idFaq !== updateHelpFaq.idFaq;
      });
      newHelpFaQData.push(helpObject);
      let updatedEdit = await axios(config);

      if (!updatedEdit.data.hasOwnProperty("error")) {
        dispatch(
          helpActions.setSubmitSuccessfull({
            submitSuccessful: true,
          })
        );
        setTimeout(() => {
          dispatch(
            helpActions.setSubmitSuccessfull({
              submitSuccessful: false,
            })
          );
        }, 200);
        dispatch(helpActions.helpData({ helpData: newHelpFaQData }));
      } else {
        console.log("update help error1", updatedEdit.data.error);
        dispatch(
          helpActions.setHelpError({
            errorCreateHelp: updatedEdit.data.error,
          })
        );
        setTimeout(() => {
          dispatch(
            helpActions.setHelpError({
              errorCreateHelp: "",
            })
          );
        }, 20000);
        setTimeout(() => {
          dispatch(
            helpActions.setSubmitSuccessfull({ submitSuccessful: true })
          );
        }, 20000);
        setTimeout(() => {
          dispatch(
            helpActions.setSubmitSuccessfull({ submitSuccessful: false })
          );
        }, 20200);
      }
    } catch (e) {
      console.log("update help error2", e);
      dispatch(
        helpActions.setHelpError({
          errorCreateHelp: JSON.stringify(e),
        })
      );
      setTimeout(() => {
        dispatch(
          helpActions.setHelpError({
            errorCreateHelp: "",
          })
        );
      }, 20000);
      setTimeout(() => {
        dispatch(helpActions.setSubmitSuccessfull({ submitSuccessful: true }));
      }, 20000);
      setTimeout(() => {
        dispatch(helpActions.setSubmitSuccessfull({ submitSuccessful: false }));
      }, 20200);
    }
    dispatch(helpActions.setSpinner({ spinner: false }));
  };
};

export const createHelpContent = function (createHelpObject, helpFaQData) {
  return async function (dispatch) {
    dispatch(helpActions.setSpinner({ spinner: true }));
    try {
      const data = JSON.stringify({
        universityCode: createHelpObject.universityCode,
        category: createHelpObject.category,
        answer: createHelpObject.answer,
        question: createHelpObject.question,
      });
      let newFaqData = [...helpFaQData];
      const config = {
        method: "post",
        url: CREATE_FAQ,
        headers: {
          "Content-Type": "application/json",
        },
        data: data,
      };
      let createHelpFaq = await axios(config);

      if (!createHelpFaq.data.hasOwnProperty("error")) {
        createHelpObject.idFaq = createHelpFaq.data.data; // this is auto increment id
        newFaqData.push(createHelpObject);
        dispatch(
          helpActions.setSubmitSuccessfull({
            submitSuccessful: true,
          })
        );
        setTimeout(() => {
          dispatch(
            helpActions.setSubmitSuccessfull({
              submitSuccessful: false,
            })
          );
        }, 200);
        dispatch(helpActions.helpData({ helpData: newFaqData }));
      } else {
        console.log("update help error1", createHelpFaq.data.error);
        dispatch(
          helpActions.setHelpError({
            errorCreateHelp: createHelpFaq.data.error,
          })
        );
        setTimeout(() => {
          dispatch(
            helpActions.setHelpError({
              errorCreateHelp: "",
            })
          );
        }, 20000);
        setTimeout(() => {
          dispatch(
            helpActions.setSubmitSuccessfull({ submitSuccessful: true })
          );
        }, 20000);
        setTimeout(() => {
          dispatch(
            helpActions.setSubmitSuccessfull({ submitSuccessful: false })
          );
        }, 20200);
      }
    } catch (e) {
      console.log("update help error2", e);
      dispatch(
        helpActions.setHelpError({
          errorCreateHelp: JSON.stringify(e),
        })
      );
      setTimeout(() => {
        dispatch(
          helpActions.setHelpError({
            errorCreateHelp: "",
          })
        );
      }, 20000);
      setTimeout(() => {
        dispatch(helpActions.setSubmitSuccessfull({ submitSuccessful: true }));
      }, 20000);
      setTimeout(() => {
        dispatch(helpActions.setSubmitSuccessfull({ submitSuccessful: false }));
      }, 20200);
    }
    dispatch(helpActions.setSpinner({ spinner: false }));
  };
};

export const deleteHelpContent = function (deleteObject, helpFaQData) {
  return async function (dispatch) {
    dispatch(helpActions.setSpinner({ spinner: true }));
    try {
      var config = {
        method: "delete",
        url: DELETE_FAQ,
        headers: {},
        params: {
          universityCode: deleteObject.universityCode,
          idFaq: deleteObject.idFaq,
        },
      };

      let newHelpFaQData = helpFaQData.filter((data) => {
        return data.idFaq != deleteObject.idFaq;
      });

      let updatedEdit = await axios(config);
      if (!updatedEdit.data.hasOwnProperty("error")) {
        dispatch(
          helpActions.setSubmitSuccessfull({
            submitSuccessful: true,
          })
        );
        setTimeout(() => {
          dispatch(
            helpActions.setSubmitSuccessfull({
              submitSuccessful: false,
            })
          );
        }, 200);
        dispatch(helpActions.helpData({ helpData: newHelpFaQData }));
        // dispatch(helpActions.helpData({ helpData: faqData.data }));
      } else {
        console.log("update help error1", updatedEdit.data.error);
        dispatch(
          helpActions.setHelpError({
            errorCreateHelp: updatedEdit.data.error,
          })
        );
        setTimeout(() => {
          dispatch(
            helpActions.setHelpError({
              errorCreateHelp: "",
            })
          );
        }, 20000);
        setTimeout(() => {
          dispatch(
            helpActions.setSubmitSuccessfull({ submitSuccessful: true })
          );
        }, 20000);
        setTimeout(() => {
          dispatch(
            helpActions.setSubmitSuccessfull({ submitSuccessful: false })
          );
        }, 20200);
      }
    } catch (e) {
      console.log("update help error2", e);
      dispatch(
        helpActions.setHelpError({
          errorCreateHelp: JSON.stringify(e),
        })
      );
      setTimeout(() => {
        dispatch(
          helpActions.setHelpError({
            errorCreateHelp: "",
          })
        );
      }, 20000);
      setTimeout(() => {
        dispatch(helpActions.setSubmitSuccessfull({ submitSuccessful: true }));
      }, 20000);
      setTimeout(() => {
        dispatch(helpActions.setSubmitSuccessfull({ submitSuccessful: false }));
      }, 20200);
    }
    dispatch(helpActions.setSpinner({ spinner: false }));
  };
};

export const getFaq = function (code) {
  return async function (dispatch) {
    var axios = require("axios");

    var config = {
      method: "get",
      url: GET_FAQ_URL,
      headers: {},
      params: {
        universityCode: code,
        searchKeyword: '""',
      },
    };
    try {
      let faqData = await axios(config);
      dispatch(helpActions.helpData({ helpData: faqData.data }));
    } catch (e) {}
  };
};

/*



axios(config)



*/
