import React from "react";
import { Formik, Field, ErrorMessage } from "formik";
import { useSelector, useDispatch } from "react-redux";
import { Form } from "formik-semantic-ui-react";
import { editHelpContent } from "./store/actions";
import { Loader, Dimmer } from "semantic-ui-react";

export const EditHelp = function () {
  const editHelp = useSelector((state) => state.help.editHelp);
  const helpData = useSelector((state) => state.help.helpData);
  const spinner = useSelector((state) => state.help.spinner);
  const dispatch = useDispatch();
  const validate = (formValues) => {
    const error = {};
    if (!formValues.question) {
      error.question = "Please write an Question";
    }
    if (!formValues.answer) {
      error.answer = "Please write an Answer";
    }

    return error;
  };
  const initialValues = {
    category: editHelp.category,
    question: editHelp.question,
    answer: editHelp.answer,
  };
  let universityCode = useSelector((state) => state.auth.universityCode);
  let uniCode = universityCode;
  const onSubmit = (formValues) => {
    let updateHelpObject = {
      idFaq: editHelp.idFaq,
      universityCode: uniCode,
      category: formValues.category,
      answer: formValues.answer,
      question: formValues.question,
    };
    dispatch(editHelpContent(updateHelpObject, helpData));
  };

  if (spinner) {
    return (
      <React.Fragment>
        <Dimmer active>
          <Loader size="big">Loading</Loader>
        </Dimmer>
      </React.Fragment>
    );
  } else {
    return (
      <React.Fragment>
        <Formik
          className="ui segment"
          initialValues={initialValues}
          validate={validate}
          onSubmit={onSubmit}
        >
          <Form>
            <div className="row modelFormUi">
            <div className="field col-4">
              <label>Category</label>
              <Field type="text" name="category" disabled={true} />
              <ErrorMessage name="category">
                {(errorMsg) => {
                  return <p style={{ color: "red" }}>{errorMsg}</p>;
                }}
              </ErrorMessage>
            </div>
            <div className="field col-4">
              <label>Question</label>
              <Field type="text" name="question" />
              <ErrorMessage name="question">
                {(errorMsg) => {
                  return <p style={{ color: "red" }}>{errorMsg}</p>;
                }}
              </ErrorMessage>
            </div>
            <div className="field col-4">
              <label>Answer</label>
              <Field type="text" name="answer" />
              <ErrorMessage name="answer">
                {(errorMsg) => {
                  return <p style={{ color: "red" }}>{errorMsg}</p>;
                }}
              </ErrorMessage>
            </div>
            <div className="col-12 mt-3">
            <button
              className="btn btn-primary"
              type="submit"
            >
              Submit
            </button>
            </div>
            </div>
          </Form>
        </Formik>
      </React.Fragment>
    );
  }
};
