import { createSlice } from "@reduxjs/toolkit";

const departmentSlice = createSlice({
  name: "departmentReducer",
  initialState: {
    departmentsData: [],
    loaderDepartmentsSpinner: false,
    errorCreateDepartment: "",
    submitSuccessful: false,
    spinner: false,
  }, //{ counter: 0, showCounter: false },
  reducers: {
    departmentsData(state, action) {
      state.departmentsData = [...action.payload.departmentsData];
    },
    createDepartment(state, action) {
      state.departmentsData = [
        ...state.departmentsData,
        action.payload.departmentData,
      ];
    },
    errorCreateDepartment(state, action) {
      state.errorCreateDepartment = action.payload.error;
    },
    setSubmitSuccessfull(state, action) {
      state.submitSuccessful = action.payload.submitSuccessful;
    },
  },
});

/*
departmentsData contains all the departments 
departmentData without 's' contain only created department 
 */
export const departmentActions = departmentSlice.actions;
export default departmentSlice;
