import { GET_DEPARTMENTS, CREATE_DEPARTMENTS } from "../../API";
import { departmentActions } from "./index";
const axios = require("axios");

export const getDepartments = function (universityCode) {
  const config = {
    method: "get",
    url: GET_DEPARTMENTS,
    headers: {},
    params: {
      universityCode: universityCode,
    },
  };

  return async function (dispatch) {
    try {
      let departmentsData = await axios(config);
      dispatch(
        departmentActions.departmentsData({
          departmentsData: departmentsData.data,
        })
      );
    } catch (e) {
      /* this action is called at the time of login of this fails we connot do anything 
      through SNS topic development will already be notified*/
    }
  };
};

export const createDepartment = function (formValues, universityCode) {
  return async function (dispatch) {
    var data = JSON.stringify({
      universityCode: universityCode,
      departmentName: formValues.departmentName,
      departmentHead: formValues.departmentHead,
    });

    var config = {
      method: "post",
      url: CREATE_DEPARTMENTS,
      headers: {
        "Content-Type": "application/json",
      },
      data: data,
    };
    try {
      const createDepartmentData = await axios(config);
      if (createDepartmentData.data.hasOwnProperty("data")) {
        dispatch(
          departmentActions.createDepartment({
            departmentData: {
              ...formValues,
              ...{ departmentCode: createDepartmentData.data.data },
            },
          })
        );

        dispatch(
          departmentActions.setSubmitSuccessfull({
            submitSuccessful: true,
          })
        );
        setTimeout(() => {
          dispatch(
            departmentActions.setSubmitSuccessfull({
              submitSuccessful: false,
            })
          );
        }, 200);
      } else {
        dispatch(
          departmentActions.errorCreateDepartment({
            error: createDepartmentData.data.error,
          })
        );
      }
    } catch (e) {
      console.log("Error2", e);
      dispatch(
        departmentActions.errorCreateDepartment({ error: JSON.stringify(e) })
      );
    }
  };
};
