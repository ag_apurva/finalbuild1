/*eslint eqeqeq: "off"*/
import React, { useState } from "react";
import { useSelector } from "react-redux";
import { CreateDepartment } from "./CreateDepartments";
import { MyProfile } from "../Profile/Profile";
import logo from "../image/attendance.png";
export const Departments = function () {
  const userName = useSelector((state) => state.auth.myProfile.userName);
  const userRole = useSelector((state) => state.auth.myProfile.userRole);
  const [myProfile, setMyProfile] = useState(false);
  const [createDepartmentPopUp, setCreateDepartmentPopUp] = useState(false);
  const departmentsData = useSelector(
    (state) => state.departments.departmentsData
  );
  let profileImage = useSelector((state) => state.auth.profileImage);
  if (profileImage == "") {
    profileImage = logo;
  }
  const renderDepartments = function () {
    return departmentsData.map((item, index) => {
      let { departmentCode, departmentName, departmentHead } = item;
      departmentHead = departmentHead ? departmentHead : "";
      return (
        <tr key={index}>
          <td>{departmentCode}</td>
          <td>{departmentName}</td>
          <td>{departmentHead}</td>
        </tr>
      );
    });
  };
  if (myProfile) {
    return <MyProfile open={myProfile} onClose={() => setMyProfile(false)} />;
  } else if (createDepartmentPopUp) {
    return (
      <CreateDepartment
        open={createDepartmentPopUp}
        onClose={() => setCreateDepartmentPopUp(false)}
      />
    );
  } else {
    return (
      <div className="main-content bg-light">
        <header>
          <h4>
            <label htmlFor="nav-toggel" className="hamburgerIcon">
              <i className="fa fa-bars" aria-hidden="true"></i>
            </label>
            <span className="name">Create Departments</span>
          </h4>
          <div className="user-wrapper" onClick={() => setMyProfile(true)}>
            <img src={profileImage} width="50px" height="50px" alt="" />
            <div>
              <h6>{userName}</h6>
              <small>{userRole}</small>
            </div>
          </div>
        </header>
        <main>
          <div className="table-content tableGridUi">
            <table className="table table-hover">
              <thead>
                <tr>
                  <th scope="col" className="col-1">Department Code</th>
                  <th scope="col" className="col-2">Department Name</th>
                  <th scope="col">Department Head</th>
                </tr>
              </thead>
              <tbody>{renderDepartments()}</tbody>
            </table>
          </div>
          <div className="mt-4">
            <button
              className="btn btn-primary"
              onClick={() => setCreateDepartmentPopUp(true)}
            >
              Add Department
            </button>
          </div>
        </main>
      </div>
    );
  }
};
