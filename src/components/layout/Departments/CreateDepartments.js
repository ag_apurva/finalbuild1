import React from "react";
import { Formik, Field, ErrorMessage } from "formik";
import { Modal, Button, FormField } from "semantic-ui-react";
import { useSelector, useDispatch } from "react-redux";
import { Form } from "formik-semantic-ui-react";
import { createDepartment } from "./store/actions";
const TextError = function (props) {
  return <p style={{ color: "red" }}>{props.children}</p>;
};

export const CreateDepartment = function (props) {
  const departmentsData = useSelector(
    (state) => state.departments.departmentsData
  );
  const departmentNames = departmentsData.map((item) => item.departmentName);
  const createDepartmentError = useSelector(
    (state) => state.departments.errorCreateDepartment
  );
  const submitSuccessful = useSelector(
    (state) => state.departments.submitSuccessful
  );
  if (submitSuccessful) {
    props.onClose(false);
  }
  const dispatch = useDispatch();
  const validate = (formValues) => {
    let error = {};
    if (!formValues.departmentName) {
      error.departmentName = "* Please provide an Department Name";
    }
    if (formValues.departmentName) {
      if (departmentNames.includes(formValues.departmentName)) {
        error.departmentName = "* Department Exists";
      }
    }
    if (!formValues.departmentHead) {
      error.departmentHead = "* Please provide an Department Head";
    }
    return error;
  };
  const universityCode = useSelector((state) => state.auth.universityCode);
  const onSubmit = async (formValues) => {
    dispatch(
      createDepartment(
        {
          departmentName: formValues.departmentName,
          departmentHead: formValues.departmentHead,
        },
        universityCode
      )
    );
  };
  return (
    <Modal
      open={props.open}
      onClose={props.onClose}
      style={{ overflow: "auto", maxHeight: 600 }}
      dimmer={"inverted"}
      closeIcon
    >
      <Modal.Header>
        <h3>Add Department</h3>
        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
          <span>&times;</span>
        </button>
      </Modal.Header>
      <Modal.Content>
        <Formik
          initialValues={{ departmentName: "", departmentHead: "" }}
          validate={validate}
          onSubmit={onSubmit}
        >
          <Form>
          <div className="row modelFormUi">
            <FormField className="col-6">
              <label htmlFor="deptId">Department Name</label>
              <Field id="deptId" name="departmentName" />
              <div className="errorMessage">
              <ErrorMessage name="departmentName" component={TextError} />
              </div>
            </FormField>
            <FormField className="col-6">
              <label htmlFor="depthead">Department Head</label>
              <Field id="depthead" name="departmentHead" />
              <div className="errorMessage">
              <ErrorMessage name="departmentHead" component={TextError} />
              </div>
            </FormField>
            <div className="col-12 mt-4">
            <Button type="submit" className="btn btn-primary">Submit</Button>
            </div>
            </div>
          </Form>
        </Formik>
      </Modal.Content>
      <Modal.Actions>
        {createDepartmentError ? (
          <p>{createDepartmentError}</p>
        ) : (
          <React.Fragment />
        )}
      </Modal.Actions>
    </Modal>
  );
};
