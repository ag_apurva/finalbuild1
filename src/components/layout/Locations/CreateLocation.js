import { Formik, Field, ErrorMessage } from "formik";
import { Modal, FormField, Button } from "semantic-ui-react";
import { Form } from "formik-semantic-ui-react";
import { Loader, Dimmer } from "semantic-ui-react";
import * as yup from "yup";
import React from "react";

import { useSelector, useDispatch } from "react-redux";
import { createLocation } from "./store/actions";
const TextError = function (props) {
  return <p style={{ color: "red" }}>{props.children}</p>;
};

export const CreateLocation = function (props) {
  const locationsData = useSelector((state) => state.locations.locationsData);
  const validationSchema = yup.object().shape({
    classRoomName: yup.string().required("Class Room Name is Required"),
    classRoomDetail: yup.string().required("Class Room Detail is Required"),
    classRoomType: yup.string().required("Class Room Type is Required"),
  });
  const submitSuccessful = useSelector(
    (state) => state.locations.submitSuccessful
  );
  const spinner = useSelector((state) => state.locations.spinner);
  const errorCreateLocation = useSelector(
    (state) => state.locations.errorCreateLocation
  );
  if (submitSuccessful) {
    props.setCreateLocationPopUpFlag(false);
  }
  const dispatch = useDispatch();
  const universityCode = useSelector((state) => state.auth.universityCode);
  return (
    <Modal
      style={{ overflow: "auto", maxHeight: 600 }}
      dimmer={"inverted"}
      open={props.createLocationPopUpFlag}
      closeIcon
      onClose={() => props.setCreateLocationPopUpFlag(false)}
    >
      <Modal.Header>
        <h3>Create Location</h3>
        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
          <span>&times;</span>
        </button>
      </Modal.Header>
      {spinner ? (
        <Modal.Content>
          <Dimmer active>
            <Loader size="big">Loading</Loader>
          </Dimmer>
        </Modal.Content>
      ) : (
        <Modal.Content>
          <Formik
            initialValues={{
              classRoomName: "",
              classRoomDetail: "",
              classRoomType: "",
            }}
            onSubmit={async (formValues) => {
              console.log("formValues", formValues);
              dispatch(createLocation(formValues, universityCode));
            }}
            validationSchema={validationSchema}
            validate={(formValues) => {
              let errors = {};
              if (!formValues.classRoomName) {
                errors.classRoomName = "*Please provide a Class Room Name";
              }
              if (formValues.classRoomName) {
                const classRoom = locationsData.find((location) => {
                  return (
                    location.classRoomName.toLowerCase() ===
                    formValues.classRoomName.toLowerCase()
                  );
                });
                if (classRoom) {
                  errors.classRoomName = "*This  Class Room Name Exists";
                }
              }
              if (!formValues.classRoomDetail) {
                errors.classRoomDetail = "*Please provide a Class Room Detail";
              }
              if (!formValues.classRoomType) {
                errors.classRoomType = "*Please provide a Class Room Type";
              }

              return errors;
            }}
          >
            {(formik) => {
              return (
                <Form>
                  <div className="row modelFormUi">
                  <FormField className="col-4">
                    <label>Class Room Name</label>
                    <Field name="classRoomName" />
                    <div className="errorMessage">
                    <ErrorMessage name="classRoomName" component={TextError} />
                    </div>
                  </FormField>
                  <FormField className="col-4">
                    <label>Class Room Detail</label>
                    <Field name="classRoomDetail" />
                    <div className="errorMessage">
                    <ErrorMessage
                      name="classRoomDetail"
                      component={TextError}
                    />
                    </div>
                  </FormField>
                  <FormField className="col-4">
                    <label>Class Room Type</label>
                    <Field name="classRoomType" />
                    <div className="errorMessage">
                    <ErrorMessage name="classRoomType" component={TextError} />
                    </div>
                  </FormField>
                  <div className="col-12 mt-4">
                  <Button type="submit" className="btn btn-primary">Submit</Button>
                  </div>
                  </div>
                </Form>
              );
            }}
          </Formik>
        </Modal.Content>
      )}
      <Modal.Actions>
        {errorCreateLocation ? (
          <p>{errorCreateLocation}</p>
        ) : (
          <React.Fragment />
        )}
      </Modal.Actions>
    </Modal>
  );
};
