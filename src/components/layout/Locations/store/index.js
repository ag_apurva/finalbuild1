import { createSlice } from "@reduxjs/toolkit";

const locationSlice = createSlice({
  name: "locationReducer",
  initialState: {
    locationsData: [],
    submitSuccessful: false,
    spinner: false,
    errorCreateLocation: "",
  },
  reducers: {
    locationData(state, action) {
      state.locationsData = [...action.payload.locationData];
    },
    addLocation(state, action) {
      state.locationsData.push(action.payload.locationsData);
    },
    setSpinner(state, action) {
      state.spinner = action.payload.spinner;
    },
    setLocationError(state, action) {
      state.errorCreateLocation = action.payload.errorCreateLocation;
    },
    setSubmitSuccessfull(state, action) {
      state.submitSuccessful = action.payload.submitSuccessful;
    },
  },
});

export const locationActions = locationSlice.actions;
export default locationSlice;
