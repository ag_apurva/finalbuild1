import { GET_LOCATION, CREATE_LOCATION } from "../../API";
import axios from "axios";
import { locationActions } from "./index";

export const getLocations = function (code) {
  return async function (dispatch) {
    const config = {
      method: "get",
      url: GET_LOCATION,
      headers: {},
      params: {
        universityCode: code,
      },
    };

    try {
      let locations = await axios(config);
      if (!locations.data.hasOwnProperty("error")) {
        dispatch(
          locationActions.locationData({ locationData: locations.data })
        );
      }
    } catch (e) {}
  };
};

export const createLocation = function (formValues, universityCode) {
  return async function (dispatch) {
    dispatch(locationActions.setSpinner({ spinner: true }));

    try {
      var data = JSON.stringify({
        universityCode: universityCode,
        classRoomName: formValues.classRoomName,
        classRoomDetail: formValues.classRoomDetail,
        classRoomType: formValues.classRoomType,
      });

      var config = {
        method: "post",
        url: CREATE_LOCATION,
        headers: {
          "Content-Type": "application/json",
        },
        data: data,
      };
      let createLocation = await axios(config);

      if (!createLocation.data.hasOwnProperty("error")) {
        let locationsData = {
          classRoomName: formValues.classRoomName,
          classRoomDetail: formValues.classRoomDetail,
          classRoomType: formValues.classRoomType,
          universityCode: universityCode,
        };
        dispatch(locationActions.addLocation({ locationsData: locationsData }));
        dispatch(
          locationActions.setSubmitSuccessfull({ submitSuccessful: true })
        );
        setTimeout(() => {
          dispatch(
            locationActions.setSubmitSuccessfull({ submitSuccessful: false })
          );
        }, 200);
      } else {
        dispatch(
          locationActions.setLocationError({
            errorCreateLocation: createLocation.data.error,
          })
        );
        setTimeout(() => {
          dispatch(
            locationActions.setLocationError({
              errorCreateLocation: "",
            })
          );
        }, 20000);
        setTimeout(() => {
          dispatch(
            locationActions.setSubmitSuccessfull({ submitSuccessful: true })
          );
        }, 20000);
        setTimeout(() => {
          dispatch(
            locationActions.setSubmitSuccessfull({ submitSuccessful: false })
          );
        }, 20200);
      }
    } catch (e) {
      dispatch(
        locationActions.setLocationError({
          errorCreateLocation: JSON.stringify(e),
        })
      );
      setTimeout(() => {
        dispatch(
          locationActions.setLocationError({
            errorCreateLocation: "",
          })
        );
      }, 20000);
      setTimeout(() => {
        dispatch(
          locationActions.setSubmitSuccessfull({ submitSuccessful: true })
        );
      }, 20000);
      setTimeout(() => {
        dispatch(
          locationActions.setSubmitSuccessfull({ submitSuccessful: false })
        );
      }, 20200);
    }
    dispatch(locationActions.setSpinner({ spinner: false }));
  };
};
