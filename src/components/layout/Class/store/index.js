import { createSlice } from "@reduxjs/toolkit";

const classSlice = createSlice({
  name: "classReducer",
  initialState: {
    classData: [],
    spinner: false,
    submitSuccessful: false,
    errorCreateClass: "",
  }, //{ counter: 0, showCounter: false },
  reducers: {
    classData(state, action) {
      state.classData = [...action.payload];
    },
    addclass(state, action) {
      state.classData.push(action.payload);
    },
    setSpinner(state, action) {
      state.spinner = action.payload.spinner;
    },
    errorCreateClass(state, action) {
      state.errorCreateClass = action.payload.errorCreateClass;
    },
    setSubmitSuccessfull(state, action) {
      state.submitSuccessful = action.payload.submitSuccessful;
    },
  },
});

export const classActions = classSlice.actions;
export default classSlice;
