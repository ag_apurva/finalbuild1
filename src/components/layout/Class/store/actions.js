import { GET_CLASS, CREATE_CLASS } from "../../API";
import { classActions } from "./index";
import axios from "axios";
export const getClasses = (code) => {
  return async function (dispatch) {
    var config = {
      method: "get",
      url: GET_CLASS,
      headers: {},
      params: {
        universityCode: code,
      },
    };

    try {
      let classData = await axios(config);
      if (!classData.data.hasOwnProperty("error")) {
        dispatch(classActions.classData(classData.data));
      }
    } catch (e) {
      console.log("error retriving Class", e);
    }
  };
};

export const createClass = function (formValues, universityCode) {
  return async function (dispatch) {
    dispatch(classActions.setSpinner({ spinner: true }));
    var data = JSON.stringify(formValues);

    var config = {
      method: "post",
      url: CREATE_CLASS,
      headers: {
        "Content-Type": "application/json",
      },
      data: data,
    };

    try {
      const createClassData = await axios(config);
      if (!createClassData.data.hasOwnProperty("error")) {
        dispatch(getClasses(universityCode));
        dispatch(classActions.setSubmitSuccessfull({ submitSuccessful: true }));
        setTimeout(() => {
          dispatch(
            classActions.setSubmitSuccessfull({ submitSuccessful: false })
          );
        }, 200);
      } else {
        dispatch(
          classActions.errorCreateClass({
            errorCreateClass: createClassData.data.error,
          })
        );
        setTimeout(() => {
          dispatch(
            classActions.errorCreateClass({
              errorCreateClass: "",
            })
          );
        }, 20000);
        setTimeout(() => {
          dispatch(
            classActions.setSubmitSuccessfull({ submitSuccessful: true })
          );
        }, 20000);
        setTimeout(() => {
          dispatch(
            classActions.setSubmitSuccessfull({ submitSuccessful: false })
          );
        }, 20200);
      }
    } catch (e) {
      dispatch(
        classActions.errorCreateClass({
          errorCreateClass: JSON.stringify(e),
        })
      );
      setTimeout(() => {
        dispatch(
          classActions.errorCreateClass({
            errorCreateClass: "",
          })
        );
      }, 20000);
      setTimeout(() => {
        dispatch(classActions.setSubmitSuccessfull({ submitSuccessful: true }));
      }, 20000);
      setTimeout(() => {
        dispatch(
          classActions.setSubmitSuccessfull({ submitSuccessful: false })
        );
      }, 20200);
    }
    dispatch(classActions.setSpinner({ spinner: false }));
  };
};
