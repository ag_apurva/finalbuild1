/*eslint eqeqeq: "off"*/
import React, { useState } from "react";
import { useSelector } from "react-redux";
import { CreateClass } from "./CreateClass";
import { MyProfile } from "../Profile/Profile";
import logo from "../image/attendance.png";
export const Class = function (props) {
  const userName = useSelector((state) => state.auth.myProfile.userName);
  const userRole = useSelector((state) => state.auth.myProfile.userRole);
  const [createClassPopUp, setCreateClassPopUp] = useState(false);
  const [myProfile, setMyProfile] = useState(false);
  const classesData = useSelector((state) => state.class.classData);
  let profileImage = useSelector((state) => state.auth.profileImage);
  if (profileImage == "") {
    profileImage = logo;
  }
  const renderClasses = function () {
    return classesData.map((item, index) => {
      let {
        //idClass,
        admissionCode,
        subYear,
        division,
        semester,
        subDivision,
      } = item;
      return (
        <tr key={index}>
          <td>{admissionCode}</td>
          <td>{subYear}</td>
          <td>{division}</td>
          <td>{semester}</td>
          <td>{subDivision}</td>
        </tr>
      );
    });
  };

  if (myProfile) {
    return <MyProfile open={myProfile} onClose={() => setMyProfile(false)} />;
  } else if (createClassPopUp) {
    return (
      <CreateClass
        open={createClassPopUp}
        onClose={() => setCreateClassPopUp(false)}
      />
    );
  } else {
    return (
      <div className="main-content bg-light">
        <header>
          <h4>
            <label htmlFor="nav-toggel" className="hamburgerIcon">
              <i className="fa fa-bars" aria-hidden="true"></i>
            </label>
            <span className="name">Class</span>
          </h4>

          <div className="user-wrapper" onClick={() => setMyProfile(true)}>
            <img src={profileImage} width="50px" height="50px" alt="" />
            <div>
              <h6>{userName}</h6>
              <small>{userRole}</small>
            </div>
          </div>
        </header>
        <main>
          <div className="table-content tableGridUi">
            <table className="table table-hover">
              <thead>
                <tr>
                  <th scope="col">Admission Code</th>
                  <th scope="col">SubYear</th>
                  <th scope="col">Division</th>
                  <th scope="col">Semester</th>
                  <th scope="col">SubDivision</th>
                </tr>
              </thead>
              <tbody>{renderClasses()}</tbody>
            </table>
          </div>
          <div className="mt-4">
          <button
            className="btn btn-primary"
            onClick={() => setCreateClassPopUp(true)}
          >
            Add Class
          </button>
          </div>
        </main>
      </div>
    );
  }
};
