/*eslint eqeqeq: "off"*/
import React, { useState } from "react";
import { Formik, Field, ErrorMessage } from "formik";
import { Modal, Button, FormField } from "semantic-ui-react";
import { useSelector, useDispatch } from "react-redux";
import { Form } from "formik-semantic-ui-react";
import { createClass } from "./store/actions";
// import { Loader, Dimmer } from "semantic-ui-react";
const TextError = function (props) {
  return <p style={{ color: "red" }}>{props.children}</p>;
};

export const CreateClass = function (props) {
  const [semsisterFlag, setSemsisteFlag] = useState(true);
  const [semsisters, setSemsisters] = useState([]);
  const admissionsData = useSelector((state) => state.batch.admissionsData);
  const admissionCode = admissionsData.map((item) => ({
    admissionCode: item.admissionCode,
  }));
  const errorCreateClass = useSelector((state) => state.class.errorCreateClass);
  const submitSuccessful = useSelector((state) => state.class.submitSuccessful);
  if (submitSuccessful) {
    props.onClose();
  }
  const spinner = useSelector((state) => state.batch.spinner);
  const dispatch = useDispatch();
  const validate = (formValues) => {
    let error = {};
    if (!formValues.admissionCode) {
      error.admissionCode = "Please provide an AdmissionCode";
    }
    if (!formValues.semister) {
      error.semister = "Please provide an Semistername";
    }
    if (!formValues.division) {
      error.division = "Please provide an Division";
    }
    if (!formValues.subYear) {
      error.subYear = "Please provide an SubYear";
    }
    if (!formValues.subdivision) {
      error.subdivision = "Please provide an subdivision";
    }

    return error;
  };
  const universityCode = useSelector((state) => state.auth.universityCode);
  const onSubmit = async (formValues) => {
    console.log("formValues", formValues);
    dispatch(createClass(formValues, universityCode));
  };

  const onChangeCheck = (e) => {
    if (e.target.name == "subYear") {
      if (e.target.value == "1") {
        setSemsisteFlag(false);
        setSemsisters([{ item: 1 }, { item: 2 }]);
      }
      if (e.target.value === "2") {
        setSemsisteFlag(false);
        setSemsisters([{ item: 3 }, { item: 4 }]);
      }
      if (e.target.value === "3") {
        setSemsisteFlag(false);
        setSemsisters([{ item: 5 }, { item: 6 }]);
      }
      if (e.target.value === "4") {
        setSemsisteFlag(false);
        setSemsisters([{ item: 7 }, { item: 8 }]);
      }
    }
  };
  return (
    <Modal
      open={props.open}
      onClose={props.onClose}
      style={{ overflow: "auto", maxHeight: 600 }}
      dimmer={"inverted"}
      closeIcon
    >
      <Modal.Header>
        <h3>Add Class</h3>
        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
          <span>&times;</span>
        </button>
      </Modal.Header>
      {spinner ? (
        <Modal.Content>
          <div className="ui active inverted dimmer">
            <div className="ui loader"></div>
          </div>
        </Modal.Content>
      ) : (
        <Modal.Content>
          <Formik
            initialValues={{
              admissionCode: "",
              subYear: "",
              division: "",
              semister: "",
              quarter: "",
              subdivision: "",
            }}
            validate={validate}
            onSubmit={onSubmit}
          >
            <Form>
            <div className="row modelFormUi">
              <FormField className="col-4">
                <label htmlFor="deptId">Admission Code</label>
                <Field id="deptId" name="admissionCode">
                  {(props) => {
                    return (
                      <select {...props.field}>
                        <option value="">None</option>
                        {admissionCode.map((item, index) => (
                          <option key={index} value={item.admissionCode}>
                            {item.admissionCode}
                          </option>
                        ))}
                      </select>
                    );
                  }}
                </Field>
                <div className="errorMessage">
                <ErrorMessage name="admissionCode" component={TextError} />
                </div>
              </FormField>
              <FormField className="col-4">
                <label htmlFor="subYearId">SubYear</label>
                <Field id="subYearId" name="subYear">
                  {(props) => {
                    return (
                      <select
                        {...props.field}
                        onChange={(e) => {
                          onChangeCheck(e);
                          props.field.onChange(e);
                        }}
                      >
                        <option value="">None</option>
                        {[
                          { item: 1 },
                          { item: 2 },
                          { item: 3 },
                          { item: 4 },
                        ].map((item, index) => (
                          <option key={index} value={item.item}>
                            {item.item}
                          </option>
                        ))}
                      </select>
                    );
                  }}
                </Field>
                <div className="errorMessage">
                <ErrorMessage name="subYear" component={TextError} />
                </div>
              </FormField>
              <FormField className="col-4">
                <label htmlFor="semisterId">Semister</label>
                <Field id="semisterId" name="semister">
                  {(props) => {
                    return (
                      <select {...props.field} disabled={semsisterFlag}>
                        <option value="">None</option>
                        {semsisters.map((item, index) => (
                          <option key={index} value={item.item}>
                            {item.item}
                          </option>
                        ))}
                      </select>
                    );
                  }}
                </Field>
                <div className="errorMessage">
                <ErrorMessage name="semister" component={TextError} />
                </div>
              </FormField>
              <FormField className="col-4">
                <label htmlFor="divisionId">Division</label>
                <Field id="divisionId" name="division" />
                <div className="errorMessage">
                <ErrorMessage name="division" component={TextError} />
                </div>
              </FormField>
              <FormField className="col-4">
                <label htmlFor="subdivisionId">Sub Division</label>
                <Field id="subdivisionId" name="subdivision" />
                <div className="errorMessage">
                <ErrorMessage name="subdivision" component={TextError} />
                </div>
              </FormField>
              <div className="col-12 mt-4">
              <Button type="submit" className="btn btn-primary">Submit</Button>
              </div>
              </div>
            </Form>
          </Formik>
        </Modal.Content>
      )}
      <Modal.Actions>
        {errorCreateClass ? <p>{errorCreateClass}</p> : <React.Fragment />}{" "}
      </Modal.Actions>
    </Modal>
  );
};
