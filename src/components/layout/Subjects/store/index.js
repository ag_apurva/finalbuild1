import { createSlice } from "@reduxjs/toolkit";

const subjectSlice = createSlice({
  name: "subjectReducer",
  initialState: {
    subjectsData: [],
    submitSuccessful: false,
    spinner: false,
    errorCreateSubject: "",
  },
  reducers: {
    subjectData(state, action) {
      state.subjectsData = [...action.payload.subjectData];
    },
    addSubjects(state, action) {
      state.subjectsData.push(action.payload.subjectData);
    },
    setSpinner(state, action) {
      state.spinner = action.payload.spinner;
    },
    setSubjectError(state, action) {
      state.errorCreateSubject = action.payload.errorCreateSubject;
    },
    setSubmitSuccessfull(state, action) {
      state.submitSuccessful = action.payload.submitSuccessful;
    },
  },
});

export const subjectActions = subjectSlice.actions;
export default subjectSlice;
