import { GET_SUBJECT, CREATE_SUBJECT } from "../../API";
import axios from "axios";
import { subjectActions } from "./index";

export const getSubjects = function (code) {
  return async function (dispatch) {
    const config = {
      method: "get",
      url: GET_SUBJECT,
      headers: {},
      params: {
        universityCode: code,
      },
    };

    try {
      let subjects = await axios(config);
      if (!subjects.data.hasOwnProperty("error")) {
        dispatch(subjectActions.subjectData({ subjectData: subjects.data }));
      }
    } catch (e) {}
  };
};

export const createSubject = function (formValues, universityCode) {
  return async function (dispatch) {
    dispatch(subjectActions.setSpinner({ spinner: true }));
    const data = JSON.stringify({
      universityCode: universityCode,
      subjectName: formValues.subjectName,
    });

    var config = {
      method: "post",
      url: CREATE_SUBJECT,
      headers: {
        "Content-Type": "application/json",
      },
      data: data,
    };

    try {
      let createSubject = await axios(config);

      if (!createSubject.data.hasOwnProperty("error")) {
        let subjectCode = createSubject.data.data; //since create Subject was supposed to return Subjetc Code
        let subjectData = {
          subjectName: formValues.subjectName,
          subjectCode: subjectCode,
          universityCode: universityCode,
        };
        dispatch(subjectActions.addSubjects({ subjectData: subjectData }));
        dispatch(
          subjectActions.setSubmitSuccessfull({ submitSuccessful: true })
        );
        setTimeout(() => {
          dispatch(
            subjectActions.setSubmitSuccessfull({ submitSuccessful: false })
          );
        }, 200);
      } else {
        dispatch(
          subjectActions.setSubjectError({
            errorCreateSubject: createSubject.data.error,
          })
        );
        setTimeout(() => {
          dispatch(
            subjectActions.setSubjectError({
              errorCreateSubject: "",
            })
          );
        }, 20000);
        setTimeout(() => {
          dispatch(
            subjectActions.setSubmitSuccessfull({ submitSuccessful: true })
          );
        }, 20000);
        setTimeout(() => {
          dispatch(
            subjectActions.setSubmitSuccessfull({ submitSuccessful: false })
          );
        }, 20200);
      }
    } catch (e) {
      console.log("error creating subject", e);
      dispatch(
        subjectActions.setSubjectError({
          errorCreateSubject: JSON.stringify(e),
        })
      );
      setTimeout(() => {
        dispatch(
          subjectActions.setSubjectError({
            errorCreateSubject: "",
          })
        );
      }, 20000);
      setTimeout(() => {
        dispatch(
          subjectActions.setSubmitSuccessfull({ submitSuccessful: true })
        );
      }, 20000);
      setTimeout(() => {
        dispatch(
          subjectActions.setSubmitSuccessfull({ submitSuccessful: false })
        );
      }, 20200);
    }
    dispatch(subjectActions.setSpinner({ spinner: false }));
  };
};
