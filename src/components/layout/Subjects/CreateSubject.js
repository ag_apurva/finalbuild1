import * as React from "react";
import { useSelector, useDispatch } from "react-redux";
import { Modal, FormField, Button } from "semantic-ui-react";
import { Form } from "formik-semantic-ui-react";
import { Formik, Field, ErrorMessage } from "formik";
import { Loader, Dimmer } from "semantic-ui-react";
import { createSubject } from "./store/actions";
const TextError = function (props) {
  return <p style={{ color: "red" }}>{props.children}</p>;
};

export const CreateSubject = (props) => {
  const subjectsData = useSelector((state) => state.subjects.subjectsData);
  const universityCode = useSelector((state) => state.auth.universityCode);
  const dispatch = useDispatch();
  const validate = (formValues) => {
    const errors = {};
    if (!formValues.subjectName) {
      errors.subjectName = "* Please provide a Subject Name";
    }

    if (formValues.subjectName) {
      const subjectCheck = subjectsData.find((subject) => {
        return (
          subject.subjectName.toString().toLowerCase() ===
          formValues.subjectName.toLowerCase()
        );
      });
      if (subjectCheck) {
        errors.subjectName = "* Subject Already Exists";
      }
    }
    return errors;
  };
  const submitSuccessful = useSelector(
    (state) => state.subjects.submitSuccessful
  );
  const spinner = useSelector((state) => state.subjects.spinner);
  const errorCreateSubject = useSelector(
    (state) => state.subjects.errorCreateSubject
  );
  if (submitSuccessful) {
    props.setCreateSubjectPopUpFlag(false);
  }
  const onSubmit = async function (formValues) {
    dispatch(createSubject(formValues, universityCode));
  };
  if (submitSuccessful) {
    props.setCreateSubjectPopUpFlag(false);
  }
  return (
    <Modal
      dimmer={"inverted"}
      open={props.createSubjectPopUpFlag}
      closeIcon
      onClose={() => props.setCreateSubjectPopUpFlag(false)}
    >
      <Modal.Header>
        <h3>Create Subject</h3>
        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
          <span>&times;</span>
        </button>
      </Modal.Header>
      {spinner ? (
        <Modal.Content>
          <Dimmer active>
            <Loader size="big">Loading</Loader>
          </Dimmer>
        </Modal.Content>
      ) : (
        <Modal.Content>
          <Formik
            initialValues={{ subjectName: "" }}
            onSubmit={onSubmit}
            inverted={"dimmer"}
            validate={validate}
          >
            {(formik) => {
              return (
                <Form>
                  <div className="row modelFormUi">
                  <FormField className="col-12">
                    <label htmlFor="subjectId">Subject</label>
                    <Field id="subjectId" name="subjectName" />
                    <div className="errorMessage">
                    <ErrorMessage name="subjectName" component={TextError} />
                    </div>
                  </FormField>
                  <div className="col-12 mt-4">
                  <Button type="submit" className="btn btn-primary">Submit</Button>
                  </div>
                  </div>
                </Form>
              );
            }}
          </Formik>
        </Modal.Content>
      )}
      <Modal.Actions>
        {errorCreateSubject ? <p>{errorCreateSubject}</p> : <React.Fragment />}
      </Modal.Actions>
    </Modal>
  );
};
