/*eslint eqeqeq: "off"*/
import React, { useState } from "react";
import { useSelector } from "react-redux";
import { CreateSubject } from "./CreateSubject";
import { MyProfile } from "../Profile/Profile";
import logo from "../image/attendance.png";
export const Subjects = function (props) {
  const userName = useSelector((state) => state.auth.myProfile.userName);
  const userRole = useSelector((state) => state.auth.myProfile.userRole);
  const [myProfile, setMyProfile] = useState(false);

  const [createSubjectPopUpFlag, setCreateSubjectPopUpFlag] = useState(false);
  const subjectsData = useSelector((state) => state.subjects.subjectsData);
  let profileImage = useSelector((state) => state.auth.profileImage);
  if (profileImage == "") {
    profileImage = logo;
  }
  const renderSubjects = () => {
    return subjectsData.map((item, index) => {
      const SubjName = item.subjectName || " ";

      return (
        <tr key={index}>
          <td>{SubjName}</td>
        </tr>
      );
    });
  };
  if (myProfile) {
    return <MyProfile open={myProfile} onClose={() => setMyProfile(false)} />;
  } else {
    return (
      <div className="main-content bg-light">
        <header>
          <h4>
            <label htmlFor="nav-toggel" className="hamburgerIcon">
              <i className="fa fa-bars" aria-hidden="true"></i>
            </label>
            <span className="name">Subjects</span>
          </h4>

          {/* <div className="search-wrapper">
            <span>
              <i className="fa fa-search" aria-hidden="true"></i>
            </span>
            <input type="search" placeholder="Search here" />
          </div> */}

          <div className="user-wrapper" onClick={() => setMyProfile(true)}>
            <img src={profileImage} width="50px" height="50px" alt="" />
            <div>
              <h6>{userName}</h6>
              <small>{userRole}</small>
            </div>
          </div>
        </header>

        <main>
          <div className="filter-wrapper"></div>

          <div className="table-content tableGridUi mt-4">
            <table className="table table-hover">
              <thead>
                <tr>
                  <th scope="col">Subjects</th>
                </tr>
              </thead>
              <tbody>{renderSubjects()}</tbody>
            </table>
          </div>
          <div className="mt-4">
          <button
            color="teal"
            className="btn btn-primary"
            onClick={() => setCreateSubjectPopUpFlag(true)}
          >
            Add Subject
          </button>
          </div>
          {createSubjectPopUpFlag ? (
            <CreateSubject
              createSubjectPopUpFlag={createSubjectPopUpFlag}
              setCreateSubjectPopUpFlag={setCreateSubjectPopUpFlag}
            />
          ) : (
            <div></div>
          )}
        </main>
      </div>
    );
  }
};
