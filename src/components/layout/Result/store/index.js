import { createSlice } from "@reduxjs/toolkit";

const resultSlice = createSlice({
  name: "resultReducer",
  initialState: { studentResult: [] }, //{ counter: 0, showCounter: false },
  reducers: {
    resultsData(state, action) {
      state.studentResult = action.payload.studentResult;
    },
  },
});

export const resultActions = resultSlice.actions;

export default resultSlice;
