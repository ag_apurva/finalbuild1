import { GET_RESULT } from "../../API";
import axios from "axios";
import { resultActions } from "./index";
export const loadResultForStudent = function (UserCode, UniCode) {
  return async function (dispatch) {
    dispatch(resultActions.resultsData({ studentResult: [] }));
    const config = {
      method: "get",
      url: GET_RESULT,
      headers: {},
      params: {
        universityCode: UniCode,
        studentUserCode: UserCode,
      },
    };
    try {
      let data = await axios(config);
      dispatch(resultActions.resultsData({ studentResult: data.data }));
    } catch (e) {}
  };
};
