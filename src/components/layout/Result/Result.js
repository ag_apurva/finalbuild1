/*eslint eqeqeq: "off"*/
import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import "react-datetime/css/react-datetime.css";
import { loadResultForStudent } from "./store/actions";
import { Formik, Field, ErrorMessage } from "formik";
import logo from "../image/attendance.png";
import { Form } from "formik-semantic-ui-react";
import { MyProfile } from "../Profile/Profile";
export const Result = function (props) {
  const userName = useSelector((state) => state.auth.myProfile.userName);
  const userRole = useSelector((state) => state.auth.myProfile.userRole);
  const [myProfile, setMyProfile] = useState(false);
  let studentsData = useSelector((state) => state.students.studentsData);
  let studentResult = useSelector((state) => state.results.studentResult);
  studentsData = studentsData.map((item) => {
    return { userCode: item.userCode, userName: item.userName };
  });
  let profileImage = useSelector((state) => state.auth.profileImage);
  if (profileImage == "") {
    profileImage = logo;
  }
  const dispatch = useDispatch();

  const renderResult = function () {
    return studentResult.map((item, index) => {
      let { examType, marks, subjectName, weightAge } = item;
      return (
        <tr key={index}>
          <td>{examType}</td>
          <td>{marks}</td>
          <td>{item.outOf}</td>
          <td>{subjectName}</td>
          <td>{item.relativeResult}</td>
          <td>{weightAge}</td>
        </tr>
      );
    });
  };
  const universityCode = useSelector((state) => state.auth.universityCode);
  if (myProfile) {
    return <MyProfile open={myProfile} onClose={() => setMyProfile(false)} />;
  } else {
    return (
      <div className="main-content bg-light">
        <header>
          <h4>
            <label htmlFor="nav-toggel" className="hamburgerIcon">
              <i className="fa fa-bars" aria-hidden="true"></i>
            </label>
            <span className="name">Result</span>
          </h4>

          <div className="user-wrapper" onClick={() => setMyProfile(true)}>
            <img src={profileImage} width="50px" height="50px" alt="" />
            <div>
              <h6>{userName}</h6>
              <small>{userRole}</small>
            </div>
          </div>
        </header>
        <main>
          <div className="filter-wrapper">
            <Formik
              initialValues={{
                studentCode: "",
              }}
              onSubmit={(formValues) => {
                dispatch(
                  loadResultForStudent(formValues.studentCode, universityCode)
                );
              }}
              validate={(formValues) => {
                let errors = {};
                if (!formValues.studentCode) {
                  errors.studentCode = "* Please choose a student Name";
                }
                return errors;
              }}
            >
              <Form className="resultsForm">
                <div className="resultsLabel">
                  <label>Stream</label>
                  <div className="streamField">
                  <Field type="text" name="studentCode">
                    {(props) => {
                      return (
                        <select className="form-control" {...props.field}>
                          <option value="">Select</option>
                          {studentsData.map((person) => (
                            <option
                              key={person.userCode}
                              value={person.userCode}
                            >
                              {person.userName}
                            </option>
                          ))}
                        </select>
                      );
                    }}
                  </Field>
                  <div className="errorMessage">
                    <ErrorMessage name="studentCode">
                      {(errorMsg) => {
                        return <p>{errorMsg}</p>;
                      }}
                    </ErrorMessage>
                  </div>
                  </div>
                </div>
                <div className="fil-btn">
                  <button
                    type="submit"
                    className="btn btn-primary mb-2"
                    style={{ backgroundColor: "#4cadad" }}
                  >
                    GO
                  </button>
                </div>
              </Form>
            </Formik>
          </div>
          <div className="table-content tableGridUi mt-4">
            <table className="table table-hover">
              <thead>
                <tr>
                  <th scope="col">ExamType</th>
                  <th scope="col">​​Marks</th>
                  <th scope="col">​​OutOf</th>
                  <th scope="col">​​Subject Name</th>
                  <th scope="col">Marks Relative</th>
                  <th scope="col">WeightAge</th>
                </tr>
              </thead>
              <tbody>{renderResult()}</tbody>
            </table>
          </div>
        </main>
      </div>
    );
  }
};
