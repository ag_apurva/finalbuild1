import { Modal } from "semantic-ui-react";
import React, { useState } from "react";
import { useSelector } from "react-redux";
import { DeleteProfessors } from "./DeleteProfessors";

import { EditProfessors } from "./EditProfessors";
import { Button } from "semantic-ui-react";
export const ProfessorsProfile = function (props) {
  const [editProfessor, setEditProfessor] = useState(true);
  const [deleteProfessor, setDeleteProfessor] = useState(false);
  const submitSuccessful = useSelector(
    (state) => state.professors.submitSuccessful
  );
  const errorCreateProfessor = useSelector(
    (state) => state.professors.errorCreateProfessor
  );
  let errorEditProfessor = useSelector(
    (state) => state.professors.errorEditProfessor
  );
  if (submitSuccessful) {
    props.onClose();
  }
  //const spinner = useSelector((state) => state.professors.spinner);

  return (
    <Modal open={props.open} onClose={props.onClose} dimmer={"inverted"} closeIcon>
      <Modal.Header>
        <Button
          className={deleteProfessor === true ? "item active" : "item"}
          onClick={() => {
            setEditProfessor(false);
            setDeleteProfessor(true);
          }}
        >
          Delete Professor
        </Button>
        <Button
          className={editProfessor === true ? "item active" : "item"}
          onClick={() => {
            setEditProfessor(true);
            setDeleteProfessor(false);
          }}
        >
          Edit Professor
        </Button>
        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
          <span>&times;</span>
        </button>
      </Modal.Header>
      <Modal.Content>
        {deleteProfessor === true ? <DeleteProfessors /> : <React.Fragment />}
        {editProfessor === true ? <EditProfessors /> : <React.Fragment />}
      </Modal.Content>
      <Modal.Actions>
        {errorCreateProfessor ? (
          <p>{errorCreateProfessor}</p>
        ) : (
          <React.Fragment />
        )}
        {errorEditProfessor ? <p>{errorEditProfessor}</p> : <React.Fragment />}
      </Modal.Actions>
    </Modal>
  );
};
