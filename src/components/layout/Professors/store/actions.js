/*eslint eqeqeq: "off"*/
import axios from "axios";
import { professorActions } from "./index";
import { Storage } from "aws-amplify";
import { GET_PROFESSORS, CREATE_PROFESSORS, DELETE_PROFESSOR } from "../../API";

export const getProfessors = function (code) {
  return async function (dispatch) {
    var config = {
      method: "get",
      url: GET_PROFESSORS,
      headers: {},
      params: {
        universityCode: code,
      },
    };

    try {
      let professorsData = await axios(config);
      //console.log("professorsData", professorsData);
      dispatch(
        professorActions.professorsData({ professorData: professorsData.data })
      );

      dispatch(downloadImagesForProfessors(professorsData.data, code));
    } catch (e) {
      console.log("error Loading Departments", e);
    }
  };
};

const checkImage = async (url) => {
  return new Promise((resolve, reject) => {
    let request = new XMLHttpRequest();
    request.open("GET", url, true);
    request.send();
    request.onload = function () {
      if (request.status == 200) {
        //if(statusText == OK)
        resolve();
      } else {
        reject();
      }
    };
  });
};

export const downloadImagesForProfessors = (professors, universityCode) => {
  return async (dispatch) => {
    try {
      let newprofessors = professors.map((item) => {
        let newStudentObject = { ...item };
        return newStudentObject;
      });

      for (let i = 0; i < newprofessors.length; i++) {
        newprofessors[i].userImage = "";
        const userCode = newprofessors[i].userCode;
        let path = universityCode + "/" + userCode + "/profileImage.jpg";

        try {
          const downloadedImage = await Storage.get(path.toString(), {
            contentType: "image/jpeg",
            expires: 2400,
          });

          await checkImage(downloadedImage);
          newprofessors[i].userImage = downloadedImage;
        } catch (e) {
          newprofessors[i].userImage = "";
        }
      }
      dispatch(
        professorActions.professorsData({ professorData: newprofessors })
      );
    } catch (e) {
      console.log("error loading professor image", e);
    }
  };
};

export const createProfessor = function (
  professorCreateObject,
  professorsDataArray,
  universityCode
) {
  return async function (dispatch) {
    dispatch(professorActions.setSpinner({ spinner: true }));
    try {
      const config = {
        method: "post",
        url: CREATE_PROFESSORS,
        headers: {
          "Content-Type": "application/json",
        },
        data: JSON.stringify(professorCreateObject),
      };
      const professorsData = await axios(config);

      console.log("professorsData", professorsData);
      if (!professorsData.data.hasOwnProperty("error")) {
        professorCreateObject.userCode = professorsData.data.data;
        //console.log("professorCreateObject", professorCreateObject);
        let newProfessorsData = [...professorsDataArray];
        newProfessorsData.push(professorCreateObject);

        dispatch(
          professorActions.professorsData({
            professorData: newProfessorsData,
          })
        );

        dispatch(
          professorActions.setSubmitSuccessfull({
            submitSuccessful: true,
          })
        );
        setTimeout(() => {
          dispatch(
            professorActions.setSubmitSuccessfull({
              submitSuccessful: false,
            })
          );
        }, 200);
      } else {
        dispatch(
          professorActions.setProfessorError({
            errorCreateProfessor: professorsData.data.error,
          })
        );
        setTimeout(() => {
          dispatch(
            professorActions.setProfessorError({
              errorCreateProfessor: "",
            })
          );
        }, 20000);
        setTimeout(() => {
          dispatch(
            professorActions.setSubmitSuccessfull({
              submitSuccessful: true,
            })
          );
        }, 20000);
        setTimeout(() => {
          dispatch(
            professorActions.setSubmitSuccessfull({
              submitSuccessful: false,
            })
          );
        }, 20200);
      }
    } catch (e) {
      console.log("error create professor", e);
      dispatch(
        professorActions.setProfessorError({
          errorCreateProfessor: JSON.stringify(e),
        })
      );
      setTimeout(() => {
        dispatch(
          professorActions.setProfessorError({
            errorCreateProfessor: "",
          })
        );
      }, 20000);
      setTimeout(() => {
        dispatch(
          professorActions.setSubmitSuccessfull({
            submitSuccessful: true,
          })
        );
      }, 20000);
      setTimeout(() => {
        dispatch(
          professorActions.setSubmitSuccessfull({
            submitSuccessful: false,
          })
        );
      }, 20200);
    }

    dispatch(professorActions.setSpinner({ spinner: false }));
  };
};

export const deleteProfessor = function (deleteProfessorObject, professorData) {
  return async function (dispatch) {
    try {
      var config = {
        method: "delete",
        url: DELETE_PROFESSOR,
        headers: {},
        params: {
          universityCode: deleteProfessorObject.universityCode,
          professorEmail: deleteProfessorObject.professorEmail,
        },
      };
      dispatch(professorActions.setSpinner({ spinner: true }));
      let deleteProfessor = await axios(config);
      //console.log("deleteProfessor", deleteProfessor);
      let tempProfessorData = professorData.filter((professor) => {
        return professor.userEmail != deleteProfessorObject.professorEmail;
      });
      // console.log("tempProfessorData", tempProfessorData);
      if (!deleteProfessor.data.hasOwnProperty("error")) {
        dispatch(
          professorActions.professorsData({
            professorData: tempProfessorData,
          })
        );
        dispatch(
          professorActions.setSubmitSuccessfull({
            submitSuccessful: true,
          })
        );
        setTimeout(() => {
          dispatch(
            professorActions.setSubmitSuccessfull({
              submitSuccessful: false,
            })
          );
        }, 200);
      } else {
        dispatch(
          professorActions.setProfessorError({
            errorCreateProfessor: deleteProfessor.data.error,
          })
        );
        setTimeout(() => {
          dispatch(
            professorActions.setProfessorError({
              errorCreateProfessor: "",
            })
          );
        }, 20000);
        setTimeout(() => {
          dispatch(
            professorActions.setSubmitSuccessfull({
              submitSuccessful: true,
            })
          );
        }, 20000);
        setTimeout(() => {
          dispatch(
            professorActions.setSubmitSuccessfull({
              submitSuccessful: false,
            })
          );
        }, 20200);
      }
    } catch (e) {
      dispatch(
        professorActions.setProfessorError({
          errorCreateProfessor: JSON.stringify(e),
        })
      );
      setTimeout(() => {
        dispatch(
          professorActions.setProfessorError({
            errorCreateProfessor: "",
          })
        );
      }, 20000);
      setTimeout(() => {
        dispatch(
          professorActions.setSubmitSuccessfull({
            submitSuccessful: true,
          })
        );
      }, 20000);
      setTimeout(() => {
        dispatch(
          professorActions.setSubmitSuccessfull({
            submitSuccessful: false,
          })
        );
      }, 20200);
    }

    dispatch(professorActions.setSpinner({ spinner: false }));
  };
};

export const editProfessorFunction = function (
  editProfessorObject,
  professorData
) {
  return async function (dispatch) {
    dispatch(professorActions.setSpinner({ spinner: true }));
    let tempProfessorData = professorData.filter((professor) => {
      return professor.userCode != editProfessorObject.userCode;
    });

    //
    const config = {
      method: "put",
      url:
        "https://l12w9l17oj.execute-api.us-east-2.amazonaws.com/build/professors/editprofessor",
      headers: {
        "Content-Type": "application/json",
      },
      data: JSON.stringify(editProfessorObject),
    };
    try {
      const professorsData = await axios(config);
      console.log("professorsData", professorsData);
      if (!professorsData.data.hasOwnProperty("error")) {
        tempProfessorData.push(editProfessorObject);

        dispatch(
          professorActions.professorsData({
            professorData: tempProfessorData,
          })
        );
        dispatch(
          professorActions.setSubmitSuccessfull({
            submitSuccessful: true,
          })
        );
        setTimeout(() => {
          dispatch(
            professorActions.setSubmitSuccessfull({
              submitSuccessful: false,
            })
          );
        }, 200);
      } else {
        dispatch(
          professorActions.setProfessorEditError({
            errorEditProfessor: professorsData.data.error,
          })
        );
        setTimeout(() => {
          dispatch(
            professorActions.setProfessorEditError({
              errorEditProfessor: "",
            })
          );
        }, 20000);
        setTimeout(() => {
          dispatch(
            professorActions.setSubmitSuccessfull({
              submitSuccessful: true,
            })
          );
        }, 20000);
        setTimeout(() => {
          dispatch(
            professorActions.setSubmitSuccessfull({
              submitSuccessful: false,
            })
          );
        }, 20200);
      }
    } catch (e) {
      console.log("error create professor", e);
      dispatch(
        professorActions.setProfessorEditError({
          errorEditProfessor: JSON.stringify(e),
        })
      );
      setTimeout(() => {
        dispatch(
          professorActions.setProfessorEditError({
            errorEditProfessor: "",
          })
        );
      }, 20000);
      setTimeout(() => {
        dispatch(
          professorActions.setSubmitSuccessfull({
            submitSuccessful: true,
          })
        );
      }, 20000);
      setTimeout(() => {
        dispatch(
          professorActions.setSubmitSuccessfull({
            submitSuccessful: false,
          })
        );
      }, 20200);
    }
    dispatch(professorActions.setSpinner({ spinner: false }));
  };
};
