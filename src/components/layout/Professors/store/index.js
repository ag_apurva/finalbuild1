import { createSlice } from "@reduxjs/toolkit";

const professorSlice = createSlice({
  name: "professorReducer",
  initialState: {
    professorData: [],
    errorCreateProfessor: "",
    submitSuccessful: false,
    spinner: false,
    editProfessor: {},
    errorEditProfessor: "",
  },
  reducers: {
    professorsData(state, action) {
      state.professorData = action.payload.professorData;
    },
    setEditProfessorObject(state, action) {
      state.editProfessor = action.payload.editProfessor;
    },
    setSpinner(state, action) {
      state.spinner = action.payload.spinner;
    },
    setProfessorError(state, action) {
      state.errorCreateProfessor = action.payload.errorCreateProfessor;
    },
    setSubmitSuccessfull(state, action) {
      state.submitSuccessful = action.payload.submitSuccessful;
    },
    setProfessorEditError(state, action) {
      state.errorEditProfessor = action.payload.errorEditProfessor;
    },
  },
});

export const professorActions = professorSlice.actions;

export default professorSlice;
