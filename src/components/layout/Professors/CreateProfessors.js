/*eslint eqeqeq: "off"*/
import React from "react";
import { Formik, Field, ErrorMessage } from "formik";
import { Modal, FormField } from "semantic-ui-react";
import { useSelector, useDispatch } from "react-redux";
import { Form } from "formik-semantic-ui-react";
import { createProfessor } from "./store/actions";
import "react-datetime/css/react-datetime.css";
import validator from "validator";
import "react-datetime/css/react-datetime.css";
import Datetime from "react-datetime";
import moment from "moment";
const TextError = function (props) {
  return <p style={{ color: "red" }}>{props.children}</p>;
};

export const CreateProfessors = function (props) {
  let professorData = useSelector((state) => state.professors.professorData);

  const submitSuccessful = useSelector(
    (state) => state.professors.submitSuccessful
  );
  //console.log("submitSuccessful", submitSuccessful);
  const spinner = useSelector((state) => state.professors.spinner);
  const errorCreateProfessor = useSelector(
    (state) => state.professors.errorCreateProfessor
  );
  const universityCode = useSelector((state) => state.auth.universityCode);
  if (submitSuccessful) {
    props.onClose(false);
  }
  const validate = (formValues) => {
    console.log("formValues", formValues);
    let error = {};
    if (!formValues.email) {
      error.email = "Please provide an Email";
    }
    if (!formValues.name) {
      error.name = "Please provide a name";
    }
    if (formValues.email && !validator.isEmail(formValues.email)) {
      error.email = "* Please provide a valid email";
    }

    if (formValues.phoneNumber) {
      let filter = /^[1-9]{1}[0-9]{2}[0-9]{7}$/;
      if (!filter.test(formValues.phoneNumber)) {
        error.phoneNumber = "*Please provide a 10 digit Phone Number";
      }
    }

    if (!formValues.phoneNumber) {
      error.phoneNumber = "Please provide a Phone Number";
    }
    if (!formValues.registrationNumber) {
      error.registrationNumber = "Please provide a registrationNumber";
    }

    if (!formValues.address) {
      error.address = "Please provide a Address";
    }
    if (!formValues.pincode) {
      error.pincode = "Please provide a Pincode";
    }
    if (formValues.pincode && formValues.pincode.length !== 6) {
      error.pincode = "* Please provide a 6 digit Phone Name";
    }

    if (!formValues.designation) {
      error.designation = "Please provide a Designation";
    }
    if (!formValues.bloodGroup) {
      error.bloodGroup = "Please provide a bloodGroup";
    }
    if (!formValues.userSecondaryName) {
      error.userSecondaryName = "Please provide a User Secondary Name";
    }
    if (
      formValues.userSecondaryNumber &&
      formValues.userSecondaryNumber.length !== 10
    ) {
      error.userSecondaryNumber = "* Please provide a 10 digit Phone Name";
    }
    if (formValues.userSecondaryNumber) {
      let filter = /^[1-9]{1}[0-9]{2}[0-9]{7}$/;
      if (!filter.test(formValues.userSecondaryNumber)) {
        error.userSecondaryNumber = "*Please provide a 10 digit Phone Number";
      }
    }
    if (formValues.userDOB) {
      var a = moment(formValues.userDOB);
      var b = moment(new Date());
      if (b.diff(a, "years") < 21) {
        error.userDOB = "Please provide a professor age of more than 20 years";
      }
      if (b.diff(a, "years") > 50) {
        error.userDOB = "Please provide a professor age of less than 50 years";
      }
    }
    if (!formValues.userSecondaryNumber) {
      error.userSecondaryNumber = "Please provide a User Secondary Number";
    }

    console.log("Error", error);
    return error;
  };
  const dispatch = useDispatch();

  const submitProfessor = async (formValues) => {
    //console.log("formValues", formValues);
    let professorCreateObject = {};
    professorCreateObject.userEmail = formValues.email;
    professorCreateObject.userName = formValues.name;
    professorCreateObject.universityCode = universityCode;
    professorCreateObject.userPhoneNumber = "+" + formValues.phoneNumber;
    professorCreateObject.userAddress = formValues.address;
    professorCreateObject.userPinCode = formValues.pincode;
    professorCreateObject.userRegistrationNumber =
      formValues.registrationNumber;
    professorCreateObject.userDesignation = formValues.designation;
    professorCreateObject.userDOB = moment(formValues.userDOB).format(
      "YYYY-MM-DD"
    );
    professorCreateObject.userSecondaryName = formValues.userSecondaryName;

    professorCreateObject.bloodGroup = formValues.bloodGroup;
    professorCreateObject.userImage = "";
    professorCreateObject.userSecondaryNumber =
      "+" + formValues.userSecondaryNumber;
    console.log("professorCreateObject", professorCreateObject);

    dispatch(
      createProfessor(professorCreateObject, professorData, universityCode)
    );
  };

  return (
    <Modal
      open={props.open}
      onClose={props.onClose}
      dimmer={"inverted"}
      closeIcon
      style={{ overflow: "auto", maxHeight: 600 }}
    >
      <Modal.Header>
        <h3>Create Professor</h3>
        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
          <span>&times;</span>
        </button>
        </Modal.Header>
      {spinner ? (
        <Modal.Content>
          <div className="ui active inverted dimmer">
            <div className="ui loader"></div>
          </div>
        </Modal.Content>
      ) : (
        <Modal.Content>
          <Formik
            initialValues={{
              email: "",
              name: "",
              phoneNumber: "",
              registrationNumber: "",
              address: "",
              pincode: "",
              designation: "",
              userDOB: new Date(),
              userSecondaryName: "",
              userSecondaryNumber: "",
              bloodGroup: "",
            }}
            validate={validate}
            onSubmit={submitProfessor}
          >
            <Form>
            <div className="row modelFormUi">
              <FormField className="col-4">
                <label htmlFor="nameLabel">Professor Name</label>
                <Field id="nameLabel" type="text" name="name" />
                <div className="errorMessage">
                <ErrorMessage name="name" component={TextError} />
                </div>
              </FormField>
              <FormField className="col-4">
                <label htmlFor="emailLabel">Professor Email</label>
                <Field id="emailLabel" type="text" name="email" />
                <div className="errorMessage">
                <ErrorMessage name="email" component={TextError} />
                </div>
              </FormField>
              <FormField className="col-4">
                <label htmlFor="rollNumberLabel">Phone Number</label>
                <Field id="rollNumberLabel" type="text" name="phoneNumber" />
                <div className="errorMessage">
                <ErrorMessage name="phoneNumber" component={TextError} />
                </div>
              </FormField>
              <FormField className="col-4">
                <label htmlFor="designationLabel">Designation</label>
                <Field id="designationLabel" type="text" name="designation" />
                <div className="errorMessage">
                <ErrorMessage name="designation" component={TextError} />
                </div>
              </FormField>
              <FormField className="col-4">
                <label htmlFor="emailLabel">Registration Number</label>
                <Field id="emailLabel" type="text" name="registrationNumber" />
                <div className="errorMessage">
                <ErrorMessage name="registrationNumber" component={TextError} />
                </div>
              </FormField>
              <FormField className="col-4">
                <label htmlFor="emailLabel">Address</label>
                <Field id="emailLabel" type="text" name="address" />
                <div className="errorMessage">
                <ErrorMessage name="address" component={TextError} />
                </div>
              </FormField>
              <FormField className="col-4">
                <label htmlFor="emailLabel">Pincode</label>
                <Field id="emailLabel" type="text" name="pincode" />
                <div className="errorMessage">
                <ErrorMessage name="pincode" component={TextError} />
                </div>
              </FormField>
              <FormField className="col-4">
                <label htmlFor="userDOBLabel">Date Of Birth</label>
                <Field id="userDOBLabel" type="text" name="userDOB">
                  {(props) => {
                    const { setFieldValue } = props.form;
                    return (
                      <Datetime
                        {...props.field}
                        timeFormat={false}
                        dateFormat="YYYY-MM-DD"
                        onChange={(event) => {
                          setFieldValue(props.field.name, event._d);
                        }}
                      />
                    );
                  }}
                </Field>
                <div className="errorMessage">
                <ErrorMessage name="userDOB" component={TextError} />
                </div>
              </FormField>
              <FormField className="col-4">
                <label htmlFor="emailLabel">Secondary Contact Name</label>
                <Field id="emailLabel" type="text" name="userSecondaryName" />
                <div className="errorMessage">
                <ErrorMessage name="userSecondaryName" component={TextError} />
                </div>
              </FormField>
              <FormField className="col-4">
                <label htmlFor="emailLabel">Secondary Contact Number</label>
                <Field id="emailLabel" type="text" name="userSecondaryNumber" />
                <div className="errorMessage">
                <ErrorMessage
                  name="userSecondaryNumber"
                  component={TextError}
                />
                </div>
              </FormField>
              <FormField className="col-4">
                <label htmlFor="bloodGroupLabel">Blood Group</label>
                <Field id="bloodGroupLabel" type="text" name="bloodGroup" />
                <div className="errorMessage">
                <ErrorMessage name="bloodGroup" component={TextError} />
                </div>
              </FormField>
              <div className="col-12 mt-4">
              <button type="submit" className="btn btn-primary">Submit</button>
              </div>
              </div>
            </Form>
          </Formik>
        </Modal.Content>
      )}
      <Modal.Actions>
        {errorCreateProfessor ? (
          <p>{errorCreateProfessor}</p>
        ) : (
          <React.Fragment />
        )}
      </Modal.Actions>
    </Modal>
  );
};

/*
    
    
     
    
    
    
    userDOB
    
    
    
    
    ")
{\"\":\"checkprof2@gmail.com\",\"\":\"checkprof2\",
\"\":\"+1122334455\",\"\":\"checkprof2D\",
\"\":\"checkprof2R\",\"\":\"checkprof2\",
\"\":\"123456\",\"\":\"TR036\",\"\":\"B-ve\",
\"\":\"checkprof2\",\"\":\"+1111122222\"}


*/
