/*eslint eqeqeq: "off"*/
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import "../css/student.css";
import logo from "../image/attendance.png";
import { ProfessorsProfile } from "./ProfessorsProfile";
import { CreateProfessors } from "./CreateProfessors";
import { MyProfile } from "../Profile/Profile";
import { professorActions } from "./store/index";
import { getProfessors } from "./store/actions";
export const Professors = function () {
  const userName = useSelector((state) => state.auth.myProfile.userName);
  const userRole = useSelector((state) => state.auth.myProfile.userRole);
  const [myProfile, setMyProfile] = useState(false);
  const [editProfessorPopUp, editProfessorsAddPopUp] = useState(false);
  const [emailSelected, setEmail] = useState("");
  const [createProfessor, setCreateProfessor] = useState(false);
  let profileImage = useSelector((state) => state.auth.profileImage);
  if (profileImage == "") {
    profileImage = logo;
  }
  const universityCode = useSelector((state) => state.auth.universityCode);
  const spinner = useSelector((state) => state.professors.spinner);
  console.log("spinner", spinner);
  const professorData = useSelector((state) => state.professors.professorData);
  const dispatch = useDispatch();
  const renderProfessors = () => {
    return professorData.map((item, index) => {
      let { userEmail, userName, userPhone, userImage } = item;

      if (userImage == "") {
        userImage = logo;
      }
      console.log("item", item);
      return (
        <tr key={index}>
          {editProfessorPopUp && emailSelected == userEmail ? (
            <ProfessorsProfile
              open={editProfessorPopUp}
              onClose={() => editProfessorsAddPopUp(false)}
            />
          ) : (
            <React.Fragment />
          )}
          <td>
            <span>
              <img src={userImage} width="50px" height="50px" alt="" />
            </span>
          </td>
          <td>{userName}</td>
          <td>
            <span>{userEmail}</span>
          </td>
          <td>{userPhone}</td>
          <td className="action-cell">
            <button
              onClick={() => {
                editProfessorsAddPopUp(true);
                setEmail(userEmail);
                dispatch(
                  professorActions.setEditProfessorObject({
                    editProfessor: item,
                  })
                );
              }}
            >
              <i className="fa fa-edit" aria-hidden="true"></i>
            </button>
            <button
              onClick={() => {
                editProfessorsAddPopUp(true);
                setEmail(userEmail);
                dispatch(
                  professorActions.setEditProfessorObject({
                    editProfessor: item,
                  })
                );
              }}
            >
              <i className="fa fa-trash" aria-hidden="true"></i>
            </button>
          </td>
        </tr>
      );
    });
  };
  if (myProfile) {
    return <MyProfile open={myProfile} onClose={() => setMyProfile(false)} />;
  } else if (createProfessor) {
    return (
      <CreateProfessors
        open={createProfessor}
        onClose={() => setCreateProfessor(false)}
      />
    );
  } else {
    return (
      <div className="main-content bg-light">
        <header>
          <h4>
            <label htmlFor="nav-toggel" className="hamburgerIcon">
              <i className="fa fa-bars" aria-hidden="true"></i>
            </label>
            <span className="name">Professors</span>
          </h4>

          <div className="user-wrapper" onClick={() => setMyProfile(true)}>
            <img src={profileImage} width="50px" height="50px" alt="" />
            <div>
              <h6>{userName}</h6>
              <small>{userRole}</small>
            </div>
          </div>
        </header>
        <main>
          <div className="table-content tableGridUi mb-3">
            <table className="table table-hover">
              <thead>
                <tr>
                  <th scope="col">Profile Picture</th>
                  <th scope="col">Name</th>
                  <th scope="col">UserEmail</th>
                  <th scope="col">UserContact</th>
                  <th className="text-center">Action</th>
                </tr>
              </thead>
              <tbody>{renderProfessors()}</tbody>
            </table>
          </div>
          <div className="mt-4">
            <button
              className="btn btn-primary"
              onClick={() => setCreateProfessor(true)}
            >
              Add Professor
            </button>
            <button
              className="btn btn-primary ml-2"
              onClick={() => dispatch(getProfessors(universityCode))}
            >
              Refresh
            </button>
          </div>
        </main>
      </div>
    );
  }
};
