import React from "react";
import { Formik, Field, ErrorMessage } from "formik";
import { FormField } from "semantic-ui-react";
import { useSelector, useDispatch } from "react-redux";
import { Form } from "formik-semantic-ui-react";
import "react-datetime/css/react-datetime.css";
import moment from "moment";
import { editProfessorFunction } from "./store/actions";
import "../css/student.css";
import "react-datetime/css/react-datetime.css";
import Datetime from "react-datetime";
const TextError = function (props) {
  return <p style={{ color: "red" }}>{props.children}</p>;
};

/*
the form to edit the students profile.
this component also gets passed the univeristyid(UniCode) of the admin
so that whenever any students data gets changed its calls the 
student data query and reload student data.
*/
export const EditProfessors = function () {
  const editProfessor = useSelector((state) => state.professors.editProfessor);
  // console.log("edit Professor", editProfessor);

  const professorData = useSelector((state) => state.professors.professorData);
  const dispatch = useDispatch();
  const spinner = useSelector((state) => state.students.spinner);
  const validate = (formValues) => {
    let error = {};
    if (!formValues.userName) {
      error.userName = "* Please provide a full Name";
    }
    if (!formValues.userPhoneNumber) {
      error.userPhoneNumber = "Please provide a Phone Name";
    }
    if (
      formValues.userPhoneNumber &&
      formValues.userPhoneNumber.length !== 10
    ) {
      error.userPhoneNumber = "* Please provide a 10 digit Phone Name";
    }

    if (!formValues.userParentNumber) {
      error.userParentNumber = "Please provide a Parent Phone Name";
    }
    if (
      formValues.userParentNumber &&
      formValues.userParentNumber.length !== 10
    ) {
      error.userParentNumber = "* Please provide a 10 digit Phone Name";
    }
    if (!formValues.userParentName) {
      error.userParentName = "Please provide a Parent Name";
    }
    if (!formValues.userRegistrationNumber) {
      error.userRegistrationNumber = "Please provide a Registration Number";
    }

    if (!formValues.userDOB) {
      error.userDOB = "Please provide a userDOB";
    }
    if (formValues.userDOB) {
      var a = moment(formValues.userDOB);
      var b = moment(new Date());
      if (b.diff(a, "years") < 16) {
        error.userDOB = "Please provide a student age of more than 15 years";
      }
      if (b.diff(a, "years") > 50) {
        error.userdob = "Please provide a student age of less than 50 years";
      }
    }

    if (
      formValues.userParentNumber &&
      formValues.userParentNumber.length !== 10
    ) {
      error.userParentNumber = "* Please provide a 10 digit Parent  Phone Name";
    }
    if (!formValues.userAddress) {
      error.userAddress = "Please provide a Address";
    }
    if (!formValues.userPinCode) {
      error.userPinCode = "Please provide a userPinCode";
    }
    if (!formValues.bloodGroup) {
      error.bloodGroup = "Please provide a bloodGroup";
    }
    //  console.log("error", error);
    return error;
  };
  const universityCode = useSelector((state) => state.auth.universityCode);
  /*
address: "checkprof1"
​bloodGroup: "B-ve"
​designation: "checkprof1"
​pinCode: "123456"
​userCode: "USR059"
​userEmail: "checkprof1@gmail.com"
​userImage: ""
​userName: "checkprof1"
​userPhone: "+1122334455"
​userRegistrationNo: "checkprof1"
​userSecondaryName: "checkprof1"
​userSecondaryNumber: "+1122334455"
*/
  const onSubmit = function (formValues) {
    // console.log("formValues", formValues);
    let professorEditObject = {};
    professorEditObject.userAddress = formValues.userAddress;
    professorEditObject.bloodGroup = formValues.bloodGroup;
    professorEditObject.userDesignation = formValues.designation;
    professorEditObject.userPinCode = formValues.userPinCode;
    professorEditObject.userEmail = formValues.userEmail;
    professorEditObject.userImage = "";
    professorEditObject.userName = formValues.userName;
    professorEditObject.userPhoneNumber = "+" + formValues.userPhoneNumber;

    professorEditObject.userSecondaryName = formValues.userParentName;
    professorEditObject.userSecondaryNumber = "+" + formValues.userParentNumber;

    professorEditObject.userRegistrationNumber =
      formValues.userRegistrationNumber;

    professorEditObject.userCode = editProfessor.userCode;
    professorEditObject.universityCode = universityCode;
    professorEditObject.userDOB = moment(formValues.userDOB).format(
      "YYYY-MM-DD"
    );
    //  console.log("editProfessor", editProfessor);
    // console.log("professorEditObject", professorEditObject);
    dispatch(editProfessorFunction(professorEditObject, professorData));
  };

  // console.log("spinner", spinner);
  if (spinner) {
    return (
      <React.Fragment>
        <div className="ui active inverted dimmer">
          <div className="ui loader"></div>
        </div>
      </React.Fragment>
    );
  } else {
    //console.log("editProfessor", editProfessor);
    return (
      <React.Fragment>
        <p>EditStudent</p>
        <Formik
          initialValues={{
            userEmail: editProfessor.userEmail,
            userName: editProfessor.userName,
            designation: editProfessor.userDesignation,
            userRegistrationNumber: editProfessor.userRegistrationNumber,
            userPhoneNumber: editProfessor.userPhoneNumber
              ? editProfessor.userPhoneNumber.toString().slice(1, 11)
              : "",
            userDOB: new Date(editProfessor.userDOB),
            userAddress: editProfessor.userAddress,
            userPinCode: editProfessor.userPinCode,
            bloodGroup: editProfessor.bloodGroup,
            userParentName: editProfessor.userSecondaryName,
            userParentNumber: editProfessor.userSecondaryNumber
              ? editProfessor.userSecondaryNumber.toString().slice(1, 11)
              : "",
          }}
          validate={validate}
          onSubmit={onSubmit}
        >
          <Form>
          <div className="row modelFormUi">
            <FormField className="col-4">
              <label htmlFor="nameLabel">Student Name</label>
              <Field id="nameLabel" type="text" name="userName" />
              <div className="errorMessage">
              <ErrorMessage name="userName" component={TextError} />
              </div>
            </FormField>
            <FormField className="col-4">
              <label htmlFor="emailLabel">Student Email</label>
              <Field
                id="emailLabel"
                type="text"
                name="userEmail"
                disabled={true}
              />
              <div className="errorMessage">
              <ErrorMessage name="userEmail" component={TextError} />
              </div>
            </FormField>
            <FormField className="col-4">
              <label htmlFor="nameLabel">Date of Birth</label>
              <Field id="emailLabel" type="text" name="userDOB">
                {(props) => {
                  const { setFieldValue } = props.form;
                  return (
                    <Datetime
                      {...props.field}
                      timeFormat={false}
                      dateFormat="YYYY-MM-DD"
                      onChange={(event) => {
                        setFieldValue(props.field.name, event._d);
                      }}
                    />
                  );
                }}
              </Field>
              <div className="errorMessage">
              <ErrorMessage name="userDOB" component={TextError} />
              </div>
            </FormField>
            <FormField className="col-4">
              <label htmlFor="emailLabel">Phone Number</label>
              <Field id="emailLabel" type="text" name="userPhoneNumber" />
              <div className="errorMessage">
              <ErrorMessage name="userPhoneNumber" component={TextError} />
              </div>
            </FormField>
            <FormField className="col-4">
              <label htmlFor="emailLabel">Registration Number</label>
              <Field
                id="emailLabel"
                type="text"
                name="userRegistrationNumber"
              />
              <div className="errorMessage">
              <ErrorMessage
                name="userRegistrationNumber"
                component={TextError}
              />
              </div>
            </FormField>
            <FormField className="col-4">
              <label htmlFor="designationLabel">designation</label>
              <Field id="designationLabel" type="text" name="designation" />
              <div className="errorMessage">
              <ErrorMessage name="designation" component={TextError} />
              </div>
            </FormField>
            <FormField className="col-4">
              <label htmlFor="userAddressLabel">Address</label>
              <Field id="userAddressLabel" type="text" name="userAddress" />
              <div className="errorMessage">
              <ErrorMessage name="userAddress" component={TextError} />
              </div>
            </FormField>
            <FormField className="col-4">
              <label htmlFor="emailLabel">userPinCode</label>
              <Field id="emailLabel" type="text" name="userPinCode" />
              <div className="errorMessage">
              <ErrorMessage name="userPinCode" component={TextError} />
              </div>
            </FormField>
            <FormField className="col-4">
              <label htmlFor="emailLabel">Parent Name</label>
              <Field id="emailLabel" type="text" name="userParentName" />
              <div className="errorMessage">
              <ErrorMessage name="userParentName" component={TextError} />
              </div>
            </FormField>
            <FormField className="col-4">
              <label htmlFor="emailLabel">Parent Phone Number</label>
              <Field id="emailLabel" type="text" name="userParentNumber" />
              <div className="errorMessage">
              <ErrorMessage name="userParentNumber" component={TextError} />
              </div>
            </FormField>
            <FormField className="col-4">
              <label htmlFor="bloodGroupLabel">Blood Group</label>
              <Field id="bloodGroupLabel" type="text" name="bloodGroup" />
              <div className="errorMessage">
              <ErrorMessage name="bloodGroup" component={TextError} />
              </div>
            </FormField>
            <div className="col-12 mt-4">
            <button type="submit" className="btn btn-primary">Submit</button>
            </div>
            </div>
          </Form>
        </Formik>
      </React.Fragment>
    );
  }
};

/*
bloodGroup: null
​division: "A"
​idClass: 30
​phoneNumber: "+9407259076"
​semester: 1
​subYear: 1
​userAddress: "1"
​userCode: "USR090"
​userDOB: "2021-08-31T00:00:00.000Z"
​userEmail: "check8@gmail.com"
​userName: "check8"
​userParentContactNo: "+1234567891"
​userParentName:"Parent"
​userPhone: "+9407259076"
​userPinCod "123456"
​userRegistrationNo: "1"
​userRollNo: 12345
​"​userAddress": "1"
​"​userDOB": "2021-08-31"
​"​userParentContactNo": "+1234567891"
​"​userParentName": "Parent"
​"​useruserPinCode": "123456"

    
  
address: "checkprof1"
​bloodGroup: "B-ve"
​designation: "checkprof1"
​pinCode: "123456"
​userCode: "USR059"
​userEmail: "checkprof1@gmail.com"
​userImage: ""
​userName: "checkprof1"
​userPhone: "+1122334455"
​userRegistrationNo: "checkprof1"
​userSecondaryName: "checkprof1"
​userSecondaryNumber: "+1122334455"


    
*/
