import React from "react";
import { useDispatch, useSelector } from "react-redux";

import { deleteProfessor } from "./store/actions";
export const DeleteProfessors = function () {
  const editProfessor = useSelector((state) => state.professors.editProfessor);
  const spinner = useSelector((state) => state.professors.spinner);
  console.log("spinner", spinner);
  const universityCode = useSelector((state) => state.auth.universityCode);
  const dispatch = useDispatch();
  const professorData = useSelector((state) => state.professors.professorData);

  if (spinner) {
    return (
      <React.Fragment>
        <div className="ui active inverted dimmer">
          <div className="ui loader"></div>
        </div>
      </React.Fragment>
    );
  } else {
    return (
      <React.Fragment>
        <div className="deleteBlock">
        <p>Delete Professors</p>
        <button
          className="btn btn-primary mb-2"
          onClick={() =>
            dispatch(
              deleteProfessor(
                {
                  professorEmail: editProfessor.userEmail,
                  universityCode: universityCode,
                },
                professorData
              )
            )
          }
        >
          Delete
        </button>
        </div>
      </React.Fragment>
    );
  }
};
