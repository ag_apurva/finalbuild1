/*eslint eqeqeq: "off"*/
import React, { useState } from "react";
import { Formik, Field, ErrorMessage } from "formik";
import { Modal, Button, FormField } from "semantic-ui-react";
import { useSelector, useDispatch } from "react-redux";
import { Form } from "formik-semantic-ui-react";
import "react-datetime/css/react-datetime.css";
import Datetime from "react-datetime";
import moment from "moment";
import { createSchedule } from "./store/actions";

const TextError = function (props) {
  return <p style={{ color: "red" }}>{props.children}</p>;
};
export const CreateSchedule = function (props) {
  const scheduleData = useSelector((state) => state.schedule.scheduleData);
  const [disableDepartmentDropDown, setDepartmentDropDown] = useState(true);
  const [disableSemisterDropDown, setSemisterDropDown] = useState(true);
  const [disableDivisionDropDown, setDivisionDropDown] = useState(true);
  const [admissionCodeSelected, setAdmissionCode] = useState("");
  const [semesterSelected, setSemester] = useState("");
  const [classId, setClassId] = useState("");
  const admissionsData = useSelector((state) => state.batch.admissionsData);
  const [deptDataRelativeAdmYear, setDeptDataRelativeAdmYear] = useState([]);
  const [
    semesterDataRelativeAdmissionCode,
    setSemesterDataRelativeAdmissionCode,
  ] = useState([]);
  const [
    divisionsRelativeSemesterAdmCode,
    setDivisionsRelativeSemesterAdmCode,
  ] = useState([]);
  let admissionsYear = admissionsData.map((item) => item.admissionYear);
  admissionsYear = [...new Set(admissionsYear)];
  const classesData = useSelector((state) => state.class.classData);
  const [
    classDataRelativeAdmissionCode,
    setClassDataRelativeAdmissionCode,
  ] = useState([]);
  const submitSuccessful = useSelector(
    (state) => state.schedule.submitSuccessful
  );

  if (submitSuccessful) {
    props.onClose();
  }
  const professorData = useSelector((state) => state.professors.professorData);
  const locationsData = useSelector((state) => state.locations.locationsData);
  const subjectsData = useSelector((state) => state.subjects.subjectsData);

  const validate = function (formValues) {
    const errors = {};
    if (!formValues.admissionYear) {
      errors.admissionYear = "* Please select a admission Year";
    }
    if (!formValues.departmentCode) {
      errors.departmentCode = "* Please select a Department Code";
    }
    if (!formValues.semister) {
      errors.semister = "* Please select a Semister";
    }
    if (!formValues.division) {
      errors.division = "* Please select a Division";
    }
    if (!formValues.location) {
      errors.location = "* Please select a Location";
    }
    if (!formValues.subject) {
      errors.subject = "* Please select a Subject Name";
    }
    if (!formValues.professor) {
      errors.professor = "* Please select a Professor Name";
    }
    if (
      formValues.startDate &&
      formValues.endDate &&
      moment(formValues.startDate) > moment(formValues.endDate)
    ) {
      errors.endDate = "Please select endDate greater than startDate";
    }

    if (
      formValues.startTime &&
      formValues.endTime &&
      moment(formValues.startTime) >= moment(formValues.endTime)
    ) {
      errors.endTime = "Please select endTime greater than startTime";
    }
    return errors;
  };
  const dispatch = useDispatch();

  const admissionsYearChange = function (e) {
    if (e.target.value !== "") {
      let filteradmissionData = admissionsData.filter(
        (item) => item.admissionYear == e.target.value
      );
      if (filteradmissionData.length > 0) {
        setDeptDataRelativeAdmYear(filteradmissionData);
        setDepartmentDropDown(false);
      }
    }
  };

  const departmentCodeChange = function (e) {
    if (e.target.value !== "") {
      let admissionCodeSelected = deptDataRelativeAdmYear.find(
        (item) => item.departmentCode === e.target.value
      );
      if (admissionCodeSelected) {
        setAdmissionCode(admissionCodeSelected.admissionCode);
        let tempClassesData = classesData.filter(
          (classItem) =>
            classItem.admissionCode == admissionCodeSelected.admissionCode
        );
        if (tempClassesData.length > 0) {
          setClassDataRelativeAdmissionCode(tempClassesData);
          let tempSemester = tempClassesData.map((item) => item.semester);
          tempSemester = [...new Set(tempSemester)];
          setSemesterDataRelativeAdmissionCode(tempSemester);
          setSemisterDropDown(false);
        }
      }
    }
  };

  const semesterChange = function (e) {
    if (e.target.value != "") {
      setSemester(e.target.value);
      let tempDivisionData = classDataRelativeAdmissionCode.filter((item) => {
        return (
          item.admissionCode == admissionCodeSelected &&
          item.semester == e.target.value
        );
      });
      if (tempDivisionData.length > 0) {
        let divisions = tempDivisionData.map((item) => item.division);
        divisions = [...new Set(divisions)];
        setDivisionsRelativeSemesterAdmCode(divisions);
        setDivisionDropDown(false);
      }
    }
  };

  const divisionChange = function (e) {
    if (e.target.value != "") {
      let tempClassId = classDataRelativeAdmissionCode.find((item) => {
        return (
          item.admissionCode == admissionCodeSelected &&
          item.semester == semesterSelected &&
          item.division == e.target.value
        );
      });

      if (tempClassId) {
        setClassId(tempClassId["idClass"]);
      }
      //console.log("classId", tempClassId["idClass"]);
      // this is the class id
    }
  };
  const universityCode = useSelector((state) => state.auth.universityCode);
  const idClassSelected = useSelector((state) => state.schedule.idClass);
  const onSubmit = async (formValues, argument1) => {
    let subject = subjectsData.find(
      (subject) => subject.subjectCode == formValues.subject
    );
    let professor = professorData.find(
      (professor) => professor.userCode == formValues.professor
    );
    let obj = {};
    obj["subjectCode"] = subject.subjectCode;
    obj["subjectName"] = subject.subjectName;
    obj["professorName"] = professor.userName;
    obj["professorCode"] = professor.userCode;
    obj["idClass"] = classId;

    obj["location"] = formValues.location;
    obj["startTimeBoundaryCheck"] = moment(formValues.startTime)
      .add(1, "minute")
      .format("HH:mm");
    obj["endTimeBoundaryCheck"] = moment(formValues.endTime)
      .subtract(1, "minute")
      .format("HH:mm");

    obj["startDate"] = moment(formValues.startDate).format("YYYY-MM-DD");
    obj["startTime"] = moment(formValues.startTime).format("HH:mm");
    obj["endDate"] = moment(formValues.endDate).format("YYYY-MM-DD");
    obj["endTime"] = moment(formValues.endTime).format("HH:mm");
    obj["universityCode"] = universityCode;
    let classSelected = false;
    if (classId == idClassSelected) {
      classSelected = true;
    }
    dispatch(createSchedule(obj, scheduleData, classSelected));
    //console.log("obj", obj);
  };
  const spinner = useSelector((state) => state.schedule.spinner);
  const errorCreateSchedule = useSelector(
    (state) => state.schedule.errorCreateSchedule
  );

  return (
    <Modal
      open={props.open}
      onClose={props.onClose}
      dimmer={"inverted"}
      closeIcon
      style={{ overflow: "auto", maxHeight: 600 }}
    >
      <Modal.Header>
        <h3>Create Schedule</h3>
        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
          <span>&times;</span>
        </button>
      </Modal.Header>
      {spinner ? (
        <Modal.Content>
          <div className="ui active inverted dimmer">
            <div className="ui loader"></div>
          </div>
        </Modal.Content>
      ) : (
        <Modal.Content>
          <Formik
            initialValues={{
              admissionYear: "",
              departmentCode: "",
              semister: "",
              division: "",
              location: "",
              subject: "",
              professor: "",
              startDate: new Date(),
              endDate: new Date(),
              startTime: moment(new Date()),
              endTime: moment(new Date()),
            }}
            validate={validate}
            onSubmit={onSubmit}
          >
            <Form>
              <div className="row modelFormUi">
              <FormField className="col-3">
                <label htmlFor="admissionYearLabel">Year</label>
                <Field id="admissionYearLabel" type="text" name="admissionYear">
                  {(props) => {
                    return (
                      <select
                        className="form-control"
                        name={props.field.name}
                        onBlur={props.field.onBlur}
                        onChange={(e) => {
                          admissionsYearChange(e);
                          props.field.onChange(e);
                        }}
                      >
                        <option value="">Select</option>
                        {admissionsYear.map((year, index) => {
                          return (
                            <option key={index} value={year}>
                              {year}
                            </option>
                          );
                        })}
                      </select>
                    );
                  }}
                </Field>
                <div className="errorMessage">
                  <ErrorMessage name="admissionYear" component={TextError} />
                </div>
              </FormField>
              <FormField className="col-3">
                <label htmlFor="departmentLabel">Department Name</label>
                <Field id="departmentLabel" type="text" name="departmentCode">
                  {(props) => {
                    return (
                      <select
                        className="form-control"
                        name={props.field.name}
                        onBlur={props.field.onBlur}
                        onChange={(e) => {
                          departmentCodeChange(e);
                          props.field.onChange(e);
                        }}
                        disabled={disableDepartmentDropDown}
                      >
                        <option value="">Select</option>
                        {deptDataRelativeAdmYear.map((item, index) => {
                          return (
                            <option key={index} value={item.departmentCode}>
                              {item.departmentName}
                            </option>
                          );
                        })}
                      </select>
                    );
                  }}
                </Field>
                <div className="errorMessage">
                  <ErrorMessage name="departmentCode" component={TextError} />
                </div>
              </FormField>
              <FormField className="col-3">
                <label htmlFor="semisterLabel">Semister</label>
                <Field id="semisterLabel" type="text" name="semister">
                  {(props) => {
                    return (
                      <select
                        className="form-control"
                        name={props.field.name}
                        onBlur={props.field.onBlur}
                        onChange={(e) => {
                          semesterChange(e);
                          props.field.onChange(e);
                        }}
                        disabled={disableSemisterDropDown}
                      >
                        <option value="">Select</option>
                        {semesterDataRelativeAdmissionCode.map(
                          (item, index) => {
                            return (
                              <option key={index} value={item}>
                                {item}
                              </option>
                            );
                          }
                        )}
                      </select>
                    );
                  }}
                </Field>
                <div className="errorMessage">
                  <ErrorMessage name="semister" component={TextError} />
                </div>
              </FormField>
              <FormField className="col-3">
                <label htmlFor="semisterLabel">Division</label>
                <Field id="semisterLabel" type="text" name="division">
                  {(props) => {
                    return (
                      <select
                        className="form-control"
                        name={props.field.name}
                        onBlur={props.field.onBlur}
                        onChange={(e) => {
                          divisionChange(e);
                          props.field.onChange(e);
                        }}
                        disabled={disableDivisionDropDown}
                      >
                        <option value="">Select</option>
                        {divisionsRelativeSemesterAdmCode.map((item, index) => {
                          return (
                            <option key={index} value={item}>
                              {item}
                            </option>
                          );
                        })}
                      </select>
                    );
                  }}
                </Field>
                <div className="errorMessage">
                  <ErrorMessage name="division" component={TextError} />
                </div>
              </FormField>
              <FormField className="col-12">
                <label htmlFor="semisterLabel">Location</label>
                <Field id="semisterLabel" type="text" name="location">
                  {(props) => {
                    return (
                      <select {...props.field}>
                        <option value="">Select</option>
                        {locationsData.map((item, index) => {
                          return (
                            <option key={index} value={item.classRoomName}>
                              {item.classRoomName}
                            </option>
                          );
                        })}
                      </select>
                    );
                  }}
                </Field>
                <div className="errorMessage">
                  <ErrorMessage name="location" component={TextError} />
                </div>
              </FormField>
              <FormField className="col-6">
                <label htmlFor="semisterLabel">Subject</label>
                <Field id="semisterLabel" type="text" name="subject">
                  {(props) => {
                    return (
                      <select {...props.field}>
                        <option value="">Select</option>
                        {subjectsData.map((item, index) => {
                          return (
                            <option key={index} value={item.subjectCode}>
                              {item.subjectName}
                            </option>
                          );
                        })}
                      </select>
                    );
                  }}
                </Field>
                <div className="errorMessage">
                  <ErrorMessage name="subject" component={TextError} />
                </div>
              </FormField>
              <FormField className="col-6">
                <label htmlFor="professorLabel">Professor</label>
                <Field id="professorLabel" type="text" name="professor">
                  {(props) => {
                    return (
                      <select {...props.field}>
                        <option value="">Select</option>
                        {professorData.map((item, index) => {
                          return (
                            <option key={index} value={item.userCode}>
                              {item.userName}
                            </option>
                          );
                        })}
                      </select>
                    );
                  }}
                </Field>
                <div className="errorMessage">
                  <ErrorMessage name="professor" component={TextError} />
                </div>
              </FormField>
              <FormField className="col-3">
                <label htmlFor="startDateLabel">Academic Start Data</label>
                <Field name="startDate" id="startDateLabel">
                  {(props) => {
                    const { setFieldValue } = props.form;
                    return (
                      <Datetime
                        {...props.field}
                        timeFormat={false}
                        dateFormat="YYYY-MM-DD"
                        onChange={(event) => {
                          setFieldValue(props.field.name, event._d);
                        }}
                      />
                    );
                  }}
                </Field>
                <div className="errorMessage">
                  <ErrorMessage name="startDate" component={TextError} />
                </div>
              </FormField>
              <FormField className="col-3">
                <label htmlFor="endDateLabel">Academic End Data</label>
                <Field name="endDate" id="endDateLabel">
                  {(props) => {
                    const { setFieldValue } = props.form;
                    return (
                      <Datetime
                        {...props.field}
                        timeFormat={false}
                        dateFormat="YYYY-MM-DD"
                        onChange={(event) => {
                          setFieldValue(props.field.name, event._d);
                        }}
                      />
                    );
                  }}
                </Field>
                <div className="errorMessage">
                  <ErrorMessage name="endDate" component={TextError} />
                </div>
              </FormField>
              <FormField className="col-3">
                <label htmlFor="startTimeLabel">Start Time</label>
                <Field name="startTime" id="startTimeLabel">
                  {(props) => {
                    const { setFieldValue } = props.form;
                    return (
                      <Datetime
                        {...props.field}
                        dateFormat={false}
                        onChange={(event) => {
                          setFieldValue(props.field.name, event._d);
                        }}
                      />
                    );
                  }}
                </Field>
                <div className="errorMessage">
                  <ErrorMessage name="startTime" component={TextError} />
                </div>
              </FormField>
              <FormField className="col-3">
                <label htmlFor="endTimeLabel">End Time</label>
                <Field name="endTime" id="endTimeLabel">
                  {(props) => {
                    const { setFieldValue } = props.form;
                    return (
                      <Datetime
                        {...props.field}
                        dateFormat={false}
                        onChange={(event) => {
                          setFieldValue(props.field.name, event._d);
                        }}
                      />
                    );
                  }}
                </Field>
                <div className="errorMessage">
                  <ErrorMessage name="endTime" component={TextError} />
                </div>
              </FormField>
              <div className="col-12 mt-3">
                <Button type="submit">Submit</Button>
              </div>
              </div>
            </Form>
          </Formik>
        </Modal.Content>
      )}
      {/* <Modal.Actions>
        {errorCreateSchedule ? (
          <p>{errorCreateSchedule}</p>
        ) : (
          <React.Fragment />
        )}
      </Modal.Actions> */}
    </Modal>
  );
};
