/*eslint eqeqeq: "off"*/
import { Form } from "formik-semantic-ui-react";
import { Formik, Field, ErrorMessage } from "formik";
import { useSelector, useDispatch } from "react-redux";
import "react-datetime/css/react-datetime.css";
import Datetime from "react-datetime";
import moment from "moment";
import React, { useState } from "react";
import { changeSchedule, deleteSchedule } from "./store/actions";
//import { Loader, Dimmer } from "semantic-ui-react";
const TextError = function (props) {
  return <p style={{ color: "red" }}>{props.children}</p>;
};

export const Task = function (props) {
  const [taskForm, setTaskForm] = useState(true);

  let scheduleData = useSelector((state) => state.schedule.scheduleData);
  scheduleData = { ...scheduleData };
  const universityCode = useSelector((state) => state.auth.universityCode);
  const taskDetails = useSelector((state) => state.schedule.taskDetails);
  //console.log("taskDetails", taskDetails);
  //console.log("taskForm", taskForm);
  const subjectsData = useSelector((state) => state.subjects.subjectsData);
  // console.log("subject Data", subjectsData);
  const professorData = useSelector((state) => state.professors.professorData);

  const idClass = useSelector((state) => state.schedule.idClass);
  const dispatch = useDispatch();
  const onSubmitTask = (formValues, argument1, argument2) => {
    let professorDataSelected = professorData.find((professor) => {
      return professor.userName == formValues.professor;
    });
    let subjectDataSelected = subjectsData.find((subject) => {
      return subject.subjectName == formValues.subject;
    });

    let changeScheduleObject = {};
    changeScheduleObject.idClass = idClass;
    changeScheduleObject.dateNew = moment(formValues.date).format("YYYY-MM-DD");
    changeScheduleObject.dateOld = moment(taskDetails.date).format(
      "YYYY-MM-DD"
    );

    changeScheduleObject.idClassScheduleMonthly =
      taskDetails.classScheduleMonthlyId;

    changeScheduleObject.endTimeNew = moment(formValues.endTime).format(
      "HH:mm"
    );
    changeScheduleObject.endTimeOld = moment(
      taskDetails.modifiedWebEndTime
    ).format("HH:mm");
    changeScheduleObject.startTimeNew = moment(formValues.startTime).format(
      "HH:mm"
    );
    changeScheduleObject.startTimeOld = moment(
      taskDetails.modifiedWebStartTime
    ).format("HH:mm");
    changeScheduleObject.location = formValues.location;

    changeScheduleObject.professorCodeNew = professorDataSelected.userCode;
    changeScheduleObject.professorNameNew = professorDataSelected.userName;
    changeScheduleObject.professorCodeOld = taskDetails.professorCode;
    changeScheduleObject.professorNameOld = taskDetails.professorName;

    changeScheduleObject.subjectCodeNew = subjectDataSelected.subjectCode;
    changeScheduleObject.subjectNameNew = subjectDataSelected.subjectName;

    changeScheduleObject.subjectCodeOld = taskDetails.subjectCode;
    changeScheduleObject.subjectNameOld = taskDetails.subjectName;

    changeScheduleObject.universityCode = universityCode;
    changeScheduleObject.startTimeBoundaryCheck = moment(formValues.startTime)
      .add(1, "minute")
      .format("HH:mm");
    changeScheduleObject.endTimeBoundaryCheck = moment(formValues.endTime)
      .subtract(1, "minute")
      .format("HH:mm");
    // console.log("changeScheduleObject", changeScheduleObject);
    dispatch(changeSchedule(changeScheduleObject, scheduleData));
    /*
idClass
dateNew;
dateOld;
idClassScheduleMonthly;
endTimeNew;
endTimeOld;
startTimeNew;
startTimeOld;
location;
professorCodeNew;
professorNameNew;
professorCodeOld;
professorNameOld;
    subjectCodeNew;
    subjectNameNew;
    subjectCodeOld;
    subjectNameOld;
    universityCode;
    startTimeBoundaryCheck;
    endTimeBoundaryCheck;

inputBody.hasOwnProperty("idClass") &&
            inputBody.hasOwnProperty("dateOld") &&
            inputBody.hasOwnProperty("dateNew") &&
            inputBody.hasOwnProperty("idClassScheduleMonthly") &&
            inputBody.hasOwnProperty("startTimeNew") &&
            inputBody.hasOwnProperty("endTimeNew") &&
            inputBody.hasOwnProperty("location") &&
            inputBody.hasOwnProperty("subjectOld") &&
            inputBody.hasOwnProperty("subjectNew") &&
            inputBody.hasOwnProperty("startTimeOld") &&
            inputBody.hasOwnProperty("endTimeOld") &&
            inputBody.hasOwnProperty("professorNameOld") &&
            inputBody.hasOwnProperty("professorNameNew") &&
            inputBody.hasOwnProperty("professorEmail") &&
            inputBody.hasOwnProperty("professorEmailOld") &&
            inputBody.hasOwnProperty("universityCode") &&
            inputBody.hasOwnProperty("startTimeBoundaryCheck") &&
            inputBody.hasOwnProperty("endTimeBoundartCheck")
    */
  };

  const locationsData = useSelector((state) => state.locations.locationsData);
  let subjectsName = subjectsData.map((item) => ({
    name: item.subjectName,
  }));
  subjectsName = subjectsName.filter(
    (subject) => subject.name != taskDetails.subjectName
  );
  let locationsName = locationsData.map((item) => ({
    name: item.classRoomName,
  }));

  let professorName = professorData.map((item) => ({
    userName: item.userName,
    userCode: item.userCode,
  }));
  professorName = professorName.filter(
    (professor) => professor.userName != taskDetails.professorName
  );

  locationsName = locationsName.filter((location) => {
    return location.name != taskDetails.location;
  });
  const admissionYear = useSelector((state) => state.schedule.admissionYear);
  const departmentName = useSelector((state) => state.schedule.departmentName);
  const semister = useSelector((state) => state.schedule.semister);
  const division = useSelector((state) => state.schedule.division);
  //const spinner = useSelector((state) => state.schedule.spinner);

  const taskError = useSelector((state) => state.schedule.taskError);

  const deleteScheduleFunction = function () {
    let deleteScheduleObject = {
      idClass,
      universityCode,
      classScheduleMonthlyId: taskDetails.classScheduleMonthlyId,
      dateNew: moment(taskDetails.date).format("YYYY-MM-DD"),
      startTime: moment(taskDetails.modifiedWebStartTime).format("HH:mm:ss"),
      endTime: moment(taskDetails.modifiedWebEndTime).format("HH:mm:ss"),
      profCode: taskDetails.professorCode,
    };
    //console.log("deleteSchedule", deleteScheduleObject);
    dispatch(deleteSchedule(deleteScheduleObject, scheduleData));
  };

  // if (spinner) {
  //   return (
  //     <React.Fragment>
  //       <Dimmer active inverted>
  //         <Loader inverted>Loading</Loader>
  //       </Dimmer>
  //     </React.Fragment>
  //   );
  // } else {
  // }
  return (
    <div className="col-lg-3 task-wrap bg-white">
      <div className="task-heading">
        <h4>Task Details</h4>
        <div className="headerBtnIc">
          <button onClick={() => setTaskForm(!taskForm)}>
            Enable/Disbale Form
          </button>
        </div>
      </div>
      <div></div>
      <div className="row task-card">
        <div className="task-inner">
          <div className="task-head" style={{ background: "#ffecc9" }}>
            <small>Class Details</small>
          </div>
        </div>

        <div className="cardDetails col2block">
          <div className="cardBlock">
            <span className="fieldLabel">Admission Year</span>
            <div>{admissionYear}</div>
          </div>
          <div className="cardBlock">
            <span className="fieldLabel">Semester </span>
            <div>{semister}</div>
          </div>
          <div className="cardBlock">
            <span className="fieldLabel">Department </span>
            <div>{departmentName}</div>
          </div>
          <div className="cardBlock">
              <span className="fieldLabel">Division </span>
              <div>{division}</div>
          </div>
        </div>
      </div>
      <div className="row task-card">
        <div className="taskHeaderParent task-inner">
          <div className="task-head" style={{ background: "#ffecc9" }}>
            <small>Timing Details</small>
          </div>
          <div className="headerRight">
            <button className="svgIconBtn delsvgicon">
                <svg viewBox="0 0 14 18">
                  <path d="M1,16 C1,17.1 1.9,18 3,18 L11,18 C12.1,18 13,17.1 13,16 L13,4 L1,4 L1,16 L1,16 Z M14,1 L10.5,1 L9.5,0 L4.5,0 L3.5,1 L0,1 L0,3 L14,3 L14,1 L14,1 Z" />
                </svg>
              </button>
            </div>
        </div>

        <div className="col-12 ">
          {/* <div className="time">
            <button
              className="btn btn-primary mb-2"
              style={{ backgroundColor: "#4cadad" }}
              onClick={() => deleteScheduleFunction()}
              disabled={taskForm}
            >
              Delete Me
            </button>
          </div> */}

          <Formik
            enableReinitialize
            initialValues={{
              location: taskDetails.location,
              subject: taskDetails.subjectName,
              professor: taskDetails.professorName,
              date: new Date(taskDetails.date),
              startTime: new Date(taskDetails.modifiedWebStartTime),
              endTime: new Date(taskDetails.modifiedWebEndTime),
            }}
            validate={(formValues) => {
              // console.log("formValues", formValues);
              const errors = {};
              if (
                formValues.startTime &&
                formValues.endTime &&
                moment(formValues.startTime) >= moment(formValues.endTime)
              ) {
                errors.endTime = "Please select endTime greater than startTime";
              }
              //  console.log("errors", errors);
              return errors;
            }}
            onSubmit={onSubmitTask}
          >
            {(formik) => {
              return (
                <Form>
                  <div className="formControlRow">
                      <label htmlFor="locationLabel">Location</label>
                      <Field name="location" id="locationLabel">
                        {(props) => {
                          return (
                            <select
                              className="form-control"
                              name={props.field.name}
                              onBlur={props.field.onBlur}
                              onChange={(e) => {
                                props.field.onChange(e);
                              }}
                              disabled={taskForm}
                            >
                              <option value={formik.values[props.field.name]}>
                                {formik.values[props.field.name]}
                              </option>
                              {locationsName.map((location, index) => {
                                return (
                                  <option key={index} value={location.name}>
                                    {location.name}
                                  </option>
                                );
                              })}
                            </select>
                          );
                        }}
                      </Field>
                      <ErrorMessage name="location" component={TextError} />
                    </div>
                  <div className="formControlRow">
                      <label htmlFor="subjectLabel">Subject</label>
                      <Field name="subject" id="subjectLabel">
                        {(props) => {
                          return (
                            <select
                              className="form-control"
                              name={props.field.name}
                              onBlur={props.field.onBlur}
                              onChange={(e) => {
                                props.field.onChange(e);
                              }}
                              disabled={taskForm}
                            >
                              <option value={formik.values[props.field.name]}>
                                {formik.values[props.field.name]}
                              </option>
                              {subjectsName.map((subject, index) => {
                                return (
                                  <option key={index} value={subject.name}>
                                    {subject.name}
                                  </option>
                                );
                              })}
                            </select>
                          );
                        }}
                      </Field>
                      <ErrorMessage name="subject" component={TextError} />
                  </div>
                  <div className="formControlRow">
                      <label htmlFor="professorLabel">Professor</label>
                      <Field name="professor" id="professorLabel">
                        {(props) => {
                          return (
                            <select
                              className="form-control"
                              name={props.field.name}
                              onBlur={props.field.onBlur}
                              onChange={(e) => {
                                props.field.onChange(e);
                              }}
                              disabled={taskForm}
                            >
                              <option value={formik.values[props.field.name]}>
                                {formik.values[props.field.name]}
                              </option>
                              {professorName.map((professor, index) => {
                                //console.log("professorName", professorName);
                                return (
                                  <option
                                    key={index}
                                    value={professor.userName}
                                  >
                                    {professor.userName}
                                  </option>
                                );
                              })}
                            </select>
                          );
                        }}
                      </Field>
                      <ErrorMessage name="location" />
                  </div>
                  <div className="formControlRow">
                      <label htmlFor="classRoomLinkId">Class Room Link</label>
                      <Field name="classRoomLink" id="classRoomLinkId" />
                      <ErrorMessage
                        name="classRoomLink"
                        component={TextError}
                      />
                  </div>
                  <div className="formControlRow">
                      <label htmlFor="dateLabel">Data</label>
                      <Field name="date" id="dateLabel">
                        {(props) => {
                          const { setFieldValue } = props.form;
                          return (
                            <Datetime
                              {...props.field}
                              timeFormat={false}
                              dateFormat="YYYY-MM-DD"
                              onChange={(event) => {
                                setFieldValue(props.field.name, event._d);
                              }}
                              inputProps={{ disabled: taskForm }}
                            />
                          );
                        }}
                      </Field>
                      <ErrorMessage name="date" component={TextError} />
                  </div>
                  <div className="formControlRow">
                      <label htmlFor="startTimeLabel">Start Time</label>
                      <Field name="startTime" id="startTimeLabel">
                        {(props) => {
                          const { setFieldValue } = props.form;
                          return (
                            <Datetime
                              {...props.field}
                              dateFormat={false}
                              onChange={(event) => {
                                setFieldValue(props.field.name, event._d);
                              }}
                              inputProps={{ disabled: taskForm }}
                            />
                          );
                        }}
                      </Field>
                      <ErrorMessage name="startTime" component={TextError} />
                  </div>
                  <div className="formControlRow">
                      <label htmlFor="endTimeLabel">End Time</label>
                      <Field name="endTime" id="endTimeLabel">
                        {(props) => {
                          const { setFieldValue } = props.form;
                          return (
                            <Datetime
                              {...props.field}
                              dateFormat={false}
                              onChange={(event) => {
                                setFieldValue(props.field.name, event._d);
                              }}
                              inputProps={{ disabled: taskForm }}
                            />
                          );
                        }}
                      </Field>
                      <ErrorMessage name="endTime" component={TextError} />
                  </div>
                  <div className="formControlRow">
                      <button
                        type="submit"
                        className="btn btn-primary mb-2"
                        style={{ backgroundColor: "#4cadad" }}
                        inputProps={{ disabled: taskForm }}
                      >
                        Submit
                      </button>
                  </div>
                  <div className="formControlRow">
                    {taskError ? <p>{taskError}</p> : <React.Fragment />}
                  </div>
                </Form>
              );
            }}
          </Formik>
        </div>
      </div>
    </div>
  );
};
/*

changeId: 0
classId: 36
​classScheduleMonthlyId: 1324
​date: "2021-09-07T00:00:00.000Z"
​height: 60
​lastUpdatedEpochTime: 0
​link: null
​location: "ClassRoom12"
​modifiedWebEndTime: "2021-09-07 15:40:00"
​modifiedWebStartTime: "2021-09-07 14:40:00"
​professorCode: "USR054"
​professorName: "Innocurve User"
​scheduleCode: "SCH2"
​subjectCode: "SBJ02"
​subjectName: "English"
​top: 40

changeScheduleObject.= formValues.;
    date: Date Tue Sep 07 2021 05:30:00 GMT+0530 (India Standard Time)
​endTime: Date Tue Sep 07 2021 15:40:00 GMT+0530 (India Standard Time)
​location: "ClassRoom12"
​professor: "Innocurve User"
​startTime: Date Tue Sep 07 2021 14:40:00 GMT+0530 (India Standard Time)
​subject: "English"
            inputBody.hasOwnProperty("idClass") &&
            inputBody.hasOwnProperty("endDate") &&
            inputBody.hasOwnProperty("endTime") &&
            inputBody.hasOwnProperty("startDate") &&
            inputBody.hasOwnProperty("startTime") &&
            inputBody.hasOwnProperty("location") &&
            inputBody.hasOwnProperty("startTimeBoundaryCheck") &&
            inputBody.hasOwnProperty("endTimeBoundaryCheck") &&
            inputBody.hasOwnProperty("professorCode") &&
            inputBody.hasOwnProperty("professorName") &&
            inputBody.hasOwnProperty("subjectCode") &&
            inputBody.hasOwnProperty("subjectName") &&
            inputBody.hasOwnProperty("universityCode")

            ​userCode: "USR054"
​​userEmail: "innocurveapp@gmail.com"
​​userImage: ""​​
userName: "Innocurve User"

idSubject: 48
​​subjectCode: "SBJ02"
​​subjectName: "English"
​​universityCode: "TR036"

    */
