import { createSlice } from "@reduxjs/toolkit";
const scheduleSlice = createSlice({
  name: "scheduleReducer",
  initialState: {
    scheduleData: {},
    scheduleError: "",
    taskDetails: {},
    admissionYear: "",
    departmentName: "",
    semister: "",
    division: "",
    submitSuccessful: false,
    spinner: false,
    errorCreateSchedule: "",
    idClass: "",
    taskError: "",
    taskDeleted: false,
  },
  reducers: {
    scheduleData(state, action) {
      //  console.log("Action", action.payload.scheduleData);
      state.scheduleData = { ...action.payload.scheduleData };
    },
    setScheduleError(state, action) {
      state.scheduleError = action.payload.scheduleError;
    },
    setCreateScheduleError(state, action) {
      //     console.log("action");
      state.errorCreateSchedule = action.payload.errorCreateSchedule;
    },
    setTaskDetails(state, action) {
      state.taskDetails = action.payload.taskDetails;
    },
    setClassDetails(state, action) {
      state.idClass = action.payload.idClass;
    },
    setClassValues(state, action) {
      const {
        admissionYear,
        departmentCode,
        division,
        semister,
      } = action.payload.classFilter;
      state.admissionYear = admissionYear;
      state.departmentName = departmentCode;
      state.semister = semister;
      state.division = division;
    },
    setSpinner(state, action) {
      state.spinner = action.payload.spinner;
    },
    setSubmitSuccessfull(state, action) {
      state.submitSuccessful = action.payload.submitSuccessful;
    },
    setTaskError(state, action) {
      state.taskError = action.payload.taskError;
    },
    setTaskHidden(state, action) {
      state.taskDeleted = action.payload.taskDeleted;
    },
  },
});

export const scheduleActions = scheduleSlice.actions;
export default scheduleSlice;
