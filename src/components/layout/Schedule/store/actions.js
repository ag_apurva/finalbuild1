/*eslint eqeqeq: "off"*/
import {
  GET_SCHEDULE,
  CREATE_SCHEDULE,
  DELETE_SCHEDULE,
  CHECK_FOR_CONFLICT,
  SCHEDULE_DATA_PROFESSOR,
  SCHEDULE_DATA_LOCATION,
} from "../../API";
import { scheduleActions } from "./index";
import moment from "moment";
import axios from "axios";

export const getScheduleRespectiveClassId = function (ClassId, universityCode) {
  return async function (dispatch) {
    dispatch(scheduleActions.setSpinner({ spinner: true }));
    const config = {
      method: "get",
      url: GET_SCHEDULE,
      params: { universityCode: universityCode, classId: ClassId },
      headers: {},
    };
    try {
      let scheduleData = await axios(config);
      let processedData = ProcessScheduleData(scheduleData.data);
      // console.log("processedData", processedData);
      dispatch(
        scheduleActions.scheduleData({ scheduleData: { ...processedData } })
      );
    } catch (e) {
      dispatch(
        scheduleActions.setScheduleError({ scheduleError: JSON.stringify(e) })
      );
    }
    dispatch(scheduleActions.setSpinner({ spinner: false }));
  };
};

export const changeSchedule = function (changeScheduleObject, scheduleData) {
  return async function (dispatch) {
    dispatch(scheduleActions.setSpinner({ spinner: true }));
    try {
      const config = {
        method: "put",
        url: CHECK_FOR_CONFLICT,
        headers: {
          "Content-Type": "application/json",
        },
        data: JSON.stringify(changeScheduleObject),
      };

      let scheduleChangeData = await axios(config);
      // console.log("scheduleChangeData", scheduleChangeData);
      if (!scheduleChangeData.data.hasOwnProperty("error")) {
        dispatch(scheduleActions.setTaskHidden({ taskDeleted: true }));

        setTimeout(() => {
          dispatch(scheduleActions.setTaskHidden({ taskDeleted: false }));
        }, 200);
        let dateNew = changeScheduleObject.dateNew;
        let dateOld = changeScheduleObject.dateOld;

        let processedData = ProcessScheduleData(
          JSON.parse(scheduleChangeData.data.data)
        );
        //console.log("Eeeeeee11", processedData, dateNew);
        //console.log("Eeeeeee22", processedData[dateNew]);
        if (dateOld != dateNew) {
          // let processedDateOld = processedData[dateOld];
          // let processedDateNew = processedData[dateNew];
          scheduleData[dateOld] = processedData[dateOld];
          scheduleData[dateNew] = processedData[dateNew];
          dispatch(
            scheduleActions.scheduleData({ scheduleData: scheduleData })
          );
        } else {
          // let processedDateNew = processedData[dateNew];
          // console.log("processedDataNew", processedDateNew);
          // //let scheduleData;
          scheduleData[dateNew] = processedData[dateNew];
          dispatch(
            scheduleActions.scheduleData({ scheduleData: scheduleData })
          );
        }
      } else {
        dispatch(
          scheduleActions.setTaskError({
            taskError: scheduleChangeData.data.error,
          })
        );

        setTimeout(() => {
          dispatch(
            scheduleActions.setTaskError({
              taskError: "",
            })
          );
        }, 10000);
      }
    } catch (e) {
      console.log("error change in Schedule", e);

      dispatch(
        scheduleActions.setTaskError({
          taskError: JSON.stringify(e),
        })
      );

      setTimeout(() => {
        dispatch(
          scheduleActions.setTaskError({
            taskError: "",
          })
        );
      }, 10000);
    }
    dispatch(scheduleActions.setSpinner({ spinner: false }));
  };
};

export const setClassSelectionValues = function (formValues) {
  return async function (dispatch) {
    dispatch(scheduleActions.setClassValues({ classFilter: formValues }));
  };
};

//most important function in the Schedule component
//this same function is used in ProfessorForm
export const ProcessScheduleData = (unfilteredScheduleData) => {
  var dataMapped = {};
  unfilteredScheduleData.forEach((item) => {
    var itemDate = moment(item.date).format("YYYY-MM-DD");
    let newItem = { ...item };
    if (!dataMapped.hasOwnProperty(itemDate)) {
      dataMapped[itemDate] = [];
    }
    newItem["modifiedWebStartTime"] = moment(item.modifiedWebStartTime);
    newItem["modifiedWebEndTime"] = moment(item.modifiedWebEndTime);
    //console.log("111", newItem);
    dataMapped[itemDate].push(newItem);
  });
  // all the keys in dataMapped are the dates in yyyy-mm-dd format and contains the starttime and end time
  //of various classes

  var dataMappedMain = {};
  Object.keys(dataMapped).forEach((item) => {
    dataMappedMain[item] = {};
    for (let i = 4; i <= 23; i++) {
      dataMappedMain[item][i] = [];
    }
    //corresponding to each date we set the values of classes from 6 to 21
    dataMapped[item].forEach((data) => {
      let startTime = moment(data.modifiedWebStartTime);
      let endTime = moment(data.modifiedWebEndTime);
      let minutes = moment.duration(endTime.diff(startTime)).asMinutes();
      //console.log("222", startTime, endTime, minutes, startTime.minutes());
      //we find out how long the classes will go on and set the hieght of the class in as minutes
      //dataMappedMain has values of each date corresponding to hours.
      //each hous has top values telling how much after beginnning of hour the schedule will lie.
      dataMappedMain[item][startTime.hour()].push({
        ...data,
        top: startTime.minutes(),
        height: minutes,
      });
    });
  });

  /* corresponding to each data there is an hour and corresponding to every hour there is an array.
    {
    '2021-04-01':{
        8: [],
        9: []
      }
    }*/

  Object.keys(dataMappedMain).forEach((item) => {
    //we iterate through each day
    Object.keys(dataMappedMain[item]).forEach((data) => {
      //for each data hour where we have no class set the value of height and top to zero.
      if (dataMappedMain[item][data].length === 0) {
        dataMappedMain[item][data].push({ height: 0, top: 0 });
      }
    });
  });
  // console.log("dataMappedMain", dataMappedMain);
  return dataMappedMain;
};

export const createSchedule = function (
  createSceduleObject,
  scheduleData,
  classSelected
) {
  return async function (dispatch) {
    dispatch(scheduleActions.setSpinner({ spinner: true }));

    try {
      var config = {
        method: "post",
        url: CREATE_SCHEDULE,
        headers: {
          "Content-Type": "application/json",
        },
        data: JSON.stringify(createSceduleObject),
      };

      const createSchedule = await axios(config);
      // console.log("classSelected", classSelected);
      if (!createSchedule.data.hasOwnProperty("error")) {
        if (classSelected) {
          dispatch(
            getScheduleRespectiveClassId(
              createSceduleObject.idClass,
              createSceduleObject.universityCode
            )
          );
        }

        dispatch(
          scheduleActions.setSubmitSuccessfull({
            submitSuccessful: true,
          })
        );
        setTimeout(() => {
          dispatch(
            scheduleActions.setSubmitSuccessfull({
              submitSuccessful: false,
            })
          );
        }, 200);
      } else {
        // console.log("createSchedule.data.error", createSchedule.data.error);
        dispatch(
          scheduleActions.setCreateScheduleError({
            errorCreateSchedule: createSchedule.data.error,
          })
        );
        setTimeout(() => {
          dispatch(
            scheduleActions.setCreateScheduleError({
              errorCreateSchedule: "",
            })
          );
        }, 20000);
        setTimeout(() => {
          dispatch(
            scheduleActions.setSubmitSuccessfull({ submitSuccessful: true })
          );
        }, 20000);
        setTimeout(() => {
          dispatch(
            scheduleActions.setSubmitSuccessfull({ submitSuccessful: false })
          );
        }, 20200);
      }
    } catch (e) {
      dispatch(
        scheduleActions.setCreateScheduleError({
          errorCreateSchedule: JSON.stringify(e),
        })
      );
      setTimeout(() => {
        dispatch(
          scheduleActions.setCreateScheduleError({
            errorCreateSchedule: "",
          })
        );
      }, 20000);
      setTimeout(() => {
        dispatch(
          scheduleActions.setSubmitSuccessfull({ submitSuccessful: true })
        );
      }, 20000);
      setTimeout(() => {
        dispatch(
          scheduleActions.setSubmitSuccessfull({ submitSuccessful: false })
        );
      }, 20200);
    }
    dispatch(scheduleActions.setSpinner({ spinner: false }));
  };
};

export const deleteSchedule = function (deleteScheudule, scheduleData) {
  return async function (dispatch) {
    const config = {
      method: "delete",
      url: DELETE_SCHEDULE,
      params: {
        idClass: deleteScheudule.idClass,
        universityCode: deleteScheudule.universityCode,
        dateNew: deleteScheudule.dateNew,
        idClassScheduleMonthly: deleteScheudule.classScheduleMonthlyId,
        startTime: deleteScheudule.startTime,
        endTime: deleteScheudule.endTime,
        profCode: deleteScheudule.profCode,
      },
      headers: {},
    };

    // console.log("config", config);
    try {
      let deleteSchedule = await axios(config);
      // let deleteSchedule = await new Promise((resolve, reject) => {
      //   resolve({
      //     data: {
      //       error: "fddfdffffff",
      //     },
      //   });
      // });

      //   console.log("deleteSchedule", deleteSchedule);
      if (!deleteSchedule.data.hasOwnProperty("error")) {
        dispatch(scheduleActions.setTaskHidden({ taskDeleted: true }));

        setTimeout(() => {
          dispatch(scheduleActions.setTaskHidden({ taskDeleted: false }));
        }, 200);
        //console.log("before", JSON.parse(deleteSchedule.data.data));
        let processedData = ProcessScheduleData(
          JSON.parse(deleteSchedule.data.data)
        );
        let dateNew = deleteScheudule.dateNew;
        //console.log("processedData", processedData, dateNew);
        scheduleData[dateNew] = processedData[dateNew];
        dispatch(scheduleActions.scheduleData({ scheduleData: scheduleData }));
      } else {
        dispatch(
          scheduleActions.setTaskError({
            taskError: deleteSchedule.data.error,
          })
        );

        setTimeout(() => {
          dispatch(
            scheduleActions.setTaskError({
              taskError: "",
            })
          );
        }, 10000);
      }
    } catch (e) {
      dispatch(scheduleActions.setTaskError({ taskError: JSON.stringify(e) }));
      setTimeout(() => {
        dispatch(
          scheduleActions.setTaskError({
            taskError: "",
          })
        );
      }, 10000);
    }
  };
};

export const professorScheduleData = function (professorScheduleObject) {
  return async function (dispatch) {
    // console.log("professorScheduleObject", professorScheduleObject);
    var config = {
      method: "get",
      url: SCHEDULE_DATA_PROFESSOR,
      headers: {},
      params: {
        universityCode: professorScheduleObject.universityCode,
        email: professorScheduleObject.email,
      },
    };
    try {
      let professorScheduleData = await axios(config);

      if (!professorScheduleData.data.hasOwnProperty("error")) {
        let processedData = ProcessScheduleData(professorScheduleData.data);
        // console.log("processedData", processedData);
        dispatch(
          scheduleActions.scheduleData({
            scheduleData: { ...processedData },
          })
        );
      } else {
      }
    } catch (e) {
      console.log("Error processing schedule data", e);
    }
  };
};

export const locationScheduleData = function (locationScheduleObject) {
  return async function (dispatch) {
    // console.log("locationScheduleObject", locationScheduleObject);
    var config = {
      method: "get",
      url: SCHEDULE_DATA_LOCATION,
      headers: {},
      params: {
        universityCode: locationScheduleObject.universityCode,
        locationName: locationScheduleObject.locationName,
      },
    };
    try {
      let professorScheduleData = await axios(config);

      if (!professorScheduleData.data.hasOwnProperty("error")) {
        let processedData = ProcessScheduleData(professorScheduleData.data);
        // console.log("processedData", processedData);
        dispatch(
          scheduleActions.scheduleData({
            scheduleData: { ...processedData },
          })
        );
      } else {
      }
    } catch (e) {
      console.log("E", e);
    }
  };
};
