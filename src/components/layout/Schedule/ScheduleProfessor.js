/*eslint eqeqeq: "off"*/
import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Form } from "formik-semantic-ui-react";
import { Formik, Field, ErrorMessage } from "formik";
import { professorScheduleData } from "./store/actions";
import "../css/student.css";
import "../css/schedule.css";

// import { scheduleActions } from "./store/index";
export const ScheduleProfessor = function () {
  const professorData = useSelector((state) => state.professors.professorData);
  let professorNames = professorData.map((item) => ({
    name: item.userName,
    email: item.userEmail,
  }));
  console.log("professorData", professorData);
  const scheduleError = useSelector((state) => state.class.scheduleError);

  const universityCode = useSelector((state) => state.auth.universityCode);
  const classValidation = function (formValues) {
    const errors = {};
    if (!formValues.professorName) {
      errors.professorName = "* Please select a Class Room";
    }

    return errors;
  };
  const dispatch = useDispatch();

  //const spinner = useSelector((state) => state.schedule.spinner);

  const [initialValues, setInitialValues] = useState({
    professorName: "",
  });

  return (
    <Formik
      enableReinitialize
      initialValues={initialValues}
      validate={classValidation}
      onSubmit={(formValues) => {
        console.log("formValues", formValues);
        setInitialValues({ ...formValues });
        dispatch(
          professorScheduleData({
            email: formValues.professorName,
            universityCode: universityCode,
          })
        );
      }}
    >
      {(formik) => {
        return (
          <Form setSubmitting={true}>
            <div className="filter-wrapper">
              <div className="filter">
                <label htmlFor="professorNameLabel">Year</label>
                <Field
                  value={formik.values.admissionYear}
                  id="professorNameLabel"
                  type="text"
                  name="professorName"
                >
                  {(props) => {
                    return (
                      <select
                        {...props.fields}
                        className="form-control"
                        name={props.field.name}
                        onBlur={props.field.onBlur}
                        onChange={(e) => {
                          props.field.onChange(e);
                        }}
                      >
                        <option value="">Select</option>
                        {professorNames.map((profeesor, index) => {
                          return (
                            <option key={index} value={profeesor.email}>
                              {profeesor.name}
                            </option>
                          );
                        })}
                      </select>
                    );
                  }}
                </Field>
                <ErrorMessage name="classRoom" />
              </div>
              <div className="filter fil-btn">
                <label></label>
                <button
                  type="submit"
                  className="btn btn-primary mb-2"
                  style={{ backgroundColor: "#4cadad" }}
                >
                  GO
                </button>
                {scheduleError ? <p>{scheduleError}</p> : <React.Fragment />}
              </div>
            </div>
          </Form>
        );
      }}
    </Formik>
  );
};
