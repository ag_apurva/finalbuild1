/*eslint eqeqeq: "off"*/
import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Form } from "formik-semantic-ui-react";
import { Formik, Field, ErrorMessage } from "formik";
import { locationScheduleData } from "./store/actions";
import "../css/student.css";
import "../css/schedule.css";
// import { Loader, Dimmer } from "semantic-ui-react";
// import { scheduleActions } from "./store/index";
export const ScheduleLocation = function () {
  const locationsData = useSelector((state) => state.locations.locationsData);
  let locationsName = locationsData.map((item) => ({
    name: item.classRoomName,
  }));

  const scheduleError = useSelector((state) => state.class.scheduleError);

  const universityCode = useSelector((state) => state.auth.universityCode);
  const classValidation = function (formValues) {
    const errors = {};
    if (!formValues.classRoom) {
      errors.classRoom = "* Please select a Class Room";
    }

    return errors;
  };
  const dispatch = useDispatch();

  //const spinner = useSelector((state) => state.schedule.spinner);

  const [initialValues, setInitialValues] = useState({
    classRoom: "",
  });
  // if (spinner) {
  //   return (
  //     <React.Fragment>
  //       <Dimmer active>
  //         <Loader size="small">Loading</Loader>
  //       </Dimmer>
  //     </React.Fragment>
  //   );
  // } else {
  // }
  return (
    <Formik
      enableReinitialize
      initialValues={initialValues}
      validate={classValidation}
      onSubmit={(formValues) => {
        console.log("formValues", formValues);
        setInitialValues({ ...formValues }); //locationScheduleData
        dispatch(
          locationScheduleData({
            locationName: formValues.classRoom,
            universityCode: universityCode,
          })
        );
      }}
    >
      {(formik) => {
        return (
          <Form setSubmitting={true}>
            <div className="filter-wrapper">
              <div className="filter">
                <label htmlFor="classRoomLabel">Year</label>
                <Field
                  value={formik.values.admissionYear}
                  id="classRoomLabel"
                  type="text"
                  name="classRoom"
                >
                  {(props) => {
                    return (
                      <select
                        {...props.fields}
                        className="form-control"
                        name={props.field.name}
                        onBlur={props.field.onBlur}
                        onChange={(e) => {
                          props.field.onChange(e);
                        }}
                      >
                        <option value="">Select</option>
                        {locationsName.map((location, index) => {
                          return (
                            <option key={index} value={location.name}>
                              {location.name}
                            </option>
                          );
                        })}
                      </select>
                    );
                  }}
                </Field>
                <ErrorMessage name="classRoom" />
              </div>
              <div className="filter fil-btn">
                <label></label>
                <button
                  type="submit"
                  className="btn btn-primary mb-2"
                  style={{ backgroundColor: "#4cadad" }}
                >
                  GO
                </button>
                {scheduleError ? <p>{scheduleError}</p> : <React.Fragment />}
              </div>
            </div>
          </Form>
        );
      }}
    </Formik>
  );
};
