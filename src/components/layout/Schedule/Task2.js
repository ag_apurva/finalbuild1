import { Form } from "formik-semantic-ui-react";
import { Formik, Field, ErrorMessage } from "formik";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { useSelector } from "react-redux";
import "react-datetime/css/react-datetime.css";
import Datetime from "react-datetime";
import moment from "moment";
const TextError = function (props) {
  return <p style={{ color: "red" }}>{props.children}</p>;
};

export const Task = function (props) {
  const onSubmitTask = (formValues) => {
    console.log("formValues", formValues);
  };
  const subjectsData = useSelector((state) => state.subjects.subjectsData);
  const locationsData = useSelector((state) => state.locations.locationsData);
  const subjectsName = subjectsData.map((item) => ({
    name: item.subjectName,
  }));
  const locationsName = locationsData.map((item) => ({
    name: item.classRoomName,
  }));
  const taskDetails = useSelector((state) => state.schedule.taskDetails);
  console.log("taskDetails", taskDetails);
  const professorData = useSelector((state) => state.professors.professorData);
  const professorName = professorData.map((item) => ({
    userName: item.userName,
    userCode: item.userCode,
  }));
  const admissionYear = useSelector((state) => state.schedule.admissionYear);
  const departmentName = useSelector((state) => state.schedule.departmentName);
  const semister = useSelector((state) => state.schedule.semister);
  const division = useSelector((state) => state.schedule.division);

  return (
    <div className="col-lg-3 task-wrap bg-white">
      <div className="task-heading pt-2">
        <h4>Task Details</h4>
        <span>
          <button>Edit/Delete</button>
        </span>
      </div>
      <div></div>
      <div className="row task-card">
        <div className="task-inner">
          <div className="task-head" style={{ background: "#ffecc9" }}>
            <small>Class Details</small>
          </div>
        </div>

        <div className="col-12 ">
          <div className="time">
            <div>
              <p>
                <b>Admission Year </b> {admissionYear}
              </p>
            </div>
            <div>
              <p>
                <b>Semester </b> {semister}
              </p>
            </div>
          </div>
          <div className="time">
            <div>
              <p>
                <b>Department </b>
                {departmentName}
              </p>
            </div>
            <div>
              <p>
                <b>Division </b> {division}
              </p>
            </div>
          </div>
        </div>
      </div>
      <div className="row task-card">
        <div className="task-inner">
          <div className="task-head" style={{ background: "#ffecc9" }}>
            <small>Timing Details</small>
          </div>
        </div>

        <div className="col-12 ">
          <div className="time">
            <button
              className="btn btn-primary mb-2"
              style={{ backgroundColor: "#4cadad" }}
            >
              Delete Me
            </button>
          </div>

          <Formik
            initialValues={{
              location: taskDetails.location,
              subject: taskDetails.subjectName,
              professor: taskDetails.professorName,
              date: new Date(taskDetails.data),
              startTime: new Date(taskDetails.modifiedWebStartTime),
              endTime: new Date(taskDetails.modifiedWebEndTime),
            }}
            validate={(formValues) => {
              //console.log("formValues", formValues);
              const errors = {};
              if (
                formValues.startTime &&
                formValues.endTime &&
                moment(formValues.startTime) >= moment(formValues.endTime)
              ) {
                errors.endTime = "Please select endTime greater than startTime";
              }
              return errors;
            }}
            onSubmit={onSubmitTask}
          >
            {(formik) => {
              return (
                <Form>
                  <div>
                    <div>
                      <label htmlFor="locationLabel">Location</label>
                      <Field name="location" id="locationLabel">
                        {(props) => {
                          const { setFieldValue } = props.form;
                          console.log("props", props);
                          return (
                            <Autocomplete
                              style={{ width: 150 }}
                              className="filter"
                              name={props.field.name}
                              options={locationsName}
                              getOptionLabel={(option) => {
                                // console.log("option1", option);
                                return option.name || "";
                              }}
                              getOptionSelected={(option, value) => {
                                // console.log("option21", option, value);
                                // console.log("option22", option.name == value);
                                return option.name == value;
                              }}
                              renderOption={(option) => {
                                return option.name;
                              }}
                              renderInput={(params) => {
                                return (
                                  <TextField {...params} label={"Location"} />
                                );
                              }}
                              onChange={(event, input) => {
                                setFieldValue(props.field.name, input);
                              }}
                              value={props.field.value}
                            />
                          );
                        }}
                      </Field>
                      <ErrorMessage name="location" component={TextError} />
                    </div>
                  </div>
                  <div className="time">
                    <div>
                      <label htmlFor="subjectLabel">Subject</label>
                      <Field name="subject" id="subjectLabel">
                        {(props) => {
                          const { setFieldValue } = props.form;
                          return (
                            <Autocomplete
                              {...props.field}
                              style={{ width: 150 }}
                              className="filter"
                              name={props.field.name}
                              options={subjectsName}
                              getOptionLabel={(option) => {
                                console.log("option211", option);
                                return option.name || "";
                              }}
                              getOptionSelected={(option, value) => {
                                console.log("option212", option, value);

                                return option.name == value;
                              }}
                              renderOption={(option) => {
                                return option.name;
                              }}
                              renderInput={(params) => {
                                return (
                                  <TextField {...params} label={"Subject"} />
                                );
                              }}
                              onChange={(event, input) => {
                                setFieldValue(props.field.name, input);
                              }}
                            />
                          );
                        }}
                      </Field>
                      <ErrorMessage name="subject" component={TextError} />
                    </div>
                  </div>
                  <div className="time">
                    <div>
                      <label htmlFor="professorLabel">Professor</label>
                      <Field name="professor" id="professorLabel">
                        {(props) => {
                          const { setFieldValue } = props.form;
                          return (
                            <Autocomplete
                              style={{ width: 150 }}
                              className="filter"
                              name={props.field.name}
                              options={professorName}
                              value={props.field.value}
                              getOptionLabel={(option) => {
                                console.log("option11", option);
                                return option.userName || "";
                              }}
                              getOptionSelected={(option, value) => {
                                console.log("option21", option, value);
                                console.log(
                                  "option22",
                                  option.userName === value
                                );
                                return option.userName === value;
                              }}
                              renderOption={(option) => {
                                return option.userName;
                              }}
                              renderInput={(params) => {
                                return (
                                  <TextField {...params} label={"Professor"} />
                                );
                              }}
                              onChange={(event, input) => {
                                setFieldValue(props.field.name, input);
                              }}
                            />
                          );
                        }}
                      </Field>
                      <ErrorMessage name="location" />
                    </div>
                  </div>
                  <div className="time">
                    <div>
                      <label htmlFor="classRoomLinkId">Class Room Link</label>
                      <Field name="classRoomLink" id="classRoomLinkId" />
                      <ErrorMessage
                        name="classRoomLink"
                        component={TextError}
                      />
                    </div>
                  </div>
                  <div className="time">
                    <div>
                      <label htmlFor="dateLabel">Data</label>
                      <Field name="date" id="dateLabel">
                        {(props) => {
                          const { setFieldValue } = props.form;
                          return (
                            <Datetime
                              {...props.field}
                              timeFormat={false}
                              dateFormat="YYYY-MM-DD"
                              onChange={(event) => {
                                setFieldValue(props.field.name, event._d);
                              }}
                            />
                          );
                        }}
                      </Field>
                      <ErrorMessage name="date" component={TextError} />
                    </div>
                  </div>
                  <div className="time">
                    <div>
                      <label htmlFor="startTimeLabel">Start Time</label>
                      <Field name="startTime" id="startTimeLabel">
                        {(props) => {
                          const { setFieldValue } = props.form;
                          return (
                            <Datetime
                              {...props.field}
                              dateFormat={false}
                              onChange={(event) => {
                                setFieldValue(props.field.name, event._d);
                              }}
                            />
                          );
                        }}
                      </Field>
                      <ErrorMessage name="startTime" component={TextError} />
                    </div>
                  </div>
                  <div className="time">
                    <div>
                      <label htmlFor="endTimeLabel">End Time</label>
                      <Field name="endTime" id="endTimeLabel">
                        {(props) => {
                          const { setFieldValue } = props.form;
                          return (
                            <Datetime
                              {...props.field}
                              dateFormat={false}
                              onChange={(event) => {
                                setFieldValue(props.field.name, event._d);
                              }}
                            />
                          );
                        }}
                      </Field>
                      <ErrorMessage name="endTime" component={TextError} />
                    </div>
                  </div>
                  <div className="time">
                    <div>
                      <button
                        type="submit"
                        className="btn btn-primary mb-2"
                        style={{ backgroundColor: "#4cadad" }}
                      >
                        Submit
                      </button>
                    </div>
                  </div>
                </Form>
              );
            }}
          </Formik>
        </div>
      </div>
    </div>
  );
};
