/*eslint eqeqeq: "off"*/
import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Form } from "formik-semantic-ui-react";
import { Formik, Field, ErrorMessage } from "formik";
import {
  getScheduleRespectiveClassId,
  setClassSelectionValues,
} from "./store/actions";
import "../css/student.css";
import "../css/schedule.css";
import { scheduleActions } from "./store/index";
export const ScheduleClassId = function () {
  const [disableDepartmentDropDown, setDepartmentDropDown] = useState(true);
  const [disableSemisterDropDown, setSemisterDropDown] = useState(true);
  const [disableDivisionDropDown, setDivisionDropDown] = useState(true);
  const [admissionCodeSelected, setAdmissionCode] = useState("");
  const [semesterSelected, setSemester] = useState("");
  const [classId, setClassId] = useState("");
  const admissionsData = useSelector((state) => state.batch.admissionsData);
  const [deptDataRelativeAdmYear, setDeptDataRelativeAdmYear] = useState([]);
  const [
    semesterDataRelativeAdmissionCode,
    setSemesterDataRelativeAdmissionCode,
  ] = useState([]);

  const [
    divisionsRelativeSemesterAdmCode,
    setDivisionsRelativeSemesterAdmCode,
  ] = useState([]);
  let admissionsYear = admissionsData.map((item) => item.admissionYear);
  admissionsYear = [...new Set(admissionsYear)];
  const classesData = useSelector((state) => state.class.classData);
  const scheduleError = useSelector((state) => state.class.scheduleError);
  const [
    classDataRelativeAdmissionCode,
    setClassDataRelativeAdmissionCode,
  ] = useState([]);
  const universityCode = useSelector((state) => state.auth.universityCode);
  const classValidation = function (formValues) {
    const errors = {};
    if (!formValues.admissionYear) {
      errors.admissionYear = "* Please select a admission Year";
    }
    if (!formValues.departmentCode) {
      errors.departmentCode = "* Please select a Department Code";
    }
    if (!formValues.semister) {
      errors.semister = "* Please select a Semister";
    }
    if (!formValues.division) {
      errors.division = "* Please select a Division";
    }
    return errors;
  };
  const dispatch = useDispatch();

  const admissionsYearChange = function (e) {
    if (e.target.value !== "") {
      let filteradmissionData = admissionsData.filter(
        (item) => item.admissionYear == e.target.value
      );
      if (filteradmissionData.length > 0) {
        setDeptDataRelativeAdmYear(filteradmissionData);
        setDepartmentDropDown(false);
      }
    }
  };

  const departmentCodeChange = function (e) {
    if (e.target.value !== "") {
      let admissionCodeSelected = deptDataRelativeAdmYear.find(
        (item) => item.departmentCode === e.target.value
      );
      if (admissionCodeSelected) {
        setAdmissionCode(admissionCodeSelected.admissionCode);
        let tempClassesData = classesData.filter(
          (classItem) =>
            classItem.admissionCode == admissionCodeSelected.admissionCode
        );
        if (tempClassesData.length > 0) {
          setClassDataRelativeAdmissionCode(tempClassesData);
          let tempSemester = tempClassesData.map((item) => item.semester);
          tempSemester = [...new Set(tempSemester)];
          setSemesterDataRelativeAdmissionCode(tempSemester);
          setSemisterDropDown(false);
        }
      }
    }
  };

  const semesterChange = function (e) {
    if (e.target.value != "") {
      setSemester(e.target.value);
      let tempDivisionData = classDataRelativeAdmissionCode.filter((item) => {
        return (
          item.admissionCode == admissionCodeSelected &&
          item.semester == e.target.value
        );
      });
      if (tempDivisionData.length > 0) {
        let divisions = tempDivisionData.map((item) => item.division);
        divisions = [...new Set(divisions)];
        setDivisionsRelativeSemesterAdmCode(divisions);
        setDivisionDropDown(false);
      }
    }
  };

  const divisionChange = function (e) {
    if (e.target.value != "") {
      let tempClassId = classDataRelativeAdmissionCode.find((item) => {
        return (
          item.admissionCode == admissionCodeSelected &&
          item.semester == semesterSelected &&
          item.division == e.target.value
        );
      });

      if (tempClassId) {
        setClassId(tempClassId["idClass"]);
      }
    }
  };
  const spinner = useSelector((state) => state.schedule.spinner);

  const [initialValues, setInitialValues] = useState({
    admissionYear: "",
    departmentCode: "",
    semister: "",
    division: "",
  });
  if (spinner) {
    return (
      <div className="ui active inverted dimmer">
        <div className="ui loader"></div>
      </div>
    );
  } else {
    return (
      <Formik
        enableReinitialize
        initialValues={initialValues}
        validate={classValidation}
        onSubmit={(formValues, actions, argument2) => {
          actions.setSubmitting(false);

          setInitialValues({ ...formValues });
          dispatch(scheduleActions.setClassDetails({ idClass: classId }));
          dispatch(scheduleActions.scheduleData({ scheduleData: [] }));
          dispatch(setClassSelectionValues(formValues));
          dispatch(getScheduleRespectiveClassId(classId, universityCode));
        }}
      >
        {(formik) => {
          return (
            <Form setSubmitting={true}>
              <div className="filter-wrapper">
                <div className="filter">
                  <label htmlFor="admissionYearLabel">Year</label>
                  <Field
                    value={formik.values.admissionYear}
                    id="admissionYearLabel"
                    type="text"
                    name="admissionYear"
                  >
                    {(props) => {
                      return (
                        <select
                          {...props.fields}
                          className="form-control"
                          name={props.field.name}
                          onBlur={props.field.onBlur}
                          onChange={(e) => {
                            admissionsYearChange(e);
                            props.field.onChange(e);
                          }}
                        >
                          <option value={formik.values[props.field.name]}>
                            {formik.values[props.field.name]
                              ? formik.values[props.field.name]
                              : "Select"}
                          </option>
                          {admissionsYear.map((year, index) => {
                            return (
                              <option key={index} value={year}>
                                {year}
                              </option>
                            );
                          })}
                        </select>
                      );
                    }}
                  </Field>
                  <div className="errorMessage">
                    <ErrorMessage name="admissionYear" />
                  </div>
                </div>
                <div className="filter">
                  <label htmlFor="departmentLabel">Department Name</label>
                  <Field id="departmentLabel" type="text" name="departmentCode">
                    {(props) => {
                      return (
                        <select
                          className="form-control"
                          name={props.field.name}
                          onBlur={props.field.onBlur}
                          onChange={(e) => {
                            departmentCodeChange(e);
                            props.field.onChange(e);
                          }}
                          disabled={disableDepartmentDropDown}
                        >
                          <option value={formik.values[props.field.name]}>
                            {formik.values[props.field.name]
                              ? formik.values[props.field.name]
                              : "Select"}
                          </option>
                          {deptDataRelativeAdmYear.map((item, index) => {
                            return (
                              <option key={index} value={item.departmentCode}>
                                {item.departmentName}
                              </option>
                            );
                          })}
                        </select>
                      );
                    }}
                  </Field>
                  <div className="errorMessage">
                    <ErrorMessage name="departmentCode" />
                  </div>
                </div>

                <div className="filter">
                  <label htmlFor="semisterLabel">Semister</label>
                  <Field id="semisterLabel" type="text" name="semister">
                    {(props) => {
                      return (
                        <select
                          className="form-control"
                          name={props.field.name}
                          onBlur={props.field.onBlur}
                          onChange={(e) => {
                            semesterChange(e);
                            props.field.onChange(e);
                          }}
                          disabled={disableSemisterDropDown}
                        >
                          <option value={formik.values[props.field.name]}>
                            {formik.values[props.field.name]
                              ? formik.values[props.field.name]
                              : "Select"}
                          </option>
                          {semesterDataRelativeAdmissionCode.map(
                            (item, index) => {
                              return (
                                <option key={index} value={item}>
                                  {item}
                                </option>
                              );
                            }
                          )}
                        </select>
                      );
                    }}
                  </Field>
                  <div className="errorMessage">
                    <ErrorMessage name="semister" />
                  </div>
                </div>

                <div className="filter">
                  <label htmlFor="semisterLabel">Division</label>
                  <Field id="semisterLabel" type="text" name="division">
                    {(props) => {
                      return (
                        <select
                          className="form-control"
                          name={props.field.name}
                          onBlur={props.field.onBlur}
                          onChange={(e) => {
                            divisionChange(e);
                            props.field.onChange(e);
                          }}
                          disabled={disableDivisionDropDown}
                        >
                          <option value={formik.values[props.field.name]}>
                            {formik.values[props.field.name]
                              ? formik.values[props.field.name]
                              : "Select"}
                          </option>
                          {divisionsRelativeSemesterAdmCode.map(
                            (item, index) => {
                              return (
                                <option key={index} value={item}>
                                  {item}
                                </option>
                              );
                            }
                          )}
                        </select>
                      );
                    }}
                  </Field>
                  <div className="errorMessage">
                    <ErrorMessage name="division" />
                  </div>
                </div>

                <div className="filter fil-btn mt-4">
                  <label></label>
                  <button
                    type="submit"
                    className="btn btn-primary mb-2"
                  >
                    GO
                  </button>
                  {scheduleError ? <p>{scheduleError}</p> : <React.Fragment />}
                </div>
              </div>
            </Form>
          );
        }}
      </Formik>
    );
  }
};

// /*eslint eqeqeq: "off"*/
// import React, { useState } from "react";
// import { useSelector, useDispatch } from "react-redux";
// import { Form } from "formik-semantic-ui-react";
// import { Formik, Field, ErrorMessage } from "formik";
// import {
//   getScheduleRespectiveClassId,
//   setClassSelectionValues,
// } from "./store/actions";
// import "../css/student.css";
// import "../css/schedule.css";
// //import { Loader, Dimmer } from "semantic-ui-react";
// import { scheduleActions } from "./store/index";
// export const ScheduleClassId = function () {
//   const [disableDepartmentDropDown, setDepartmentDropDown] = useState(true);
//   const [disableSemisterDropDown, setSemisterDropDown] = useState(true);
//   const [disableDivisionDropDown, setDivisionDropDown] = useState(true);
//   const [admissionCodeSelected, setAdmissionCode] = useState("");
//   const [semesterSelected, setSemester] = useState("");
//   const [classId, setClassId] = useState("");
//   const admissionsData = useSelector((state) => state.batch.admissionsData);
//   const [deptDataRelativeAdmYear, setDeptDataRelativeAdmYear] = useState([]);
//   const [
//     semesterDataRelativeAdmissionCode,
//     setSemesterDataRelativeAdmissionCode,
//   ] = useState([]);

//   const [
//     divisionsRelativeSemesterAdmCode,
//     setDivisionsRelativeSemesterAdmCode,
//   ] = useState([]);
//   let admissionsYear = admissionsData.map((item) => item.admissionYear);
//   admissionsYear = [...new Set(admissionsYear)];
//   const classesData = useSelector((state) => state.class.classData);
//   const scheduleError = useSelector((state) => state.class.scheduleError);
//   const [
//     classDataRelativeAdmissionCode,
//     setClassDataRelativeAdmissionCode,
//   ] = useState([]);
//   const universityCode = useSelector((state) => state.auth.universityCode);
//   const classValidation = function (formValues) {
//     const errors = {};
//     if (!formValues.admissionYear) {
//       errors.admissionYear = "* Please select a admission Year";
//     }
//     if (!formValues.departmentCode) {
//       errors.departmentCode = "* Please select a Department Code";
//     }
//     if (!formValues.semister) {
//       errors.semister = "* Please select a Semister";
//     }
//     if (!formValues.division) {
//       errors.division = "* Please select a Division";
//     }
//     return errors;
//   };
//   const dispatch = useDispatch();

//   const admissionsYearChange = function (e) {
//     if (e.target.value !== "") {
//       let filteradmissionData = admissionsData.filter(
//         (item) => item.admissionYear == e.target.value
//       );
//       if (filteradmissionData.length > 0) {
//         setDeptDataRelativeAdmYear(filteradmissionData);
//         setDepartmentDropDown(false);
//       }
//     }
//   };

//   const departmentCodeChange = function (e) {
//     if (e.target.value !== "") {
//       let admissionCodeSelected = deptDataRelativeAdmYear.find(
//         (item) => item.departmentCode === e.target.value
//       );
//       if (admissionCodeSelected) {
//         setAdmissionCode(admissionCodeSelected.admissionCode);
//         let tempClassesData = classesData.filter(
//           (classItem) =>
//             classItem.admissionCode == admissionCodeSelected.admissionCode
//         );
//         if (tempClassesData.length > 0) {
//           setClassDataRelativeAdmissionCode(tempClassesData);
//           let tempSemester = tempClassesData.map((item) => item.semester);
//           tempSemester = [...new Set(tempSemester)];
//           setSemesterDataRelativeAdmissionCode(tempSemester);
//           setSemisterDropDown(false);
//         }
//       }
//     }
//   };

//   const semesterChange = function (e) {
//     if (e.target.value != "") {
//       setSemester(e.target.value);
//       let tempDivisionData = classDataRelativeAdmissionCode.filter((item) => {
//         return (
//           item.admissionCode == admissionCodeSelected &&
//           item.semester == e.target.value
//         );
//       });
//       if (tempDivisionData.length > 0) {
//         let divisions = tempDivisionData.map((item) => item.division);
//         divisions = [...new Set(divisions)];
//         setDivisionsRelativeSemesterAdmCode(divisions);
//         setDivisionDropDown(false);
//       }
//     }
//   };

//   const divisionChange = function (e) {
//     if (e.target.value != "") {
//       let tempClassId = classDataRelativeAdmissionCode.find((item) => {
//         return (
//           item.admissionCode == admissionCodeSelected &&
//           item.semester == semesterSelected &&
//           item.division == e.target.value
//         );
//       });

//       if (tempClassId) {
//         setClassId(tempClassId["idClass"]);
//       }
//     }
//   };

//   const [initialValues, setInitialValues] = useState({
//     admissionYear: "",
//     departmentCode: "",
//     semister: "",
//     division: "",
//   });
//   console.log("initialValues", initialValues);
//   const spinner = useSelector((state) => state.schedule.spinner);
//   if (spinner) {
//     return (
//       <div className="ui active inverted dimmer">
//         <div className="ui loader"></div>
//       </div>
//     );
//   } else {
//     return (
//       <Formik
//         enableReinitialize
//         initialValues={initialValues}
//         validate={classValidation}
//         onSubmit={(formValues, actions, argument2) => {
//           actions.setSubmitting(true);

//           setInitialValues({ ...formValues });
//           dispatch(scheduleActions.setClassDetails({ idClass: classId }));
//           dispatch(scheduleActions.scheduleData({ scheduleData: [] }));
//           dispatch(setClassSelectionValues(formValues));
//           dispatch(getScheduleRespectiveClassId(classId, universityCode));
//         }}
//       >
//         {(formik) => {
//           return (
//             <Form setSubmitting={true}>
//               <div className="filter-wrapper">
//                 <div className="filter">
//                   <label htmlFor="admissionYearLabel">Year</label>
//                   <Field
//                     value={formik.values.admissionYear}
//                     id="admissionYearLabel"
//                     type="text"
//                     name="admissionYear"
//                   >
//                     {(props) => {
//                       return (
//                         <select
//                           {...props.fields}
//                           className="form-control"
//                           name={props.field.name}
//                           onBlur={props.field.onBlur}
//                           onChange={(e) => {
//                             admissionsYearChange(e);
//                             props.field.onChange(e);
//                           }}
//                         >
//                           <option value={formik.values[props.field.name]}>
//                             {formik.values[props.field.name]
//                               ? formik.values[props.field.name]
//                               : "Select"}
//                           </option>
//                           {admissionsYear.map((year, index) => {
//                             return (
//                               <option key={index} value={year}>
//                                 {year}
//                               </option>
//                             );
//                           })}
//                         </select>
//                       );
//                     }}
//                   </Field>
//                   <ErrorMessage name="admissionYear" />
//                 </div>
//                 <div className="filter">
//                   <label htmlFor="departmentLabel">Department Name</label>
//                   <Field id="departmentLabel" type="text" name="departmentCode">
//                     {(props) => {
//                       return (
//                         <select
//                           className="form-control"
//                           name={props.field.name}
//                           onBlur={props.field.onBlur}
//                           onChange={(e) => {
//                             departmentCodeChange(e);
//                             props.field.onChange(e);
//                           }}
//                           disabled={disableDepartmentDropDown}
//                         >
//                           <option value={formik.values[props.field.name]}>
//                             {formik.values[props.field.name]
//                               ? formik.values[props.field.name]
//                               : "Select"}
//                           </option>
//                           {deptDataRelativeAdmYear.map((item, index) => {
//                             return (
//                               <option key={index} value={item.departmentCode}>
//                                 {item.departmentCode}
//                               </option>
//                             );
//                           })}
//                         </select>
//                       );
//                     }}
//                   </Field>
//                   <ErrorMessage name="departmentCode" />
//                 </div>

//                 <div className="filter">
//                   <label htmlFor="semisterLabel">Semister</label>
//                   <Field id="semisterLabel" type="text" name="semister">
//                     {(props) => {
//                       return (
//                         <select
//                           className="form-control"
//                           name={props.field.name}
//                           onBlur={props.field.onBlur}
//                           onChange={(e) => {
//                             semesterChange(e);
//                             props.field.onChange(e);
//                           }}
//                           disabled={disableSemisterDropDown}
//                         >
//                           <option value={formik.values[props.field.name]}>
//                             {formik.values[props.field.name]
//                               ? formik.values[props.field.name]
//                               : "Select"}
//                           </option>
//                           {semesterDataRelativeAdmissionCode.map(
//                             (item, index) => {
//                               return (
//                                 <option key={index} value={item}>
//                                   {item}
//                                 </option>
//                               );
//                             }
//                           )}
//                         </select>
//                       );
//                     }}
//                   </Field>
//                   <ErrorMessage name="semister" />
//                 </div>

//                 <div className="filter">
//                   <label htmlFor="semisterLabel">Division</label>
//                   <Field id="semisterLabel" type="text" name="division">
//                     {(props) => {
//                       return (
//                         <select
//                           className="form-control"
//                           name={props.field.name}
//                           onBlur={props.field.onBlur}
//                           onChange={(e) => {
//                             divisionChange(e);
//                             props.field.onChange(e);
//                           }}
//                           disabled={disableDivisionDropDown}
//                         >
//                           <option value={formik.values[props.field.name]}>
//                             {formik.values[props.field.name]
//                               ? formik.values[props.field.name]
//                               : "Select"}
//                           </option>
//                           {divisionsRelativeSemesterAdmCode.map(
//                             (item, index) => {
//                               return (
//                                 <option key={index} value={item}>
//                                   {item}
//                                 </option>
//                               );
//                             }
//                           )}
//                         </select>
//                       );
//                     }}
//                   </Field>
//                   <ErrorMessage name="division" />
//                 </div>

//                 <div className="filter fil-btn">
//                   <label></label>
//                   <button
//                     type="submit"
//                     className="btn btn-primary mb-2"
//                     style={{ backgroundColor: "#4cadad" }}
//                   >
//                     GO
//                   </button>
//                   {scheduleError ? <p>{scheduleError}</p> : <React.Fragment />}
//                 </div>
//               </div>
//             </Form>
//           );
//         }}
//       </Formik>
//     );
//   }
// };
