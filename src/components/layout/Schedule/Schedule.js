/*eslint eqeqeq: "off"*/
import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import moment from "moment";
import { CreateSchedule } from "./CreateSchedule";
// import { ProcessScheduleData } from "./store/actions";
import "../css/student.css";
import "../css/schedule.css";
import logo from "../image/attendance.png";
import { ScheduleClassId } from "./ScheduleClassId";
import { Task } from "./Task";
import Calendar from "react-calendar";
import { scheduleActions } from "./store/index";
import { MyProfile } from "../Profile/Profile";
import { ScheduleLocation } from "./ScheduleLocation";
import { ScheduleProfessor } from "./ScheduleProfessor";

export const Schedule = function () {
  const userName = useSelector((state) => state.auth.myProfile.userName);
  const userRole = useSelector((state) => state.auth.myProfile.userRole);

  const [myProfile, setMyProfile] = useState(false);
  const [taskHidden, setTaskHidden] = useState(true);
  const [processedDateData, setProcessedDateData] = useState({}); //contains the date related schedule
  const [selectedDate, setSelectedDate] = useState(new Date());
  const [scheduleFilter, setScheduleFilter] = useState("Class");
  const [createSchedulePopUp, setCreateSchedulePopUp] = useState(false);
  let profileImage = useSelector((state) => state.auth.profileImage);
  if (profileImage == "") {
    profileImage = logo;
  }
  const dispatch = useDispatch();
  const scheduleData = useSelector((state) => state.schedule.scheduleData);
  const taskDeleted = useSelector((state) => state.schedule.taskDeleted);
  //console.log("taskDeleted", taskDeleted);
  if (taskDeleted) {
    setTimeout(() => {
      setTaskHidden(true);
    }, 2000);
  }
  useEffect(() => {
    let todayDay = new Date();
    if (selectedDate != todayDay) {
      let newDateScheduleObject =
        scheduleData[moment(selectedDate).format("YYYY-MM-DD")] || {};
      setProcessedDateData(newDateScheduleObject);
    } else {
      setProcessedDateData(
        scheduleData[moment(new Date()).format("YYYY-MM-DD")] || {}
      );
    }
  }, [scheduleData, selectedDate]);

  const [filteredScheduleData, setFilteredScheduleData] = useState([]);
  useEffect(() => {
    if (Object.keys(scheduleData).length > 0) {
      setFilteredScheduleData(scheduleData);
    }
  }, [scheduleData]);

  const renderClass = (item, top, height, values) => {
    let subjectName = "",
      startTime = "",
      endTime = "",
      minutes = "",
      location = "",
      profName = "";
    if (height !== 0) {
      subjectName = values.subjectName;
      startTime = moment(values.modifiedWebStartTime).format("h:mm");
      endTime = moment(values.modifiedWebEndTime).format("h:mm");
      minutes = moment
        .duration(
          moment(values.modifiedWebEndTime).diff(
            moment(values.modifiedWebStartTime)
          )
        )
        .asMinutes();
      location = values.location;
      profName = values.professorName;
    }
    return (
      <div
        style={{ display: height ? "" : "none" }}
        className="row timeline-row"
        onClick={() => {
          //setTaskDetails(values);
          setTaskHidden(true);
          setTimeout(() => {
            setTaskHidden(false);
            dispatch(scheduleActions.setTaskDetails({ taskDetails: values }));
          }, 200);
        }}
      >
        <div className="col-md-12 timeline-pad" style={{ top: top + "px" }}>
          <div
            className="timeline-inner"
            id={item % 2 ? "cat-2" : "cat-1"}
            style={{ height: height - 1 + "px" }}
          >
            <div className="t-block">
              <p>
                <strong>{subjectName}</strong>
              </p>
              <p>
                {startTime} - {endTime} ({minutes} minutes)
              </p>
            </div>
            <div className="t-block">
              <p>
                <small>{location}</small>
              </p>
              <p>{profName}</p>
            </div>
          </div>
        </div>
      </div>
    );
  };
  const renderLi = (item) => {
    return (
      <li>
        <span></span>
        {item.data.map((values) =>
          renderClass(item.item, values.top, values.height, values)
        )}
        <div className="time">
          <span>{item.item}</span>
        </div>
      </li>
    );
  };

  const renderList = () => {
    let dateList = processedDateData;
    let newList = [];
    Object.keys(dateList).forEach((item) => {
      newList.push({ item, data: dateList[item] });
    });

    // this is how the list component is built out.
    // let list = [
    //   { item: 8, data: [{ top: 0, height: 0 }] },
    //   { item: 9, data: [{ top: 30, height: 60 }] },
    //   { item: 10, data: [{ top: 30, height: 150 }] },
    //   { item: 11, data: [{ top: 30, height: 0 }] },
    //   { item: 12, data: [{ top: 30, height: 0 }] },
    //   { item: 13, data: [{ top: 30, height: 60 }] },
    //   { item: 14, data: [{ top: 0, height: 0 }] },
    //   { item: 15, data: [{ top: 30, height: 60 }] },
    //   { item: 16, data: [{ top: 30, height: 60 }] },
    // ];
    return (
      <ul>
        {newList.map((item) => {
          return renderLi(item);
        })}
      </ul>
    );
  };

  /*
  we have a variable called schedule filter the dropdown decides its values
  based on its value we present different forms to show schedule filter
  */

  const incrementDate = () => {
    let value = selectedDate;
    let currentMomentDate = new moment(new Date(value));
    let incrementedDate = currentMomentDate.add(1, "days");

    setTaskHidden(true);
    //setSelectedDate(moment(incrementedDate).format("YYYY-MM-DD"));
    setSelectedDate(new Date(incrementedDate));
    setProcessedDateData(
      filteredScheduleData[moment(incrementedDate).format("YYYY-MM-DD")] || {}
    );
  };

  //works in same way as incremented date
  const decrementDate = () => {
    let value = selectedDate;
    let currentMomentDate = new moment(new Date(value));
    let decrementedDate = currentMomentDate.subtract(1, "days");

    setTaskHidden(true);
    setSelectedDate(new Date(decrementedDate));
    //setSelectedDate(moment(decrementedDate).format("YYYY-MM-DD"));
    setProcessedDateData(
      filteredScheduleData[moment(decrementedDate).format("YYYY-MM-DD")] || {}
    );
  };
  // const spinner = useSelector((state) => state.schedule.spinner);
  // if (spinner) {
  //   return (
  //     <div className="ui active inverted dimmer">
  //       <div className="ui loader"></div>
  //     </div>
  //   );
  // } else
  if (myProfile) {
    return <MyProfile open={myProfile} onClose={() => setMyProfile(false)} />;
  } else {
    return (
      <div className="main-content bg-light">
        <header>
          <h4>
            <label htmlFor="nav-toggel" className="hamburgerIcon">
              <i className="fa fa-bars" aria-hidden="true"></i>
            </label>
            <span className="name">Schedule</span>
          </h4>

          <div className="search-wrapper">
            <span>
              <i className="fa fa-search" aria-hidden="true"></i>
            </span>
            <input type="search" placeholder="Search here" />
          </div>

          <div className="user-wrapper" onClick={() => setMyProfile(true)}>
            <img src={profileImage} width="50px" height="50px" alt="" />
            <div>
              <h6>{userName}</h6>
              <small>{userRole}</small>
            </div>
          </div>
        </header>
        <main>
          <div className="filterTopParent">
            <div className="filter-wrapper">
              <div className="filterLeft">
                <label htmlFor="dropDownSelect">Schedule Filter</label>
                <div className="scheduleField">
                  <select
                    className="form-control"
                    onChange={(e) => {
                      setScheduleFilter(e.target.value);
                      setTaskHidden(true);
                      dispatch(
                        scheduleActions.scheduleData({
                          scheduleData: {},
                        })
                      );
                    }}
                  >
                    <option value={"Class"}>Class</option>
                    <option value={"Professor"}>Professor</option>
                    <option value={"Location"}>Location</option>
                  </select>
                </div>
              </div>
              <div className="filterRight">
                <button
                  className="btn btn-primary mb-2"
                  onClick={() => {
                    setCreateSchedulePopUp(true);
                    setTaskHidden(true);
                  }}
                >
                  Create Schedule
                </button>
              </div>
              {createSchedulePopUp ? (
                <CreateSchedule
                  open={createSchedulePopUp}
                  onClose={() => setCreateSchedulePopUp(false)}
                />
              ) : (
                <React.Fragment />
              )}
            </div>
            {scheduleFilter === "Class" ? (
              <ScheduleClassId />
            ) : (
              <React.Fragment />
            )}

            {scheduleFilter === "Professor" ? (
              <ScheduleProfessor />
            ) : (
              <React.Fragment />
            )}
            {scheduleFilter === "Location" ? (
              <ScheduleLocation />
            ) : (
              <React.Fragment />
            )}
          </div>

          <div className="schedule-content">
            <div className="row mt-4">
              <div className="col-lg-3">
                <div className="row">
                  <div className="col-12">
                    <div className="calendar" style={{ borderRadius: "5px" }}>
                      <div className="datepicker">
                        <div className="cal-inner-wrapper">
                          <Calendar
                            value={selectedDate}
                            onChange={(e) => {
                              setSelectedDate(new Date(e));
                              setProcessedDateData(
                                filteredScheduleData[
                                  moment(new Date(e)).format("YYYY-MM-DD")
                                ] || {}
                              );
                            }}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-12 mt-5">
                    <div className="category-wrapper">
                      <div className="category">
                        <h4>Category</h4>
                      </div>
                      <div
                        className="search-wrapper mt-4"
                        style={{ paddingLeft: "15px", borderRadius: "10px" }}
                      >
                        <input type="search" placeholder="Search" />
                      </div>

                      <div className="category-list">
                        <ul>
                          <li>
                            <label className="container">
                              Select All
                              <input type="checkbox" />
                              <span className="checkmark"></span>
                            </label>
                          </li>
                          <li>
                            <label className="container">
                              Batch
                              <input type="checkbox" />
                              <span className="checkmark " id="cat-1"></span>
                            </label>
                          </li>
                          <li>
                            <label className="container">
                              Course
                              <input type="checkbox" />
                              <span className="checkmark" id="cat-2"></span>
                            </label>
                          </li>
                          <li>
                            <label className="container">
                              ClassName
                              <input type="checkbox" />
                              <span className="checkmark" id="cat-3"></span>
                            </label>
                          </li>
                          <li>
                            <label className="container">
                              Division
                              <input type="checkbox" />
                              <span className="checkmark" id="cat-4"></span>
                            </label>
                          </li>
                          <li>
                            <label className="container">
                              Teaching Staff
                              <input type="checkbox" />
                              <span className="checkmark" id="cat-5"></span>
                            </label>
                          </li>
                          <li>
                            <label className="container">
                              Type of ClassName
                              <input type="checkbox" />
                              <span className="checkmark" id="cat-6"></span>
                            </label>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className={taskHidden ? "col-lg-9" : "col-lg-6"}>
                <div className="timeline timelineHome">
                  <div className="timeline-head">
                    <div className="timeline-heading">
                      <h4>{moment(selectedDate).format("MMMM Do YYYY")}</h4>
                    </div>
                    <div className="date-inc">
                      <button onClick={decrementDate} className="calBtn">
                        {/* <img src={"../image/left-arrow.png"} alt="" /> */}
                        <svg viewBox="0 0 512 512">
                          <path d="M353,450a15,15,0,0,1-10.61-4.39L157.5,260.71a15,15,0,0,1,0-21.21L342.39,54.6a15,15,0,1,1,21.22,21.21L189.32,250.1,363.61,424.39A15,15,0,0,1,353,450Z"/>
                        </svg>
                      </button>
                      <button onClick={incrementDate} className="calBtn right">
                        <svg viewBox="0 0 512 512">
                          <path d="M353,450a15,15,0,0,1-10.61-4.39L157.5,260.71a15,15,0,0,1,0-21.21L342.39,54.6a15,15,0,1,1,21.22,21.21L189.32,250.1,363.61,424.39A15,15,0,0,1,353,450Z"/>
                        </svg>
                      </button>
                    </div>
                  </div>
                  {renderList()}
                </div>
              </div>

              {!taskHidden ? <Task /> : <React.Fragment />}
            </div>
          </div>
        </main>
      </div>
    );
  }
};
