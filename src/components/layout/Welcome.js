import { useHistory } from "react-router-dom";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { logOutAmplify } from "../authorization/store/actions";
import "./css/student.css";
import { Schedule } from "./Schedule/Schedule";
import { Students } from "./Students/Students";
import { Departments } from "./Departments/Departments";
import { Batch } from "./Batch/Batch";
import { Class } from "./Class/Class";
import { Locations } from "./Locations/Locations";
import { Contacts } from "./Contacts/Contacts";
import { Subjects } from "./Subjects/Subjects";
import { Result } from "./Result/Result";
import { Help } from "./Help/Help";
import { Link } from "react-router-dom";
import { Professors } from "./Professors/Professors";
import scheduleImage from "./image/schedule.png";
import attendanceImage from "./image/attendance.png";
import logoImage from "./image/campus-logo.png";
import { scheduleActions } from "./Schedule/store/index";
export const Welcome = function () {
  const [selected, setSelected] = useState("schedule");
  const [selectedSub, setSelectedSub] = useState("");
  const dispatch = useDispatch();
  let history = useHistory();

  const changeFunction = (name) => {
    dispatch(scheduleActions.scheduleData({ scheduleData: [] }));
    setSelectedSub("");
    if (name === "schedule") {
      setSelected("schedule");
    }
    if (name === "attendance") {
      setSelected("attendance");
    }
    if (name === "examresult") {
      setSelected("examresult");
    }
    if (name === "studentData") {
      setSelected("studentData");
    }

    if (name === "notification") {
      setSelected("notification");
    }

    if (name === "help") {
      setSelected("help");
    }
    if (name === "result") {
      setSelected("result");
    }
    if (name === "contacts") {
      setSelected("contacts");
    }
    if (name === "studentcorner") {
      setSelected("studentcorner");
    }
    if (name === "aluminidata") {
      setSelected("aluminidata");
    }

    if (name === "professorData") {
      setSelected("professorData");
    }

    if (name === "createDepartments") {
      setSelectedSub("create");
      setSelected("createDepartments");
    }
    if (name === "createBatch") {
      setSelectedSub("create");
      setSelected("createBatch");
    }
    if (name === "createClass") {
      setSelectedSub("create");
      setSelected("createClass");
    }
    if (name === "createSubject") {
      setSelectedSub("create");
      setSelected("createSubject");
    }
    if (name === "createLocation") {
      setSelectedSub("create");
      setSelected("createLocation");
    }
    if (name === "logout") {
      dispatch(logOutAmplify(history));
    }
  };

  const changeSubFunction = (name) => {
    dispatch(scheduleActions.scheduleData({ scheduleData: [] }));
    if (name === "create") {
      name = (selectedSub!==name) ? name : "";
      setSelectedSub(name);
    }
  };

  return (
    <div className="body">
      <input type="checkbox" id="nav-toggel" />
      <div className="sidebar">
        <div className="sidebar-brand">
          <h3>
            <img src={logoImage} alt="" />
            <span>Campus Live</span>
          </h3>
        </div>
        <div className="sidebar-menu">
          <ul>
            <li>
            <Link to="/welcome">
              <button
                id={selected === "schedule" ? "active" : ""}
                onClick={() => changeFunction("schedule")}
              >
                <i>
                <svg viewBox="0 0 20 20">
                  <path d="M1 4c0-1.1.9-2 2-2h14a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V4zm2 2v12h14V6H3zm2-6h2v2H5V0zm8 0h2v2h-2V0zM5 9h2v2H5V9zm0 4h2v2H5v-2zm4-4h2v2H9V9zm0 4h2v2H9v-2zm4-4h2v2h-2V9zm0 4h2v2h-2v-2z"/>
                </svg>
                </i>
                <span>Schedule</span>
              </button>
            </Link>
            </li>
            <li>
            <Link to="/welcome">
              <button
                id={selected === "attendance" ? "active" : ""}
                onClick={() => changeFunction("attendance")}
              >
                <i>
                <svg viewBox="0 0 18 20">
                  <path d="M16,2 L15,2 L15,0 L13,0 L13,2 L5,2 L5,0 L3,0 L3,2 L2,2 C0.9,2 0,2.9 0,4 L0,18 C0,19.1 0.9,20 2,20 L16,20 C17.1,20 18,19.1 18,18 L18,4 C18,2.9 17.1,2 16,2 L16,2 Z M16,18 L2,18 L2,7 L16,7 L16,18 L16,18 Z" id="Shape"/>
                  <rect height="5" id="Rectangle-path" width="5" x="4" y="9"/>
                </svg>
                </i>
                <span>Attendance</span>
              </button>
            </Link>
            </li>
            <li>
              <button
                id={selected === "examresult" ? "active" : ""}
                onClick={() => changeFunction("examresult")}
              >
                <i>
                <svg viewBox="0 0 512 512">
                  <path d="M417.875,46H94.2126A13.2576,13.2576,0,0,0,81,59.2126V452.875A13.1835,13.1835,0,0,0,94.2126,466H417.875A13.1072,13.1072,0,0,0,431,452.875V59.2126A13.1814,13.1814,0,0,0,417.875,46ZM238.5,396.3493H151a13.125,13.125,0,0,1,0-26.25h87.5C255.7308,370.3674,255.7757,396.0737,238.5,396.3493Zm0-96.25H151a13.125,13.125,0,0,1,0-26.25h87.5C255.7308,274.1174,255.7757,299.8237,238.5,300.0993ZM273.85,194.75c-.2563,17.2351-25.9829,17.2672-26.25-.0011V126.7623a41.3874,41.3874,0,0,1,82.7747,0V194.75c-.282,17.2458-25.968,17.265-26.25-.0011V176.2866H273.85Zm96.4252,176.2241-43.0493,43.05a12.6979,12.6979,0,0,1-9.2755,3.8506,12.9869,12.9869,0,0,1-9.2755-3.8506l-18.2007-18.2c-11.918-12.3474,6.2036-30.6357,18.551-18.55l8.9252,8.9241,33.7738-33.7738C364.2043,340.5007,382.311,358.6587,370.2755,370.9741Zm0-96.25-43.0493,43.05a12.6979,12.6979,0,0,1-9.2755,3.8506,12.9869,12.9869,0,0,1-9.2755-3.8506l-18.2007-18.2c-11.918-12.3474,6.2036-30.6357,18.551-18.55l8.9252,8.9241,33.7738-33.7738C364.2043,244.2507,382.311,262.4087,370.2755,274.7241Z"/>
                  <path d="M304.125,126.7623a15.1374,15.1374,0,0,0-30.2747,0v23.2743H304.125Z"/>
                </svg>
                </i>
                <span>Exam Result </span>
              </button>
            </li>
            <li>
              <button
                id={selected === "notification" ? "active" : ""}
                onClick={() => changeFunction("notification")}
              >
                <i>
                <svg viewBox="1 -1 100 100">
                  <path d="M82.5,57.9V37.6c0-17.5-14.1-31.6-31.6-31.6S19.4,20.1,19.4,37.6v20.3l-8.2,16.2h79.7L82.5,57.9z"/>
                  <path d="M50.9,92.1c7.3,0,13.3-6,13.3-13.3H37.6C37.6,86.1,43.6,92.1,50.9,92.1z"/>
                </svg>
                </i>
                <span>Notification</span>
              </button>
            </li>
            <li>
              <button
                id={selected === "studentData" ? "active" : ""}
                onClick={() => changeFunction("studentData")}
              >
                <i>
                <svg viewBox="0 0 24 24">
                  <polygon points="12,3 1,9 12,15 23,9 "/>
                  <polygon points="19,12.8 12,17 5,12.8 5,17.2 12,21 19,17.2 "/>
                </svg>
                </i>
                <span>Student Data</span>
              </button>
            </li>
            <li>
              <button
                id={selected === "professorData" ? "active" : ""}
                onClick={() => changeFunction("professorData")}
              >
                <i>
                <svg viewBox="0 0 500 500">
                  <path d="M415.762,346.214c-19.078-24.896-42.156-41.063-75.223-50.236  l-30.983,108.475c0,9.992-8.181,18.172-18.172,18.172c-9.988,0-18.169-8.18-18.169-18.172v-86.311  c0-12.536-10.178-22.715-22.715-22.715c-12.536,0-22.713,10.179-22.713,22.715v86.311c0,9.992-8.181,18.172-18.17,18.172  c-9.992,0-18.173-8.18-18.173-18.172l-30.983-108.475c-33.068,9.262-56.145,25.34-75.221,50.236  c-7.542,9.812-11.64,29.527-11.908,40.07c0.09,2.725,0,5.906,0,9.082v18.172v18.173c0,20.078,16.264,36.34,36.343,36.34h281.648  c20.078,0,36.345-16.262,36.345-36.34v-18.173v-18.172c0-3.176-0.089-6.357,0-9.082C427.393,375.741,423.3,356.025,415.762,346.214z   M155.102,128.073c0,53.059,33.078,131.013,95.398,131.013c61.237,0,95.396-77.954,95.396-131.013  c0-53.057-42.702-96.124-95.396-96.124C197.806,31.949,155.102,75.016,155.102,128.073z" />
                </svg>
                </i>
                <span>Professor Data</span>
              </button>
            </li>
            <li>
              <button
                id={selected === "studentcorner" ? "active" : ""}
                onClick={() => changeFunction("studentcorner")}
              >
                <i>
                <svg viewBox="0 0 32 32">
                  <path d="M23.8,2H8.2C6.5,2,5.1,3.4,5.1,5.1v24.6c0,0.2,0.2,0.3,0.4,0.3l10.4-4.5c0.1,0,0.2,0,0.2,0  L26.6,30c0.2,0.1,0.4-0.1,0.4-0.3V5.1C26.9,3.4,25.5,2,23.8,2z"/>
                </svg>
                </i>
                <span>Student Corner</span>
              </button>
            </li>
            <li>
              <button
                id={selected === "aluminidata" ? "active" : ""}
                onClick={() => changeFunction("aluminidata")}
              >
                <i>
                <svg viewBox="0 0 32 32">
                  <path d="M24.972,12.288C24.608,7.657,20.723,4,16,4c-4.04,0-7.508,2.624-8.627,6.451C4.181,11.559,2,14.583,2,18  c0,4.411,3.589,8,8,8h13c3.86,0,7-3.141,7-7C30,15.851,27.93,13.148,24.972,12.288z M20,18h-8c-0.552,0-1-0.447-1-1  c0-0.552,0.448-1,1-1h8c0.552,0,1,0.448,1,1C21,17.553,20.552,18,20,18z"/>
                </svg>
                </i>
                <span>Alumni Data</span>
              </button>
            </li>
            <li>
              <button
                id={selected === "help" ? "active" : ""}
                onClick={() => changeFunction("help")}
              >
                <i>
                <svg viewBox="0 0 512 512">
                  <path d="M256,32C132.3,32,32,132.3,32,256s100.3,224,224,224c123.7,0,224-100.3,224-224S379.7,32,256,32z M276.2,358.7   c-0.5,17.8-13.7,28.8-30.8,28.3c-16.4-0.5-29.3-12.2-28.8-30.1c0.5-17.8,14.1-29.1,30.5-28.6C264.3,328.8,276.8,340.9,276.2,358.7z    M324.9,231.4c-4.2,5.9-13.6,13.5-25.4,22.7l-13.1,9c-6.4,4.9-10.4,10.7-12.5,17.3c-1.1,3.5-1.9,12.6-2.1,18.7   c-0.1,1.2-0.8,3.9-4.5,3.9c-3.7,0-35,0-39.1,0c-4.1,0-4.6-2.4-4.5-3.6c0.6-16.6,3-30.3,9.9-41.3c9.3-14.8,35.5-30.4,35.5-30.4   c4-3,7.1-6.2,9.5-9.7c4.4-6,8-12.7,8-19.9c0-8.3-2-16.2-7.3-22.8c-6.2-7.7-12.9-11.4-25.8-11.4c-12.7,0-20.1,6.4-25.4,14.8   c-5.3,8.4-4.4,18.3-4.4,27.3H175c0-34,8.9-55.7,27.7-68.5c12.7-8.7,28.9-12.5,47.8-12.5c24.8,0,44.5,4.6,61.9,17.8   c16.1,12.2,24.6,29.4,24.6,52.6C337,209.7,332,221.7,324.9,231.4z"/>
                </svg>
                </i>
                <span>Help/FAQ</span>
              </button>
            </li>
            <li>
              <button
                id={selected === "contacts" ? "active" : ""}
                onClick={() => changeFunction("contacts")}
              >
                <i>
                <svg viewBox="0 0 20 20">
                    <path d="M6 4H5a1 1 0 1 1 0-2h11V1a1 1 0 0 0-1-1H4a2 2 0 0 0-2 2v16c0 1.1.9 2 2 2h12a2 2 0 0 0 2-2V5a1 1 0 0 0-1-1h-7v8l-2-2-2 2V4z"/>
                  </svg>
                </i>
                <span>Contacts</span>
              </button>
            </li>
            <li className={selectedSub === "create" ? "withSubItems active" : "withSubItems"}>
              <button
                onClick={() => changeSubFunction("create")}
              >
                <i>
                <svg viewBox="0 0 32 32">
                  <path d="M23,9H9C8.6,9,8.2,8.7,8,8.3s0-0.9,0.4-1.1l7-5c0.3-0.2,0.8-0.2,1.2,0l7,5c0.4,0.3,0.5,0.7,0.4,1.1S23.4,9,23,9z"/>
                  <path d="M21,18H11c-0.6,0-1-0.4-1-1v-5c0-0.6,0.4-1,1-1h10c0.6,0,1,0.4,1,1v5C22,17.6,21.6,18,21,18z"/>
                  <path d="M9,31H5c-0.6,0-1-0.4-1-1v-9c0-0.6,0.4-1,1-1h9c0.6,0,1,0.4,1,1v4c0,0.6-0.4,1-1,1c-2.2,0-4,1.8-4,4C10,30.6,9.6,31,9,31z"/>
                  <path d="M27,31h-4c-0.6,0-1-0.4-1-1c0-2.2-1.8-4-4-4c-0.6,0-1-0.4-1-1v-4c0-0.6,0.4-1,1-1h9c0.6,0,1,0.4,1,1v9   C28,30.6,27.6,31,27,31z"/>
                </svg>
                </i>
                <span>Create New</span>
              </button>
        <div className={selectedSub === "create" ? "collapse show" : "collapse"}>
          <ul className="btn-toggle-nav list-unstyled fw-normal pb-1 small">
            <li>
                <button
                  id={selected === "createDepartments" ? "active" : ""}
                  onClick={() => changeFunction("createDepartments")}
                >
                  <span>Create Departments</span>
                </button>
            </li>
            <li>
                <button
                  id={selected === "createBatch" ? "active" : ""}
                  onClick={() => changeFunction("createBatch")}
                >
                  <span>Create Batch</span>
                </button>
              </li>
              <li>
              <button
                id={selected === "createClass" ? "active" : ""}
                onClick={() => changeFunction("createClass")}
              >
                <span>Create Class</span>
              </button>
            </li>
            <li>
              <button
                id={selected === "createLocation" ? "active" : ""}
                onClick={() => changeFunction("createLocation")}
              >
                <span>Create Location</span>
              </button>
            </li>
            <li>
              <button
                id={selected === "createSubject" ? "active" : ""}
                onClick={() => changeFunction("createSubject")}
              >
                <span>Create Subject</span>
              </button>
            </li>
                </ul>
              </div>
            </li>
            <li>
              <button
                className={selected === "logout" ? "active" : ""}
                onClick={() => changeFunction("logout")}
              >
                <i>
                <svg viewBox="0 0 96 96">
                  <path d="M48,36a5.9966,5.9966,0,0,0,6-6V6A6,6,0,0,0,42,6V30A5.9966,5.9966,0,0,0,48,36Z"/>
                  <path d="M76.0078,22.9746a6,6,0,0,0-8.0156,8.93A29.39,29.39,0,0,1,78,54a30,30,0,0,1-60,0A29.39,29.39,0,0,1,28.0078,31.9043a6,6,0,0,0-8.0156-8.93,42.0009,42.0009,0,1,0,56.0156,0Z"/>
                </svg>
                </i>
                <span>Logout</span>
              </button>
            </li>
          </ul>
        </div>
      </div>
      {selected === "schedule" ? <Schedule /> : <React.Fragment />}
      {selected === "attendance" ? <Schedule /> : <React.Fragment />}
      {selected === "examresult" ? <Result /> : <React.Fragment />}
      {selected === "notification" ? <Schedule /> : <React.Fragment />}
      {selected === "studentcorner" ? <Students /> : <React.Fragment />}
      {selected === "studentData" ? <Students /> : <React.Fragment />}
      {selected === "professorData" ? <Professors /> : <React.Fragment />}
      {selected === "aluminidata" ? <Students /> : <React.Fragment />}
      {selected === "help" ? <Help /> : <React.Fragment />}
      {selected === "contacts" ? <Contacts /> : <React.Fragment />}
      {selected === "createDepartments" ? <Departments /> : <React.Fragment />}
      {selected === "createBatch" ? <Batch /> : <React.Fragment />}
      {selected === "createClass" ? <Class /> : <React.Fragment />}
      {selected === "createSubject" ? <Subjects /> : <React.Fragment />}
      {selected === "createLocation" ? <Locations /> : <React.Fragment />}
    </div>
  );
};
