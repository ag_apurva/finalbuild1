import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { downLoadImage } from "../../authorization/store/actions";
import { profileActions } from "./store/index";
import { Storage } from "aws-amplify";
export const UploadProfilePhoto = function () {
  const myProfile = useSelector((state) => state.auth.myProfile);
  const spinner = useSelector((state) => state.profile.spinner);
  const dispatch = useDispatch();
  let userCode = myProfile.userCode;
  let universityCode = myProfile.universityCode;
  const submitSuccessful = useSelector(
    (state) => state.profile.submitSuccessful
  );

  console.log("submitSuccessful", submitSuccessful);
  const handleChange = async (e) => {
    const file = e.target.files[0];
    if (file.size > 1048576) {
      alert("Please image size of less than 1 MB");
    } else {
      try {
        dispatch(profileActions.setSpinner({ spinner: true }));
        dispatch(
          profileActions.setSubmitSuccessfull({ submitSuccessful: true })
        );
        let path = universityCode + "/" + userCode + "/profileImage.jpg";

        await Storage.put(path, file, {
          contentType: "image/jpg",
        });
        dispatch(downLoadImage(path));
        dispatch();
      } catch (e) {
        //console.log("error uploading image", e);
      }
      dispatch(profileActions.setSpinner({ spinner: false }));
      dispatch(
        profileActions.setSubmitSuccessfull({ submitSuccessful: false })
      );
    }
  };

  return (
    <div>
      {spinner ? (
        <div className="ui active inverted dimmer">
          <div className="ui loader"></div>
        </div>
      ) : (
        <React.Fragment />
      )}
      <input
        type="file"
        accept="image/jpg"
        onChange={(evt) => handleChange(evt)}
      />
      Upload Image (less than 1MB)
    </div>
  );
};
