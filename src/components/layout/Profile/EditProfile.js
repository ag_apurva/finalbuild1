import { FormField, Button } from "semantic-ui-react";
import { Formik, Field, ErrorMessage } from "formik";
import { Form } from "formik-semantic-ui-react";
import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { saveMyProfile } from "./store/actions";
// import { Loader, Dimmer } from "semantic-ui-react";
export const EditProfile = function () {
  const myProfile = useSelector((state) => state.auth.myProfile);
  const universityCode = useSelector((state) => state.auth.universityCode);
  const dispatch = useDispatch();

  const spinner = useSelector((state) => state.profile.spinner);

  const profile = useSelector((state) => state.profile);
  console.log("profile", profile);
  const submitFunction = function (formValues) {
    let saveMyProfileObject = {
      userEmail: myProfile.userEmail,
      userAddress: formValues.userAddress,
      userDesignation: formValues.userDesignation,
      userName: formValues.userName,
      userPhone: "+" + formValues.userPhone,
      userPincode: formValues.userPincode,
      universityCode: universityCode,
      userCode: myProfile["userCode"],
    };
    dispatch(saveMyProfile(saveMyProfileObject));
  };

  if (spinner) {
    return (
      <React.Fragment>
        <div className="ui active inverted dimmer">
          <div className="ui loader"></div>
        </div>
      </React.Fragment>
    );
  } else {
    return (
      <Formik
        initialValues={{
          userEmail: myProfile.userEmail,
          userName: myProfile.userName,
          userDesignation: myProfile.userDesignation,
          userAddress: myProfile.userAddress,
          userPincode: myProfile.userPincode,
          userPhone: myProfile.userPhone
            ? myProfile.userPhone.toString().slice(1, 11)
            : "",
        }}
        validate={(formValues) => {
          const errors = {};
          if (!formValues.userName) {
            errors.userName = "* Please provide a Name";
          }
          if (!formValues.userDesignation) {
            errors.userDesignation = "* Please provide a Designation";
          }
          if (!formValues.userAddress) {
            errors.userAddress = "* Please provide a Address";
          }
          if (!formValues.userPincode) {
            errors.userPincode = "* Please provide a PinCode";
          }
          if (!formValues.userPhone) {
            errors.userPhone = "* Please provide a PhoneNumber";
          }
          if (formValues.userPincode) {
            let filter = /^[1-9]{1}[0-9]{2}[0-9]{3}$/;
            if (!filter.test(formValues.userPincode)) {
              errors.userPincode = "* Please provide a 6 digit PinCode";
            }
          }
          if (formValues.userPhone) {
            let filter = /^[1-9]{1}[0-9]{2}[0-9]{7}$/;
            if (!filter.test(formValues.userPhone)) {
              errors.userPhone = "* Please provide a 10 digit PhoneNumber";
            }
          }
          return errors;
        }}
        onSubmit={submitFunction}
      >
        <Form>
        <div className="row modelFormUi">
          <FormField className="col-4">
            <label htmlFor="idName">Name</label>
            <Field id="idName" name="userName" />
            <div className="errorMessage"><ErrorMessage name="userName" /></div>
          </FormField>
          <FormField className="col-4">
            <label htmlFor="idEmail">userName/Email</label>
            <div className="errorMessage">
            <Field id="idEmail" type="email" name="userEmail" disabled={true} />
            </div>
          </FormField>
          <FormField className="col-4">
            <label htmlFor="idDesignation">Designation</label>
            <Field id="idDesignation" name="userDesignation" />
            <div className="errorMessage">
            <ErrorMessage name="userDesignation" />
            </div>
          </FormField>
          <FormField className="col-4">
            <label htmlFor="idPhoneNumber">PhoneNumber</label>
            <Field id="idPhoneNumber" name="userPhone" />
            <div className="errorMessage">
            <ErrorMessage name="userPhone" />
            </div>
          </FormField>
          <FormField className="col-4">
            <label htmlFor="idAddress">Address</label>
            <Field id="idAddress" name="userAddress" />
            <div className="errorMessage">
            <ErrorMessage name="userAddress" />
            </div>
          </FormField>
          <FormField className="col-4">
            <label htmlFor="idPinCode">PinCode</label>
            <Field id="idPinCode" name="userPincode" />
            <div className="errorMessage">
            <ErrorMessage name="userPincode" />
            </div>
          </FormField>

          <FormField className="col-12 mt-4">
            <Button type="submit" className="btn btn-primary">Submit</Button>
          </FormField>
          </div>
        </Form>
      </Formik>
    );
  }
};
