/*eslint eqeqeq: "off"*/
import React, { useState } from "react";
import { Modal, Button } from "semantic-ui-react";
import { UploadProfilePhoto } from "./UploadProfilePhoto";
import { EditProfile } from "./EditProfile";
import { useSelector } from "react-redux";
export const MyProfile = function (props) {
  const [editProfile, setEditProfile] = useState("selected");
  const [uploadProfilePhoto, setUploadProfilePhoto] = useState("");
  // const [uploadUniversityLogo, setUploadUniversityLogo] = useState("");
  const submitSuccessful = useSelector(
    (state) => state.profile.submitSuccessful
  );

  const errorCreateProfile = useSelector(
    (state) => state.profile.errorCreateProfile
  );
  if (submitSuccessful) {
    props.onClose();
  }
  return (
    <Modal
      open={props.open}
      onClose={props.onClose}
      dimmer={"inverted"}
      closeIcon
      style={{ overflow: "auto", maxHeight: 600 }}
    >
      <Modal.Header>
        <Button
          className={editProfile === "selected" ? "item active" : "item"}
          onClick={() => {
            setEditProfile("selected");
            setUploadProfilePhoto("");
            //    setUploadUniversityLogo("");
          }}
        >
          EditProfile
        </Button>
        <Button
          className={uploadProfilePhoto === "selected" ? "item active" : "item"}
          onClick={() => {
            setEditProfile("");
            setUploadProfilePhoto("selected");
            //setUploadUniversityLogo("");
          }}
        >
          UploadProfilePhoto
        </Button>
        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
          <span>&times;</span>
        </button>
        {/* <Button
          className={
            uploadUniversityLogo === "selected" ? "item active" : "item"
          }
          onClick={() => {
            setEditProfile("");
            setUploadProfilePhoto("");
            setUploadUniversityLogo("selected");
          }}
        >
          Upload University Logo
        </Button> */}
      </Modal.Header>
      <Modal.Content>
        {editProfile === "selected" ? <EditProfile /> : <React.Fragment />}
        {uploadProfilePhoto === "selected" ? (
          <UploadProfilePhoto />
        ) : (
          <React.Fragment />
        )}
        {/* {uploadUniversityLogo === "selected" ? (
          <UploadProfilePhoto />
        ) : (
          <React.Fragment />
        )} */}
      </Modal.Content>
      <Modal.Actions>
        {errorCreateProfile ? <p>{errorCreateProfile}</p> : <React.Fragment />}
      </Modal.Actions>
    </Modal>
  );
};
