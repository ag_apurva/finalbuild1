import { createSlice } from "@reduxjs/toolkit";

const profileSlice = createSlice({
  name: "profileReducer",
  initialState: {
    submitSuccessful: false,
    spinner: false,
    errorCreateProfile: "",
  },
  reducers: {
    setSpinner(state, action) {
      state.spinner = action.payload.spinner;
    },
    setProfileError(state, action) {
      state.errorCreateProfile = action.payload.errorCreateProfile;
    },
    setSubmitSuccessfull(state, action) {
      state.submitSuccessful = action.payload.submitSuccessful;
    },
  },
});

export const profileActions = profileSlice.actions;
export default profileSlice;
