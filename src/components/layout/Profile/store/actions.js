import { SAVE_PROFILE } from "../../API";
import { authActions } from "../../../authorization/store/index";
import { profileActions } from "./index";

import axios from "axios";

export const saveMyProfile = function (saveMyProfileObject) {
  return async function (dispatch) {
    try {
      dispatch(profileActions.setSpinner({ spinner: true }));

      const data = JSON.stringify({
        userAddress: saveMyProfileObject.userAddress,
        universityCode: saveMyProfileObject.universityCode,
        userCode: saveMyProfileObject.userCode,
        userName: saveMyProfileObject.userName,
        userDesignation: saveMyProfileObject.userDesignation,
        userPhone: saveMyProfileObject.userPhone,
        userPincode: saveMyProfileObject.userPincode,
      });

      const config = {
        method: "post",
        url: SAVE_PROFILE,
        headers: {
          "Content-Type": "application/json",
        },
        data: data,
      };
      let saveProfileData = await axios(config);

      if (!saveProfileData.data.hasOwnProperty("error")) {
        dispatch(authActions.setMyProfile({ myProfile: saveMyProfileObject }));
        dispatch(
          profileActions.setSubmitSuccessfull({
            submitSuccessful: true,
          })
        );

        setTimeout(() => {
          dispatch(
            profileActions.setSubmitSuccessfull({
              submitSuccessful: false,
            })
          );
        }, 200);
      } else {
        dispatch(
          profileActions.setProfileError({
            errorCreateProfile: saveProfileData.data.error,
          })
        );
        setTimeout(() => {
          dispatch(
            profileActions.setProfileError({
              errorCreateProfile: "",
            })
          );
        }, 20000);
        setTimeout(() => {
          dispatch(
            profileActions.setSubmitSuccessfull({ submitSuccessful: true })
          );
        }, 20000);
        setTimeout(() => {
          dispatch(
            profileActions.setSubmitSuccessfull({ submitSuccessful: false })
          );
        }, 20200);
      }
    } catch (e) {
      dispatch(
        profileActions.setProfileError({
          errorCreateProfile: "createSubject.data.error",
        })
      );
      setTimeout(() => {
        dispatch(
          profileActions.setProfileError({
            errorCreateProfile: "",
          })
        );
      }, 20000);
      setTimeout(() => {
        dispatch(
          profileActions.setSubmitSuccessfull({ submitSuccessful: true })
        );
      }, 20000);
      setTimeout(() => {
        dispatch(
          profileActions.setSubmitSuccessfull({ submitSuccessful: false })
        );
      }, 20200);
    }
    dispatch(profileActions.setSpinner({ spinner: false }));
  };
};
