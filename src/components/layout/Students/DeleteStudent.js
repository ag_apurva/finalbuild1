import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { deleteStudent } from "./store/actions";
// import { Loader, Dimmer } from "semantic-ui-react";
export const DeleteStudent = function (props) {
  const editStudent = useSelector((state) => state.students.editStudent);
  const universityCode = useSelector((state) => state.auth.universityCode);
  const studentsData = useSelector((state) => state.students.studentsData);
  const dispatch = useDispatch();
  const onsubmit = function () {
    // console.log("delete student", {
    //   universityCode: universityCode,
    //   studentEmail: editStudent.userEmail,
    // });
    dispatch(
      deleteStudent(
        { universityCode: universityCode, studentEmail: editStudent.userEmail },
        studentsData
      )
    );
  };
  const spinner = useSelector((state) => state.students.spinner);
  if (spinner) {
    return (
      <React.Fragment>
        <div className="ui active inverted dimmer">
          <div className="ui loader"></div>
        </div>
      </React.Fragment>
    );
  } else {
    return (
      <React.Fragment>
        <div className="deleteBlock">
        <p>Are you sure you want to delete {editStudent.name} ? </p>
        <button
          className="btn btn-primary mb-2"
          onClick={() => onsubmit()}
        >
          Delete {editStudent.name}
        </button>
        </div>
      </React.Fragment>
    );
  }
};
