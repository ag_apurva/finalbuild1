/*eslint eqeqeq: "off"*/
import React, { useState } from "react";
import { Formik, Field, ErrorMessage } from "formik";
import { Modal, FormField } from "semantic-ui-react";
import { useSelector, useDispatch } from "react-redux";
import { Form } from "formik-semantic-ui-react";
import { createStudent } from "./store/actions";
import "react-datetime/css/react-datetime.css";
import Datetime from "react-datetime";
import moment from "moment";
import validator from "validator";

const TextError = function (props) {
  return <p style={{ color: "red" }}>{props.children}</p>;
};

export const CreateStudent = function (props) {
  const [division, setDivision] = useState("");
  const [disableDepartmentDropDown, setDepartmentDropDown] = useState(true);
  const [disableSemisterDropDown, setSemisterDropDown] = useState(true);
  const [disableDivisionDropDown, setDivisionDropDown] = useState(true);
  const studentsData = useSelector((state) => state.students.studentsData);
  const [admissionCodeSelected, setAdmissionCode] = useState("");
  const [semesterSelected, setSemester] = useState("");
  const [classId, setClassId] = useState("");
  const admissionsData = useSelector((state) => state.batch.admissionsData);
  const [deptDataRelativeAdmYear, setDeptDataRelativeAdmYear] = useState([]);
  const [
    semesterDataRelativeAdmissionCode,
    setSemesterDataRelativeAdmissionCode,
  ] = useState([]);
  const [
    divisionsRelativeSemesterAdmCode,
    setDivisionsRelativeSemesterAdmCode,
  ] = useState([]);
  let admissionsYear = admissionsData.map((item) => item.admissionYear);
  admissionsYear = [...new Set(admissionsYear)];
  const classesData = useSelector((state) => state.class.classData);
  const [
    classDataRelativeAdmissionCode,
    setClassDataRelativeAdmissionCode,
  ] = useState([]);

  const submitSuccessful = useSelector(
    (state) => state.students.submitSuccessful
  );
  const spinner = useSelector((state) => state.students.spinner);
  const errorCreateStudent = useSelector(
    (state) => state.students.errorCreateStudent
  );
  const universityCode = useSelector((state) => state.auth.universityCode);
  if (submitSuccessful) {
    props.onClose(false);
  }
  const validate = (formValues) => {
    let error = {};
    if (!formValues.userEmail) {
      error.userEmail = "Please provide an Email";
    }
    if (!formValues.userName) {
      error.userName = "Please provide a name";
    }
    if (formValues.userEmail && !validator.isEmail(formValues.userEmail)) {
      error.userEmail = "* Please provide a valid userEmail";
    }
    if (!formValues.userPhoneNumber) {
      error.userPhoneNumber = "Please provide a Phone Name";
    }
    if (
      formValues.userPhoneNumber &&
      formValues.userPhoneNumber.length !== 10
    ) {
      error.userPhoneNumber = "* Please provide a 10 digit Phone Name";
    }
    if (formValues.userPhoneNumber) {
      let filter = /^[1-9]{1}[0-9]{2}[0-9]{7}$/;
      if (!filter.test(formValues.userPhoneNumber)) {
        error.userPhoneNumber = "*Please provide a 10 digit Phone Number";
      }
    }
    if (!formValues.userRegistrationNumber) {
      error.userRegistrationNumber = "Please provide a userRegistrationNumber";
    }
    if (!formValues.userParentNumber) {
      error.userParentNumber = "Please provide a Parent Phone Number";
    }
    if (
      formValues.userParentNumber &&
      formValues.userParentNumber.length !== 10
    ) {
      error.userParentNumber = "* Please provide a 10 digit Phone Name";
    }
    if (formValues.userParentNumber) {
      let filter = /^[1-9]{1}[0-9]{2}[0-9]{7}$/;
      if (!filter.test(formValues.userParentNumber)) {
        error.userParentNumber = "*Please provide a 10 digit Phone Number";
      }
    }
    if (!formValues.userParentName) {
      error.userParentName = "Please provide a userParentName";
    }
    if (!formValues.userDOB) {
      error.userDOB = "Please provide a userDOB";
    }

    if (formValues.userDOB) {
      var a = moment(formValues.userDOB);
      var b = moment(new Date());
      if (b.diff(a, "years") < 16) {
        error.userDOB = "Please provide a student age of more than 15 years";
      }
      if (b.diff(a, "years") > 50) {
        error.userDOB = "Please provide a student age of less than 50 years";
      }
    }

    if (!formValues.admissionYear) {
      error.admissionYear = "Please provide a Admission Year";
    }
    if (!formValues.departmentCode) {
      error.departmentCode = "Please provide a Department Name";
    }
    if (!formValues.semister) {
      error.semister = "Please provide a Semester";
    }
    if (!formValues.division) {
      error.division = "Please provide a Division";
    }
    if (!formValues.userAddress) {
      error.userAddress = "Please provide a Address";
    }
    if (!formValues.userPinCode) {
      error.userPinCode = "Please provide a Pincode";
    }
    if (formValues.userPinCode && formValues.userPinCode.length !== 6) {
      error.userPinCode = "* Please provide a 6 digit Phone Name";
    }
    if (!formValues.userRollNumber) {
      error.userRollNumber = "Please provide a Roll Number";
    }
    if (!formValues.bloodGroup) {
      error.bloodGroup = "Please provide a bloodGroup";
    }

    if (
      formValues.userParentNumber &&
      formValues.userParentNumber.length !== 10
    ) {
      error.userParentNumber = "* Please provide a 10 digit Phone Name";
    }
    if (formValues.userParentNumber) {
      let filter = /^[1-9]{1}[0-9]{2}[0-9]{7}$/;
      if (!filter.test(formValues.userParentNumber)) {
        error.userParentNumber = "*Please provide a 10 digit Phone Number";
      }
    }
    if (!formValues.userParentName) {
      error.userParentName = "Please provide a userParentName";
    }
    if (!formValues.bloodGroup) {
      error.bloodGroup = "Please provide a Blood Group";
    }
    return error;
  };
  const dispatch = useDispatch();

  const admissionsYearChange = function (e) {
    if (e.target.value !== "") {
      let filteradmissionData = admissionsData.filter(
        (item) => item.admissionYear == e.target.value
      );
      if (filteradmissionData.length > 0) {
        setDeptDataRelativeAdmYear(filteradmissionData);
        setDepartmentDropDown(false);
      }
    }
  };

  const departmentCodeChange = function (e) {
    if (e.target.value !== "") {
      let admissionCodeSelected = deptDataRelativeAdmYear.find(
        (item) => item.departmentCode === e.target.value
      );
      if (admissionCodeSelected) {
        setAdmissionCode(admissionCodeSelected.admissionCode);
        let tempClassesData = classesData.filter(
          (classItem) =>
            classItem.admissionCode == admissionCodeSelected.admissionCode
        );
        if (tempClassesData.length > 0) {
          setClassDataRelativeAdmissionCode(tempClassesData);
          let tempSemester = tempClassesData.map((item) => item.semester);
          tempSemester = [...new Set(tempSemester)];
          setSemesterDataRelativeAdmissionCode(tempSemester);
          setSemisterDropDown(false);
        }
      }
    }
  };

  const semesterChange = function (e) {
    if (e.target.value != "") {
      setSemester(e.target.value);
      let tempDivisionData = classDataRelativeAdmissionCode.filter((item) => {
        return (
          item.admissionCode == admissionCodeSelected &&
          item.semester == e.target.value
        );
      });
      if (tempDivisionData.length > 0) {
        let divisions = tempDivisionData.map((item) => item.division);
        divisions = [...new Set(divisions)];
        setDivisionsRelativeSemesterAdmCode(divisions);
        setDivisionDropDown(false);
      }
    }
  };

  const divisionChange = function (e) {
    if (e.target.value != "") {
      setDivision(e.target.value);
      let tempClassId = classDataRelativeAdmissionCode.find((item) => {
        return (
          item.admissionCode == admissionCodeSelected &&
          item.semester == semesterSelected &&
          item.division == e.target.value
        );
      });

      if (tempClassId) {
        setClassId(tempClassId["idClass"]);
      }
    }
  };

  const submitStudent = async (formValues) => {
    console.log("formValues", formValues);
    let studentCreateObject = {};

    studentCreateObject.userEmail = formValues.userEmail;
    studentCreateObject.userName = formValues.userName;
    studentCreateObject.universityCode = universityCode;
    studentCreateObject.userPhoneNumber = "+" + formValues.userPhoneNumber;
    studentCreateObject.userAddress = formValues.userAddress;
    studentCreateObject.userPinCode = formValues.userPinCode;
    studentCreateObject.userRegistrationNumber =
      formValues.userRegistrationNumber;
    studentCreateObject.userRollNumber = formValues.userRollNumber;
    studentCreateObject.userDOB = moment(formValues.userDOB).format(
      "YYYY-MM-DD"
    );
    studentCreateObject.userParentNumber = "+" + formValues.userParentNumber;
    studentCreateObject.userParentName = formValues.userParentName;
    studentCreateObject.bloodGroup = formValues.bloodGroup;

    studentCreateObject.idClass = classId;
    studentCreateObject.semester = semesterSelected;
    studentCreateObject.division = division;
    studentCreateObject.userImage = "";
    console.log("studentCreateObject", studentCreateObject);

    /*
                   
                    obj. = student.UserDOB;
                    obj. = student.UserPinCode;
                    obj. = student.UserEmail;
                    obj. = student.UserName
                    obj. = student.UserPhone
                    obj. = student.UserRollNo
                    obj. = student.UserParentName
                    obj. = student.UserParentContactNo
                    obj. = student.UserAddress
                    obj.bloodGroup = student.BloodGroup
                    obj. = student.UserRegistrationNo
                    obj.idClass = student.ClassId;
                    obj. = student.SubYear;
                    obj. = student.Semester;
                    obj. = student.Division;


    */
    dispatch(createStudent(studentCreateObject, formValues, studentsData));
  };

  return (
    <Modal
      open={props.open}
      onClose={props.onClose}
      dimmer={"inverted"}
      closeIcon
      style={{ overflow: "auto", maxHeight: 600 }}
    >
      <Modal.Header>
        <h3>Create Student</h3>
        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
          <span>&times;</span>
        </button>
        </Modal.Header>
      {spinner ? (
        <Modal.Content>
          <div className="ui active inverted dimmer">
            <div className="ui loader"></div>
          </div>
        </Modal.Content>
      ) : (
        <Modal.Content>
          <Formik
            initialValues={{
              admissionYear: "",
              departmentCode: "",
              semister: "",
              division: "",
              userEmail: "",
              userName: "",
              userRollNumber: "",
              userPhoneNumber: "",
              userDOB: new Date(),
              userRegistrationNumber: "",
              userAddress: "",
              userPinCode: "",
              userParentName: "",
              userParentNumber: "",
              bloodGroup: "",
            }}
            validate={validate}
            onSubmit={submitStudent}
          >
            <Form>
              <div className="row modelFormUi">
              <FormField className="col-4">
                <label htmlFor="nameLabel">Student Name</label>
                <Field id="nameLabel" type="text" name="userName" />
                <div className="errorMessage">
                <ErrorMessage name="userName" component={TextError} />
                </div>
              </FormField>
              <FormField className="col-4">
                <label htmlFor="emailLabel">Student Email</label>
                <Field id="emailLabel" type="text" name="userEmail" />
                <div className="errorMessage">
                <ErrorMessage name="userEmail" component={TextError} />
                </div>
              </FormField>
              <FormField className="col-4">
                <label htmlFor="admissionYearLabel">Year</label>
                <Field id="admissionYearLabel" type="text" name="admissionYear">
                  {(props) => {
                    return (
                      <select
                        className="form-control"
                        name={props.field.name}
                        onBlur={props.field.onBlur}
                        onChange={(e) => {
                          admissionsYearChange(e);
                          props.field.onChange(e);
                        }}
                      >
                        <option value="">Select</option>
                        {admissionsYear.map((year, index) => {
                          return (
                            <option key={index} value={year}>
                              {year}
                            </option>
                          );
                        })}
                      </select>
                    );
                  }}
                </Field>
                <div className="errorMessage">
                <ErrorMessage name="admissionYear" component={TextError} />
                </div>
              </FormField>
              <FormField className="col-4">
                <label htmlFor="departmentLabel">Department Name</label>
                <Field id="departmentLabel" type="text" name="departmentCode">
                  {(props) => {
                    return (
                      <select
                        className="form-control"
                        name={props.field.name}
                        onBlur={props.field.onBlur}
                        onChange={(e) => {
                          departmentCodeChange(e);
                          props.field.onChange(e);
                        }}
                        disabled={disableDepartmentDropDown}
                      >
                        <option value="">Select</option>
                        {deptDataRelativeAdmYear.map((item, index) => {
                          return (
                            <option key={index} value={item.departmentCode}>
                              {item.departmentName}
                            </option>
                          );
                        })}
                      </select>
                    );
                  }}
                </Field>
                <div className="errorMessage">
                <ErrorMessage name="departmentCode" component={TextError} />
                </div>
              </FormField>
              <FormField className="col-4">
                <label htmlFor="semisterLabel">Semister</label>
                <Field id="semisterLabel" type="text" name="semister">
                  {(props) => {
                    return (
                      <select
                        className="form-control"
                        name={props.field.name}
                        onBlur={props.field.onBlur}
                        onChange={(e) => {
                          semesterChange(e);
                          props.field.onChange(e);
                        }}
                        disabled={disableSemisterDropDown}
                      >
                        <option value="">Select</option>
                        {semesterDataRelativeAdmissionCode.map(
                          (item, index) => {
                            return (
                              <option key={index} value={item}>
                                {item}
                              </option>
                            );
                          }
                        )}
                      </select>
                    );
                  }}
                </Field>
                <div className="errorMessage">
                <ErrorMessage name="semister" component={TextError} />
                </div>
              </FormField>
              <FormField className="col-4">
                <label htmlFor="semisterLabel">Division</label>
                <Field id="semisterLabel" type="text" name="division">
                  {(props) => {
                    return (
                      <select
                        className="form-control"
                        name={props.field.name}
                        onBlur={props.field.onBlur}
                        onChange={(e) => {
                          divisionChange(e);
                          props.field.onChange(e);
                        }}
                        disabled={disableDivisionDropDown}
                      >
                        <option value="">Select</option>
                        {divisionsRelativeSemesterAdmCode.map((item, index) => {
                          return (
                            <option key={index} value={item}>
                              {item}
                            </option>
                          );
                        })}
                      </select>
                    );
                  }}
                </Field>
                <div className="errorMessage">
                <ErrorMessage name="division" component={TextError} />
                </div>
              </FormField>
              <FormField className="col-4">
                <label htmlFor="rollNumberLabel">Phone Number</label>
                <Field
                  id="rollNumberLabel"
                  type="text"
                  name="userPhoneNumber"
                />
                <div className="errorMessage">
                <ErrorMessage name="userPhoneNumber" component={TextError} />
                </div>
              </FormField>
              <FormField className="col-4">
                <label htmlFor="emailLabel">Date Of Birth</label>
                <Field id="emailLabel" type="text" name="userDOB">
                  {(props) => {
                    const { setFieldValue } = props.form;
                    return (
                      <Datetime
                        {...props.field}
                        timeFormat={false}
                        dateFormat="YYYY-MM-DD"
                        onChange={(event) => {
                          setFieldValue(props.field.name, event._d);
                        }}
                      />
                    );
                  }}
                </Field>
                <div className="errorMessage">
                <ErrorMessage name="userDOB" component={TextError} />
                </div>
              </FormField>
              <FormField className="col-4">
                <label htmlFor="rollNumberLabel">Roll Number</label>
                <Field id="rollNumberLabel" type="text" name="userRollNumber" />
                <div className="errorMessage">
                <ErrorMessage name="userRollNumber" component={TextError} />
                </div>
              </FormField>
              <FormField className="col-4">
                <label htmlFor="emailLabel">Registration Number</label>
                <Field
                  id="emailLabel"
                  type="text"
                  name="userRegistrationNumber"
                />
                <div className="errorMessage">
                <ErrorMessage
                  name="userRegistrationNumber"
                  component={TextError}
                />
                </div>
              </FormField>
              <FormField className="col-4">
                <label htmlFor="emailLabel">Address</label>
                <Field id="emailLabel" type="text" name="userAddress" />
                <div className="errorMessage">
                <ErrorMessage name="userAddress" component={TextError} />
                </div>
              </FormField>
              <FormField className="col-4">
                <label htmlFor="emailLabel">Pincode</label>
                <Field id="emailLabel" type="text" name="userPinCode" />
                <div className="errorMessage">
                <ErrorMessage name="userPinCode" component={TextError} />
                </div>
              </FormField>
              <FormField className="col-4">
                <label htmlFor="emailLabel">Parent Name</label>
                <Field id="emailLabel" type="text" name="userParentName" />
                <div className="errorMessage">
                <ErrorMessage name="userParentName" component={TextError} />
                </div>
              </FormField>
              <FormField className="col-4">
                <label htmlFor="emailLabel">Parent Phone Number</label>
                <Field id="emailLabel" type="text" name="userParentNumber" />
                <div className="errorMessage">
                <ErrorMessage name="userParentNumber" component={TextError} />
                </div>
              </FormField>
              <FormField className="col-4">
                <label htmlFor="bloodGroupLabel">Blood Group</label>
                <Field id="bloodGroupLabel" type="text" name="bloodGroup" />
                <div className="errorMessage">
                <ErrorMessage name="bloodGroup" component={TextError} />
                </div>
              </FormField>
              <div className="col-12 mt-4">
                <button type="submit" className="btn btn-primary">Submit</button>
              </div>
              </div>
            </Form>
          </Formik>
        </Modal.Content>
      )}
      <Modal.Actions>
        {errorCreateStudent ? <p>{errorCreateStudent}</p> : <React.Fragment />}
      </Modal.Actions>
    </Modal>
  );
};
