import { Modal } from "semantic-ui-react";
import React, { useState } from "react";
import { DeleteStudent } from "./DeleteStudent";
import { useSelector } from "react-redux";
import { EditStudent } from "./EditStudent";
import { Button } from "semantic-ui-react";
export const StudentsProfile = function (props) {
  const [editStudent, setEditStudent] = useState(true);
  const [deleteStudent, setDeleteStudent] = useState(false);
  const errorCreateStudent = useSelector(
    (state) => state.students.errorCreateStudent
  );
  const submitSuccessful = useSelector(
    (state) => state.students.submitSuccessful
  );
  if (submitSuccessful) {
    props.onClose();
  }
  return (
    <Modal open={props.open} onClose={props.onClose} dimmer={"inverted"} closeIcon>
      <Modal.Header>
        <Button
          className={deleteStudent === true ? "item active" : "item"}
          onClick={() => {
            setEditStudent(false);
            setDeleteStudent(true);
          }}
        >
          Delete Student
        </Button>
        <Button
          className={editStudent === true ? "item active" : "item"}
          onClick={() => {
            setEditStudent(true);
            setDeleteStudent(false);
          }}
        >
          Edit Student
        </Button>
        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
          <span>&times;</span>
        </button>
      </Modal.Header>
      <Modal.Content>
        {deleteStudent === true ? (
          <DeleteStudent
            open={deleteStudent}
            onClose={() => setDeleteStudent(false)}
          />
        ) : (
          <React.Fragment />
        )}
        {editStudent === true ? (
          <EditStudent
            open={editStudent}
            onClose={() => setEditStudent(false)}
          />
        ) : (
          <React.Fragment />
        )}
      </Modal.Content>
      <Modal.Actions>
        {errorCreateStudent ? <p>{errorCreateStudent}</p> : <React.Fragment />}
      </Modal.Actions>
    </Modal>
  );
};
