import { createSlice } from "@reduxjs/toolkit";
const initalState = {
  studentsData: [],
  submitSuccessful: false,
  spinner: false,
  errorCreateStudent: "",
};
const studentsSlice = createSlice({
  name: "studentsReducer",
  initialState: initalState,
  reducers: {
    loadStudents(state, action) {
      state.studentsData = action.payload.studentsData;
    },
    setEditStudentObject(state, action) {
      state.editStudent = action.payload.editStudent;
    },
    setSpinner(state, action) {
      state.spinner = action.payload.spinner;
    },
    setStudentError(state, action) {
      state.errorCreateStudent = action.payload.errorCreateStudent;
    },
    setSubmitSuccessfull(state, action) {
      state.submitSuccessful = action.payload.submitSuccessful;
    },
  },
});

export const studentsActions = studentsSlice.actions;
export default studentsSlice;
