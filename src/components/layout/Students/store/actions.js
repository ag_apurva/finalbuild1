/*eslint eqeqeq: "off"*/
import { studentsActions } from "./index";
import {
  LOAD_STUDENTS,
  CREATE_STUDENTS,
  EDIT_STUDENTS,
  DELETE_STUDENTS,
} from "../../API";
import { Storage } from "aws-amplify";
const axios = require("axios");

export const loadStudents = function (code) {
  return async function (dispatch) {
    var config = {
      method: "get",
      url: LOAD_STUDENTS,
      headers: {
        "Content-Type": "application/json",
        "x-api-key": "ZU3QgpqZK773dcNoDRFfK42hZJ9IMzCM2TOXmMYQ",
      },
      params: {
        universityCode: code,
      },
    };

    try {
      let data = await axios(config);

      dispatch(studentsActions.loadStudents({ studentsData: data.data }));
      // console.log("studentsData", data.data);
      dispatch(downloadImagesForStudents(data.data, code));
    } catch (e) {}
  };
};

export const deleteStudent = function (deleteStudentObject, studentData) {
  return async function (dispatch) {
    dispatch(studentsActions.setSpinner({ spinner: true }));
    try {
      var config = {
        method: "delete",
        url: DELETE_STUDENTS,
        headers: {},
        params: {
          universityCode: deleteStudentObject.universityCode,
          studentEmail: deleteStudentObject.studentEmail,
        },
      };

      let studentDeleteObject = await axios(config);
      if (!studentDeleteObject.data.hasOwnProperty("error")) {
        const neewStudent = studentData.filter((student) => {
          return (
            student.userEmail.toLowerCase() !==
            deleteStudentObject.studentEmail.toLowerCase()
          );
        });
        dispatch(studentsActions.loadStudents({ studentsData: neewStudent }));
        dispatch(
          studentsActions.setSubmitSuccessfull({
            submitSuccessful: true,
          })
        );

        setTimeout(() => {
          dispatch(
            studentsActions.setSubmitSuccessfull({
              submitSuccessful: false,
            })
          );
        }, 200);
      } else {
        dispatch(
          studentsActions.setStudentError({
            errorCreateStudent: studentDeleteObject.data.error,
          })
        );
        setTimeout(() => {
          dispatch(
            studentsActions.setStudentError({
              errorCreateStudent: "",
            })
          );
        }, 20000);
        setTimeout(() => {
          dispatch(
            studentsActions.setSubmitSuccessfull({ submitSuccessful: true })
          );
        }, 20000);
        setTimeout(() => {
          dispatch(
            studentsActions.setSubmitSuccessfull({ submitSuccessful: false })
          );
        }, 20200);
      }
    } catch (e) {
      dispatch(
        studentsActions.setStudentError({
          errorCreateStudent: JSON.stringify(e),
        })
      );
      setTimeout(() => {
        dispatch(
          studentsActions.setStudentError({
            errorCreateStudent: "",
          })
        );
      }, 20000);
      setTimeout(() => {
        dispatch(
          studentsActions.setSubmitSuccessfull({ submitSuccessful: true })
        );
      }, 20000);
      setTimeout(() => {
        dispatch(
          studentsActions.setSubmitSuccessfull({ submitSuccessful: false })
        );
      }, 20200);
    }
    dispatch(studentsActions.setSpinner({ spinner: false }));
  };
};

export const createStudent = function (
  studentCreateObject,
  formValues,
  studentsData
) {
  return async function (dispatch) {
    dispatch(studentsActions.setSpinner({ spinner: true }));
    try {
      const data = JSON.stringify(studentCreateObject);
      var config = {
        method: "post",
        url: CREATE_STUDENTS,
        headers: {
          "Content-Type": "application/json",
        },
        data: data,
      };
      let studentCreatedObject = await axios(config);
      if (!studentCreatedObject.data.hasOwnProperty("error")) {
        dispatch(
          studentsActions.setSubmitSuccessfull({
            submitSuccessful: true,
          })
        );
        setTimeout(() => {
          dispatch(
            studentsActions.setSubmitSuccessfull({
              submitSuccessful: false,
            })
          );
        }, 200);
        // let studentObect = {
        //   userCode: studentCreatedObject.data.data,
        //   userDOB: studentCreateObject.userDOB,
        //   userPinCode: studentCreateObject.userPinCode,
        //   userEmail: studentCreateObject.studentEmail,
        //   userName: studentCreateObject.studentName,
        //   userPhone: studentCreateObject.phoneNumber,
        //   userRollNo: studentCreateObject.userRollNo,
        //   userParentName: studentCreateObject.parentName,
        //   userParentContactNo: studentCreateObject.parentPhoneNumber,
        //   userAddress: studentCreateObject.address,
        //   bloodGroup: "",
        //   userRegistrationNo: studentCreateObject.userRegistrationNo,
        //   idClass: studentCreateObject.idClass,
        //   subYear: "",
        //   semester: formValues.semister,
        //   division: formValues.division,
        //   userImage: "",
        // };
        let tempStudentsArray = [...studentsData, studentCreateObject];
        dispatch(
          studentsActions.loadStudents({ studentsData: tempStudentsArray })
        );
      } else {
        dispatch(
          studentsActions.setStudentError({
            errorCreateStudent: studentCreatedObject.data.error,
          })
        );
        setTimeout(() => {
          dispatch(
            studentsActions.setStudentError({
              errorCreateStudent: "",
            })
          );
        }, 20000);
        setTimeout(() => {
          dispatch(
            studentsActions.setSubmitSuccessfull({ submitSuccessful: true })
          );
        }, 20000);
        setTimeout(() => {
          dispatch(
            studentsActions.setSubmitSuccessfull({ submitSuccessful: false })
          );
        }, 20200);
      }
    } catch (e) {
      dispatch(
        studentsActions.setStudentError({
          errorCreateStudent: JSON.stringify(e),
        })
      );
      setTimeout(() => {
        dispatch(
          studentsActions.setStudentError({
            errorCreateStudent: "",
          })
        );
      }, 20000);
      setTimeout(() => {
        dispatch(
          studentsActions.setSubmitSuccessfull({ submitSuccessful: true })
        );
      }, 20000);
      setTimeout(() => {
        dispatch(
          studentsActions.setSubmitSuccessfull({ submitSuccessful: false })
        );
      }, 20200);
    }
    dispatch(studentsActions.setSpinner({ spinner: false }));
  };
};

export const editStudents = function (studentEditObject, studentsData) {
  return async function (dispatch) {
    dispatch(studentsActions.setSpinner({ spinner: true }));
    try {
      const data = JSON.stringify(studentEditObject);
      let newStudentsData = studentsData.filter(
        (student) =>
          student.userEmail.toLowerCase() !==
          studentEditObject.userEmail.toLowerCase()
      );

      // console.log("newStudentsData1", newStudentsData);

      newStudentsData.push(studentEditObject);
      // console.log("newStudentsData2", newStudentsData);
      var config = {
        method: "put",
        url: EDIT_STUDENTS,
        headers: {
          "Content-Type": "application/json",
        },
        data: data,
      };

      let editStudentObject = await axios(config);
      // console.log("editStudentObject", editStudentObject);

      if (!editStudentObject.data.hasOwnProperty("error")) {
        dispatch(
          studentsActions.loadStudents({ studentsData: newStudentsData })
        );
        // console.log("LOADED");
        dispatch(
          studentsActions.setSubmitSuccessfull({
            submitSuccessful: true,
          })
        );

        setTimeout(() => {
          dispatch(
            studentsActions.setSubmitSuccessfull({
              submitSuccessful: false,
            })
          );
        }, 200);
      } else {
        dispatch(
          studentsActions.setStudentError({
            errorCreateStudent: editStudentObject.data.error,
          })
        );
        setTimeout(() => {
          dispatch(
            studentsActions.setStudentError({
              errorCreateStudent: "",
            })
          );
        }, 20000);
        setTimeout(() => {
          dispatch(
            studentsActions.setSubmitSuccessfull({ submitSuccessful: true })
          );
        }, 20000);
        setTimeout(() => {
          dispatch(
            studentsActions.setSubmitSuccessfull({ submitSuccessful: false })
          );
        }, 20200);
      }
    } catch (e) {
      console.log("error", e);
      dispatch(
        studentsActions.setStudentError({
          errorCreateStudent: JSON.stringify(e),
        })
      );
      setTimeout(() => {
        dispatch(
          studentsActions.setStudentError({
            errorCreateStudent: "",
          })
        );
      }, 20000);
      setTimeout(() => {
        dispatch(
          studentsActions.setSubmitSuccessfull({ submitSuccessful: true })
        );
      }, 20000);
      setTimeout(() => {
        dispatch(
          studentsActions.setSubmitSuccessfull({ submitSuccessful: false })
        );
      }, 20200);
    }
    dispatch(studentsActions.setSpinner({ spinner: false }));
  };
};

const checkImage = async (url) => {
  return new Promise((resolve, reject) => {
    let request = new XMLHttpRequest();
    request.open("GET", url, true);
    request.send();
    request.onload = function () {
      if (request.status == 200) {
        //if(statusText == OK)
        resolve();
      } else {
        reject();
      }
    };
  });
};

export const downloadImagesForStudents = (students, universityCode) => {
  return async (dispatch) => {
    try {
      let newStudents = students.map((item) => {
        let newStudentObject = { ...item };
        return newStudentObject;
      });

      for (let i = 0; i < newStudents.length; i++) {
        newStudents[i].userImage = "";
        const userCode = newStudents[i].userCode;
        let path = universityCode + "/" + userCode + "/profileImage.jpg";
        try {
          const downloadedImage = await Storage.get(path.toString(), {
            contentType: "image/jpeg",
            expires: 2400,
          });
          await checkImage(downloadedImage);
          newStudents[i].userImage = downloadedImage;
        } catch (e) {
          newStudents[i].userImage = "";
        }
      }
      dispatch(studentsActions.loadStudents({ studentsData: newStudents }));
    } catch (e) {
      console.log("error loading student image", e);
    }
  };
};
