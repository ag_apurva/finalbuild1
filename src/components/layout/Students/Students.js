/*eslint eqeqeq: "off"*/
import React, { useState } from "react";
import { studentsActions } from "./store/index";
import { loadStudents } from "./store/actions";
import { useSelector, useDispatch } from "react-redux";
import "../css/student.css";
import logo from "../image/attendance.png";
import { StudentsProfile } from "./StudentsProfile";
import { CreateStudent } from "./CreateStudent";
import { MyProfile } from "../Profile/Profile";

export const Students = function () {
  const userName = useSelector((state) => state.auth.myProfile.userName);
  const userRole = useSelector((state) => state.auth.myProfile.userRole);
  const [myProfile, setMyProfile] = useState(false);
  const [editStudentPopUp, editStudentsAddPopUp] = useState(false);
  const [emailSelected, setEmail] = useState("");
  const [createStudent, setCreateStudent] = useState(false);
  const studentsData = useSelector((state) => state.students.studentsData);
  const universityCode = useSelector((state) => state.auth.universityCode);
  // console.log("studentsData", studentsData);
  const dispatch = useDispatch();
  let profileImage = useSelector((state) => state.auth.profileImage);

  if (profileImage == "") {
    profileImage = logo;
  }
  const renderStudents = () => {
    return studentsData.map((item, index) => {
      let {
        userEmail,
        userName,
        userPhoneNumber,
        userRegistrationNumber,
        userRollNumber,
        semester,
        division,
        userImage,
      } = item;
      // console.log("item", item);
      if (userImage == "") {
        userImage = logo;
      }
      return (
        <tr key={index}>
          {editStudentPopUp && emailSelected === userEmail ? (
            <StudentsProfile
              open={editStudentPopUp}
              onClose={() => editStudentsAddPopUp(false)}
            />
          ) : (
            <React.Fragment />
          )}
          <td>
            <span>
              <img src={userImage} width="50px" height="50px" alt="" />
            </span>
          </td>
          <td>{userName}</td>
          <td>
            <span>{userPhoneNumber}</span>
          </td>
          <td>{userRegistrationNumber}</td>
          <td>{userRollNumber}</td>
          <td>{semester}</td>
          <td>{division}</td>
          <td className="action-cell">
            <button
              onClick={() => {
                editStudentsAddPopUp(true);
                setEmail(userEmail);
                dispatch(
                  studentsActions.setEditStudentObject({
                    editStudent: item,
                  })
                );
              }}
            >
              <i className="fa fa-edit" aria-hidden="true"></i>
            </button>
            <button
              onClick={() => {
                editStudentsAddPopUp(true);
                setEmail(userEmail);
                dispatch(
                  studentsActions.setEditStudentObject({
                    editStudent: item,
                  })
                );
              }}
            >
              <i className="fa fa-trash" aria-hidden="true"></i>
            </button>
          </td>
        </tr>
      );
    });
  };

  if (myProfile) {
    return <MyProfile open={myProfile} onClose={() => setMyProfile(false)} />;
  } else if (createStudent) {
    return (
      <CreateStudent
        open={createStudent}
        onClose={() => setCreateStudent(false)}
      />
    );
  } else {
    return (
      <div className="main-content bg-light">
        <header>
          <h4>
            <label htmlFor="nav-toggel" className="hamburgerIcon">
              <i className="fa fa-bars" aria-hidden="true"></i>
            </label>
            <span className="name">Students</span>
          </h4>

          <div className="user-wrapper" onClick={() => setMyProfile(true)}>
            <img src={profileImage} width="50px" height="50px" alt="" />
            <div>
              <h6>{userName}</h6>
              <small>{userRole}</small>
            </div>
          </div>
        </header>
        <main>
          <div className="table-content tableGridUi">
            <table className="table table-hover">
              <thead>
                <tr>
                  <th style={{ width: "12.5%" }}>Profile Picture</th>
                  <th style={{ width: "15%" }}>Name</th>
                  <th style={{ width: "15%" }}>PhoneNumber</th>
                  <th style={{ width: "15%" }}>RegNumber</th>
                  <th style={{ width: "12.5%" }}>Roll</th>
                  <th style={{ width: "12.5%" }}>Semester</th>
                  <th style={{ width: "12.5%" }}>Division</th>
                  <th style={{ width: "12.5%" }} className="text-center">Action</th>
                </tr>
              </thead>
              <tbody>{renderStudents()}</tbody>
            </table>
          </div>
          <div className="mt-4">
            <button
              className="btn btn-primary"
              onClick={() => setCreateStudent(true)}
            >
              Add Student
            </button>
            <button
              className="btn btn-primary ml-2"
              onClick={() => dispatch(loadStudents(universityCode))}
            >
              Refresh
            </button>
          </div>
        </main>
      </div>
    );
  }
};
