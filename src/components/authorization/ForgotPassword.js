/*eslint eqeqeq: "off"*/
import { useDispatch, useSelector } from "react-redux";
import { Modal, FormField, Button } from "semantic-ui-react";
import { Form } from "formik-semantic-ui-react";
import { Formik, Field, ErrorMessage } from "formik";
import TextError from "./TextError";
import React from "react";
import { forgotPasswordFirstAction } from "./store/actions";
import { authActions } from "./store/index";
import validator from "validator";

const ForgotPassword = function (props) {
  const forgotPasswordError = useSelector(
    (state) => state.auth.forgotPasswordError
  );
  const submitSuccessful = useSelector((state) => state.auth.submitSuccessful);
  if (submitSuccessful) {
    props.onClose();
  }
  const dispatch = useDispatch();
  const validateOne = function (formValues) {
    let error = {};
    if (!formValues.email) {
      error.password = "* Please provide a email";
    }
    if (formValues.email && !validator.isEmail(formValues.email)) {
      error.email = "* Please provide a  correct email";
    }
    return error;
  };

  const onSubmitOne = function (formValues) {
    dispatch(
      authActions.setforgotPasswordError({
        forgotPasswordError: "",
      })
    );

    dispatch(forgotPasswordFirstAction(formValues));
  };

  return (
    <Modal open={props.open} onClose={props.onClose} dimmer={"inverted"}>
      <Modal.Header>Forgot Password</Modal.Header>
      <Modal.Content>
        <Formik
          initialValues={{ email: "" }}
          onSubmit={onSubmitOne}
          inverted={"dimmer"}
          validate={validateOne}
        >
          {(formik) => {
            return (
              <Form>
                <FormField>
                  <label htmlFor="emailId">Email</label>
                  <Field id="emailId" name="email" />
                  <ErrorMessage name="email" component={TextError} />
                </FormField>
                <Button type="submit">Submit</Button>
              </Form>
            );
          }}
        </Formik>
      </Modal.Content>
      <Modal.Actions>
        {forgotPasswordError ? (
          <p style={{ color: "red" }}>{forgotPasswordError}</p>
        ) : (
          <React.Fragment />
        )}
      </Modal.Actions>
    </Modal>
  );
};

export default ForgotPassword;
