/*eslint eqeqeq: "off"*/
import { ADMIN_PROFILE, FORGOT_CODES, REGISTER_AGAIN } from "../API";
import { Auth, Storage } from "aws-amplify";
import { authActions } from "./index";
import { loadStudents } from "../../layout/Students/store/actions";
import { getDepartments } from "../../layout/Departments/store/actions";
import { getAdmissions } from "../../layout/Batch/store/actions";
import { getClasses } from "../../layout/Class/store/actions";
import { getSubjects } from "../../layout/Subjects/store/actions";
import { getLocations } from "../../layout/Locations/store/actions";
import { getProfessors } from "../../layout/Professors/store/actions";
import { getContacts } from "../../layout/Contacts/store/actions"; //getFaq
import { getFaq } from "../../layout/Help/store/actions"; //getFaq
import axios from "axios";

export const signUpAmplify = function (formValues, history) {
  return async function (dispatch) {
    try {
      await Auth.signUp({
        username: formValues.email,
        password: formValues.password,
        attributes: {
          email: formValues.email,
          name: formValues.name,
          phone_number: "+" + formValues.phone,
        },
        clientMetadata: {
          Name: formValues.name,
          email: formValues.username,
          phoneNumber: formValues.phone,
          Address: formValues.address,
          college_name: formValues.college_name,
          designation: formValues.designation,
          pincode: formValues.pincode,
          state: formValues.state,
          country: formValues.country,
        },
      });
      // console.log("signedUpData", signedUpData);
      dispatch(
        authActions.signUp({
          confirmSignUp: true,
        })
      );
      history.push("/");
    } catch (e) {
      //  console.log("error while sign up", e);
      if (e.message == "An account with the given email already exists.") {
        dispatch(accountExistFunction(formValues.email));
        dispatch(
          authActions.setRegisterAgainFormValues({
            registerAgainFormValues: { ...formValues },
          })
        );
      } else {
        dispatch(authActions.errorSignUp({ error: e.message }));
      }
    }
  };
};
/*
the SignIn API seems to be calling a 
lot api to fetch the data at the start of app
*/
export const signInAmplify = function (formValues, history, formikFunctions) {
  return async function (dispatch) {
    dispatch(authActions.setSpinner({ spinner: true }));
    localStorage.setItem("universityCode", formValues.universityCode);

    dispatch(getDepartments(formValues.universityCode));
    dispatch(getAdmissions(formValues.universityCode));
    dispatch(getClasses(formValues.universityCode));
    dispatch(getSubjects(formValues.universityCode));
    dispatch(getLocations(formValues.universityCode));
    dispatch(getContacts(formValues.universityCode));
    dispatch(getFaq(formValues.universityCode));

    try {
      let data = await Auth.signIn(formValues.email, formValues.password, {
        UniCode: formValues.universityCode,
      });
      // console.log("data", data);
      dispatch(loadStudents(formValues.universityCode));
      dispatch(getProfessors(formValues.universityCode));
      dispatch(myProfile(formValues.universityCode, formValues.email));
      dispatch(
        authActions.signIn({
          idToken: data.signInUserSession.idToken.jwtToken,
          accessToken: data.signInUserSession.accessToken.jwtToken,
          refreshToken: data.signInUserSession.refreshToken.jwtToken,
        })
      );
      history.push("/welcome");
    } catch (e) {
      console.log("errror login", e);
      if (
        e.message ===
        "PreAuthentication failed with error Incorrect University Code."
      ) {
        console.log("incorrect code");
        formikFunctions.setFieldError(
          "universityCode",
          "Incorrect University Code"
        );
      } else if (e.message === "Incorrect username or password.") {
        formikFunctions.setFieldError(
          "password",
          "Incorrect username or password"
        );
      } else if (e.message === "User does not exist.") {
        formikFunctions.setFieldError("email", "User does not exist");
      }
    }
    dispatch(authActions.setSpinner({ spinner: false }));
  };
};

export const forgotPasswordAmplify = function () {
  return async function (dispatch) {};
};

export const signUpAgainAmplify = function () {
  return async function (dispatch) {};
};

export const logOutAmplify = function (history) {
  return async function (dispatch) {
    try {
      await Auth.signOut();
    } catch (e) {}
    dispatch(authActions.logOut());
    history.push("/");
  };
};

export const myProfile = function (code, email) {
  return async function (dispatch) {
    var config = {
      method: "get",
      url: ADMIN_PROFILE,
      headers: {},
      params: {
        universityCode: code,
        userEmail: email,
      },
    };
    try {
      let myProfile = await axios(config);
      // console.log("myProfile", myProfile);
      // console.log("myProfile data", myProfile.data);
      dispatch(authActions.setMyProfile({ myProfile: { ...myProfile.data } }));
      let userCode = myProfile.data.userCode;
      let universityCode = myProfile.data.universityCode;
      let path = universityCode + "/" + userCode + "/profileImage.jpg";
      //console.log("path", path);
      dispatch(downLoadImage(path));
      dispatch(
        authActions.setUniversityCode({
          universityCode: myProfile.data.universityCode,
        })
      );
    } catch (e) {
      console.log("error myProfile  Data", e);
    }
  };
};

export const downLoadImage = (path) => {
  return async function (dispatch) {
    try {
      const downloadedImage = await Storage.get(path.toString(), {
        contentType: "image/jpeg",
        expires: 2400,
      });
      try {
        await checkImage(downloadedImage);
        dispatch(
          authActions.setProfileImage({ profileImage: downloadedImage })
        );
      } catch (e) {
        dispatch(authActions.setProfileImage({ profileImage: "" }));
      }
    } catch (e) {
      console.log("error Loading Profile Image", e);
      dispatch(authActions.setProfileImage({ profileImage: "" }));
    }
  };
};

const checkImage = async (url) => {
  return new Promise((resolve, reject) => {
    let request = new XMLHttpRequest();
    request.open("GET", url, true);
    request.send();
    request.onload = function () {
      if (request.status == 200) {
        //if(statusText == OK)
        resolve();
      } else {
        reject();
      }
    };
  });
};

export const forgotPasswordSecondAction = (email, code, new_password) => {
  return async function (dispatch) {
    try {
      await Auth.forgotPasswordSubmit(email, code, new_password);
      dispatch(authActions.setSubmitSuccessfull({ submitSuccessful: true }));
      setTimeout(() => {
        dispatch(authActions.setSubmitSuccessfull({ submitSuccessful: false }));
      }, 20200);
    } catch (error) {
      console.log("error2", error);
      dispatch(
        authActions.setforgotPasswordError({
          forgotPasswordError: error.message,
        })
      );
    }
  };
};

export const forgotPasswordFirstAction = (forgotPasswordObject) => {
  // console.log("forgotPasswordObject", forgotPasswordObject);

  return async function (dispatch) {
    try {
      await Auth.forgotPassword(forgotPasswordObject.email);
      dispatch(
        authActions.setEmailForgotPassword({
          emailForgotPassword: forgotPasswordObject.email,
        })
      );
      dispatch(
        authActions.setforgotPasswordSuccessful({
          forgotPasswordSuccessful: true,
        })
      );

      // console.log("code", code);
    } catch (error) {
      // console.log("password reset error", error.message);
      dispatch(
        authActions.setforgotPasswordError({
          forgotPasswordError: error.message,
        })
      );
    }
  };
};

export const forgotCodesAuth = (formValues) => {
  return async function (dispatch) {
    dispatch(authActions.setSpinner({ spinner: true }));
    try {
      await Auth.signIn(formValues.email, formValues.password);
      dispatch(
        authActions.setForgotCodeSuccessfull({ forgotCodesSuccessful: true })
      );
      dispatch(forgotCodesData(formValues.email));
    } catch (e) {
      dispatch(authActions.setForgotCodeError({ forgotCodesError: e.message }));
    }
    dispatch(authActions.setSpinner({ spinner: false }));
  };
};

export const forgotCodesData = function (email) {
  return async function (dispatch) {
    var config = {
      method: "get",
      url: FORGOT_CODES,
      params: {
        userEmail: email,
      },
    };
    try {
      let forgotCodesData = await axios(config);
      if (!forgotCodesData.data.hasOwnProperty("error")) {
        dispatch(
          authActions.forgotCodesData({ forgotCodesData: forgotCodesData.data })
        );
      } else {
        dispatch(
          authActions.setForgotCodeError({
            forgotCodesError: forgotCodesData.data.error,
          })
        );
      }
    } catch (e) {
      console.log("error retriveing forgot codes ", e);
      dispatch(
        authActions.setForgotCodeError({
          forgotCodesError: JSON.stringify(e),
        })
      );
    }
  };
};

const accountExistFunction = function (email) {
  return async function (dispatch) {
    dispatch(authActions.setAccountExists({ accountExists: true }));
    dispatch(forgotCodesData(email));
  };
};

export const callRegisterAgain = function (formValues, history) {
  return async function (dispatch) {
    try {
      let dataInput = JSON.stringify(formValues);
      let config = {
        method: "post",
        url: REGISTER_AGAIN,
        data: dataInput,
      };

      let registerAgainData = await axios(config);
      //console.log("registerAgainData", registerAgainData);
      if (!registerAgainData.data.hasOwnProperty("error")) {
        history.push("/");
      } else {
        dispatch(
          authActions.setRegisterAgainError({
            registerAgainError: registerAgainData.data.error,
          })
        );
      }
    } catch (e) {
      console.log("Error registering again", e);
      dispatch(
        authActions.setRegisterAgainError({
          registerAgainError: JSON.stringify(e),
        })
      );
    }
  };
};
