import { createSlice } from "@reduxjs/toolkit";
const initalState = {
  isAuthenticated: false,
  confirmSignUp: false,
  idToken: "",
  accessToken: "",
  refreshToken: "",
  errorSignUp: "",
  spinner: false,
  myProfile: {},
  universityCode: "",
  profileImage: "",
  forgotPasswordError: "",
  submitSuccessful: false,
  forgotCodesError: "",
  forgotCodesSuccessful: false,
  forgotCodesData: [],
  accountExists: false,
  registerAgainFormValues: {},
  registerAgainError: "",
  forgotPasswordSuccessful: false,
  emailForgotPassword: "",
};

const authSlice = createSlice({
  name: "authReducer",
  initialState: initalState,
  reducers: {
    signIn(state, action) {
      state.isAuthenticated = true;
      state.idToken = action.payload.idToken;
      state.accessToken = action.payload.accessToken;
      state.refreshToken = action.payload.refreshToken;
    },
    signUp(state, action) {
      state.errorSignUp = "";
    },
    logOut(state) {
      state.isAuthenticated = false;
      state.idToken = "";
      state.accessToken = "";
      state.refreshToken = "";
    },
    errorSignUp(state, action) {
      state.errorSignUp = action.payload.error;
    },
    setSpinner(state, action) {
      state.spinner = action.payload.spinner;
    },
    setMyProfile(state, action) {
      state.myProfile = action.payload.myProfile;
    },
    setUniversityCode(state, action) {
      state.universityCode = action.payload.universityCode;
    },
    setProfileImage(state, action) {
      state.profileImage = action.payload.profileImage;
    },
    setforgotPasswordError(state, action) {
      state.forgotPasswordError = action.payload.forgotPasswordError;
    },
    setSubmitSuccessfull(state, action) {
      state.submitSuccessful = action.payload.submitSuccessful;
    },
    setForgotCodeError(state, action) {
      state.forgotCodesError = action.payload.forgotCodesError;
    },
    setForgotCodeSuccessfull(state, action) {
      state.forgotCodesSuccessful = action.payload.forgotCodesSuccessful;
    },
    forgotCodesData(state, action) {
      state.forgotCodesData = action.payload.forgotCodesData;
    },
    setAccountExists(state, action) {
      state.accountExists = action.payload.accountExists;
    },
    setRegisterAgainFormValues(state, action) {
      state.registerAgainFormValues = Object.assign(
        {},
        {
          ...action.payload.registerAgainFormValues,
        }
      );
    },
    setRegisterAgainError(state, action) {
      state.registerAgainError = action.payload.registerAgainError;
    },
    setforgotPasswordSuccessful(state, action) {
      state.forgotPasswordSuccessful = action.payload.forgotPasswordSuccessful;
    },
    setEmailForgotPassword(state, action) {
      state.emailForgotPassword = action.payload.emailForgotPassword;
    },
  },
});

export const authActions = authSlice.actions;
export default authSlice;
