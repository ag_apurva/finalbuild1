import { Modal } from "semantic-ui-react";
import React from "react";
import { useSelector } from "react-redux";
const ForgotCodesData = function (props) {
  const forgotCodesData = useSelector((state) => state.auth.forgotCodesData);
  const renderUniversityData = function () {
    return forgotCodesData.map((item) => {
      return (
        <div className="item" key={item.UniCode}>
          <div className="left aligned content">{item.universityName}</div>
          <div className="right aligned content">{item.universityCode}</div>
          <div className="right aligned content">{item.userRole}</div>
        </div>
      );
    });
  };
  const forgotCodesError = useSelector((state) => state.auth.forgotCodesError);
  return (
    <Modal open={props.open} onClose={props.onClose} dimmer={"inverted"}>
      <Modal.Header>User is Registered for these University</Modal.Header>
      <Modal.Content>
        <div className="content">
          <div className="ui divided items">
            <div className="item">
              <div className="left aligned content">Collge</div>
              <div className="right aligned content">Code</div>
              <div className="right aligned content">Role</div>
            </div>
            {renderUniversityData()}
          </div>
        </div>
      </Modal.Content>
      <Modal.Actions>
        <button onClick={() => props.onClose()}>Close</button>
      </Modal.Actions>
      <Modal.Actions>
        {forgotCodesError ? (
          <p style={{ color: "red" }}>{forgotCodesError}</p>
        ) : (
          <React.Fragment />
        )}
      </Modal.Actions>
    </Modal>
  );
};
export default ForgotCodesData;
