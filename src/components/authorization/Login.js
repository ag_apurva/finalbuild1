/*eslint eqeqeq: "off"*/
import "./css/style.css";
import { authActions } from "./store/index";
import { Link } from "react-router-dom";
import CampusFavicon from "./image/campus-favicon.png";
import * as Yup from "yup";
import { Form } from "formik-semantic-ui-react";
import { Loader, Dimmer } from "semantic-ui-react";
import { Formik, Field, ErrorMessage } from "formik";
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import ForgotCodes from "./ForgotCodes";
import ForgotPassword2 from "./ForgotPassword2";
import ForgotPassword from "./ForgotPassword";
import ForgotCodesData from "./ForgotCodesData";
import "semantic-ui-css/semantic.min.css";
import { signInAmplify } from "./store/actions";
import TextError from "./TextError";
const Login = () => {
  const [forgotCodes, setForgotCodes] = useState(false);
  const [forgotPassword, setForgotPassword] = useState(false);
  const spinner = useSelector((state) => state.auth.spinner);
  const forgotCodesSuccessful = useSelector(
    (state) => state.auth.forgotCodesSuccessful
  );

  const forgotPasswordSuccessful = useSelector(
    (state) => state.auth.forgotPasswordSuccessful
  );

  const errorSignIn = useSelector((state) => state.auth.errorSignIn);
  const dispatch = useDispatch();
  let history = useHistory();
  const SigninSchema = Yup.object().shape({
    universityCode: Yup.string().required("* Please provide a code"),
    email: Yup.string()
      .email("Invalid email")
      .required("* Please provide a email"),
    password: Yup.string()
      .required("* Please provide a password")
      .matches(
        /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
        "Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and one special case Character"
      ),
  });

  const onSubmit = async (formValues, formikFunctions) => {
    dispatch(signInAmplify(formValues, history, formikFunctions));
  };
  const validate = function (formValues) {
    let errors = {};
    return errors;
  };
  if (forgotPasswordSuccessful) {
    return (
      <ForgotPassword2
        open={forgotPasswordSuccessful}
        onClose={() => {
          dispatch(
            authActions.setforgotPasswordSuccessful({
              forgotPasswordSuccessful: false,
            })
          );
          setForgotPassword(false);
          dispatch(
            authActions.setEmailForgotPassword({
              emailForgotPassword: "",
            })
          );
        }}
      />
    );
  } else if (forgotCodesSuccessful) {
    return (
      <ForgotCodesData
        open={forgotCodesSuccessful}
        onClose={() => {
          dispatch(
            authActions.setForgotCodeSuccessfull({
              forgotCodesSuccessful: false,
            })
          );
          setForgotCodes(false);
          dispatch(
            authActions.forgotCodesData({
              forgotCodesData: [],
            })
          );
          dispatch(
            authActions.setForgotCodeError({
              forgotCodesError: "",
            })
          );
        }}
      />
    );
  } else if (forgotCodes) {
    return (
      <ForgotCodes
        open={forgotCodes}
        onClose={() => {
          setForgotCodes(false);
          dispatch(authActions.setForgotCodeError({ forgotCodesError: "" }));
        }}
      />
    );
  } else if (forgotPassword) {
    return (
      <ForgotPassword
        open={forgotPassword}
        onClose={() => {
          setForgotPassword(false);
        }}
      />
    );
  } else {
    return (
      <div className="wrapper">
        {spinner ? (
          <Dimmer active>
            <Loader size="big">Loading</Loader>
          </Dimmer>
        ) : (
          <React.Fragment />
        )}
        <div className="login">
          <div className="row inner-wrap">
            <div className="col-lg-6 col-md-5 col-12 p-0 bg-light back">
              <div className="login-bg"></div>
            </div>

            <div className="col-lg-6 col-md-7 col-12 bg-white">
              <div className="inner-login">
                <div className="mb-8 campus-logo">
                  <img
                    src={CampusFavicon}
                    className="img"
                    alt="CampusFavicon"
                  />
                </div>

                <div className="login-content">
                  <div
                    className="login-pane fade show active"
                    role="tabpanel"
                    id="login"
                  >
                    <p className="mt-6 mb-6 pt-2 pb-2">
                      <Link to="/">
                        Use your credentials to Login or Sign Up!
                      </Link>
                    </p>
                    <Formik
                      initialValues={{
                        universityCode: "TR036",
                        email: "nupur@mobifolio.net",
                        password: "Vishal@1",
                      }}
                      validationSchema={SigninSchema}
                      validate={validate}
                      onSubmit={onSubmit}
                    >
                      <Form>
                        <div className="form-group">
                          <label>University Code</label>
                          <div className="input-group">
                            <div className="input-group-prepend">
                              <span className="input-group-text">
                                <i
                                  className="fa fa-university"
                                  aria-hidden="true"
                                ></i>
                              </span>
                            </div>
                            <Field
                              name="universityCode"
                              className="form-control"
                            />
                          </div>
                          <ErrorMessage
                            name="universityCode"
                            component={TextError}
                          />
                        </div>
                        <div className="form-group">
                          <label>Email Address</label>
                          <div className="input-group">
                            <div className="input-group-prepend">
                              <span className="input-group-text">
                                <i
                                  className="fa fa-envelope-o"
                                  aria-hidden="true"
                                ></i>
                              </span>
                            </div>
                            <Field name="email" className="form-control" />
                          </div>
                          <ErrorMessage name="email" component={TextError} />
                        </div>

                        <div className="form-group password-field">
                          <label>Password</label>
                          <div className="input-group">
                            <div className="input-group-prepend">
                              <span className="input-group-text">
                                <i
                                  className="fa fa-lock"
                                  aria-hidden="true"
                                ></i>
                              </span>
                            </div>
                            <Field
                              name="password"
                              type="password"
                              className="form-control"
                              autoComplete="on"
                            />
                          </div>
                          <ErrorMessage name="password" component={TextError} />
                        </div>

                        <div className="row" style={{ paddingTop: "10px" }}>
                          <div className="col-12">
                            <button
                              type="submit"
                              className="btn btn-primary btn-block"
                            >
                              Login
                            </button>
                          </div>
                        </div>
                        <div className="row" style={{ paddingTop: "10px" }}>
                          {errorSignIn ? <p>{errorSignIn}</p> : <p></p>}
                        </div>
                      </Form>
                    </Formik>
                    <div
                      className="col-12"
                      style={{ paddingTop: "10px", paddingLeft: "20px" }}
                    >
                      <Link to="/">I forgot my username/password</Link>
                    </div>
                    <div
                      className="col-12"
                      style={{ paddingTop: "10px", paddingLeft: "20px" }}
                    >
                      <Link to="/register">Register</Link>
                    </div>
                    <div
                      className="col-12"
                      style={{ paddingTop: "10px", paddingLeft: "20px" }}
                    >
                      <Link to="/" onClick={() => setForgotCodes(true)}>
                        Get University Code
                      </Link>
                    </div>
                    <div
                      className="col-12"
                      style={{ paddingTop: "10px", paddingLeft: "20px" }}
                    >
                      <Link to="/" onClick={() => setForgotPassword(true)}>
                        Forgot Password
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
};

export default Login;
