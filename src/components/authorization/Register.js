import { Link } from "react-router-dom";
import * as Yup from "yup";
import CampusFavicon from "./image/campus-favicon.png";
import { FormField } from "semantic-ui-react";
import { Form } from "formik-semantic-ui-react";
import { Formik, Field, ErrorMessage } from "formik";
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import React from "react";
import RegisterAgain from "./RegisterAgain";
import { signUpAmplify } from "./store/actions";
import { authActions } from "./store/index";
const Register = function () {
  const dispatch = useDispatch();
  let history = useHistory();
  const errorSignUp = useSelector((state) => state.auth.errorSignUp);
  const accountExists = useSelector((state) => state.auth.accountExists);
  /*
  formAttributes carry with them all other attributes.
  errors contains the message which tell await Auth.signUp failed
  */
  const SignupSchema = Yup.object().shape({
    name: Yup.string()
      .required("* Please provide a name")
      .matches(/^[A-Za-z 0-9]+$/, "Alphanumeric character not allowed"),
    designation: Yup.string().min(5).required("* Please provide a designation"),
    phone: Yup.string()
      .required("* Please provide a phone")
      .matches(
        /^[1-9]{1}[0-9]{2}[0-9]{7}$/,
        "*Please provide a 10 digit Phone Number that does not start with 0"
      ),
    address: Yup.string().min(5).required("* Please provide a address"),
    pincode: Yup.string()
      .required("* Please provide a pincode")
      .matches(/^[1-9]{1}[0-9]{2}[0-9]{3}$/, "* Provide a 6 digit PinCode"),
    country: Yup.string().required("* Please provide a country"),
    email: Yup.string()
      .email("Invalid email")
      .required("* Please provide a email"),
    password: Yup.string()
      .required("* Please provide a password")
      .matches(
        /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
        "Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and one special case Character"
      ),
    confirmPassword: Yup.string().required(
      "* Please provide password once more "
    ),
    college_name: Yup.string().required("* Please provide college name"),
    state: Yup.string().required("* Please provide a state"),
  });

  const validate = function (formValues) {
    let errors = {};

    if (
      formValues.password &&
      formValues.confirmPassword &&
      formValues.password !== formValues.confirmPassword
    ) {
      errors.confirmPassword = "* Passwords do not match";
    }
    return errors;
  };

  const onSubmit = async (formValues) => {
    dispatch(signUpAmplify(formValues, history));
  };
  if (accountExists) {
    return (
      <RegisterAgain
        open={accountExists}
        onClose={() => {
          dispatch(authActions.setAccountExists({ setAccountExists: false }));
          dispatch(
            authActions.forgotCodesData({
              forgotCodesData: [],
            })
          );
          dispatch(
            authActions.setRegisterAgainFormValues({
              registerAgainFormValues: {},
            })
          );

          dispatch(
            authActions.setRegisterAgainError({
              registerAgainError: "",
            })
          );
        }}
      />
    );
  } else {
    return (
      <div className="">
        <div className="">
          <div className="row inner-wrap-signup">
            <div className="col-lg-6 col-md-8 col mx-auto sign-con">
              <div className="">
                <div className="login-content">
                  <div className="mb-8 campus-logo">
                    <img
                      src={CampusFavicon}
                      alt="CampusFavicon"
                      style={{
                        width: "20%",
                        marginBottom: "20px",
                        display: "block",
                        margin: "auto",
                      }}
                    />
                  </div>
                  <h2 className="text-center">Sign Up!</h2>

                  <div
                    className="login-pane fade show active"
                    role="tabpanel"
                    id="login"
                  >
                    <p className="mt-6 mb-6 pb-1 text-center">
                      Sign-up to start your 30 day free trial!
                    </p>
                    <Formik
                      initialValues={{
                        name: "",
                        email: "",
                        designation: "",
                        college_name: "",
                        phone: "",
                        address: "",
                        pincode: "",
                        state: "",
                        country: "",
                        password: "",
                        confirmPassword: "",
                      }}
                      validationSchema={SignupSchema}
                      validate={validate}
                      onSubmit={onSubmit}
                    >
                      <Form>
                        <div className="form-group">
                          <label>Full Name</label>
                          <FormField>
                            <Field
                              className="form-control"
                              name="name"
                              type="text"
                              placeholder="Jeo Smith"
                            />
                          </FormField>
                          <ErrorMessage name="name" />
                        </div>
                        <div className="form-group">
                          <label>Email</label>
                          <FormField>
                            <Field
                              className="form-control"
                              name="email"
                              type="email"
                              placeholder="you-email@example.com"
                            />
                          </FormField>
                          <ErrorMessage name="email" />
                        </div>
                        <div className="form-group">
                          <label>Designation</label>
                          <FormField>
                            <Field
                              className="form-control"
                              name="designation"
                              type="text"
                              placeholder="Professor"
                            />
                          </FormField>
                          <ErrorMessage name="designation" />
                        </div>
                        <div className="form-group">
                          <label>College Name</label>
                          <FormField>
                            <Field
                              className="form-control"
                              name="college_name"
                              type="text"
                              placeholder="College Name"
                            />
                          </FormField>
                          <ErrorMessage name="designation" />
                        </div>
                        <div className="form-group">
                          <label>Phone Number</label>
                          <FormField>
                            <Field
                              className="form-control"
                              name="phone"
                              type="text"
                              placeholder="9876320510"
                            />
                          </FormField>
                          <ErrorMessage name="phone" />
                        </div>
                        <div className="form-group">
                          <label>Address</label>
                          <FormField>
                            <Field name="address">
                              {(props) => {
                                return (
                                  <textarea
                                    className="form-control"
                                    {...props.field}
                                    type="text"
                                    rows="1"
                                  ></textarea>
                                );
                              }}
                            </Field>
                            <ErrorMessage name="address" />
                          </FormField>
                        </div>
                        <div className="form-group">
                          <label>Pincode</label>
                          <FormField>
                            <Field
                              className="form-control"
                              name="pincode"
                              type="text"
                            />
                          </FormField>
                          <ErrorMessage name="pincode" />
                        </div>
                        <div className="form-group">
                          <label>State</label>
                          <FormField>
                            <Field
                              className="form-control"
                              name="state"
                              type="text"
                              placeholder="Maharastra"
                            />
                          </FormField>
                          <ErrorMessage name="state" />
                        </div>
                        <div className="form-group">
                          <label>Country</label>
                          <FormField>
                            <Field
                              className="form-control"
                              name="country"
                              type="text"
                              placeholder="India"
                            />
                          </FormField>
                          <ErrorMessage name="country" />
                        </div>

                        <div className="form-group password-field">
                          <label>Password</label>
                          <FormField>
                            <Field
                              name="password"
                              type="password"
                              className="form-control"
                              autoComplete="on"
                            />
                          </FormField>
                          <ErrorMessage name="password" />
                        </div>

                        <div className="form-group password-field">
                          <label>Confirm Password</label>
                          <FormField>
                            <Field
                              name="confirmPassword"
                              type="password"
                              className="form-control"
                              autoComplete="on"
                            />
                          </FormField>
                          <ErrorMessage name="confirmPassword" />
                        </div>
                        <div className="form-group password-field"></div>
                        <div className="row"></div>
                        <div className="row">
                          <div className="col-12">
                            <button
                              type="submit"
                              className="btn btn-primary btn-block"
                            >
                              Submit
                            </button>
                          </div>
                        </div>
                      </Form>
                    </Formik>
                    <div className="row">
                      <Link to="./">Already Signed Up? Login Now</Link>
                    </div>
                    <div className="row">
                      {errorSignUp ? <p>{errorSignUp}</p> : <React.Fragment />}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
};

export default Register;
