import { useDispatch, useSelector } from "react-redux";
import { Modal, FormField, Button } from "semantic-ui-react";
import { Form } from "formik-semantic-ui-react";
import { Formik, Field, ErrorMessage } from "formik";
import TextError from "./TextError";
import * as validator from "validator";
import { Loader, Dimmer } from "semantic-ui-react";
import React from "react";
import { forgotCodesAuth } from "./store/actions";

const ForgotCodes = function (props) {
  const spinner = useSelector((state) => state.auth.spinner);
  const dispatch = useDispatch();
  const forgotCodesError = useSelector((state) => state.auth.forgotCodesError);

  const validate = function (formValues) {
    let error = {};
    if (!formValues.email) {
      error.password = "* Please provide a email";
    }
    if (formValues.email && !validator.isEmail(formValues.email)) {
      error.email = "* Please provide a  correct email";
    }

    if (!formValues.password) {
      error.password = "* Please enter a password";
    }
    return error;
  };
  const onSubmit = function (formValues) {
    dispatch(forgotCodesAuth(formValues));
  };

  if (spinner) {
    return (
      <Dimmer active>
        <Loader size="big">Loading</Loader>
      </Dimmer>
    );
  } else {
    return (
      <Modal open={props.open} onClose={props.onClose} dimmer={"inverted"}>
        <Modal.Header>Forgot Codes</Modal.Header>
        <Modal.Content>
          <Formik
            initialValues={{
              email: "",
              password: "",
            }}
            onSubmit={onSubmit}
            inverted={"dimmer"}
            validate={validate}
          >
            {(formik) => {
              return (
                <Form>
                  <FormField>
                    <label htmlFor="emailId">Email</label>
                    <Field id="emailId" name="email" />
                    <ErrorMessage name="email" component={TextError} />
                  </FormField>
                  <FormField>
                    <label htmlFor="passwordId">Password</label>
                    <Field id="passwordId" type="password" name="password" />
                    <ErrorMessage name="password" component={TextError} />
                  </FormField>
                  <Button type="submit">Submit</Button>
                </Form>
              );
            }}
          </Formik>
        </Modal.Content>
        <Modal.Actions>
          {forgotCodesError ? (
            <p style={{ color: "red" }}>{forgotCodesError}</p>
          ) : (
            <React.Fragment />
          )}
        </Modal.Actions>
      </Modal>
    );
  }
};

export default ForgotCodes;
