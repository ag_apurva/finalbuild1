import { Modal } from "semantic-ui-react";
import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { callRegisterAgain } from "./store/actions";
const RegisterAgain = function (props) {
  const forgotCodesData = useSelector((state) => state.auth.forgotCodesData);
  const registerAgainFormValues = useSelector(
    (state) => state.auth.registerAgainFormValues
  );
  const registerAgainError = useSelector(
    (state) => state.auth.registerAgainError
  );
  // console.log("registerAgainFormValues", registerAgainFormValues);
  const dispatch = useDispatch();
  const history = useHistory();
  const renderUniversityData = function () {
    return forgotCodesData.map((item, index) => {
      return (
        <div className="item" key={index}>
          <div className="left aligned content">{item.universityName}</div>
          <div className="right aligned content">{item.universityCode}</div>
          <div className="right aligned content">{item.userRole}</div>
        </div>
      );
    });
  };
  const forgotCodesError = useSelector((state) => state.auth.forgotCodesError);
  return (
    <Modal open={props.open} onClose={props.onClose} dimmer={"inverted"}>
      <Modal.Header>Register Again</Modal.Header>
      <Modal.Content>
        <div className="content">
          <div className="ui divided items">
            <div className="item">
              <div className="left aligned content">Collge</div>
              <div className="right aligned content">Code</div>
              <div className="right aligned content">Role</div>
            </div>
            {renderUniversityData()}
          </div>
          <button
            className="btn btn-primary btn-block"
            onClick={() =>
              dispatch(callRegisterAgain(registerAgainFormValues, history))
            }
          >
            Register Again
          </button>
        </div>
      </Modal.Content>
      <Modal.Actions>
        {forgotCodesError ? (
          <p style={{ color: "red" }}>{forgotCodesError}</p>
        ) : (
          <React.Fragment />
        )}
        {registerAgainError ? (
          <p style={{ color: "red" }}>{registerAgainError}</p>
        ) : (
          <React.Fragment />
        )}
      </Modal.Actions>
    </Modal>
  );
};

export default RegisterAgain;
