import { Modal } from "semantic-ui-react";
import { FormField, Button } from "semantic-ui-react";
import { Form } from "formik-semantic-ui-react";
import { Formik, Field, ErrorMessage } from "formik";
import { useState } from "react";
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { confirmSignUpAmplify } from "./store/actions";
const ConfirmSignUp = function (props) {
  const [errors, setErrors] = useState("");
  const dispatch = useDispatch();
  const errorConfirmSignUp = useSelector(
    (state) => state.auth.errorConfirmSignUp
  );
  console.log("errorConfirmSignUp", errorConfirmSignUp);
  let history = useHistory();
  const validate = function (formValues) {
    let errors = {};
    if (!formValues.confirmCode) {
      errors.confirmCode = "* Please provide a confirm Code";
    }
    return errors;
  };

  const onClick = function () {
    console.log("on click called");
  };
  return (
    <Modal open={props.open} onClose={props.onClose} dimmer={"inverted"}>
      <Modal.Header>Confirm SignUp</Modal.Header>
      <Modal.Content>
        <Formik
          initialValues={{
            email: props.email,
            confirmCode: "",
          }}
          validate={validate}
          onSubmit={async (formValues) => {
            let Options = {
              clientMetadata: props.attributes,
            };
            dispatch(
              confirmSignUpAmplify(
                props.email,
                formValues.confirmCode,
                Options,
                history
              )
            );
          }}
        >
          <Form>
            <FormField>
              <label>Email</label>
              <Field name="email" />
            </FormField>
            <FormField>
              <label>ConfirmCode</label>
              <Field name="confirmCode" />
            </FormField>
            <Button type="submit">Submit</Button>
            <ErrorMessage name="confimCode" />
            {errorConfirmSignUp}
            {/* if any error occurs setFiledErrror of confirmCode will receive that error */}
          </Form>
        </Formik>
      </Modal.Content>
      {errorConfirmSignUp ? (
        <Modal.Content>{errorConfirmSignUp}</Modal.Content>
      ) : (
        ""
      )}
      <Modal.Actions>
        <Button type="submit" onClick={onClick}>
          ReSendCode
        </Button>
      </Modal.Actions>
    </Modal>
  );
};

export default ConfirmSignUp;

export const confirmSignUpAmplify = function (email, code, options, history) {
  return async function (dispatch) {
    try {
      let signUpData = await Auth.confirmSignUp(email, code, options);
      console.log("Data", signUpData);
      dispatch(authActions.signIn());
      history.push("/welcome");
    } catch (e) {
      console.log("error Confirm singup", e);
      dispatch(authActions.errorConfirmSignUpReducer({ error: e.message }));
    }
  };
};
