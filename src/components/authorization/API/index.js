export const ADMIN_PROFILE =
  "https://l12w9l17oj.execute-api.us-east-2.amazonaws.com/build/authentication/adminmyprofile";
/*eslint eqeqeq: "off"*/
export const FORGOT_CODES =
  "https://l12w9l17oj.execute-api.us-east-2.amazonaws.com/build/admissions/forgotcodes";
export const REGISTER_AGAIN =
  "https://l12w9l17oj.execute-api.us-east-2.amazonaws.com/build/admissions/registeragain";
