/*eslint eqeqeq: "off"*/
import { useDispatch, useSelector } from "react-redux";
import { Modal, FormField, Button } from "semantic-ui-react";
import { Form } from "formik-semantic-ui-react";
import { Formik, Field, ErrorMessage } from "formik";
import TextError from "./TextError";
import React from "react";
import { forgotPasswordSecondAction } from "./store/actions";
import { authActions } from "./store/index";
import validator from "validator";

const ForgotPassword2 = function (props) {
  const forgotPasswordError = useSelector(
    (state) => state.auth.forgotPasswordError
  );

  //   const emailForgotPassword = useSelector(
  //     (state) => state.auth.emailForgotPassword
  //   );

  //   console.log("emailForgotPassword", emailForgotPassword);
  const submitSuccessful = useSelector((state) => state.auth.submitSuccessful);
  if (submitSuccessful) {
    props.onClose();
  }
  const dispatch = useDispatch();

  const onSubmitTwo = function (formValues) {
    dispatch(
      authActions.setforgotPasswordError({
        forgotPasswordError: "",
      })
    );
    dispatch(
      forgotPasswordSecondAction(
        formValues.email,
        formValues.code,
        formValues.password
      )
    );
  };

  const validate = function (formValues) {
    let error = {};
    if (!formValues.email) {
      error.password = "* Please provide a email";
    }
    if (formValues.email && !validator.isEmail(formValues.email)) {
      error.email = "* Please provide a  correct email";
    }
    if (!formValues.code) {
      error.code = "* Please enter a code";
    }
    if (!formValues.password) {
      error.password = "* Please enter a password";
    }
    if (!formValues.confirmPassword) {
      error.confirmPassword = "* Please enter a password";
    }
    if (
      formValues.confirmPassword &&
      formValues.password &&
      formValues.confirmPassword != formValues.password
    ) {
      error.confirmPassword = "* Passwords do not match";
    }
    return error;
  };
  return (
    <Modal open={props.open} onClose={props.onClose} dimmer={"inverted"}>
      <Modal.Header>Forgot Password</Modal.Header>
      <Modal.Content>
        <div>
          <Formik
            initialValues={{
              email: useSelector((state) => state.auth.emailForgotPassword),
              code: "",
              password: "",
              confirmPassword: "",
            }}
            onSubmit={onSubmitTwo}
            inverted={"dimmer"}
            validate={validate}
          >
            {(formik) => {
              return (
                <Form>
                  <FormField>
                    <label htmlFor="emailId">Email</label>
                    <Field id="emailId" name="email" />
                    <ErrorMessage name="email" component={TextError} />
                  </FormField>
                  <FormField>
                    <label htmlFor="codeId">Code</label>
                    <Field id="codeId" name="code" />
                    <ErrorMessage name="code" component={TextError} />
                  </FormField>
                  <FormField>
                    <label htmlFor="passwordId">Password</label>
                    <Field id="passwordId" type="password" name="password" />
                    <ErrorMessage name="password" component={TextError} />
                  </FormField>
                  <FormField>
                    <label htmlFor="confirmPasswordId">Confirm Password</label>
                    <Field
                      id="confirmPasswordId"
                      type="password"
                      name="confirmPassword"
                    />
                    <ErrorMessage
                      name="confirmPassword"
                      component={TextError}
                    />
                  </FormField>
                  <Button type="submit">Submit</Button>
                </Form>
              );
            }}
          </Formik>
        </div>
      </Modal.Content>
      <Modal.Actions>
        {forgotPasswordError ? (
          <p style={{ color: "red" }}>{forgotPasswordError}</p>
        ) : (
          <React.Fragment />
        )}
        <button
          onClick={() => {
            props.onClose();
          }}
        >
          Close
        </button>
      </Modal.Actions>
    </Modal>
  );
};

export default ForgotPassword2;
