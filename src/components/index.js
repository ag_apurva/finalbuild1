import * as AuthObject from "./authorization";
import { BrowserRouter, Route, Redirect } from "react-router-dom";
import { useSelector } from "react-redux";
import { Welcome } from "./layout/Welcome";
import { useHistory } from "react-router-dom";
const PrivateRoute = function (props) {
  const isAuthenticated = useSelector((state) => state.auth.isAuthenticated);

  if (isAuthenticated) {
    return <Route path={props.path} exact component={props.component} />;
  } else {
    return <Redirect to="/" />;
  }
};

const App = function () {
  let history = useHistory();
  return (
    <BrowserRouter history={history}>
      <Route path="/" exact component={AuthObject.Login} />
      <Route path="/register" exact component={AuthObject.Register} />
      <PrivateRoute path="/welcome" component={Welcome} />
      {/* <PrivateRoute path="/welcome/attendance" exact component={Schedule} />
      <PrivateRoute path="/welcome/examresult" exact component={Schedule} /> */}
    </BrowserRouter>
  );
};

export default App;
