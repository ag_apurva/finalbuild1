const mysql = require('mysql');
const moment = require("moment");
const axios = require("axios");
const pool = mysql.createPool({
    // connectionLimit: 20,
    host: 'campuslive.czmocuynmaaq.us-east-2.rds.amazonaws.com',
    user: 'admin',
    password: 'Vishal#3',
    database: 'CampusLiveDev'
});

exports.handler = async(event) => {
    console.log("event", JSON.stringify(event));
    if (event.resource === "/contacts/getcontacts") {
        /*this API is extracting information form UniContacts table based on UniCode(universityCode)*/
        if (!event.queryStringParameters) {
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: " universityCode needs to be passed in  params " })
            };
            return res;
        }
        else {
            try {
                let universityCode;
                let lastUpdatedEpochTime = 0;
                if (!event.queryStringParameters.universityCode) {
                    universityCode = event.queryStringParameters.universityCode || null; /* University Code works same as UniCode in Table  */
                    let res = {
                        "statusCode": 422,
                        "headers": {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        "body": JSON.stringify({ error: " universityCode needs to be passed in  params " })
                    };
                    /* the response is same because uniCode needs to be a part of queryStringParameters */
                    if (!universityCode || universityCode == null || universityCode == undefined) {
                        return res;
                    }
                    else {
                        return res;
                    }
                }
                else {
                    universityCode = event.queryStringParameters.universityCode
                }


                if (!event.queryStringParameters.lastUpdatedEpochTime) {
                    lastUpdatedEpochTime = 0;
                }

                try {
                    lastUpdatedEpochTime = parseInt(event.queryStringParameters.lastUpdatedEpochTime)
                    if (isNaN(lastUpdatedEpochTime)) {
                        lastUpdatedEpochTime = 0;
                    }


                    console.log("lastUpdatedEpochTime111", lastUpdatedEpochTime);

                }
                catch (e) {
                    lastUpdatedEpochTime = 0;
                }
                console.log("lastUpdatedEpochTime", lastUpdatedEpochTime);

                let extractContactsSql = `Select * from UniContacts where UniCode = ? and ChangeId != 3 and LastUpdateEpochTime >= ?`;
                let contactsData = await getOrder(extractContactsSql, [universityCode, lastUpdatedEpochTime]);
                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify(contactsData)
                };
                return res;
            }
            catch (e) {
                console.log("ERROR OCCURED");
                console.log("Internal Server Error", e);
                let res = {
                    "statusCode": 500,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ type: "Internal Server Error", error: JSON.stringify(e), category: "/contacts/getContacts" })
                };
                return res;
            }
        }
    }
    if (event.resource === "/contacts/deletecontact") {
        /*this API is deleting the contact bases on idUniContact and UniCode basically it sets the ChnageId to 3*/
        if (!event.queryStringParameters) {
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: " universityCode and idUniContact needs to be passed in  params " })
            };
            return res;
        }
        else {
            try {
                let universityCode;
                let idUniContact;
                if (!event.queryStringParameters.universityCode || !event.queryStringParameters.idUniContact) {
                    /* Modify the validation login in the ablut id statement
                    University Code works same as UniCode in Table  */
                    let res = {
                        "statusCode": 422,
                        "headers": {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        "body": JSON.stringify({ error: " universityCode and idUniContact needs to be passed in  param " })
                    };
                    return res;
                }
                else {
                    universityCode = event.queryStringParameters.universityCode;
                    idUniContact = event.queryStringParameters.idUniContact;
                }
                let deleteContactSql = `Update UniContacts set ChangeId = 3 where UniCode = ? and idUniContacts = ?`;
                let contactsData = await getOrder(deleteContactSql, [universityCode, idUniContact]);
                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ data: "UniContact deleted successfully" })
                };
                return res;
            }
            catch (e) {
                console.log("ERROR OCCURED", e);
                console.log("Internal Server Error", e);
                console.log("UniContact Deletion Failed", e);
                let res = {
                    "statusCode": 500,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ type: "Internal Server Error", error: JSON.stringify(e), category: "/contacts/getContacts" })
                };
                return res;
            }
        }
    }
    if (event.resource === "/contacts/createcontact") {
        /*   the api is create Contact which currently takes  Name,Location,Designation,PhoneNumber1,PhoneNumber2,UniCode as parameter */
        let inputBody = event.body ? JSON.parse(event.body) : {}; /* If event Body is not parsable if will be a empty object */
        if (inputBody.hasOwnProperty("UniCode") &&
            inputBody.hasOwnProperty("Name") &&
            inputBody.hasOwnProperty("Designation") &&
            inputBody.hasOwnProperty("Category")) {
            let UniCode = inputBody["UniCode"]
            let createContactObject = {};
            createContactObject['UniCode'] = inputBody["UniCode"];
            createContactObject['Name'] = inputBody["Name"];
            createContactObject['Designation'] = inputBody["Designation"];
            createContactObject['Category'] = inputBody["Category"];
            if (inputBody.hasOwnProperty("Location") && inputBody["Location"] != "") {
                createContactObject['Location'] = inputBody["Location"];
            }

            if (inputBody.hasOwnProperty("PhoneNumber1") && inputBody["PhoneNumber1"] != "") {
                createContactObject['PhoneNumber1'] = inputBody["PhoneNumber1"];
            }

            if (inputBody.hasOwnProperty("PhoneNumber2") && inputBody["PhoneNumber2"] != "") {
                createContactObject['PhoneNumber2'] = inputBody["PhoneNumber2"];
            }

            if (inputBody.hasOwnProperty("EmailId") && inputBody["EmailId"] != "") {
                createContactObject['EmailId'] = inputBody["EmailId"];
            }
            createContactObject["ChangeId"] = 1;
            createContactObject["LastUpdateEpochTime"] = Math.floor(Date.now() / 1000);
            try {
                let createContactSql = 'Insert into UniContacts set ?';
                await getOrder(createContactSql, [createContactObject])
                let newContact = await getOrder("select max(idUniContacts) as Value from UniContacts where UniCode=?", [UniCode]);
                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ data: newContact[0]["Value"] })
                };
                return res;
            }
            catch (e) {
                console.log("error in creating contact");
                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ type: "Internal Server Error", error: JSON.stringify(e), category: "/contacts/createcontact" })
                };
                return res;
            }




        }
        else {
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: " required paramater not present in body of POST Contact of Post" })
            };
            return res;
        }
    }
    if (event.resource === "/contacts/updatecontact") {
        /*   the api is to update Contact which currently takes  Name,Location,Designation,PhoneNumber1,PhoneNumber2,UniCode,idUniContact as parameter */
        let inputBody = event.body ? JSON.parse(event.body) : {}; /* If event Body is not parsable if will be a empty object */
        if (inputBody.hasOwnProperty("UniCode") &&
            inputBody.hasOwnProperty("Name") &&
            inputBody.hasOwnProperty("Designation")) {
            let idUniContact = inputBody["idUniContact"]
            let UniCode = inputBody["UniCode"]
            let updateContactObject = {};
            updateContactObject['UniCode'] = inputBody["UniCode"];
            updateContactObject['Name'] = inputBody["Name"];
            updateContactObject['Designation'] = inputBody["Designation"];
            if (inputBody.hasOwnProperty("Location") && inputBody["Location"] != "") {
                updateContactObject['Location'] = inputBody["Location"];
            }

            if (inputBody.hasOwnProperty("PhoneNumber1") && inputBody["PhoneNumber1"] != "") {
                updateContactObject['PhoneNumber1'] = inputBody["PhoneNumber1"];
            }

            if (inputBody.hasOwnProperty("PhoneNumber2") && inputBody["PhoneNumber2"] != "") {
                updateContactObject['PhoneNumber2'] = inputBody["PhoneNumber2"];
            }

            if (inputBody.hasOwnProperty("EmailId") && inputBody["EmailId"] != "") {
                updateContactObject['EmailId'] = inputBody["EmailId"];
            }
            updateContactObject["ChangeId"] = 2;

            try {
                let updateContactSql = 'update UniContacts set ? where UniCode = ? and idUniContacts = ?';
                let data = await getOrder(updateContactSql, [updateContactObject, UniCode, idUniContact])
                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ data: `Contact updated Successfully ${JSON.stringify(data)}  ${UniCode} ${idUniContact}` })
                };
                return res;
            }
            catch (e) {

                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ type: "Internal Server Error", error: JSON.stringify(e), category: "/contacts/updateContact" })
                };
                return res;
            }




        }
        else {
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: " required paramater not present in body of PUT Contact of Post" })
            };
            return res;
        }
    }
    if (event.resource === "/subjects") {
        /*this API is extracting information form Subject table based on UniCode(universityCode)*/
        if (!event.queryStringParameters) {
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: " universityCode needs to be passed in  params " })
            };
            return res;
        }
        else {
            try {
                let universityCode;
                if (!event.queryStringParameters.universityCode) {
                    universityCode = event.queryStringParameters.universityCode || null; /* University Code works same as UniCode in Table  */
                    let res = {
                        "statusCode": 422,
                        "headers": {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        "body": JSON.stringify({ error: " universityCode needs to be passed in  params " })
                    };
                    /* the response is same because uniCode needs to be a part of queryStringParameters */
                    if (!universityCode || universityCode == null || universityCode == undefined) {
                        return res;
                    }
                    else {
                        return res;
                    }
                }
                else {
                    universityCode = event.queryStringParameters.universityCode
                }
                let extractSubjectsSql = `Select * from Subject where UniCode = ?`;
                let subjectsData = await getOrder(extractSubjectsSql, [universityCode]);
                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify(subjectsData)
                };
                return res;
            }
            catch (e) {
                console.log("ERROR OCCURED");
                console.log("Internal Server Error", e);
                let res = {
                    "statusCode": 500,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ type: "Internal Server Error", error: JSON.stringify(e), category: "/subjects" })
                };
                return res;
            }
        }
    }
    if (event.resource === "/locations") {
        /*this API is extracting information form UniContacts table based on UniCode(universityCode)*/
        if (!event.queryStringParameters) {
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: " universityCode needs to be passed in  params " })
            };
            return res;
        }
        else {
            try {
                let universityCode;
                if (!event.queryStringParameters.universityCode) {
                    universityCode = event.queryStringParameters.universityCode || null; /* University Code works same as UniCode in Table  */
                    let res = {
                        "statusCode": 422,
                        "headers": {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        "body": JSON.stringify({ error: " universityCode needs to be passed in  params " })
                    };
                    /* the response is same because uniCode needs to be a part of queryStringParameters */
                    if (!universityCode || universityCode == null || universityCode == undefined) {
                        return res;
                    }
                    else {
                        return res;
                    }
                }
                else {
                    universityCode = event.queryStringParameters.universityCode
                }
                let extractLocationsSql = `Select * from UniClassRoom where UniCode = ?`;
                let locationsData = await getOrder(extractLocationsSql, [universityCode]);
                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify(locationsData)
                };
                return res;
            }
            catch (e) {
                console.log("ERROR OCCURED");
                console.log("Internal Server Error", e);
                let res = {
                    "statusCode": 500,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ type: "Internal Server Error", error: JSON.stringify(e), category: "/contacts/getContacts" })
                };
                return res;
            }
        }
    }
    if (event.resource === "/subjects/createsubjects") {
        /*this API is extracting information form Subject table based on UniCode(universityCode)*/
        /*   the api is create Contact which currently takes  Name,Location,Designation,PhoneNumber1,PhoneNumber2,UniCode as parameter */
        let inputBody = event.body ? JSON.parse(event.body) : {}; /* If event Body is not parsable if will be a empty object */
        if (inputBody.hasOwnProperty("UniCode") &&
            inputBody.hasOwnProperty("Subject")
        ) {
            let UniCode = inputBody["UniCode"]
            let createSubjectObject = {};
            createSubjectObject['UniCode'] = inputBody["UniCode"];
            if (inputBody.hasOwnProperty("Subject") && inputBody["Subject"] != "") {
                createSubjectObject['SubjName'] = inputBody["Subject"];
            }
            try {
                let valueForSubjectCode = await getTempValueForSubjectCode(UniCode);
                let subjectCode = "SBJ0" + parseInt(valueForSubjectCode);
                createSubjectObject['SubjCode'] = subjectCode;
                let createSubjectSql = 'Insert into Subject set ?';
                await getOrder(createSubjectSql, [createSubjectObject])

                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ data: subjectCode })
                };
                return res;
            }
            catch (e) {
                console.log("error in creating contact");
                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ type: "Internal Server Error", error: JSON.stringify(e), category: "/subjects/createsubjectst" })
                };
                return res;
            }
        }
        else {
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: " required paramater not present in body of POST Subject of Subject" })
            };
            return res;
        }
    }
    if (event.resource === "/locations/createlocations") {
        /*this API is extracting information form Subject table based on UniCode(universityCode)*/
        /*   the api is create Contact which currently takes  Name,Location,Designation,PhoneNumber1,PhoneNumber2,UniCode as parameter */
        let inputBody = event.body ? JSON.parse(event.body) : {}; /* If event Body is not parsable if will be a empty object */
        if (inputBody.hasOwnProperty("UniCode") &&
            inputBody.hasOwnProperty("ClassRoomName") &&
            inputBody.hasOwnProperty("ClassRoomDetail") &&
            inputBody.hasOwnProperty("ClassRoomType")
        ) {
            //ClassRoomName ClassRoomDetail ClassRoomType
            let UniCode = inputBody["UniCode"]
            let ClassRoomName = inputBody["ClassRoomName"]
            let ClassRoomDetail = inputBody["ClassRoomDetail"]
            let ClassRoomType = inputBody["ClassRoomType"]
            let createLocationObject = {};
            createLocationObject['UniCode'] = inputBody["UniCode"];
            if (inputBody.hasOwnProperty("ClassRoomName") && inputBody["ClassRoomName"] != "") {
                createLocationObject['ClassRoomName'] = inputBody["ClassRoomName"];
            }
            if (inputBody.hasOwnProperty("ClassRoomDetail") && inputBody["ClassRoomDetail"] != "") {
                createLocationObject['ClassRoomDetail'] = inputBody["ClassRoomDetail"];
            }
            if (inputBody.hasOwnProperty("ClassRoomType") && inputBody["ClassRoomType"] != "") {
                createLocationObject['ClassRoomType'] = inputBody["ClassRoomType"];
            }
            try {
                let createLocationSql = 'Insert into UniClassRoom set ?';
                await getOrder(createLocationSql, [createLocationObject])

                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ data: "Data Inserted SuccessFully" })
                };
                return res;
            }
            catch (e) {
                console.log("error in creating contact");
                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ type: "Internal Server Error", error: JSON.stringify(e), category: "/subjects/createsubjectst" })
                };
                return res;
            }
        }
        else {
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: " required paramater not present in body of POST Subject of Subject" })
            };
            return res;
        }
    }
    else if (event.resource === "/savemyprofile") {
        /*   the api is create Contact which currently takes  Name,Location,Designation,PhoneNumber1,PhoneNumber2,UniCode as parameter */
        let inputBody = event.body ? JSON.parse(event.body) : {}; /* If event Body is not parsable if will be a empty object */
        if (inputBody.hasOwnProperty("UniCode") &&
            inputBody.hasOwnProperty("UserCode") &&
            inputBody.hasOwnProperty("UserName") &&
            inputBody.hasOwnProperty("UserDesignation") &&
            inputBody.hasOwnProperty("UserPhone") &&
            inputBody.hasOwnProperty("UserAddress") &&
            inputBody.hasOwnProperty("UserPinCode")) {

            let createContactObject = {};
            let UniCode = inputBody["UniCode"];
            let UserCode = inputBody["UserCode"];

            var sqlCheck = 'SELECT * FROM UserInfo where UserCode = ? and UniCode = ? and ChangeId != 3 and UserRole="Admin"';
            let studentCheckData = await getOrder(sqlCheck, [UserCode, UniCode]);
            if (studentCheckData.length == 0) {
                let statusCode = 400;
                let res = {
                    "statusCode": statusCode,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ error: " User does not exist " })
                };

                return res;
            }

            createContactObject['UserName'] = inputBody["UserName"];
            createContactObject['UserDesignation'] = inputBody["UserDesignation"];
            createContactObject['UserPhone'] = inputBody["UserPhone"];
            createContactObject['UserAddress'] = inputBody["UserAddress"];
            createContactObject['UserPinCode'] = inputBody["UserPinCode"];


            //  ["LastUpdatedEpochTime"] = Math.floor(Date.now() / 1000)
            if (inputBody.hasOwnProperty("UserDOB") && inputBody["UserDOB"] != "") {
                createContactObject['UserDOB'] = inputBody["UserDOB"];
            }

            if (inputBody.hasOwnProperty("BloodGroup") && inputBody["BloodGroup"] != "") {
                createContactObject['BloodGroup'] = inputBody["BloodGroup"];
            }

            createContactObject['LastUpdateEpochTime'] = Math.floor(Date.now() / 1000);
            createContactObject["ChangeId"] = 2;
            try {
                let updateProfile = 'Update UserInfo set ? where UniCode = ?  and UserCode=?  and UserRole="Admin" and ChangeId!=3';
                await getOrder(updateProfile, [createContactObject, UniCode, UserCode])

                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ data: "User Updated Successfully" })
                };
                return res;
            }
            catch (e) {
                console.log("error in creating contact");
                let res = {
                    "statusCode": 400,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ type: "Internal Server Error", error: JSON.stringify(e), category: "/savemyprofile" })
                };
                return res;
            }




        }
        else {
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: " required paramater not present in body of POST Contact of Post" })
            };
            return res;
        }
    }
    else if (event.resource === "/getdepartments") {

        if (!event.queryStringParameters) {
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: " universityCode needs to be passed in  params " })
            };
            return res;
        }
        else {
            try {
                let universityCode;
                if (!event.queryStringParameters.universityCode) {
                    universityCode = event.queryStringParameters.universityCode || null; /* University Code works same as UniCode in Table  */
                    let res = {
                        "statusCode": 422,
                        "headers": {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        "body": JSON.stringify({ error: " universityCode needs to be passed in  params " })
                    };
                    /* the response is same because uniCode needs to be a part of queryStringParameters */
                    if (!universityCode || universityCode == null || universityCode == undefined) {
                        return res;
                    }
                    else {
                        return res;
                    }
                }
                else {
                    universityCode = event.queryStringParameters.universityCode
                }
                let sqlDepartmentsData = 'Select * from UniDepartment where UniCode= ?'
                const data = await getOrder(sqlDepartmentsData, [universityCode]);
                console.log("sqlDepartmentsData", data);
                const departments = [];
                data.forEach((item) => {
                    departments.push({
                        departmentName: item.UniDeptName,
                        departmentCode: item.UniDeptCode,
                        UniDeptHead: item.UniDeptHead,
                        error: null
                    })
                })

                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify(departments)
                };
                return res;
            }
            catch (e) {
                console.log("ERROR OCCURED");
                console.log("Internal Server Error", e);
                let res = {
                    "statusCode": 500,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ type: "Internal Server Error", error: JSON.stringify(e), category: "/getdepartments" })
                };
                return res;
            }
        }

    }
    else if (event.resource === "/createdepartments") {
        let inputBody = event.body ? JSON.parse(event.body) : {}; /* If event Body is not parsable if will be a empty object */
        if (inputBody.hasOwnProperty("UniCode") &&
            inputBody.hasOwnProperty("DepartmentName") &&
            inputBody.hasOwnProperty("DepartmentHead")
        ) {
            try {
                let createDepartmentsObject = {};
                let UniCode = inputBody['UniCode']
                createDepartmentsObject['UniCode'] = inputBody['UniCode'];
                createDepartmentsObject['UniDeptName'] = inputBody['DepartmentName'];
                createDepartmentsObject['UniDeptHead'] = inputBody['DepartmentHead'];

                let sqlCountValues = 'select * from CountValues where UniCode= ?';
                let dataCountValues = await getOrder(sqlCountValues, UniCode);
                let UniDeptCode = dataCountValues[0].UniDeptCode;
                console.log("UniDeptCode", UniDeptCode);
                let updateCountValues = 'update CountValues set ? where  UniCode=?';
                let createLocationSql = 'Insert into UniDepartment set ?';
                let departmentCode = "DP0" + UniDeptCode;
                createDepartmentsObject['UniDeptCode'] = departmentCode;
                await getOrder(createLocationSql, [createDepartmentsObject])
                await getOrder(updateCountValues, [{ UniDeptCode: UniDeptCode + 1 }, UniCode]);
                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ data: departmentCode })
                };
                return res;
            }
            catch (e) {
                console.log("error in creating createdepartments");
                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ type: "Internal Server Error", error: JSON.stringify(e), category: "/createdepartments" })
                };
                return res;
            }
        }
        else {
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: " required paramater not present in body of POST Create Departments" })
            };
            return res;
        }
    }
    else if (event.resource === "/admissions/getadmissions") {
        /*this API is extracting information form UniContacts table based on UniCode(universityCode)*/
        if (!event.queryStringParameters) {
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: " universityCode needs to be passed in  params " })
            };
            return res;
        }
        else {
            try {
                let universityCode;
                if (!event.queryStringParameters.universityCode) {
                    universityCode = event.queryStringParameters.universityCode || null; /* University Code works same as UniCode in Table  */
                    let res = {
                        "statusCode": 422,
                        "headers": {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        "body": JSON.stringify({ error: " universityCode needs to be passed in  params " })
                    };
                    /* the response is same because uniCode needs to be a part of queryStringParameters */
                    if (!universityCode || universityCode == null || universityCode == undefined) {
                        return res;
                    }
                    else {
                        return res;
                    }
                }
                else {
                    universityCode = event.queryStringParameters.universityCode
                }
                let sqlAdmissionInfoData = 'Select * from AdmissionInfo where UniCode= ?'
                const data = await getOrder(sqlAdmissionInfoData, [universityCode]);
                console.log("sqlAdmissionInfo", data);
                const admissionInfo = [];
                data.forEach((item) => {
                    admissionInfo.push({
                        admissionCode: item.AdmCode,
                        admissionYear: item.AdmissionYear,
                        departmentCode: item.DeptCode,
                        departmentName: item.DeptName,
                        error: null
                    })
                })
                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify(admissionInfo)
                };
                return res;
            }
            catch (e) {
                console.log("ERROR OCCURED");
                console.log("Internal Server Error", e);
                let res = {
                    "statusCode": 500,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,POST,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ type: "Internal Server Error", error: JSON.stringify(e), category: "/admissions/getadmissions" })
                };
                return res;
            }
        }
    }
    else if (event.resource === "/admissions/createadmissions") {

        let inputBody = event.body ? JSON.parse(event.body) : {}; /* If event Body is not parsable if will be a empty object */
        if (inputBody.hasOwnProperty("admissionYear") &&
            inputBody.hasOwnProperty("departmentCode") &&
            inputBody.hasOwnProperty("degreeOffered") &&
            inputBody.hasOwnProperty("academicStartData") &&
            inputBody.hasOwnProperty("academicEndData") &&
            inputBody.hasOwnProperty("DeptName") &&
            inputBody.hasOwnProperty("UniCode")
        ) {
            let UniCode = inputBody["UniCode"]
            let createAdmissionstObject = {};
            createAdmissionstObject['UniCode'] = inputBody["UniCode"];
            createAdmissionstObject['AdmissionYear'] = inputBody["admissionYear"];
            createAdmissionstObject['DeptCode'] = inputBody["departmentCode"];
            createAdmissionstObject['DegreeOffered'] = inputBody["degreeOffered"];
            createAdmissionstObject['AcadStartDate'] = inputBody["academicStartData"];
            createAdmissionstObject['AcadEndDate'] = inputBody["academicEndData"];
            createAdmissionstObject['DeptName'] = inputBody["DeptName"];

            try {
                var sqlCountValues = 'select * from CountValues where UniCode=?';
                var dataCountValues = await getOrder(sqlCountValues, [UniCode]);
                let admValue = parseInt(dataCountValues[0]["AdmCode"]) + 1;
                let admissionCode = "ADM0" + admValue;
                createAdmissionstObject['AdmCode'] = admissionCode;
                let sqlUpdateCountValues = 'update CountValues Set ? WHERE UniCode = ?';
                await getOrder(sqlUpdateCountValues, [{ AdmCode: admValue }, UniCode]);
                console.log("admissionCode", admissionCode);
                let updateAdmissionTable = 'INSERT INTO AdmissionInfo SET ?';
                let dataAdmissionTable = await getOrder(updateAdmissionTable, [createAdmissionstObject])
                console.log("dataDepartmentsTable", dataAdmissionTable)
                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,POST,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ data: admissionCode })
                };
                return res;
            }
            catch (e) {
                console.log("error in creating admissions");
                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ type: "Internal Server Error", error: JSON.stringify(e), category: "/admissions/createadmissions" })
                };
                return res;
            }
        }
        else {
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: " required paramater not present in body of POST Admissions of Create Admissions" })
            };
            return res;
        }
    }
    else if (event.resource === "/admissions/deleteadmissions") {}
    else if (event.resource === "/admissions/uploadadmissions") {

        let inputBody = event.body ? JSON.parse(event.body) : {}; /* If event Body is not parsable if will be a empty object */
        if (inputBody.hasOwnProperty("data") &&
            inputBody.hasOwnProperty("UniCode")
        ) {
            let UniCode = inputBody["UniCode"]
            let data = inputBody["data"];

            try {
                var sqlCountValues = 'select * from CountValues where UniCode=?';
                var dataCountValues = await getOrder(sqlCountValues, [UniCode]);
                let admValue = parseInt(dataCountValues[0]["AdmCode"]) + 1;
                console.log("data", data);
                for (let i = 0; i < data.length; i++) {
                    let item = data[i];
                    let createAdmissionstObject = {};
                    let admissionCode = "ADM0" + admValue;
                    console.log("admissionCode", admissionCode);
                    createAdmissionstObject['AdmCode'] = admissionCode;
                    createAdmissionstObject['UniCode'] = UniCode;
                    createAdmissionstObject['AdmissionYear'] = item["admissionYear"];
                    createAdmissionstObject['DeptName'] = item["departmentName"];
                    createAdmissionstObject['DeptCode'] = item["departmentCode"];
                    createAdmissionstObject['DegreeOffered'] = item["degreeOffered"];
                    createAdmissionstObject['AcadStartDate'] = item["academicStartData"];
                    createAdmissionstObject['AcadEndDate'] = item["academicEndData"];
                    console.log("admissionCode", admissionCode);
                    let updateAdmissionTable = 'INSERT INTO AdmissionInfo SET ?';
                    let dataAdmissionTable = await getOrder(updateAdmissionTable, [createAdmissionstObject])
                    console.log("dataDepartmentsTable", dataAdmissionTable);
                    admValue = admValue + 1;
                }
                let sqlUpdateCountValues = 'update CountValues Set ? WHERE UniCode = ?';
                await getOrder(sqlUpdateCountValues, [{ AdmCode: admValue }, UniCode]);
                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,POST,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ data: "data upload successfully" })
                };
                return res;
            }
            catch (e) {
                console.log("error in creating admissions");
                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ type: "Internal Server Error", error: JSON.stringify(e), category: "/admissions/createadmissions" })
                };
                return res;
            }
        }
        else {
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: " required paramater not present in body of POST Admissions of Create Admissions" })
            };
            return res;
        }
    }
    else if (event.resource === "/classes/uploadclasses") {

        let inputBody = event.body ? JSON.parse(event.body) : {}; /* If event Body is not parsable if will be a empty object */
        if (inputBody.hasOwnProperty("data") &&
            inputBody.hasOwnProperty("UniCode")
        ) {
            let UniCode = inputBody["UniCode"]
            let data = inputBody["data"];

            try {
                var sqlCountValues = 'select * from CountValues where UniCode=?';
                var dataCountValues = await getOrder(sqlCountValues, [UniCode]);
                let admValue = parseInt(dataCountValues[0]["AdmCode"]) + 1;
                console.log("data", data);
                for (let i = 0; i < data.length; i++) {
                    let createClassObject = {};
                    let item = data[i];
                    createClassObject['AdmCode'] = item["Admission Code Select"];
                    createClassObject['SubYear'] = item["SubYear Select"];
                    createClassObject['Semester'] = item["Semister Select"];
                    createClassObject['Division'] = item["Division Select"];
                    createClassObject['Sub Division'] = item["Sub Division Select"];
                    let updateClassTable = 'INSERT INTO Class SET ?';
                    let dataAdmissionTable = await getOrder(updateClassTable, [createClassObject]);
                    console.log("dataDepartmentsTable", dataAdmissionTable);
                }
                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,POST,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ data: "data upload successfully" })
                };
                return res;
            }
            catch (e) {
                console.log("error in creating admissions");
                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ type: "Internal Server Error", error: JSON.stringify(e), category: "/classes/uploadclasses" })
                };
                return res;
            }
        }
        else {
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: " required paramater not present in body of POST Admissions of Create Admissions" })
            };
            return res;
        }
    }
    else if (event.resource === "/tasks/notifications") {

        let inputBody = event.body ? JSON.parse(event.body) : {}; /* If event Body is not parsable if will be a empty object */
        if (inputBody.hasOwnProperty("userCode") &&
            inputBody.hasOwnProperty("title") &&
            inputBody.hasOwnProperty("body") &&
            inputBody.hasOwnProperty("classIds") &&
            inputBody.hasOwnProperty("UniCode")
        ) {
            let userCode = inputBody["userCode"];
            let body = inputBody["body"];
            let classIds = inputBody["classIds"];
            let title = inputBody["title"];
            let UniCode = inputBody["UniCode"];

            for (let i = 0; i < classIds.length; i++) {
                let classId = classIds[i];
                let classTopic = `/topics/ClassTopic_${UniCode}_${classId}`;
                try {
                    let data = JSON.stringify({
                        to: classTopic,
                        priority: "high",
                        "content-available": true,
                        data: {
                            type: title,
                            title: 'Proffessor_Notification',
                            body: body,
                        }
                    });
                    let config = {
                        method: "post",
                        content_available: 1,
                        aps: JSON.stringify({
                            "content-available": 1,
                            "mutable-content": 1,
                            alert: {
                                type: title,
                                title: 'Proffessor_Notification',
                                body: body,
                            },
                            badge: 1,
                            sound: "default",
                        }),
                        url: "https://fcm.googleapis.com/fcm/send",
                        headers: {
                            Authorization: "key=AAAA6amStzE:APA91bGJEM79aWSNM7BYs81n8PLAoQR_TylQO1d1JqYI0EB8o3UJSviMsKL2tjrxniOGxfzYEsENUUIiT7_PcJu_mhyNkeiiysllXzY7MnZcACqU9CWUE3cv4zHsxtPmG-VSydg9Z_GW",
                            "Content-Type": "application/json",
                        },
                        data: data,
                    };
                    console.log("config", config);
                    await axios(config);
                }
                catch (e) {
                    console.log("error sending to firebase professor old", e);
                }

            }

            try {
                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,POST,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ data: "data upload successfully" })
                };
                return res;
            }
            catch (e) {
                console.log("error in creating admissions");
                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ type: "Internal Server Error", error: JSON.stringify(e), category: "/tasks/notifications" })
                };
                return res;
            }
        }
        else {
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: " required paramater not present in body of POST Admissions of Create Admissions" })
            };
            return res;
        }
    }
    else if (event.resource === "/results/getresult") {
        if (!event.queryStringParameters) {
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: " universityCode and student code needs to be passed in  params " })
            };
            return res;
        }
        else {
            try {
                let universityCode, studentUserCode;
                if (!event.queryStringParameters.universityCode || !event.queryStringParameters.studentUserCode) {
                    universityCode = event.queryStringParameters.universityCode || null; /* University Code works same as UniCode in Table  */
                    let res = {
                        "statusCode": 422,
                        "headers": {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        "body": JSON.stringify({ error: " universityCode needs to be passed in  params " })
                    };
                    /* the response is same because uniCode needs to be a part of queryStringParameters */
                    if (!universityCode || universityCode == null || universityCode == undefined) {
                        return res;
                    }
                    else {
                        return res;
                    }
                }
                else {
                    universityCode = event.queryStringParameters.universityCode;
                    studentUserCode = event.queryStringParameters.studentUserCode;
                }
                let resultPerStudentSql = 'Select * from UniStudentSubjectMarks where UniCode= ? and StudentCode=?'
                const resultPerStudent = await getOrder(resultPerStudentSql, [universityCode, studentUserCode]);
                let resultWeightageSql = 'Select * from UniSubjectExams where UniCode= ?';
                const resultWeightage = await getOrder(resultWeightageSql, [universityCode]);
                let subjectSql = 'Select * from Subject where UniCode= ?';
                const resultSubject = await getOrder(subjectSql, [universityCode]);

                resultPerStudent.map((item) => {
                    const resultValue = resultWeightage.find((result) => result.SubjectCode == item.SubjCode && result.ExamType == item.ExamType)
                    item.relativeResult = parseFloat((resultValue.Weightage * item.Marks) / item.OutOf)
                    let subjectData = resultSubject.find((subject) => subject.SubjCode == item.SubjCode)
                    item.SubjectName = subjectData["SubjName"];
                    item.WeightAge = resultValue.Weightage;
                })

                const data = {
                    resultPerStudent,
                }




                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify(resultPerStudent)
                };
                return res;
            }
            catch (e) {
                console.log("ERROR OCCURED");
                console.log("Internal Server Error", e);
                let res = {
                    "statusCode": 500,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ type: "Internal Server Error", error: JSON.stringify(e), category: "/results/getresult" })
                };
                return res;
            }
        }

    }
    else if (event.resource === "/students/getstudents") {
        if (!event.queryStringParameters) {
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: " universityCode needs to be passed in  params " })
            };
            return res;
        }
        else {
            try {
                let universityCode;
                if (!event.queryStringParameters.universityCode) {
                    universityCode = event.queryStringParameters.universityCode || null; /* University Code works same as UniCode in Table  */
                    let res = {
                        "statusCode": 422,
                        "headers": {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        "body": JSON.stringify({ error: " universityCode needs to be passed in  params " })
                    };
                    /* the response is same because uniCode needs to be a part of queryStringParameters */
                    if (!universityCode || universityCode == null || universityCode == undefined) {
                        return res;
                    }
                    else {
                        return res;
                    }
                }
                else {
                    universityCode = event.queryStringParameters.universityCode
                }

                let getStudentsSql = `SELECT UserInfo.UserCode,UserInfo.UserDOB,UserInfo.UserPinCode,UserInfo.UserEmail,UserInfo.UserName,UserInfo.UserPhone,UserInfo.UserRollNo,UserInfo.UserParentName,UserInfo.UserParentContactNo,UserInfo.UserAddress,UserInfo.UserPinCode,UserInfo.BloodGroup,            UserInfo.UserRegistrationNo, ClassStudent.ClassId, Class.SubYear, Class.Semester, Class.Division FROM UserInfo INNER JOIN ClassStudent
            INNER JOIN Class
            where ClassStudent.StudentCode = UserInfo.UserCode
            and ClassStudent.UniCode = UserInfo.UniCode
            and UserInfo.UniCode = ?
            and Class.idClass = ClassStudent.ClassId
            and UserInfo.UserRole = "Student"
            and UserInfo.ChangeId != 3`;



                let students = await getOrder(getStudentsSql, [universityCode]);
                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify(students)
                };
                return res;
            }
            catch (e) {
                console.log("ERROR OCCURED");
                console.log("Internal Server Error", e);
                let res = {
                    "statusCode": 500,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,POST,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ type: "Internal Server Error", error: JSON.stringify(e), category: "/students/getstudents" })
                };
                return res;
            }
        }
    }
    else if (event.resource === "/puttask") {
        let inputBody = JSON.parse(event.body);
        if (inputBody.hasOwnProperty("UserEmail") && inputBody["UserEmail"] != "" &&
            inputBody.hasOwnProperty("UniCode") && inputBody["UniCode"] != ""

        ) {
            var sqlCheck = 'SELECT * FROM UserInfo where UserEmail = ?  and UniCode = ? and  ChangeId != ?';
            let studentCheckData = await getOrder(sqlCheck, [inputBody["UserEmail"], inputBody["UniCode"], 3]);
            //checking whether user exist in database
            if (studentCheckData.length == 0) {
                let statusCode = 400;
                let res = {
                    "statusCode": statusCode,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ error: " User does not exist " })
                };

                return res;
            }
            var sql1 = `Select * from CountValues where UniCode=?`;
            let taskIdData = await getOrder(sql1, [inputBody["UniCode"]]);
            let taskIdValue = parseInt(taskIdData[0]['TaskId'])
            let classIds = inputBody["ClassId"];
            if (classIds && classIds != undefined && classIds != " " && classIds.length > 0) {

                const obj = {};
                obj["UserCode"] = studentCheckData[0].UserCode;
                Object.keys(inputBody).forEach(item => {
                    if (!(item == "UserEmail" || item == "TaskId" || item == "LastUpdatedEpochTime" || item == "ChangeId" || inputBody[item] == "")) {
                        obj[item] = inputBody[item];
                    }
                });
                obj["ChangeId"] = 1;
                obj["LastUpdatedEpochTime"] = Math.floor(Date.now() / 1000);
                taskIdValue = taskIdValue + 1;
                obj["TaskId"] = taskIdValue;


                let sqlInsert = `Insert into UserTask Set  ?`
                let userTaskData = await getOrder(sqlInsert, [obj]);
                for (let i = 0; i < classIds.length; i++) {
                    let getStudentsSql = `SELECT UserInfo.UserCode,UserInfo.UserDOB,UserInfo.UserPinCode,UserInfo.UserEmail,UserInfo.UserName,UserInfo.UserPhone,UserInfo.UserRollNo,UserInfo.UserParentName,UserInfo.UserParentContactNo,UserInfo.UserAddress,UserInfo.UserPinCode,UserInfo.BloodGroup,            UserInfo.UserRegistrationNo, ClassStudent.ClassId, Class.SubYear, Class.Semester, Class.Division FROM UserInfo INNER JOIN ClassStudent
            INNER JOIN Class
            where ClassStudent.StudentCode = UserInfo.UserCode
            and ClassStudent.UniCode = UserInfo.UniCode
            and UserInfo.UniCode = ?
            and Class.idClass = ClassStudent.ClassId
            and UserInfo.UserRole = "Student"
            and UserInfo.ChangeId != 3 
            and Class.idClass = ?`;
                    let studentsData = await (getStudentsSql, [inputBody["UniCode"], classIds[i]]);
                    let classId = classIds[i];
                    let classTopic = `/topics/ClassTopic_${inputBody["UniCode"]}_${classId}`;
                    try {
                        let data = JSON.stringify({
                            to: classTopic,
                            priority: "high",
                            "content-available": true,
                            data: {
                                type: "AddTask",
                                title: 'Task_Notification',
                                body: "Task Created",
                            }
                        });
                        let config = {
                            method: "post",
                            content_available: 1,
                            aps: JSON.stringify({
                                "content-available": 1,
                                "mutable-content": 1,
                                alert: {
                                    type: "AddTask",
                                    title: 'Task_Notification',
                                    body: JSON.stringify(obj),
                                },
                                badge: 1,
                                sound: "default",
                            }),
                            url: "https://fcm.googleapis.com/fcm/send",
                            headers: {
                                Authorization: "key=AAAA6amStzE:APA91bGJEM79aWSNM7BYs81n8PLAoQR_TylQO1d1JqYI0EB8o3UJSviMsKL2tjrxniOGxfzYEsENUUIiT7_PcJu_mhyNkeiiysllXzY7MnZcACqU9CWUE3cv4zHsxtPmG-VSydg9Z_GW",
                                "Content-Type": "application/json",
                            },
                            data: data,
                        };
                        console.log("config", config);
                        await axios(config);
                    }
                    catch (e) {
                        console.log("error sending to firebase professor old", e);
                    }
                    let profCodeId = taskIdValue;
                    for (let i = 0; i < studentsData.length; i++) {
                        const userCode = studentsData[i].UserCode;
                        obj["ProfCode"] = studentCheckData[0].UserCode; // its the professor Code
                        obj["UserCode"] = userCode;
                        obj["ProfName"] = studentCheckData[0].UserName;
                        obj["ProfTaskId"] = profCodeId;
                        taskIdValue = taskIdValue + 1;
                        obj["TaskId"] = taskIdValue;
                        let sqlInsert = `Insert into UserTask Set  ?`
                        let userTaskData = await getOrder(sqlInsert, [obj]);
                    };

                }



            }
            else {

                const obj = {};
                obj["UserCode"] = studentCheckData[0].UserCode;
                Object.keys(inputBody).forEach(item => {
                    if (!(item == "UserEmail" || item == "TaskId" || item == "LastUpdatedEpochTime" || item == "ChangeId" || inputBody[item] == "")) {
                        obj[item] = inputBody[item];
                    }
                });

                obj["ChangeId"] = 1;
                obj["LastUpdatedEpochTime"] = Math.floor(Date.now() / 1000);
                taskIdValue = taskIdValue + 1;
                obj["TaskId"] = taskIdValue;
                let sqlInsert = `Insert into UserTask Set  ?`
                let userTaskData = await getOrder(sqlInsert, [obj]);
                console.log("userTaskDataInserted", userTaskData);
                let taskIdDatataskId = 'Select max(taskId) as value from UserTask where UserCode=? and UniCode=? and ChangeId!=3';
                let taskIdData = await getOrder(taskIdDatataskId, [studentCheckData[0].UserCode, inputBody["UniCode"]]);


                let res = {
                    "statusCode": 201,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ TaskId: taskIdData[0].value })
                };
                return res;
            }

            var countValuesUpdate = `Update CountValues where UniCode=? set TaskId=?`;
            taskIdValue = taskIdValue + 1;
            let taskIdDataUpdate = await getOrder(countValuesUpdate, [inputBody["UniCode"], taskIdValue]);

        }
        else {
            let obj = {
                error: "All keys not found to query the db(UserEmail or UniCode)",
            };
            let res = {
                statusCode: 400,
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                body: JSON.stringify({ error: obj.error }),
            };
            return res;
        }
    }
    else if (event.resource === "/classes/getclasses") {
        if (!event.queryStringParameters) {
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: " universityCode and student code needs to be passed in  params " })
            };
            return res;
        }
        else {
            try {
                let universityCode;
                if (!event.queryStringParameters.universityCode) {
                    universityCode = event.queryStringParameters.universityCode || null; /* University Code works same as UniCode in Table  */
                    let res = {
                        "statusCode": 422,
                        "headers": {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        "body": JSON.stringify({ error: " universityCode needs to be passed in  params " })
                    };
                    /* the response is same because uniCode needs to be a part of queryStringParameters */
                    if (!universityCode || universityCode == null || universityCode == undefined) {
                        return res;
                    }
                    else {
                        return res;
                    }
                }
                else {
                    universityCode = event.queryStringParameters.universityCode;
                }
                let sqlAdmissionInfoData = 'Select AdmCode from AdmissionInfo where UniCode= ?'
                const data = await getOrder(sqlAdmissionInfoData, [universityCode]);
                console.log("sqlAdmissionInfo", data);
                const classInfo = [];
                for (let i = 0; i < data.length; i++) {
                    var sql = 'select * from Class where AdmCode=?'
                    const admissionData = await getOrder(sql, data[i].AdmCode);
                    console.log("admission", admissionData);

                    admissionData.forEach(classData => {
                        let item = {
                            idClass: classData.idClass,
                            admissionCode: classData.AdmCode,
                            subYear: classData.SubYear,
                            division: classData.Division,
                            semester: classData.Semester,
                            subDivision: classData['Sub Division'],
                            quarter: classData.Quarter
                        }
                        classInfo.push(item);
                    })
                }



                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify(classInfo)
                };
                return res;
            }
            catch (e) {
                console.log("ERROR OCCURED");
                console.log("Internal Server Error", e);
                let res = {
                    "statusCode": 500,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ type: "Internal Server Error", error: JSON.stringify(e), category: "/classes/getclasses" })
                };
                return res;
            }
        }
    }

    else {
        let res = {
            "statusCode": 404,
            "headers": {
                "Content-Type": "application/json",
                "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                "Access-Control-Allow-Methods": "GET,OPTIONS",
                "Access-Control-Allow-Origin": "*",
            },
            "body": JSON.stringify({ error: " Resource does not exist " })
        };

        return res;
    }
};

let getOrder = async(sql, params) => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if (err) {
                console.log("connection failed")
                reject(err);
            }
            else {
                connection.query(sql, params, (err, results) => {
                    if (err) {
                        console.log("query failed", err)
                        reject(err);
                    }
                    console.log("-----Query Done!");
                    connection.release();
                    //console.log("-----Data: ", results);
                    resolve(results);
                });
            }

        });
    });
};


async function getTempValueForSubjectCode(UniCode) {
    return new Promise(async(resolve, reject) => {
        try {
            var sqlCountValues = 'select * from CountValues';
            var data = await getOrder(sqlCountValues, '');
            let index = data.findIndex((item) => item.UniCode === UniCode)
            var sqlCountValues = 'update CountValues Set ? WHERE UniCode = ?';
            await getOrder(sqlCountValues, [{ SubjCode: data[index]["SubjCode"] + 1 }, data[index]["UniCode"]]);
            resolve(data[index]["SubjCode"] + 1);
        }
        catch (e) {
            console.log("count values failed", e)
            reject(e)
        }
    })
    //await getOrder(sqlTempDelete,'');
}


/*
 ["LastUpdatedEpochTime"] = Math.floor(Date.now() / 1000);

      let inputBody = JSON.parse(event.body);
      console.log("inputBody", inputBody);
              else if (event.info.parentTypeName === "Query" && event.info.fieldName === "admissionData") {
            try {
                let code = event.arguments.collegeId;
                let sqlAdmissionInfoData = 'Select * from AdmissionInfo where UniCode= ?'
                const data = await getOrder(sqlAdmissionInfoData, [code]);
                console.log("sqlAdmissionInfo", data);
                const admissionInfo = [];
                data.forEach((item) => {
                    admissionInfo.push({
                        admissionCode: item.AdmCode,
                        admissionYear: item.AdmissionYear,
                        departmentCode: item.DeptCode,
                        error: null
                    })
                })
                return new Promise((resolve) => {
                    return resolve(admissionInfo)
                })
            }
            catch (e) {
                return new Promise((resolve) => {
                    return resolve([{
                        admissionCode: null,
                        admissionYear: null,
                        departmentCode: null,
                        error: JSON.stringify(e)
                    }])
                })
            }
        }
        else if (event.info.parentTypeName === "Mutation" && event.info.fieldName === "createAdmission") {
            let universityCode = event.arguments.input.universityCode;
            let admissionYear = event.arguments.input.admissionYear; //departmentCode
            let departmentCode = event.arguments.input.departmentCode; //departmentCode            
            try {
                let updateAdmissionTable = 'INSERT INTO AdmissionInfo SET ?';
                let admissionCodeSql = "select max(idAdmissionInfo) from AdmissionInfo";
                let codeData = await getOrder(admissionCodeSql, "");
                console.log("codeData", codeData);
                console.log("codeData", codeData[0]["max(idAdmissionInfo)"])
                let AdmCode = codeData[0]["max(idAdmissionInfo)"] + 1;
                console.log("AdmCode", AdmCode)

                let admissionCode = "ADM"
                if (AdmCode < 10) {
                    admissionCode += "00" + AdmCode
                }
                else if (AdmCode > 10 && AdmCode < 100) {
                    admissionCode += "0" + AdmCode
                }
                else {
                    admissionCode += AdmCode
                }
                console.log("admissionCode", admissionCode)
                let admissionData = { idAdmissionInfo: '', UniCode: universityCode, AdmCode: admissionCode, AdmissionYear: admissionYear, DeptCode: departmentCode };
                let dataAdmissionTable = await getOrder(updateAdmissionTable, admissionData)
                console.log("dataDepartmentsTable", dataAdmissionTable)

                return new Promise((resolve) => {
                    return resolve({
                        admissionCode: admissionCode,
                        admissionYear: admissionYear,
                        departmentCode: departmentCode,
                        error: null
                    })
                })
            }
            catch (e) {
                return new Promise((resolve) => {
                    return resolve({
                        admissionCode: null,
                        admissionYear: null,
                        departmentCode: null,
                        error: JSON.stringify(e)
                    })
                })
            }

        }
*/