const mysql = require("mysql");
const moment = require("moment");
const axios = require("axios");
const pool = mysql.createPool({
  // connectionLimit: 20,
  host: "campuslive.czmocuynmaaq.us-east-2.rds.amazonaws.com",
  user: "admin",
  password: "Vishal#3",
  database: "CampusLiveDev",
});
exports.handler = async (event) => {
  console.log("event", JSON.stringify(event));
  if (event.resource === "/departments/getdepartments") {
    if (!event.queryStringParameters) {
      let res = {
        statusCode: 422,
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Headers":
            "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
          "Access-Control-Allow-Methods": "GET,OPTIONS",
          "Access-Control-Allow-Origin": "*",
        },
        body: JSON.stringify({
          error: " universityCode needs to be passed in  params ",
        }),
      };
      return res;
    } else {
      try {
        let universityCode;
        if (!event.queryStringParameters.universityCode) {
          universityCode =
            event.queryStringParameters.universityCode ||
            null; /* University Code works same as UniCode in Table  */
          let res = {
            statusCode: 422,
            headers: {
              "Content-Type": "application/json",
              "Access-Control-Allow-Headers":
                "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
              "Access-Control-Allow-Methods": "GET,OPTIONS",
              "Access-Control-Allow-Origin": "*",
            },
            body: JSON.stringify({
              error: " universityCode needs to be passed in  params ",
            }),
          };
          /* the response is same because uniCode needs to be a part of queryStringParameters */
          if (
            !universityCode ||
            universityCode == null ||
            universityCode == undefined
          ) {
            return res;
          } else {
            return res;
          }
        } else {
          universityCode = event.queryStringParameters.universityCode;
        }
        let sqlDepartmentsData = "Select * from UniDepartment where UniCode= ?";
        const data = await getOrder(sqlDepartmentsData, [universityCode]);
        const departments = [];
        data.forEach((item) => {
          departments.push({
            departmentName: item.UniDeptName,
            departmentCode: item.UniDeptCode,
            departmentHead: item.UniDeptHead,
          });
        });

        let res = {
          statusCode: 200,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers":
              "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
            "Access-Control-Allow-Methods": "GET,OPTIONS",
            "Access-Control-Allow-Origin": "*",
          },
          body: JSON.stringify(departments),
        };
        return res;
      } catch (e) {
        console.log("ERROR OCCURED");
        console.log("Internal Server Error", e);
        let res = {
          statusCode: 500,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers":
              "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
            "Access-Control-Allow-Methods": "GET,OPTIONS",
            "Access-Control-Allow-Origin": "*",
          },
          body: JSON.stringify({
            type: "Internal Server Error",
            error: JSON.stringify(e),
            category: "/departments/getdepartment",
          }),
        };
        return res;
      }
    }
  } else if (event.resource === "/departments/createdepartment") {
    let inputBody = event.body
      ? JSON.parse(event.body)
      : {}; /* If event Body is not parsable if will be a empty object */
    if (
      inputBody.hasOwnProperty("universityCode") &&
      inputBody.hasOwnProperty("departmentName") &&
      inputBody.hasOwnProperty("departmentHead")
    ) {
      try {
        let createDepartmentsObject = {};
        let universityCode = inputBody["universityCode"];
        createDepartmentsObject["UniCode"] = inputBody["universityCode"];
        createDepartmentsObject["UniDeptName"] = inputBody["departmentName"];
        createDepartmentsObject["UniDeptHead"] = inputBody["departmentHead"];

        let sqlCountValues = "select * from CountValues where UniCode= ?";
        let dataCountValues = await getOrder(sqlCountValues, [universityCode]);
        let UniDeptCode = parseInt(dataCountValues[0].UniDeptCode);
        console.log("UniDeptCode", UniDeptCode);
        let updateCountValues = "update CountValues set ? where  UniCode=?";
        let createLocationSql = "Insert into UniDepartment set ?";
        let departmentCode = "DP0" + UniDeptCode;
        createDepartmentsObject["UniDeptCode"] = departmentCode;
        await getOrder(createLocationSql, [createDepartmentsObject]);
        await getOrder(updateCountValues, [
          { UniDeptCode: UniDeptCode + 1 },
          universityCode,
        ]);
        let res = {
          statusCode: 200,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers":
              "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
            "Access-Control-Allow-Methods": "GET,OPTIONS",
            "Access-Control-Allow-Origin": "*",
          },
          body: JSON.stringify({ data: departmentCode }),
        };
        return res;
      } catch (e) {
        console.log("error in creating createdepartments");
        let res = {
          statusCode: 200,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers":
              "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
            "Access-Control-Allow-Methods": "GET,OPTIONS",
            "Access-Control-Allow-Origin": "*",
          },
          body: JSON.stringify({
            type: "Internal Server Error",
            error: JSON.stringify(e),
            category: "/departments/createdepartments",
          }),
        };
        return res;
      }
    } else {
      let res = {
        statusCode: 422,
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Headers":
            "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
          "Access-Control-Allow-Methods": "GET,OPTIONS",
          "Access-Control-Allow-Origin": "*",
        },
        body: JSON.stringify({
          error:
            " required paramater not present in body of POST Create Departments",
        }),
      };
      return res;
    }
  } else if (event.resource === "/classes/getclasses") {
    if (!event.queryStringParameters) {
      let res = {
        statusCode: 422,
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Headers":
            "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
          "Access-Control-Allow-Methods": "GET,OPTIONS",
          "Access-Control-Allow-Origin": "*",
        },
        body: JSON.stringify({
          error:
            " universityCode and student code needs to be passed in  params ",
        }),
      };
      return res;
    } else {
      try {
        let universityCode;
        if (!event.queryStringParameters.universityCode) {
          universityCode =
            event.queryStringParameters.universityCode ||
            null; /* University Code works same as UniCode in Table  */
          let res = {
            statusCode: 422,
            headers: {
              "Content-Type": "application/json",
              "Access-Control-Allow-Headers":
                "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
              "Access-Control-Allow-Methods": "GET,OPTIONS",
              "Access-Control-Allow-Origin": "*",
            },
            body: JSON.stringify({
              error: " universityCode needs to be passed in  params ",
            }),
          };
          /* the response is same because uniCode needs to be a part of queryStringParameters */
          if (
            !universityCode ||
            universityCode == null ||
            universityCode == undefined
          ) {
            return res;
          } else {
            return res;
          }
        } else {
          universityCode = event.queryStringParameters.universityCode;
        }
        let sqlAdmissionInfoData =
          "Select AdmCode from AdmissionInfo where UniCode= ?";
        const data = await getOrder(sqlAdmissionInfoData, [universityCode]);
        const classInfo = [];
        for (let i = 0; i < data.length; i++) {
          var sql = "select * from Class where AdmCode=?";
          const admissionData = await getOrder(sql, data[i].AdmCode);
          admissionData.forEach((classData) => {
            let item = {
              idClass: classData.idClass,
              admissionCode: classData.AdmCode,
              subYear: classData.SubYear,
              division: classData.Division,
              semester: classData.Semester,
              subDivision: classData["Sub Division"],
              quarter: classData.Quarter,
            };
            classInfo.push(item);
          });
        }

        let res = {
          statusCode: 200,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers":
              "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
            "Access-Control-Allow-Methods": "GET,OPTIONS",
            "Access-Control-Allow-Origin": "*",
          },
          body: JSON.stringify(classInfo),
        };
        return res;
      } catch (e) {
        console.log("ERROR OCCURED");
        console.log("Internal Server Error", e);
        let res = {
          statusCode: 500,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers":
              "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
            "Access-Control-Allow-Methods": "GET,OPTIONS",
            "Access-Control-Allow-Origin": "*",
          },
          body: JSON.stringify({
            type: "Internal Server Error",
            error: JSON.stringify(e),
            category: "/classes/getclasses",
          }),
        };
        return res;
      }
    }
  } else if (event.resource === "/admissions/getadmissions") {
    if (!event.queryStringParameters) {
      let res = {
        statusCode: 422,
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Headers":
            "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
          "Access-Control-Allow-Methods": "GET,OPTIONS",
          "Access-Control-Allow-Origin": "*",
        },
        body: JSON.stringify({
          error: " universityCode needs to be passed in  params ",
        }),
      };
      return res;
    } else {
      try {
        let universityCode;
        if (!event.queryStringParameters.universityCode) {
          universityCode =
            event.queryStringParameters.universityCode ||
            null; /* University Code works same as UniCode in Table  */
          let res = {
            statusCode: 422,
            headers: {
              "Content-Type": "application/json",
              "Access-Control-Allow-Headers":
                "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
              "Access-Control-Allow-Methods": "GET,OPTIONS",
              "Access-Control-Allow-Origin": "*",
            },
            body: JSON.stringify({
              error: " universityCode needs to be passed in  params ",
            }),
          };
          /* the response is same because uniCode needs to be a part of queryStringParameters */
          if (
            !universityCode ||
            universityCode == null ||
            universityCode == undefined
          ) {
            return res;
          } else {
            return res;
          }
        } else {
          universityCode = event.queryStringParameters.universityCode;
        }
        let sqlAdmissionInfoData =
          "Select * from AdmissionInfo where UniCode= ?";
        const data = await getOrder(sqlAdmissionInfoData, [universityCode]);
        console.log("sqlAdmissionInfo", data);
        const admissionInfo = [];
        data.forEach((item) => {
          admissionInfo.push({
            admissionCode: item.AdmCode,
            admissionYear: item.AdmissionYear,
            departmentCode: item.DeptCode,
            departmentName: item.DeptName,
            academicStartData: item.AcadStartDate,
            academicEndData: item.AcadEndDate,
          });
        });
        let res = {
          statusCode: 200,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers":
              "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
            "Access-Control-Allow-Methods": "GET,OPTIONS",
            "Access-Control-Allow-Origin": "*",
          },
          body: JSON.stringify(admissionInfo),
        };
        return res;
      } catch (e) {
        console.log("ERROR OCCURED");
        console.log("Internal Server Error", e);
        let res = {
          statusCode: 500,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers":
              "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
            "Access-Control-Allow-Methods": "GET,POST,OPTIONS",
            "Access-Control-Allow-Origin": "*",
          },
          body: JSON.stringify({
            type: "Internal Server Error",
            error: JSON.stringify(e),
            category: "/admissions/getadmissions",
          }),
        };
        return res;
      }
    }
  } else if (event.resource === "/schedule/getclassdata") {
    if (!event.queryStringParameters) {
      let res = {
        statusCode: 422,
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Headers":
            "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
          "Access-Control-Allow-Methods": "GET,OPTIONS",
          "Access-Control-Allow-Origin": "*",
        },
        body: JSON.stringify({
          error: " universityCode needs to be passed in  params ",
        }),
      };
      return res;
    } else {
      try {
        let universityCode, classId;
        if (
          !event.queryStringParameters.universityCode ||
          !event.queryStringParameters.classId
        ) {
          universityCode =
            event.queryStringParameters.universityCode ||
            null; /* University Code works same as UniCode in Table  */
          let res = {
            statusCode: 422,
            headers: {
              "Content-Type": "application/json",
              "Access-Control-Allow-Headers":
                "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
              "Access-Control-Allow-Methods": "GET,OPTIONS",
              "Access-Control-Allow-Origin": "*",
            },
            body: JSON.stringify({
              error:
                " universityCode or userCode needs to be passed in  params ",
            }),
          };
          /* the response is same because uniCode needs to be a part of queryStringParameters */
          if (
            !universityCode ||
            universityCode == null ||
            universityCode == undefined
          ) {
            return res;
          } else {
            return res;
          }
        } else {
          universityCode = event.queryStringParameters.universityCode;
          classId = event.queryStringParameters.classId;
        }
        let selectClassScheduleMonthly =
          "Select ClassSchMonId,SchCode,ClassId,UniCode,Date,StartTime,EndTime,SubjCode,SubjName,ProfCode,ProfName,Location,Link,LastUpdatedEpochTime,ChangeId,ADDTIME(Date, StartTime) as newStartTime,ADDTIME(Date, EndTime) as newEndTime from ClassScheduleMonthly where ClassId=? and UniCode=? and ChangeId!=3";
        let classScheduleData = await getOrder(selectClassScheduleMonthly, [
          classId,
          universityCode,
        ]);
        let classScheduleDataArray = [];
        classScheduleData.forEach((item) => {
          let scheduleObject = {
            classScheduleMonthlyId: item.ClassSchMonId,
            scheduleCode: item.SchCode,
            classId: item.ClassId,
            date: item.Date,
            subjectCode: item.SubjCode,
            subjectName: item.SubjName,
            professorCode: item.ProfCode,
            professorName: item.ProfName,
            location: item.Location,
            link: item.Link,
            lastUpdatedEpochTime: item.LastUpdatedEpochTime,
            changeId: item.ChangeId,
            modifiedWebStartTime: item.newStartTime,
            modifiedWebEndTime: item.newEndTime,
          };
          classScheduleDataArray.push(scheduleObject);
        });
        let res = {
          statusCode: 200,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers":
              "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
            "Access-Control-Allow-Methods": "GET,OPTIONS",
            "Access-Control-Allow-Origin": "*",
          },
          body: JSON.stringify(classScheduleDataArray),
        };
        return res;
      } catch (e) {
        console.log("ERROR OCCURED");
        console.log("Internal Server Error", e);
        let res = {
          statusCode: 500,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers":
              "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
            "Access-Control-Allow-Methods": "GET,OPTIONS",
            "Access-Control-Allow-Origin": "*",
          },
          body: JSON.stringify({
            type: "Internal Server Error",
            error: JSON.stringify(e),
            category: "/schedule/getclassdata",
          }),
        };
        return res;
      }
    }
  } else if (event.resource === "/subjects/getsubjects") {
    if (!event.queryStringParameters) {
      let res = {
        statusCode: 422,
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Headers":
            "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
          "Access-Control-Allow-Methods": "GET,OPTIONS",
          "Access-Control-Allow-Origin": "*",
        },
        body: JSON.stringify({
          error: " universityCode needs to be passed in  params ",
        }),
      };
      return res;
    } else {
      try {
        let universityCode;
        if (!event.queryStringParameters.universityCode) {
          universityCode =
            event.queryStringParameters.universityCode ||
            null; /* University Code works same as UniCode in Table  */
          let res = {
            statusCode: 422,
            headers: {
              "Content-Type": "application/json",
              "Access-Control-Allow-Headers":
                "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
              "Access-Control-Allow-Methods": "GET,OPTIONS",
              "Access-Control-Allow-Origin": "*",
            },
            body: JSON.stringify({
              error: " universityCode needs to be passed in  params ",
            }),
          };
          /* the response is same because uniCode needs to be a part of queryStringParameters */
          if (
            !universityCode ||
            universityCode == null ||
            universityCode == undefined
          ) {
            return res;
          } else {
            return res;
          }
        } else {
          universityCode = event.queryStringParameters.universityCode;
        }
        let extractSubjectsSql = `Select * from Subject where UniCode = ?`;
        let subjectsData = await getOrder(extractSubjectsSql, [universityCode]);

        let subjectData = [];
        subjectsData.forEach((item) => {
          let obj = {};
          obj.idSubject = item.idSubject;
          obj.subjectName = item.SubjName;
          obj.subjectCode = item.SubjCode;
          obj.universityCode = item.UniCode;
          subjectData.push(obj);
        });
        let res = {
          statusCode: 200,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers":
              "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
            "Access-Control-Allow-Methods": "GET,OPTIONS",
            "Access-Control-Allow-Origin": "*",
          },
          body: JSON.stringify(subjectData),
        };
        return res;
      } catch (e) {
        console.log("ERROR OCCURED");
        console.log("Internal Server Error", e);
        let res = {
          statusCode: 500,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers":
              "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
            "Access-Control-Allow-Methods": "GET,OPTIONS",
            "Access-Control-Allow-Origin": "*",
          },
          body: JSON.stringify({
            type: "Internal Server Error",
            error: JSON.stringify(e),
            category: "/subjects/getsubjects",
          }),
        };
        return res;
      }
    }
  } else if (event.resource === "/locations/getlocations") {
    if (!event.queryStringParameters) {
      let res = {
        statusCode: 422,
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Headers":
            "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
          "Access-Control-Allow-Methods": "GET,OPTIONS",
          "Access-Control-Allow-Origin": "*",
        },
        body: JSON.stringify({
          error: " universityCode needs to be passed in  params ",
        }),
      };
      return res;
    } else {
      try {
        let universityCode;
        if (!event.queryStringParameters.universityCode) {
          universityCode =
            event.queryStringParameters.universityCode ||
            null; /* University Code works same as UniCode in Table  */
          let res = {
            statusCode: 422,
            headers: {
              "Content-Type": "application/json",
              "Access-Control-Allow-Headers":
                "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
              "Access-Control-Allow-Methods": "GET,OPTIONS",
              "Access-Control-Allow-Origin": "*",
            },
            body: JSON.stringify({
              error: " universityCode needs to be passed in  params ",
            }),
          };
          /* the response is same because uniCode needs to be a part of queryStringParameters */
          if (
            !universityCode ||
            universityCode == null ||
            universityCode == undefined
          ) {
            return res;
          } else {
            return res;
          }
        } else {
          universityCode = event.queryStringParameters.universityCode;
        }
        let extractLocationsSql = `Select * from UniClassRoom where UniCode = ?`;
        let locationsData = await getOrder(extractLocationsSql, [
          universityCode,
        ]);
        let locationData = [];
        locationsData.forEach((item) => {
          let obj = {};
          obj.idUniversityClassRoomId = item.UniClassRoomId;
          obj.universityCode = item.UniCode;
          obj.classRoomName = item.ClassRoomName;
          obj.classRoomDetail = item.ClassRoomDetail;
          obj.classRoomType = item.ClassRoomType;
          locationData.push(obj);
        });
        let res = {
          statusCode: 200,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers":
              "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
            "Access-Control-Allow-Methods": "GET,OPTIONS",
            "Access-Control-Allow-Origin": "*",
          },
          body: JSON.stringify(locationData),
        };
        return res;
      } catch (e) {
        console.log("ERROR OCCURED");
        console.log("Internal Server Error", e);
        let res = {
          statusCode: 500,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers":
              "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
            "Access-Control-Allow-Methods": "GET,OPTIONS",
            "Access-Control-Allow-Origin": "*",
          },
          body: JSON.stringify({
            type: "Internal Server Error",
            error: JSON.stringify(e),
            category: "/locations/getlocations",
          }),
        };
        return res;
      }
    }
  } else if (event.resource === "/notifications/profnotificationclassid") {
    let inputBody = event.body
      ? JSON.parse(event.body)
      : {}; /* If event Body is not parsable if will be a empty object */
    if (
      inputBody.hasOwnProperty("universityCode") &&
      inputBody.hasOwnProperty("idClass") &&
      inputBody.hasOwnProperty("title") &&
      inputBody.hasOwnProperty("body")
    ) {
      let title = inputBody["title"];
      let type = inputBody["type"];
      let body = inputBody["body"];
      let universityCode = inputBody["universityCode"];
      let classId = inputBody["idClass"];
      let classTopic = `/topics/ClassTopic_${universityCode}_${classId}`;
      try {
        let data = JSON.stringify({
          to: classTopic,
          priority: "high",
          "content-available": true,
          data: {
            type: type,
            title: title,
            body: body,
          },
        });
        let config = {
          method: "post",
          content_available: 1,
          aps: JSON.stringify({
            "content-available": 1,
            "mutable-content": 1,
            alert: {
              type: type,
              title: title,
              body: body,
            },
            badge: 1,
            sound: "default",
          }),
          url: "https://fcm.googleapis.com/fcm/send",
          headers: {
            Authorization:
              "key=AAAA6amStzE:APA91bGJEM79aWSNM7BYs81n8PLAoQR_TylQO1d1JqYI0EB8o3UJSviMsKL2tjrxniOGxfzYEsENUUIiT7_PcJu_mhyNkeiiysllXzY7MnZcACqU9CWUE3cv4zHsxtPmG-VSydg9Z_GW",
            "Content-Type": "application/json",
          },
          data: data,
        };
        await axios(config);
        let res = {
          statusCode: 200,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers":
              "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
            "Access-Control-Allow-Methods": "GET,OPTIONS",
            "Access-Control-Allow-Origin": "*",
          },
          body: JSON.stringify({ data: "prof class id notification sent" }),
        };

        return res;
      } catch (e) {
        console.log("error in sending notification");
        let res = {
          statusCode: 200,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers":
              "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
            "Access-Control-Allow-Methods": "GET,OPTIONS",
            "Access-Control-Allow-Origin": "*",
          },
          body: JSON.stringify({
            type: "Internal Server Error",
            error: JSON.stringify(e),
            category: "/notifications/profnotificationclassid",
          }),
        };
        return res;
      }
    } else {
      let res = {
        statusCode: 422,
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Headers":
            "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
          "Access-Control-Allow-Methods": "GET,OPTIONS",
          "Access-Control-Allow-Origin": "*",
        },
        body: JSON.stringify({
          error:
            " required paramater not present in body of POST Create Prof Notifications",
        }),
      };
      return res;
    }
  } else if (event.resource === "/results/getresults") {
    if (!event.queryStringParameters) {
      let res = {
        statusCode: 422,
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Headers":
            "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
          "Access-Control-Allow-Methods": "GET,OPTIONS",
          "Access-Control-Allow-Origin": "*",
        },
        body: JSON.stringify({
          error:
            " universityCode and student code needs to be passed in  params ",
        }),
      };
      return res;
    } else {
      try {
        let universityCode, studentUserCode;
        if (
          !event.queryStringParameters.universityCode ||
          !event.queryStringParameters.studentUserCode
        ) {
          universityCode =
            event.queryStringParameters.universityCode ||
            null; /* University Code works same as UniCode in Table  */
          let res = {
            statusCode: 422,
            headers: {
              "Content-Type": "application/json",
              "Access-Control-Allow-Headers":
                "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
              "Access-Control-Allow-Methods": "GET,OPTIONS",
              "Access-Control-Allow-Origin": "*",
            },
            body: JSON.stringify({
              error: " universityCode needs to be passed in  params ",
            }),
          };
          /* the response is same because uniCode needs to be a part of queryStringParameters */
          if (
            !universityCode ||
            universityCode == null ||
            universityCode == undefined
          ) {
            return res;
          } else {
            return res;
          }
        } else {
          universityCode = event.queryStringParameters.universityCode;
          studentUserCode = event.queryStringParameters.studentUserCode;
        }
        let resultPerStudentSql =
          "Select * from UniStudentSubjectMarks where UniCode= ? and StudentCode=?";
        const resultPerStudent = await getOrder(resultPerStudentSql, [
          universityCode,
          studentUserCode,
        ]);
        let resultWeightageSql =
          "Select * from UniSubjectExams where UniCode= ?";
        const resultWeightage = await getOrder(resultWeightageSql, [
          universityCode,
        ]);
        let subjectSql = "Select * from Subject where UniCode= ?";
        const resultSubject = await getOrder(subjectSql, [universityCode]);

        resultPerStudent.map((item) => {
          const resultValue = resultWeightage.find(
            (result) =>
              result.SubjectCode == item.SubjCode &&
              result.ExamType == item.ExamType
          );
          item.relativeResult = parseFloat(
            (resultValue.Weightage * item.Marks) / item.OutOf
          );
          let subjectData = resultSubject.find(
            (subject) => subject.SubjCode == item.SubjCode
          );
          item.SubjectName = subjectData["SubjName"];
          item.WeightAge = resultValue.Weightage;
        });

        const data = {
          resultPerStudent,
        };

        const resultFiltered = [];
        resultPerStudent.forEach((result) => {
          let obj = {};
          obj.idUniStudentSubjectMarks = result.idUniStudentSubjectMarks;
          obj.university = result.UniCode;
          obj.studentCode = result.StudentCode;
          obj.exam = result.Exam;
          obj.subjectCode = result.SubjCode;
          obj.examType = result.ExamType;
          obj.marks = result.Marks;
          obj.outOf = result.OutOf;
          obj.relativeResult = result.relativeResult;
          obj.subjectName = result.SubjectName;
          obj.weightAge = result.WeightAge;

          resultFiltered.push(obj);
        });

        let res = {
          statusCode: 200,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers":
              "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
            "Access-Control-Allow-Methods": "GET,OPTIONS",
            "Access-Control-Allow-Origin": "*",
          },
          body: JSON.stringify(resultFiltered),
        };
        return res;
      } catch (e) {
        console.log("ERROR OCCURED");
        console.log("Internal Server Error", e);
        let res = {
          statusCode: 500,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers":
              "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
            "Access-Control-Allow-Methods": "GET,OPTIONS",
            "Access-Control-Allow-Origin": "*",
          },
          body: JSON.stringify({
            type: "Internal Server Error",
            error: JSON.stringify(e),
            category: "/results/getresult",
          }),
        };
        return res;
      }
    }
  } else if (event.resource === "/admissions/createadmissions") {
    let inputBody = event.body
      ? JSON.parse(event.body)
      : {}; /* If event Body is not parsable if will be a empty object */
    if (
      inputBody.hasOwnProperty("admissionYear") &&
      inputBody.hasOwnProperty("departmentCode") &&
      inputBody.hasOwnProperty("degreeOffered") &&
      inputBody.hasOwnProperty("academicStartData") &&
      inputBody.hasOwnProperty("academicEndData") &&
      inputBody.hasOwnProperty("departmentName") &&
      inputBody.hasOwnProperty("universityCode")
    ) {
      let UniCode = inputBody["universityCode"];
      let createAdmissionstObject = {};
      createAdmissionstObject["UniCode"] = inputBody["universityCode"];
      createAdmissionstObject["AdmissionYear"] = inputBody["admissionYear"];
      createAdmissionstObject["DeptCode"] = inputBody["departmentCode"];
      createAdmissionstObject["DegreeOffered"] = inputBody["degreeOffered"];
      createAdmissionstObject["AcadStartDate"] = inputBody["academicStartData"];
      createAdmissionstObject["AcadEndDate"] = inputBody["academicEndData"];
      createAdmissionstObject["DeptName"] = inputBody["departmentName"];

      try {
        var sqlCountValues = "select * from CountValues where UniCode=?";
        var dataCountValues = await getOrder(sqlCountValues, [UniCode]);
        let admValue = parseInt(dataCountValues[0]["AdmCode"]) + 1;
        let admissionCode = "ADM0" + admValue;
        createAdmissionstObject["AdmCode"] = admissionCode;
        let sqlUpdateCountValues = "update CountValues Set ? WHERE UniCode = ?";
        await getOrder(sqlUpdateCountValues, [{ AdmCode: admValue }, UniCode]);
        console.log("admissionCode", admissionCode);
        let updateAdmissionTable = "INSERT INTO AdmissionInfo SET ?";
        let dataAdmissionTable = await getOrder(updateAdmissionTable, [
          createAdmissionstObject,
        ]);
        console.log("dataDepartmentsTable", dataAdmissionTable);
        let res = {
          statusCode: 200,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers":
              "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
            "Access-Control-Allow-Methods": "GET,POST,OPTIONS",
            "Access-Control-Allow-Origin": "*",
          },
          body: JSON.stringify({ data: admissionCode }),
        };
        return res;
      } catch (e) {
        console.log("error in creating admissions");
        let res = {
          statusCode: 200,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers":
              "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
            "Access-Control-Allow-Methods": "GET,OPTIONS",
            "Access-Control-Allow-Origin": "*",
          },
          body: JSON.stringify({
            type: "Internal Server Error",
            error: JSON.stringify(e),
            category: "/admissions/createadmissions",
          }),
        };
        return res;
      }
    } else {
      console.log("event.body", JSON.parse(event.body));
      let res = {
        statusCode: 422,
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Headers":
            "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
          "Access-Control-Allow-Methods": "GET,OPTIONS",
          "Access-Control-Allow-Origin": "*",
        },
        body: JSON.stringify({
          error:
            " required paramater not present in body of POST Admissions of Create Admissions",
        }),
      };
      return res;
    }
  } else if (event.resource === "/classes/createclasses") {
    let inputBody = event.body
      ? JSON.parse(event.body)
      : {}; /* If event Body is not parsable if will be a empty object */
    if (
      inputBody.hasOwnProperty("admissionCode") &&
      inputBody.hasOwnProperty("subYear") &&
      inputBody.hasOwnProperty("division") &&
      inputBody.hasOwnProperty("semister") &&
      inputBody.hasOwnProperty("subdivision")
    ) {
      let createClassObject = {};
      createClassObject["AdmCode"] = inputBody["admissionCode"];
      createClassObject["SubYear"] = inputBody["subYear"];
      createClassObject["Division"] = inputBody["division"];
      createClassObject["Semester"] = inputBody["semister"];
      createClassObject["Sub Division"] = inputBody["subdivision"];

      try {
        let insertClassTable = "INSERT INTO Class SET ?";
        let classData = await getOrder(insertClassTable, [createClassObject]);
        let idClassSql = "select max(idClass) from Class";
        let codeData = await getOrder(idClassSql, "");
        let res = {
          statusCode: 200,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers":
              "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
            "Access-Control-Allow-Methods": "GET,POST,OPTIONS",
            "Access-Control-Allow-Origin": "*",
          },
          body: JSON.stringify({ data: codeData[0]["max(idClass)"] }),
        };

        return res;
      } catch (e) {
        console.log("error in creating admissions");
        let res = {
          statusCode: 200,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers":
              "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
            "Access-Control-Allow-Methods": "GET,OPTIONS",
            "Access-Control-Allow-Origin": "*",
          },
          body: JSON.stringify({
            type: "Internal Server Error",
            error: JSON.stringify(e),
            category: "/classes/createclasses",
          }),
        };
        return res;
      }
    } else {
      console.log("event.body", JSON.parse(event.body));
      let res = {
        statusCode: 422,
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Headers":
            "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
          "Access-Control-Allow-Methods": "GET,OPTIONS",
          "Access-Control-Allow-Origin": "*",
        },
        body: JSON.stringify({
          error:
            " required paramater not present in body of POST Admissions of Create Admissions",
        }),
      };
      return res;
    }
  } else if (event.resource === "/professors/getprofessors") {
    try {
      let universityCode = event.queryStringParameters.universityCode;
      if (universityCode == "" || !universityCode.includes("TR")) {
        let res = {
          statusCode: 400,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers":
              "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
            "Access-Control-Allow-Methods": "GET,OPTIONS",
            "Access-Control-Allow-Origin": "*",
          },
          body: JSON.stringify({ error: "data in incorrect format" }),
        };
        return res;
      }

      let sqlTeachersData =
        "Select * from UserInfo where UserRole=? and UniCode=? and ChangeId != ?";
      let teachersData = await getOrder(sqlTeachersData, [
        "Professor",
        universityCode,
        3,
      ]);

      let teacherData = [];
      //             {
      //     "": 149,
      //     "": "USR0109",
      //     "UniCode": "TR021",
      //     "": "vishalg4851994@gmail.com",
      //     "": "John Doe",
      //     "": "+1234567891",
      //     "UserRole": "Professor",
      //     "UserActive": "True",
      //     "IsTrial": 0,
      //     "TrialEndDate": null,
      //     "UserPhoto": null,
      //     "UserDesignation": null,
      //     "UserRollNo": 1234,
      //     "UserDOB": "1987-09-10T00:00:00.000Z",
      //     "": "122344",
      //     "created_at": "2021-03-15T19:08:53.000Z",
      //     "updated_at": "2021-03-15T19:08:53.000Z",
      //     "UserParentName": null,
      //     "UserParentContactNo": null,
      //     "": null,
      //     "": null,
      //     "ChangeId": 2,
      //     "LastUpdateEpochTime": 1616264677,
      //     "BloodGroup": "B +ve",
      //     "UserFBToken": null,
      //     "ActiveSession": 0
      // },
      teachersData.forEach((item) => {
        let obj = {};
        obj.idUserInfo = item.idUserInfo;
        obj.userCode = item.UserCode;
        obj.userEmail = item.UserEmail;
        obj.userName = item.UserName;
        obj.userPhoneNumber = item.UserPhone;
        obj.designation = item.UserDesignation;
        obj.userRegistrationNumber = item.UserRegistrationNo;
        obj.address = item.UserAddress;
        obj.pinCode = item.UserPinCode;

        teacherData.push(obj);
      });
      let res = {
        statusCode: 200,
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Headers":
            "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
          "Access-Control-Allow-Methods": "GET,OPTIONS",
          "Access-Control-Allow-Origin": "*",
        },
        body: JSON.stringify(teacherData),
      };
      return res;
    } catch (e) {
      let res = {
        statusCode: 400,
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Headers":
            "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
          "Access-Control-Allow-Methods": "GET,OPTIONS",
          "Access-Control-Allow-Origin": "*",
        },
        body: JSON.stringify({
          error:
            "universityCode is not present or DataBase id down is in incorrect format",
        }),
      };
      return res;
    }
  } else if (event.resource === "/subjects/createsubjects") {
    /*this API is extracting information form Subject table based on UniCode(universityCode)*/
    /*   the api is create Contact which currently takes  Name,Location,Designation,PhoneNumber1,PhoneNumber2,UniCode as parameter */
    let inputBody = event.body
      ? JSON.parse(event.body)
      : {}; /* If event Body is not parsable if will be a empty object */
    if (
      inputBody.hasOwnProperty("universityCode") &&
      inputBody.hasOwnProperty("subjectName")
    ) {
      let UniCode = inputBody["universityCode"];
      let createSubjectObject = {};
      createSubjectObject["UniCode"] = inputBody["universityCode"];
      if (inputBody.hasOwnProperty("subjectName") && inputBody["subjectName"]) {
        createSubjectObject["SubjName"] = inputBody["subjectName"];
      }
      try {
        let valueForSubjectCode = await getTempValueForSubjectCode(UniCode);
        let subjectCode = "SBJ0" + parseInt(valueForSubjectCode);
        createSubjectObject["SubjCode"] = subjectCode;
        let createSubjectSql = "Insert into Subject set ?";
        await getOrder(createSubjectSql, [createSubjectObject]);

        let res = {
          statusCode: 200,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers":
              "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
            "Access-Control-Allow-Methods": "GET,OPTIONS",
            "Access-Control-Allow-Origin": "*",
          },
          body: JSON.stringify({ data: subjectCode }),
        };

        return res;
      } catch (e) {
        console.log("error in creating contact");
        let res = {
          statusCode: 200,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers":
              "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
            "Access-Control-Allow-Methods": "GET,OPTIONS",
            "Access-Control-Allow-Origin": "*",
          },
          body: JSON.stringify({
            type: "Internal Server Error",
            error: JSON.stringify(e),
            category: "/subjects/createsubjectst",
          }),
        };
        return res;
      }
    } else {
      let res = {
        statusCode: 422,
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Headers":
            "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
          "Access-Control-Allow-Methods": "GET,OPTIONS",
          "Access-Control-Allow-Origin": "*",
        },
        body: JSON.stringify({
          error:
            " required paramater not present in body of POST Subject of Subject",
        }),
      };
      return res;
    }
  } else if (event.resource === "/locations/createlocation") {
    /*this API is extracting information form Subject table based on UniCode(universityCode)*/
    /*   the api is create Contact which currently takes  Name,Location,Designation,PhoneNumber1,PhoneNumber2,UniCode as parameter */
    let inputBody = event.body
      ? JSON.parse(event.body)
      : {}; /* If event Body is not parsable if will be a empty object */
    if (
      inputBody.hasOwnProperty("universityCode") &&
      inputBody.hasOwnProperty("classRoomName") &&
      inputBody.hasOwnProperty("classRoomDetail") &&
      inputBody.hasOwnProperty("classRoomType")
    ) {
      //ClassRoomName ClassRoomDetail ClassRoomType
      let UniCode = inputBody["universityCode"];
      let ClassRoomName = inputBody["classRoomName"];
      let ClassRoomDetail = inputBody["classRoomDetail"];
      let ClassRoomType = inputBody["classRoomType"];
      let createLocationObject = {};
      createLocationObject["UniCode"] = inputBody["universityCode"];
      if (
        inputBody.hasOwnProperty("classRoomName") &&
        inputBody["classRoomName"] != ""
      ) {
        createLocationObject["classRoomName"] = inputBody["classRoomName"];
      }
      if (
        inputBody.hasOwnProperty("classRoomDetail") &&
        inputBody["classRoomDetail"] != ""
      ) {
        createLocationObject["classRoomDetail"] = inputBody["classRoomDetail"];
      }
      if (
        inputBody.hasOwnProperty("classRoomType") &&
        inputBody["classRoomType"] != ""
      ) {
        createLocationObject["classRoomType"] = inputBody["classRoomType"];
      }
      try {
        let createLocationSql = "Insert into UniClassRoom set ?";
        await getOrder(createLocationSql, [createLocationObject]);

        let res = {
          statusCode: 200,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers":
              "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
            "Access-Control-Allow-Methods": "GET,OPTIONS",
            "Access-Control-Allow-Origin": "*",
          },
          body: JSON.stringify({ data: "Data Inserted SuccessFully" }),
        };
        return res;
      } catch (e) {
        console.log("error in creating contact");
        let res = {
          statusCode: 200,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers":
              "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
            "Access-Control-Allow-Methods": "GET,OPTIONS",
            "Access-Control-Allow-Origin": "*",
          },
          body: JSON.stringify({
            type: "Internal Server Error",
            error: JSON.stringify(e),
            category: "/subjects/createsubjectst",
          }),
        };
        return res;
      }
    } else {
      let res = {
        statusCode: 422,
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Headers":
            "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
          "Access-Control-Allow-Methods": "GET,OPTIONS",
          "Access-Control-Allow-Origin": "*",
        },
        body: JSON.stringify({
          error:
            " required paramater not present in body of POST Subject of Subject",
        }),
      };
      return res;
    }
  } else if (event.resource === "/authentication/myprofile") {
    if (!event.queryStringParameters) {
      let res = {
        statusCode: 422,
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Headers":
            "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
          "Access-Control-Allow-Methods": "GET,OPTIONS",
          "Access-Control-Allow-Origin": "*",
        },
        body: JSON.stringify({
          error:
            " universityCode and student code needs to be passed in  params ",
        }),
      };
      return res;
    } else {
      try {
        let universityCode, userEmail;
        if (
          !event.queryStringParameters.universityCode ||
          !event.queryStringParameters.userEmail
        ) {
          universityCode =
            event.queryStringParameters.universityCode ||
            null; /* University Code works same as UniCode in Table  */
          let res = {
            statusCode: 422,
            headers: {
              "Content-Type": "application/json",
              "Access-Control-Allow-Headers":
                "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
              "Access-Control-Allow-Methods": "GET,OPTIONS",
              "Access-Control-Allow-Origin": "*",
            },
            body: JSON.stringify({
              error: " universityCode needs to be passed in  params ",
            }),
          };
          /* the response is same because uniCode needs to be a part of queryStringParameters */
          if (
            !universityCode ||
            universityCode == null ||
            universityCode == undefined
          ) {
            return res;
          } else {
            return res;
          }
        } else {
          universityCode = event.queryStringParameters.universityCode;
          userEmail = event.queryStringParameters.userEmail;
        }

        let userSql = `SELECT UserInfo.*,UI.* FROM UserInfo 
                JOIN UniversityInfo as UI
                where UserInfo.UserEmail = ?
                and UserInfo.UniCode = ?
                and UserInfo.ChangeId != ? and UserInfo.UniCode = UI.Uni_Code`;
        let codeAwait = await getOrder(userSql, [userEmail, universityCode, 3]);
        console.log("codeAwait", codeAwait);
        var bodyData = {};
        if (codeAwait.length !== 0) {
          bodyData = codeAwait[0];
        }
        console.log("bodyData", JSON.stringify(bodyData));
        let statusCode = 200;
        if (Object.keys(bodyData) == 0) {
          statusCode = 401;
        }
        let obj = {};
        obj.univerityCode = bodyData.UniCode;
        obj.universityName = bodyData.Uni_Name;
        obj.universityCountry = bodyData.Uni_Country;
        obj.universityPostalCode = bodyData.Uni_PostalCode;
        obj.uloodGroup = bodyData.BloodGroup;
        obj.userCode = bodyData.UserCode;
        obj.userAddress = bodyData.UserAddress;
        obj.userPinCode = bodyData.UserPinCode;
        obj.userEmail = bodyData.UserEmail;
        obj.userPhone = bodyData.UserPhone;
        obj.userName = bodyData.UserName;
        obj.userRole = bodyData.UserRole;
        obj.userDesignation = bodyData.UserDesignation;
        obj.userFBToken = bodyData.UserFBToken;
        obj.userRollNumber = bodyData.UserRollNo; //
        obj.lastUpdateEpochTime = bodyData.LastUpdateEpochTime;
        obj.userParentName = bodyData.UserParentName;
        obj.userParentContactNo = bodyData.UserParentContactNo;

        var res = {
          statusCode: statusCode,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers":
              "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
            "Access-Control-Allow-Methods": "GET,OPTIONS",
            "Access-Control-Allow-Origin": "*",
          },
          body: JSON.stringify(obj),
        };

        return res;
      } catch (e) {
        console.log("ERROR OCCURED");
        console.log("Internal Server Error", e);
        let res = {
          statusCode: 500,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers":
              "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
            "Access-Control-Allow-Methods": "GET,OPTIONS",
            "Access-Control-Allow-Origin": "*",
          },
          body: JSON.stringify({
            type: "Internal Server Error",
            error: JSON.stringify(e),
            category: "/authentication/myprofile",
          }),
        };
        return res;
      }
    }
  } else if (event.resource === "/authentication/savemyprofile") {
    /*   the api is create Contact which currently takes  Name,Location,Designation,PhoneNumber1,PhoneNumber2,UniCode as parameter */
    let inputBody = event.body
      ? JSON.parse(event.body)
      : {}; /* If event Body is not parsable if will be a empty object */
    if (
      inputBody.hasOwnProperty("universityCode") &&
      inputBody.hasOwnProperty("userCode") &&
      inputBody.hasOwnProperty("userName") &&
      inputBody.hasOwnProperty("userDesignation") &&
      inputBody.hasOwnProperty("userPhone") &&
      inputBody.hasOwnProperty("userAddress") &&
      inputBody.hasOwnProperty("userPinCode")
    ) {
      let createContactObject = {};
      let UniCode = inputBody["universityCode"];
      let UserCode = inputBody["userCode"];

      var sqlCheck =
        'SELECT * FROM UserInfo where UserCode = ? and UniCode = ? and ChangeId != 3 and UserRole="Admin"';
      let studentCheckData = await getOrder(sqlCheck, [UserCode, UniCode]);
      if (studentCheckData.length == 0) {
        let statusCode = 400;
        let res = {
          statusCode: statusCode,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers":
              "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
            "Access-Control-Allow-Methods": "GET,OPTIONS",
            "Access-Control-Allow-Origin": "*",
          },
          body: JSON.stringify({ error: " User does not exist " }),
        };

        return res;
      }

      createContactObject["UserName"] = inputBody["UserName"];
      createContactObject["UserDesignation"] = inputBody["UserDesignation"];
      createContactObject["UserPhone"] = inputBody["UserPhone"];
      createContactObject["UserAddress"] = inputBody["UserAddress"];
      createContactObject["UserPinCode"] = inputBody["UserPinCode"];

      //  ["LastUpdatedEpochTime"] = Math.floor(Date.now() / 1000)
      if (inputBody.hasOwnProperty("UserDOB") && inputBody["UserDOB"] != "") {
        createContactObject["UserDOB"] = inputBody["UserDOB"];
      }

      if (
        inputBody.hasOwnProperty("BloodGroup") &&
        inputBody["BloodGroup"] != ""
      ) {
        createContactObject["BloodGroup"] = inputBody["BloodGroup"];
      }

      createContactObject["LastUpdateEpochTime"] = Math.floor(
        Date.now() / 1000
      );
      createContactObject["ChangeId"] = 2;
      try {
        let updateProfile =
          'Update UserInfo set ? where UniCode = ?  and UserCode=?  and UserRole="Admin" and ChangeId!=3';
        await getOrder(updateProfile, [createContactObject, UniCode, UserCode]);

        let res = {
          statusCode: 200,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers":
              "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
            "Access-Control-Allow-Methods": "GET,OPTIONS",
            "Access-Control-Allow-Origin": "*",
          },
          body: JSON.stringify({ data: "User Updated Successfully" }),
        };
        return res;
      } catch (e) {
        console.log("error in creating contact");
        let res = {
          statusCode: 400,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers":
              "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
            "Access-Control-Allow-Methods": "GET,OPTIONS",
            "Access-Control-Allow-Origin": "*",
          },
          body: JSON.stringify({
            type: "Internal Server Error",
            error: JSON.stringify(e),
            category: "/savemyprofile",
          }),
        };
        return res;
      }
    } else {
      let res = {
        statusCode: 422,
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Headers":
            "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
          "Access-Control-Allow-Methods": "GET,OPTIONS",
          "Access-Control-Allow-Origin": "*",
        },
        body: JSON.stringify({
          error:
            " required paramater not present in body of POST Contact of Post",
        }),
      };
      return res;
    }
  } else {
    let res = {
      statusCode: 404,
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Headers":
          "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
        "Access-Control-Allow-Methods": "GET,OPTIONS",
        "Access-Control-Allow-Origin": "*",
      },
      body: JSON.stringify({ error: " Resource does not exist " }),
    };

    return res;
  }
};

let getOrder = async (sql, params) => {
  return new Promise((resolve, reject) => {
    pool.getConnection((err, connection) => {
      if (err) {
        console.log("connection failed");
        reject(err);
      } else {
        connection.query(sql, params, (err, results) => {
          if (err) {
            console.log("query failed", err);
            reject(err);
          }
          console.log("-----Query Done!");
          connection.release();
          //console.log("-----Data: ", results);
          resolve(results);
        });
      }
    });
  });
};

async function getTempValueForSubjectCode(UniCode) {
  return new Promise(async (resolve, reject) => {
    try {
      var sqlCountValues = "select * from CountValues";
      var data = await getOrder(sqlCountValues, "");
      let index = data.findIndex((item) => item.UniCode === UniCode);
      var sqlCountValues = "update CountValues Set ? WHERE UniCode = ?";
      await getOrder(sqlCountValues, [
        { SubjCode: data[index]["SubjCode"] + 1 },
        data[index]["UniCode"],
      ]);
      resolve(data[index]["SubjCode"] + 1);
    } catch (e) {
      console.log("count values failed", e);
      reject(e);
    }
  });
  //await getOrder(sqlTempDelete,'');
}
