const mysql = require('mysql');
const moment = require("moment");
const axios = require("axios");
const pool = mysql.createPool({
    // connectionLimit: 20,
    host: 'campuslive.czmocuynmaaq.us-east-2.rds.amazonaws.com',
    user: 'admin',
    password: 'Vishal#3',
    database: 'CampusLiveDev'
});


exports.handler = async(event) => {
    console.log("event", JSON.stringify(event));
    if (event.resource === "/schedule/getschedule") {
        /*
      this api in following manner 
      we receive userEmail and and uniCode we use these 2 to query UserInfo Table to get there UserCode and their UserRole
      if the userRole is Student ,we find out in which class student belongs using studentClass table and get classId
      with classid and univerity we query scheduleMonthytable to retrieve schedule data of students
      
      if the userRole is Professor we we classId from classScheduleMonthly table 
      use classId to get semester division and admission code from Class
      using admission Code we query admissionInfo table to get admission year and departmentCode 
      we alreay have departmentCode and and departmentName mapping at UI. 
      */
        if (!event.queryStringParameters) {
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: " universityCode and email needs to be passed in  params " })
            };
            return res;
        }
        else {
            try {
                let universityCode, email, lastUpdatedEpochTime;
                if (!event.queryStringParameters.universityCode || !event.queryStringParameters.email) {
                    universityCode = event.queryStringParameters.universityCode || null; /* University Code works same as UniCode in Table  */
                    let res = {
                        "statusCode": 422,
                        "headers": {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        "body": JSON.stringify({ error: " universityCode or email needs to be passed in  params " })
                    };
                    /* the response is same because uniCode needs to be a part of queryStringParameters */
                    if (!universityCode || universityCode == null || universityCode == undefined) {
                        return res;
                    }
                    else {
                        return res;
                    }
                }
                else {
                    universityCode = event.queryStringParameters.universityCode;
                    email = event.queryStringParameters.email;
                    if (event.queryStringParameters.lastUpdatedEpochTime) {
                        lastUpdatedEpochTime = event.queryStringParameters.lastUpdatedEpochTime
                    }
                    else {
                        lastUpdatedEpochTime = 0;
                    }
                }

                let identityCheckSql = 'SELECT * FROM UserInfo where UserEmail = ?  and UniCode = ? and  ChangeId != ?';
                let identityData = await getOrder(identityCheckSql, [email, universityCode, 3]);
                if (identityData.length == 0) {
                    let statusCode = 400;
                    let res = {
                        "statusCode": statusCode,
                        "headers": {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        "body": JSON.stringify({ error: " User does not exist " })
                    };

                    return res;
                }
                let userRole = identityData[0].UserRole;
                let userCode = identityData[0].UserCode;
                if (userRole === "Student") {
                    let selectClassStudent =
                        "Select * from ClassStudent where StudentCode=? and UniCode=? ";
                    let classStudentData = await getOrder(selectClassStudent, [userCode, universityCode]);
                    let classId = classStudentData[0]["ClassId"];
                    /* we are quering the ClassStudent table to find the classId in which the student belongs
                     */


                    let selectClassScheduleMonthly = "Select *,ADDTIME(Date, StartTime) as newStartTime,ADDTIME(Date, EndTime) as newEndTime from ClassScheduleMonthly where ClassId=? and UniCode=? and LastUpdatedEpochTime >= ?";
                    let classScheduleData = await getOrder(
                        selectClassScheduleMonthly, [classId, universityCode, lastUpdatedEpochTime]
                    );

                    console.log("classScheduleData", classScheduleData);
                    let classScheduleDataStudent = [];

                    classScheduleData.forEach((item) => {
                        let scheduleObject = {};
                        scheduleObject["classScheduleMonthlyId"] = item.ClassSchMonId;
                        scheduleObject["scheduleCode"] = item.SchCode;
                        scheduleObject["classId"] = item.ClassId;
                        scheduleObject["universityCode"] = item.UniCode;
                        scheduleObject["date"] = item.Date;
                        scheduleObject["startTime"] = item.StartTime;
                        scheduleObject["endTime"] = item.EndTime;
                        scheduleObject["subjectCode"] = item.SubjCode;
                        scheduleObject["subjectName"] = item.SubjName;
                        scheduleObject["professorCode"] = item.ProfCode;
                        scheduleObject["professorName"] = item.ProfName;
                        scheduleObject["location"] = item.Location;
                        scheduleObject["link"] = item.Link;
                        scheduleObject["updatedAt"] = item.UpdatedAt;
                        scheduleObject["lastUpdatedEpochTime"] = item.LastUpdatedEpochTime;
                        scheduleObject["changeId"] = item.ChangeId;
                        scheduleObject["createdAt"] = item.CreatedAt;
                        scheduleObject["modifiedWebStartTime"] = item.newStartTime;
                        scheduleObject["modifiedWebEndTime"] = item.newEndTime;
                        classScheduleDataStudent.push(scheduleObject)
                    })

                    let res = {
                        statusCode: 200,
                        headers: {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        body: JSON.stringify(classScheduleDataStudent),
                    };
                    return res;
                }
                if (userRole === "Professor") {
                    let selectClassScheduleMonthly = `Select AI.AdmissionYear,AI.DeptCode,C.Division,C.Semester,CSM.StartTime,CSM.EndTime,CSM.ClassSchMonId,CSM.ClassId,CSM.Date,CSM.SubjCode,CSM.SubjName,CSM.ProfCode,CSM.ProfName,CSM.Location,CSM.Link,CSM.LastUpdatedEpochTime,CSM.ChangeId,
                                            ADDTIME(CSM.Date, CSM.StartTime) as newStartTime,ADDTIME(CSM.Date, CSM.EndTime) as newEndTime 
                                            from ClassScheduleMonthly AS CSM 
                                            JOIN Class AS C
                                            JOIN AdmissionInfo as AI
                                            where CSM.ClassId=C.idClass and  AI.AdmCode=C.AdmCode  and CSM.ProfCode=? and CSM.UniCode=?  and CSM.LastUpdatedEpochTime>=?`;
                    let classScheduleData = await getOrder(
                        selectClassScheduleMonthly, [userCode, universityCode, lastUpdatedEpochTime]
                    );
                    console.log("classScheduleData", classScheduleData);

                    let classScheduleDataProfessor = [];

                    classScheduleData.forEach((item) => {
                        let scheduleObject = {};
                        scheduleObject["admissionYear"] = item.AdmissionYear;
                        scheduleObject["departmentCode"] = item.DeptCode;
                        scheduleObject["division"] = item.Division;
                        scheduleObject["semester"] = item.Semester;
                        scheduleObject["startTime"] = item.StartTime;
                        scheduleObject["endTime"] = item.EndTime;
                        scheduleObject["classScheduleMonthlyId"] = item.ClassSchMonId;
                        scheduleObject["classId"] = item.ClassId;
                        scheduleObject["date"] = item.Date;
                        scheduleObject["subjectCode"] = item.SubjCode;
                        scheduleObject["subjectName"] = item.SubjName;
                        scheduleObject["professorCode"] = item.ProfCode;
                        scheduleObject["professorName"] = item.ProfName;
                        scheduleObject["location"] = item.Location;
                        scheduleObject["link"] = item.Link;
                        scheduleObject["lastUpdatedEpochTime"] = item.LastUpdatedEpochTime;
                        scheduleObject["changeId"] = item.ChangeId;
                        scheduleObject["modifiedWebStartTime"] = item.newStartTime;
                        scheduleObject["modifiedWebEndTime"] = item.newEndTime;
                        classScheduleDataProfessor.push(scheduleObject)
                    })
                    let res = {
                        statusCode: 200,
                        headers: {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        body: JSON.stringify(classScheduleDataProfessor),
                    };
                    return res;
                }

                // let res = {
                //     "statusCode": 200,
                //     "headers": {
                //         "Content-Type": "application/json",
                //         "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                //         "Access-Control-Allow-Methods": "GET,OPTIONS",
                //         "Access-Control-Allow-Origin": "*",
                //     },
                //     "body": JSON.stringify(departments)
                // };
                // return res;
            }
            catch (e) {
                console.log("ERROR OCCURED");
                console.log("Internal Server Error", e);
                let res = {
                    "statusCode": 500,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ type: "Internal Server Error", error: JSON.stringify(e), category: "/schedule/getschedule" })
                };
                return res;
            }
        }

    }

    else if (event.resource === "/schedule/createschedule") {
        let inputBody = event.body ? JSON.parse(event.body) : {}; /* If event Body is not parsable if will be a empty object */
        if (inputBody.hasOwnProperty("idClass") &&
            inputBody.hasOwnProperty("endDate") &&
            inputBody.hasOwnProperty("endTime") &&
            inputBody.hasOwnProperty("startDate") &&
            inputBody.hasOwnProperty("startTime") &&
            inputBody.hasOwnProperty("location") &&
            inputBody.hasOwnProperty("startTimeBoundaryCheck") &&
            inputBody.hasOwnProperty("endTimeBoundaryCheck") &&
            inputBody.hasOwnProperty("professorCode") &&
            inputBody.hasOwnProperty("professorName") &&
            inputBody.hasOwnProperty("subjectCode") &&
            inputBody.hasOwnProperty("subjectName") &&
            inputBody.hasOwnProperty("universityCode")
        ) {
            let createClassObject = {};

            let idClass = inputBody["idClass"];
            let endDate = inputBody["endDate"];
            let endTime = inputBody["endTime"];
            let startDate = inputBody["startDate"];
            let startTime = inputBody["startTime"];

            let location = inputBody["location"];
            let startTimeBoundaryCheck = inputBody["startTimeBoundaryCheck"];
            let endTimeBoundaryCheck = inputBody["endTimeBoundaryCheck"];

            let professorCode = inputBody["professorCode"];
            let professorName = inputBody["professorName"];
            let subjectCode = inputBody["subjectCode"];
            let subjectName = inputBody["subjectName"];
            let universityCode = inputBody["universityCode"];

            try {
                if (startDate === endDate) {
                    let startTimeModified = `${startDate} ${startTimeBoundaryCheck}`;
                    let endTimeModified = `${startDate} ${endTimeBoundaryCheck}`;
                    //we are checking if there is time overlap with another class
                    let sqlCheckClassTimeOverLap = `select 'Conflict1',ClassSchMonId, StartTime, EndTime, ADDTIME(Date, StartTime) StTime, ADDTIME(Date, EndTime)  endTime , ClassId, UniCode, SubjName, ProfName, Location , Date
                     from ClassScheduleMonthly
                     where ClassId = ?
                     and Date = ?
                     and UniCode = ?
                     and ChangeId!=3
                     and 
                     (
                     (ADDTIME(Date, StartTime) <=? and ADDTIME(Date, EndTime) > ?)
                     or (ADDTIME(Date, StartTime) <? and ADDTIME(Date, EndTime) >=?)
                     ) UNION 
                     Select 'Conflict2',ClassSchMonId, StartTime, EndTime, ADDTIME(Date, StartTime) StTime, ADDTIME(Date, EndTime) edTime , ClassId, UniCode, SubjName, ProfName, Location, Date
                      from (select ClassSchMonId, StartTime, EndTime, ADDTIME(Date, StartTime) StTime, ADDTIME(Date, EndTime) edTime , ClassId, UniCode, SubjName, ProfName, Location, Date
                     		from ClassScheduleMonthly
                     		where ClassId = ?
                     		and Date = ? 
                           and UniCode = ?
                           and ChangeId!=3
                     		and ADDTIME(Date, StartTime) > ?
                     		order by StTime
                     		limit 1) D
                     where
                      ADDTIME(D.Date, D.StartTime) < ?`;
                    let studentCheckData = await getOrder(sqlCheckClassTimeOverLap, [idClass, startDate, universityCode, startTimeModified, startTimeModified, endTimeModified, endTimeModified, idClass, startDate, universityCode, startTimeModified, endTimeModified]);
                    console.log("studentCheckData", studentCheckData);
                    if (studentCheckData.length > 0) {
                        let outputBody = { error: "Class Time Overlaps with Another Class", data: studentCheckData };
                        let res = {
                            statusCode: 200,
                            headers: {
                                "Content-Type": "application/json",
                                "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                                "Access-Control-Allow-Methods": "GET,OPTIONS",
                                "Access-Control-Allow-Origin": "*",
                            },
                            body: JSON.stringify(outputBody),
                        };
                        return res;
                    }



                    let sqlCheckProfessorOverLap = `select ClassSchMonId, SchCode, StartTime, EndTime, ADDTIME(Date, StartTime) StTime, ADDTIME(Date, EndTime)  endTime , DAYOFWEEK(Date) , ClassId, UniCode, SubjName, ProfName, Location
                                          from ClassScheduleMonthly
                                          where ProfName = ?
                                          and Date = ?  
                                          and UniCode =? 
                                          and ChangeId!=3
                                          and ((ADDTIME(Date, StartTime) <=? 
                                          	and ADDTIME(Date, EndTime) > ? 
                                          )
                                          or (ADDTIME(Date, StartTime) <? 
                                          and ADDTIME(Date, EndTime) >=? 
                                          ))`;
                    let professorCheckData = await getOrder(sqlCheckProfessorOverLap, [professorName, startDate, universityCode, startTimeModified, startTimeModified, endTimeModified, endTimeModified]);
                    console.log("studentCheckData", professorCheckData);

                    if (professorCheckData.length > 0) {
                        let outputBody = { error: "Professor Time Overlaps with Another Class", data: professorCheckData };
                        let res = {
                            statusCode: 200,
                            headers: {
                                "Content-Type": "application/json",
                                "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                                "Access-Control-Allow-Methods": "GET,OPTIONS",
                                "Access-Control-Allow-Origin": "*",
                            },
                            body: JSON.stringify(outputBody),
                        };
                        return res;
                    }


                    let sqlCheckLocationOverLap = `select ClassSchMonId, SchCode, StartTime, EndTime, ADDTIME(Date, StartTime) StTime, ADDTIME(Date, EndTime)  endTime , DAYOFWEEK(Date) , ClassId, UniCode, SubjName, ProfName, Location
                                          from ClassScheduleMonthly
                                          where Location = ?
                                          and Date = ?  
                                          and UniCode =? 
                                          and ChangeId!=3
                                          and ((ADDTIME(Date, StartTime) <=? 
                                          	and ADDTIME(Date, EndTime) > ? 
                                          )
                                          or (ADDTIME(Date, StartTime) <? 
                                          and ADDTIME(Date, EndTime) >=? 
                                          ))`;
                    let locationrCheckData = await getOrder(sqlCheckLocationOverLap, [location, startDate, universityCode, startTimeModified, startTimeModified, endTimeModified, endTimeModified]);
                    console.log("studentCheckData", locationrCheckData);
                    if (locationrCheckData.length > 0) {
                        let outputBody = { error: "Location Overlaps", data: locationrCheckData };
                        let res = {
                            statusCode: 200,
                            headers: {
                                "Content-Type": "application/json",
                                "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                                "Access-Control-Allow-Methods": "GET,OPTIONS",
                                "Access-Control-Allow-Origin": "*",
                            },
                            body: JSON.stringify(outputBody),
                        };
                        return res;
                    }

                    let scheduleNumber = await getTempValueToInsertIntoTable(universityCode);
                    let scheduleCode = "SCH" + scheduleNumber.toString();
                    const createScheduleSameDate = {
                        ClassId: idClass,
                        UniCode: universityCode,
                        SchCode: scheduleCode,
                        Date: startDate,
                        StartTime: startTime,
                        EndTime: endTime,
                        SubjCode: subjectCode,
                        SubjName: subjectName,
                        ProfCode: professorCode,
                        ProfName: professorName,
                        Location: location
                    }
                    let sql = `insert into ClassScheduleMonthly set ?`
                    let data = await getOrder(sql, [createScheduleSameDate]);


                    let professorTopic = `/topics/ProfessorTopic_${universityCode}_${professorCode}`
                    let studentTopic = `/topics/ClassTopic_${universityCode}_${idClass}` // this start date is actually the end date since it got incremented
                    let detailObject = `New Class Scheduled on   ${moment(startDate).format("YYYY-MM-DD")}  from ${startTime} to ${endTime} once every week.`
                    await firebaseChangeCreateSchedule(professorTopic, detailObject);
                    await firebaseChangeCreateSchedule(studentTopic, detailObject);
                    let res = {
                        statusCode: 200,
                        headers: {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        body: JSON.stringify({ data: "Data inserted into ClassScheduleMonthly" }),
                    };
                    return res;

                }
                else {
                    let actualStartDate = startDate; // the actual date on which we are supposed to create Schedule

                    while (moment(startDate) <= moment(endDate)) {
                        let startTimeModified = `${startDate} ${startTimeBoundaryCheck}`;
                        let endTimeModified = `${startDate} ${endTimeBoundaryCheck}`;


                        let sqlCheckClassTimeOverLap = `select 'Conflict1',ClassSchMonId, StartTime, EndTime, ADDTIME(Date, StartTime) StTime, ADDTIME(Date, EndTime)  endTime , ClassId, UniCode, SubjName, ProfName, Location , Date
                     from ClassScheduleMonthly
                     where ClassId = ?
                     and Date = ?
                     and UniCode = ?
                     and 
                     (
                     (ADDTIME(Date, StartTime) <=? and ADDTIME(Date, EndTime) > ?)
                     or (ADDTIME(Date, StartTime) <? and ADDTIME(Date, EndTime) >=?)
                     ) UNION 
                     Select 'Conflict2',ClassSchMonId, StartTime, EndTime, ADDTIME(Date, StartTime) StTime, ADDTIME(Date, EndTime) edTime , ClassId, UniCode, SubjName, ProfName, Location, Date
                      from (select ClassSchMonId, StartTime, EndTime, ADDTIME(Date, StartTime) StTime, ADDTIME(Date, EndTime) edTime , ClassId, UniCode, SubjName, ProfName, Location, Date
                     		from ClassScheduleMonthly
                     		where ClassId = ?
                     		and Date = ? 
                           and UniCode = ?
                     		and ADDTIME(Date, StartTime) > ?
                     		order by StTime
                     		limit 1) D
                     where
                      ADDTIME(D.Date, D.StartTime) < ?`;
                        let studentCheckData = await getOrder(sqlCheckClassTimeOverLap, [idClass, startDate, universityCode, startTimeModified, startTimeModified, endTimeModified, endTimeModified, idClass, startDate, universityCode, startTimeModified, endTimeModified]);

                        if (studentCheckData.length > 0) {
                            let outputBody = { error: "Class Time Overlaps with Another Class", data: studentCheckData };
                            let res = {
                                statusCode: 200,
                                headers: {
                                    "Content-Type": "application/json",
                                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                                    "Access-Control-Allow-Origin": "*",
                                },
                                body: JSON.stringify(outputBody),
                            };
                            return res;
                        }


                        let sqlCheckProfessorOverLap = `select ClassSchMonId, SchCode, StartTime, EndTime, ADDTIME(Date, StartTime) StTime, ADDTIME(Date, EndTime)  endTime , DAYOFWEEK(Date) , ClassId, UniCode, SubjName, ProfName, Location
                                          from ClassScheduleMonthly
                                          where ProfName = ?
                                          and Date = ?  
                                          and UniCode =? 
                                          and ((ADDTIME(Date, StartTime) <=? 
                                          	and ADDTIME(Date, EndTime) > ? 
                                          )
                                          or (ADDTIME(Date, StartTime) <? 
                                          and ADDTIME(Date, EndTime) >=? 
                                          ))`;
                        let professorCheckData = await getOrder(sqlCheckProfessorOverLap, [professorName, startDate, universityCode, startTimeModified, startTimeModified, endTimeModified, endTimeModified]);
                        if (professorCheckData.length > 0) {
                            let outputBody = { error: "Professor Time Overlaps with Another Class", data: professorCheckData };
                            let res = {
                                statusCode: 200,
                                headers: {
                                    "Content-Type": "application/json",
                                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                                    "Access-Control-Allow-Origin": "*",
                                },
                                body: JSON.stringify(outputBody),
                            };
                            return res;
                        }





                        let sqlCheckLocationOverLap = `select ClassSchMonId, SchCode, StartTime, EndTime, ADDTIME(Date, StartTime) StTime, ADDTIME(Date, EndTime)  endTime , DAYOFWEEK(Date) , ClassId, UniCode, SubjName, ProfName, Location
                                          from ClassScheduleMonthly
                                          where Location = ?
                                          and Date = ?  
                                          and UniCode =? 
                                          and ((ADDTIME(Date, StartTime) <=? 
                                          	and ADDTIME(Date, EndTime) > ? 
                                          )
                                          or (ADDTIME(Date, StartTime) <? 
                                          and ADDTIME(Date, EndTime) >=? 
                                          ))`;
                        let locationrCheckData = await getOrder(sqlCheckLocationOverLap, [location, startDate, universityCode, startTimeModified, startTimeModified, endTimeModified, endTimeModified]);
                        if (locationrCheckData.length > 0) {
                            let outputBody = { error: "Location Overlaps", data: locationrCheckData };
                            let res = {
                                statusCode: 200,
                                headers: {
                                    "Content-Type": "application/json",
                                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                                    "Access-Control-Allow-Origin": "*",
                                },
                                body: JSON.stringify(outputBody),
                            };
                            return res;
                        }


                        startDate = moment(startDate).add(7, "days").format("YYYY-MM-DD");
                    }
                    startDate = actualStartDate;

                    while (moment(startDate) <= moment(endDate)) {
                        let scheduleNumber = await getTempValueToInsertIntoTable(universityCode);
                        let scheduleCode = "SCH" + scheduleNumber.toString();
                        let sql = `insert into ClassScheduleMonthly set ?`
                        let data = await getOrder(sql, [{ ClassId: idClass, UniCode: universityCode, SchCode: scheduleCode, Date: startDate, StartTime: startTime, EndTime: endTime, SubjCode: subjectCode, SubjName: subjectName, ProfCode: professorCode, ProfName: professorName, Location: location }])
                        startDate = moment(startDate).add(7, "days").format("YYYY-MM-DD");
                    }

                    let professorTopic = `/topics/ProfessorTopic_${universityCode}_${professorCode}`
                    let studentTopic = `/topics/ClassTopic_${universityCode}_${idClass}` // this start date is actually the end date since it got incremented
                    let detailObject = `New Class Scheduled from  ${moment(actualStartDate).format("YYYY-MM-DD")} till ${moment(startDate).format("YYYY-MM-DD")}  from ${startTime} to ${endTime} once every week.`
                    await firebaseChangeCreateSchedule(professorTopic, detailObject);
                    await firebaseChangeCreateSchedule(studentTopic, detailObject);

                    let res = {
                        statusCode: 200,
                        headers: {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        body: JSON.stringify({ data: "Data inserted into ClassScheduleMonthly" }),
                    };
                    return res;
                }
            }
            catch (e) {
                console.log("error in creating schedule");
                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ type: "Internal Server Error", error: JSON.stringify(e), category: "/schedule/createschedule" })
                };
                return res;
            }
        }
        else {
            console.log("event.body", JSON.parse(event.body))
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: " required paramater not present in body of POST Schedule of Create Schedule" })
            };
            return res;
        }


    }
    else if (event.resource === "/schedule/deleteschedule") {
        if (!event.queryStringParameters) {
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,DELETE,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: " universityCode needs to be passed in  params " })
            };
            return res;
        }
        else {

            let idClass, universityCode, dateNew, idClassScheduleMonthly, startTime, endTime, profCode;
            if (!event.queryStringParameters.idClass || !event.queryStringParameters.dateNew || !event.queryStringParameters.universityCode || !event.queryStringParameters.idClassScheduleMonthly) {
                universityCode = event.queryStringParameters.universityCode || null; /* University Code works same as UniCode in Table  */
                let res = {
                    "statusCode": 422,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,DELETE,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ error: " idClass and universityCode and dateNew and idClassScheduleMonthly needs to be passed in  params " })
                };
                return res;
            }
            else {
                idClass = event.queryStringParameters.idClass;
                universityCode = event.queryStringParameters.universityCode;
                dateNew = event.queryStringParameters.dateNew;
                idClassScheduleMonthly = event.queryStringParameters.idClassScheduleMonthly;
                startTime = event.queryStringParameters.startTime;
                endTime = event.queryStringParameters.endTime;
                profCode = event.queryStringParameters.profCode
                console.log(idClass, universityCode, dateNew, idClassScheduleMonthly, startTime, endTime, profCode);
                try {

                    let deleteObject = {};
                    deleteObject["LastUpdatedEpochTime"] = Math.floor(Date.now() / 1000);
                    deleteObject["ChangeId"] = 3;
                    let dateSchedule = `update ClassScheduleMonthly set  ? where ClassSchMonId=? and UniCode=?`;
                    await getOrder(dateSchedule, [deleteObject, idClassScheduleMonthly, universityCode]);
                    let sqlScheduleData = `Select ClassSchMonId,SchCode,ClassId,UniCode,Date,StartTime,EndTime,SubjCode,SubjName,ProfCode,ProfName,Location,Link,LastUpdatedEpochTime,ChangeId,ADDTIME(Date, StartTime) as newStartTime,ADDTIME(Date, EndTime) as newEndTime from ClassScheduleMonthly where ClassId=? and UniCode=? and Date=? and ChangeId!=3`;
                    let ScheduleData = await getOrder(sqlScheduleData, [
                        idClass,
                        universityCode,
                        dateNew,
                    ]);

                    console.log("ScheduleData", ScheduleData);
                    // //console.log("${sqlScheduleData[0]['Date']} from ${sqlScheduleData[0]['StartTime']} ${sqlScheduleData[0]['EndTime']}",sqlScheduleData[0]['Date'],sqlScheduleData[0]['StartTime'],sqlScheduleData[0]['EndTime']);
                    let cancelledClass = `Class on ${dateNew} from ${startTime} ${endTime} cancelled`;
                    console.log("cancelledTopic", cancelledClass);
                    let classTopic = `/topics/ClassTopic_${universityCode}_${idClass}`;
                    let professorTopicNew = `/topics/ProfessorTopic_${universityCode}_${profCode}`;
                    await firebase(classTopic, "Class Cancelled", cancelledClass);
                    await firebase(professorTopicNew, "Class Cancelled", cancelledClass);

                    let classScheduleDataStudent = [];
                    if (ScheduleData.length > 0) {
                        ScheduleData.forEach((item) => {
                            let scheduleObject = {};
                            scheduleObject["classScheduleMonthlyId"] = item.ClassSchMonId;
                            scheduleObject["scheduleCode"] = item.SchCode;
                            scheduleObject["classId"] = item.ClassId;
                            scheduleObject["universityCode"] = item.UniCode;
                            scheduleObject["date"] = item.Date;
                            scheduleObject["startTime"] = item.StartTime;
                            scheduleObject["endTime"] = item.EndTime;
                            scheduleObject["subjectCode"] = item.SubjCode;
                            scheduleObject["subjectName"] = item.SubjName;
                            scheduleObject["professorCode"] = item.ProfCode;
                            scheduleObject["professorName"] = item.ProfName;
                            scheduleObject["location"] = item.Location;
                            scheduleObject["link"] = item.Link;
                            scheduleObject["updatedAt"] = item.UpdatedAt;
                            scheduleObject["lastUpdatedEpochTime"] = item.LastUpdatedEpochTime;
                            scheduleObject["changeId"] = item.ChangeId;
                            scheduleObject["createdAt"] = item.CreatedAt;
                            scheduleObject["modifiedWebStartTime"] = item.newStartTime;
                            scheduleObject["modifiedWebEndTime"] = item.newEndTime;
                            classScheduleDataStudent.push(scheduleObject)
                        })
                    }
                    let outputBody = {
                        success: "Schedule Delated Success Fully",
                        data: JSON.stringify(classScheduleDataStudent),
                    };

                    console.log("outputBody", outputBody);
                    let res = {
                        statusCode: 200,
                        headers: {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,DELETE,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        body: JSON.stringify(outputBody),
                    };
                    return res;
                }
                catch (e) {
                    console.log("e", e)
                    let res = {
                        statusCode: 400,
                        headers: {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,DELETE,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        body: JSON.stringify({ error: JSON.stringify(e) }),
                    };
                    return res;
                }

            }



        }
    }
    else if (event.resource === "/schedule/checkforconflict") {
        let inputBody = JSON.parse(event.body);
        console.log("inputBody", inputBody);
        if (inputBody.hasOwnProperty("idClass") &&
            inputBody.hasOwnProperty("dateNew") &&
            inputBody.hasOwnProperty("dateOld") &&
            inputBody.hasOwnProperty("idClassScheduleMonthly") &&
            inputBody.hasOwnProperty("location") &&
            inputBody.hasOwnProperty("endTimeOld") &&
            inputBody.hasOwnProperty("endTimeNew") &&
            inputBody.hasOwnProperty("startTimeNew") &&
            inputBody.hasOwnProperty("startTimeOld") &&
            inputBody.hasOwnProperty("professorCodeNew") &&
            inputBody.hasOwnProperty("professorNameNew") &&
            inputBody.hasOwnProperty("professorCodeOld") &&
            inputBody.hasOwnProperty("professorNameOld") &&
            inputBody.hasOwnProperty("subjectCodeNew") &&
            inputBody.hasOwnProperty("subjectNameNew") &&
            inputBody.hasOwnProperty("subjectCodeOld") &&
            inputBody.hasOwnProperty("subjectNameOld") &&
            inputBody.hasOwnProperty("universityCode") &&
            inputBody.hasOwnProperty("startTimeBoundaryCheck") &&
            inputBody.hasOwnProperty("endTimeBoundaryCheck")
        ) {

            try {


                let idClass = inputBody["idClass"];
                let dateNew = inputBody["dateNew"];
                let dateOld = inputBody["dateOld"];
                let idClassScheduleMonthly = inputBody["idClassScheduleMonthly"];
                let location = inputBody["location"];
                let endTimeOld = inputBody["endTimeOld"];
                let endTimeNew = inputBody["endTimeNew"];
                let startTimeNew = inputBody["startTimeNew"];
                let startTimeOld = inputBody["startTimeOld"];
                let professorCodeNew = inputBody["professorCodeNew"];
                let professorNameNew = inputBody["professorNameNew"];
                let professorCodeOld = inputBody["professorCodeOld"];
                let professorNameOld = inputBody["professorNameOld"];
                let subjectCodeNew = inputBody["subjectCodeNew"];
                let subjectNameNew = inputBody["subjectNameNew"];
                let subjectCodeOld = inputBody["subjectCodeOld"];
                let subjectNameOld = inputBody["subjectNameOld"];
                let universityCode = inputBody["universityCode"];
                let startTimeBoundaryCheck = inputBody["startTimeBoundaryCheck"];
                let endTimeBoundartCheck = inputBody["endTimeBoundaryCheck"];

                console.log("checkforconflict", idClass, dateNew, dateOld, idClassScheduleMonthly, location, endTimeOld, endTimeNew, startTimeNew,
                    startTimeOld, professorCodeNew, professorNameNew, professorCodeOld, professorNameOld, subjectCodeNew, subjectNameNew,
                    subjectCodeOld, subjectNameOld, universityCode, startTimeBoundaryCheck, endTimeBoundartCheck);


                let changeDetails = {};
                changeDetails['UniCode'] = universityCode;
                let startTime = `${dateNew} ${startTimeBoundaryCheck}`;
                let endTime = `${dateNew} ${endTimeBoundartCheck}`;
                let updatedObject = {};
                let subjectChanged = 0;
                let professorChanged = 0;
                let startTimeChanged = 0;
                let endTimeChanged = 0;
                updatedObject['Date'] = dateNew;
                updatedObject['StartTime'] = startTimeNew;
                updatedObject['EndTime'] = endTimeNew;
                updatedObject['Location'] = location;
                updatedObject["LastUpdatedEpochTime"] = Math.floor(Date.now() / 1000);
                updatedObject["ChangeId"] = 2
                if (subjectNameOld != subjectNameNew) {
                    subjectChanged = 1;
                    changeDetails["subjectChanged"] = 1;
                    changeDetails["subjectOld"] = subjectNameOld;
                    changeDetails["subjectNew"] = subjectNameNew;

                    updatedObject['SubjName'] = subjectNameNew;
                    updatedObject['SubjCode'] = subjectCodeNew;
                }
                else {
                    changeDetails["subjectChanged"] = 0;
                    changeDetails["subjectOld"] = subjectNameOld;
                }
                if (professorNameNew != professorNameOld) {
                    professorChanged = 1;
                    changeDetails["professorChanged"] = 1;
                    changeDetails["professorOld"] = professorNameOld;
                    changeDetails["professorNew"] = professorNameNew;

                    updatedObject['ProfName'] = professorNameNew;
                    updatedObject['ProfCode'] = professorCodeNew;
                    changeDetails["professorNewUserCode"] = professorCodeNew;
                }
                else {
                    changeDetails["professorChanged"] = 0;
                    changeDetails["professorOld"] = professorNameOld;
                }

                changeDetails["professorOldUserCode"] = professorCodeOld;

                // try {

                let sqlCheckClassTimeOverLap = `select 'Conflict1',ClassSchMonId, StartTime, EndTime, ADDTIME(Date, StartTime) StTime, ADDTIME(Date, EndTime)  endTime , ClassId, UniCode, SubjName, ProfName, Location , Date
                     from ClassScheduleMonthly
                     where ClassId = ?
                     and Date = ?
                     and ClassSchMonId != ?
                     and UniCode = ?
                     and ChangeId!=3
                     and 
                     (
                     (ADDTIME(Date, StartTime) <=? and ADDTIME(Date, EndTime) > ?)
                     or (ADDTIME(Date, StartTime) <? and ADDTIME(Date, EndTime) >=?)
                     ) UNION 
                     Select 'Conflict2',ClassSchMonId, StartTime, EndTime, ADDTIME(Date, StartTime) StTime, ADDTIME(Date, EndTime) edTime , ClassId, UniCode, SubjName, ProfName, Location, Date
                      from (select ClassSchMonId, StartTime, EndTime, ADDTIME(Date, StartTime) StTime, ADDTIME(Date, EndTime) edTime , ClassId, UniCode, SubjName, ProfName, Location, Date
                     		from ClassScheduleMonthly
                     		where ClassId = ?
                     		and Date = ? 
                     		and ClassSchMonId != ?
                          and UniCode = ?
                          and ChangeId!=3
                     		and ADDTIME(Date, StartTime) > ?
                     		order by StTime
                     		limit 1) D
                     where
                      ADDTIME(D.Date, D.StartTime) < ?`;
                let studentCheckData = await getOrder(sqlCheckClassTimeOverLap, [idClass, dateNew, idClassScheduleMonthly, universityCode, startTime, startTime, endTime, endTime,
                    idClass, dateNew, idClassScheduleMonthly, universityCode, startTime, endTime
                ]);
                console.log("studentCheckData", studentCheckData);
                if (studentCheckData.length > 0) {
                    let outputBody = { error: "Class Time Overlaps with Another Class", data: studentCheckData };
                    let res = {
                        statusCode: 200,
                        headers: {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,PUT,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        body: JSON.stringify(outputBody),
                    };
                    return res;
                }

                //}                catch (e) { console.log("error studentCheckData", e); }

                // try {
                let sqlCheckProfessorOverLap = `select ClassSchMonId, SchCode, StartTime, EndTime, ADDTIME(Date, StartTime) StTime, ADDTIME(Date, EndTime)  endTime , DAYOFWEEK(Date) , ClassId, UniCode, SubjName, ProfName, Location
                                          from ClassScheduleMonthly
                                          where ProfName = ?
                                          and Date = ?  
                                          and ClassSchMonId != ?
                                          and UniCode = ?
                                          and ChangeId!=3
                                          and ((ADDTIME(Date, StartTime) <=? 
                                          	and ADDTIME(Date, EndTime) > ? 
                                          )
                                          or (ADDTIME(Date, StartTime) <? 
                                          and ADDTIME(Date, EndTime) >=? 
                                          ))`;
                let professorCheckData = await getOrder(sqlCheckProfessorOverLap, [professorNameNew, dateNew, idClassScheduleMonthly, universityCode, startTime, startTime, endTime, endTime]);
                console.log("studentCheckData", professorCheckData);

                if (professorCheckData.length > 0) {
                    let outputBody = { error: "Professor Time Overlaps with Another Class", data: professorCheckData };
                    let res = {
                        statusCode: 200,
                        headers: {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,PUT,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        body: JSON.stringify(outputBody),
                    };
                    return res;
                }


                //  }                catch (e) { console.log("error ProfessorCheckData", e); }

                //try {
                let sqlCheckLocationOverLap = `select ClassSchMonId, SchCode, StartTime, EndTime, ADDTIME(Date, StartTime) StTime, ADDTIME(Date, EndTime)  endTime , DAYOFWEEK(Date) , ClassId, UniCode, SubjName, ProfName, Location
                                          from ClassScheduleMonthly
                                          where Location = ?
                                          and Date = ?  
                                          and ClassSchMonId != ?
                                          and UniCode = ?
                                          and ChangeId!=3
                                          and ((ADDTIME(Date, StartTime) <=? 
                                          	and ADDTIME(Date, EndTime) > ? 
                                          )
                                          or (ADDTIME(Date, StartTime) <? 
                                          and ADDTIME(Date, EndTime) >=? 
                                          ))`;
                let locationrCheckData = await getOrder(sqlCheckLocationOverLap, [location, dateNew, idClassScheduleMonthly, universityCode, startTime, startTime, endTime, endTime]);
                console.log("studentCheckData", locationrCheckData);
                if (locationrCheckData.length > 0) {
                    let outputBody = { error: "Location Overlaps", data: locationrCheckData };
                    let res = {
                        statusCode: 200,
                        headers: {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,PUT,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        body: JSON.stringify(outputBody),
                    };
                    return res;
                }
                //}catch (e) { console.log("error ProfessorCheckData", e); }


                //                try {
                let updateScheduleTable = `update ClassScheduleMonthly set ? where ClassSchMonId=?`;
                await getOrder(updateScheduleTable, [updatedObject, idClassScheduleMonthly])
                // }
                // catch (e) {
                //     console.log("error updaing schedule");
                // }



                if (startTimeOld != startTimeNew) {
                    startTimeChanged = 1;
                    changeDetails["startTimeChanged"] = 1;
                    changeDetails["startTimeOld"] = startTimeOld;
                    changeDetails["startTimeNew"] = startTimeNew;
                }
                else {
                    changeDetails["startTimeChanged"] = 0;
                    changeDetails["startTimeOld"] = startTimeOld;
                }
                if (endTimeOld != endTimeNew) {
                    endTimeChanged = 1;
                    changeDetails["endTimeChanged"] = 1;
                    changeDetails["endTimeOld"] = endTimeOld;
                    changeDetails["endTimeNew"] = endTimeNew;
                }
                else {
                    changeDetails["endTimeChanged"] = 0;
                    changeDetails["endTimeOld"] = endTimeOld;
                }

                let ScheduleData; // this variable holds new schedule data
                if (dateNew != dateOld) {
                    let sqlScheduleData = `Select ClassSchMonId,SchCode,ClassId,UniCode,Date,StartTime,EndTime,SubjCode,SubjName,ProfCode,ProfName,Location,Link,LastUpdatedEpochTime,ChangeId,ADDTIME(Date, StartTime) as newStartTime,ADDTIME(Date, EndTime) as newEndTime from ClassScheduleMonthly where ClassId=? and UniCode=? and (Date=? or Date=?) and ChangeId!=3`
                    ScheduleData = await getOrder(sqlScheduleData, [idClass, universityCode, dateNew, dateOld]);
                    console.log("ScheduleData", ScheduleData);
                    changeDetails["DataChanged"] = 1;
                    changeDetails["DateOld"] = dateOld;
                    changeDetails["DateNew"] = dateNew;
                }
                else {
                    let sqlScheduleData = `Select ClassSchMonId,SchCode,ClassId,UniCode,Date,StartTime,EndTime,SubjCode,SubjName,ProfCode,ProfName,Location,Link,LastUpdatedEpochTime,ChangeId,ADDTIME(Date, StartTime) as newStartTime,ADDTIME(Date, EndTime) as newEndTime from ClassScheduleMonthly where ClassId=? and UniCode=? and Date=? and ChangeId!=3`
                    ScheduleData = await getOrder(sqlScheduleData, [idClass, universityCode, dateNew]);
                    changeDetails["DataChanged"] = 0;
                    changeDetails["DateOld"] = dateOld;
                    console.log("ScheduleData", ScheduleData);
                }
                let classTopic = `/topics/ClassTopic_${universityCode}_${idClass}`;
                let professorTopic = "";

                await firebaseChangeInScheduleStudent(classTopic, "Class Timing Changed", changeDetails);
                await firebaseChangeInScheduleProfessor("Class Timing Changed", changeDetails);
                let classScheduleDataArray = [];
                ScheduleData.forEach((item) => {
                    let scheduleObject = {
                        classScheduleMonthlyId: item.ClassSchMonId,
                        scheduleCode: item.SchCode,
                        classId: item.ClassId,
                        date: item.Date,
                        subjectCode: item.SubjCode,
                        subjectName: item.SubjName,
                        professorCode: item.ProfCode,
                        professorName: item.ProfName,
                        location: item.Location,
                        link: item.Link,
                        lastUpdatedEpochTime: item.LastUpdatedEpochTime,
                        changeId: item.ChangeId,
                        modifiedWebStartTime: item.newStartTime,
                        modifiedWebEndTime: item.newEndTime
                    }
                    classScheduleDataArray.push(scheduleObject);
                })
                let outputBody = { success: "No Overlaps", data: JSON.stringify(classScheduleDataArray) };
                let res = {
                    statusCode: 200,
                    headers: {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,PUT,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    body: JSON.stringify(outputBody),
                };
                return res;

            }
            catch (e) {
                console.log("INTERNAL SERVER ERROR", e);
                let res = {
                    statusCode: 200,
                    headers: {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,PUT,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    body: JSON.stringify({ error: JSON.stringify(e) }),
                };
                return res;
            }
            // let res = {
            //     "statusCode": 422,
            //     "headers": {
            //         "Content-Type": "application/json",
            //         "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
            //         "Access-Control-Allow-Methods": "GET,OPTIONS",
            //         "Access-Control-Allow-Origin": "*",
            //     },
            //     "body": JSON.stringify({ data: "check Successfully" })
            // };
            // return res;
        }
        else {
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,PUT,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: "Required paramters not present" })
            };
            return res;
        }




    }

    else if (event.resource === "/schedule/loadlocationschedule") {
        if (!event.queryStringParameters) {
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: " universityCode and locationName needs to be passed in  params " })
            };
            return res;
        }
        else {
            try {
                let locationName, universityCode;
                if (!event.queryStringParameters.locationName || !event.queryStringParameters.universityCode) {
                    universityCode = event.queryStringParameters.universityCode || null; /* University Code works same as UniCode in Table  */
                    let res = {
                        "statusCode": 422,
                        "headers": {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        "body": JSON.stringify({ error: " universityCode or locationName needs to be passed in  params " })
                    };
                    /* the response is same because uniCode needs to be a part of queryStringParameters */
                    if (!universityCode || universityCode == null || universityCode == undefined) {
                        return res;
                    }
                    else {
                        return res;
                    }
                }
                else {
                    locationName = event.queryStringParameters.locationName;
                    universityCode = event.queryStringParameters.universityCode;
                }
                
                    let selectClassScheduleMonthly = "Select *,ADDTIME(Date, StartTime) as newStartTime,ADDTIME(Date, EndTime) as newEndTime from ClassScheduleMonthly where Location=? and UniCode=? and ChangeId!=3";
                    let classScheduleData = await getOrder(
                        selectClassScheduleMonthly, [locationName, universityCode]
                    );

                console.log("classScheduleData",classScheduleData,locationName,universityCode)
                let classScheduleDataStudent = [];
                classScheduleData.forEach((item) => {
                    let scheduleObject = {};
                    scheduleObject["classScheduleMonthlyId"] = item.ClassSchMonId;
                    scheduleObject["scheduleCode"] = item.SchCode;
                    scheduleObject["classId"] = item.ClassId;
                    scheduleObject["universityCode"] = item.UniCode;
                    scheduleObject["date"] = item.Date;
                    scheduleObject["startTime"] = item.StartTime;
                    scheduleObject["endTime"] = item.EndTime;
                    scheduleObject["subjectCode"] = item.SubjCode;
                    scheduleObject["subjectName"] = item.SubjName;
                    scheduleObject["professorCode"] = item.ProfCode;
                    scheduleObject["professorName"] = item.ProfName;
                    scheduleObject["location"] = item.Location;
                    scheduleObject["link"] = item.Link;
                    scheduleObject["updatedAt"] = item.UpdatedAt;
                    scheduleObject["lastUpdatedEpochTime"] = item.LastUpdatedEpochTime;
                    scheduleObject["changeId"] = item.ChangeId;
                    scheduleObject["createdAt"] = item.CreatedAt;
                    scheduleObject["modifiedWebStartTime"] = item.newStartTime;
                    scheduleObject["modifiedWebEndTime"] = item.newEndTime;
                    classScheduleDataStudent.push(scheduleObject)
                })

                let res = {
                    statusCode: 200,
                    headers: {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    body: JSON.stringify(classScheduleDataStudent),
                };
                return res;



            }
            catch (e) {
                console.log("ERROR OCCURED");
                console.log("Internal Server Error", e);
                let res = {
                    "statusCode": 500,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ type: "Internal Server Error", error: JSON.stringify(e), category: "/schedule/getschedule" })
                };
                return res;
            }
        }
    }
    // else if (event.resource === "/schedule/loadprofessorschedule") {

    // }

    else {
        let res = {
            "statusCode": 404,
            "headers": {
                "Content-Type": "application/json",
                "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                "Access-Control-Allow-Methods": "GET,OPTIONS",
                "Access-Control-Allow-Origin": "*",
            },
            "body": JSON.stringify({ error: " Resource does not exist " })
        };

        return res;
    }
};


let getOrder = async(sql, params) => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if (err) {
                console.log("connection failed")
                reject(err);
            }
            else {
                connection.query(sql, params, (err, results) => {
                    if (err) {
                        console.log("query failed", err)
                        reject(err);
                    }
                    console.log("-----Query Done!");
                    connection.release();
                    //console.log("-----Data: ", results);
                    resolve(results);
                });
            }

        });
    });
};


const firebaseChangeCreateSchedule = async(topic, detailsObject) => {
    console.log("fireBase called", topic, detailsObject);


    try {
        let data = JSON.stringify({
            to: topic,
            priority: "high",
            "content-available": true,
            data: {
                type: "ScheduleChange",
                title: "New Class Scheduled",
                body: detailsObject,
            },
        });
        let config = {
            method: "post",
            content_available: 1,
            aps: JSON.stringify({
                "content-available": 1,
                "mutable-content": 1,
                alert: {
                    title: "New Class Scheduled",
                    "subject": detailsObject,
                    body: detailsObject,
                },
                badge: 1,
                sound: "default",
            }),
            url: "https://fcm.googleapis.com/fcm/send",
            headers: {
                Authorization: "key=AAAA6amStzE:APA91bGJEM79aWSNM7BYs81n8PLAoQR_TylQO1d1JqYI0EB8o3UJSviMsKL2tjrxniOGxfzYEsENUUIiT7_PcJu_mhyNkeiiysllXzY7MnZcACqU9CWUE3cv4zHsxtPmG-VSydg9Z_GW",
                "Content-Type": "application/json",
            },
            data: data,
        };
        console.log("config", config);
        await axios(config);
    }
    catch (e) {
        console.log("error sending to firebase professor old", e);
    }

    return new Promise((res) => res("resolve"));
};
async function getTempValueToInsertIntoTable(UniCode) {
    return new Promise(async(resolve, reject) => {
        try {
            console.log("UniCode", UniCode);
            var sqlCountValues = 'select * from CountValues where UniCode= ?';
            var data = await getOrder(sqlCountValues, [UniCode]);
            console.log("data", data);
            if (data.length < 1) {
                throw { e: "no data found for University" }
            }
            console.log("data", data);
            var sqlCountValues = 'update CountValues Set ? WHERE UniCode = ?';
            console.log("update CountValues", await getOrder(sqlCountValues, [{ SchCode: data[0]["SchCode"] + 1 }, data[0]["UniCode"]]))
            resolve(data[0]["SchCode"] + 1);
        }
        catch (e) {
            console.log("count values failed", e)
            reject(e)
        }
    })

    //await getOrder(sqlTempDelete,'');

}


const firebaseChangeInScheduleStudent = async(topic, title, detailsObject) => {
    console.log("fireBase called", detailsObject);

    const {
        DataChanged,
        DateOld,
        DateNew,
        endTimeChanged,
        endTimeNew,
        endTimeOld,
        professorChanged,
        professorOld,
        professorNew,
        startTimeChanged,
        startTimeNew,
        startTimeOld,
        subjectChanged,
        subjectOld,
        subjectNew,
    } = detailsObject;
    let newTime = startTimeChanged || endTimeChanged;
    let changeBody = [
        "Class on",
        DateOld,
        "from",
        startTimeOld,
        endTimeOld,
        "changed to ",
        DataChanged ? `Class  on  ${DateNew}` : `Class  on  ${DateOld}`,
        newTime ? "from" : "",
        newTime ? (startTimeChanged ? startTimeNew : startTimeOld) : "",
        newTime ? (endTimeChanged ? endTimeNew : endTimeOld) : "",
        subjectChanged ? `to Subject ${subjectNew}` : "",
        professorChanged ? `of Professor ${professorNew}` : "",
    ];
    console.log("changeBody", changeBody);
    let data = JSON.stringify({
        priority: "high",
        content_available: true,
        to: topic,
        aps: {
            "content-available": 1
        },
        data: {
            type: "ScheduleChange",
            title: title,
            body: changeBody.join(" "),
        },
    });

    let config = {
        method: "post",
        url: "https://fcm.googleapis.com/fcm/send",
        "content-available": 1,
        aps: {
            "content-available": 1
        },
        headers: {
            Authorization: "key=AAAA6amStzE:APA91bGJEM79aWSNM7BYs81n8PLAoQR_TylQO1d1JqYI0EB8o3UJSviMsKL2tjrxniOGxfzYEsENUUIiT7_PcJu_mhyNkeiiysllXzY7MnZcACqU9CWUE3cv4zHsxtPmG-VSydg9Z_GW",
            "Content-Type": "application/json",
        },
        data: data,
    };

    try {
        let data = await axios(config);
        console.log("data sent to fireBase", data);
        console.log("config", config);
        return new Promise((res) => res("resolve"));
    }
    catch (e) {
        console.log("error  sending to fireBase", e);
        return new Promise((res) => res("resolve"));
    }

};
const firebase = async(classTopic, title, detailsObject) => {

    let data = JSON.stringify({
        to: classTopic,
        content_available: true,
        priority: "high",
        data: {
            title: title,
            type: "ScheduleChange",
            body: detailsObject,
        },
    });

    let config = {
        method: "post",
        content_available: 1,
        url: "https://fcm.googleapis.com/fcm/send",
        headers: {
            Authorization: "key=AAAA6amStzE:APA91bGJEM79aWSNM7BYs81n8PLAoQR_TylQO1d1JqYI0EB8o3UJSviMsKL2tjrxniOGxfzYEsENUUIiT7_PcJu_mhyNkeiiysllXzY7MnZcACqU9CWUE3cv4zHsxtPmG-VSydg9Z_GW",
            "Content-Type": "application/json",
        },
        data: data,
    };
    console.log("notifications sent");
    try {
        let data = await axios(config);
        console.log("Data", data);
        return new Promise((res) => res("resolve"));
    }
    catch (e) {
        console.log("error sending to firebase");
        return new Promise((res) => res("resolve"));
    }


};

const firebaseChangeInScheduleProfessor = async(title, detailsObject) => {
    console.log("fireBase called", detailsObject);

    const {
        DataChanged,
        DateOld,
        DateNew,
        endTimeChanged,
        endTimeNew,
        endTimeOld,
        professorChanged,
        professorOld,
        professorNew,
        startTimeChanged,
        startTimeNew,
        startTimeOld,
        subjectChanged,
        subjectOld,
        subjectNew,
        professorOldUserCode,
        professorNewUserCode,
        UniCode
    } = detailsObject;
    let newTime = startTimeChanged || endTimeChanged;
    if (professorChanged) {

        let professorTopicNew = `/topics/ProfessorTopic_${UniCode}_${professorNewUserCode}`;
        let professorTopicOld = `/topics/ProfessorTopic_${UniCode}_${professorOldUserCode}`;
        console.log("professor changed user Code New", professorNewUserCode, professorTopicNew);
        console.log("professor changed user Code Old", professorOldUserCode, professorTopicOld);
        let changeBodyNew = [
            "Class on",
            DateOld,
            "from",
            startTimeOld,
            endTimeOld,
            "changed to ",
            DataChanged ? `Class  on  ${DateNew}` : `Class  on  ${DateOld}`,
            newTime ? "from" : "",
            newTime ? (startTimeChanged ? startTimeNew : startTimeOld) : "",
            newTime ? (endTimeChanged ? endTimeNew : endTimeOld) : "",
            subjectChanged ? `to Subject ${subjectNew}` : "",
            professorChanged ? `of Professor ${professorNew}` : "",
        ];


        try {
            let data = JSON.stringify({
                priority: "high",
                content_available: true,
                to: professorTopicNew,
                aps: {
                    "content_available": 1
                },
                data: {
                    type: "ScheduleChange",
                    title: title,
                    body: changeBodyNew.join(" "),
                },
            });
            let config = {
                method: "post",
                content_available: 1,
                aps: JSON.stringify({
                    "content-available": 1,
                    "mutable-content": 1,
                    alert: {
                        title: "New Class Scheduled",
                        "subject": detailsObject,
                        body: detailsObject,
                    },
                    badge: 1,
                    sound: "default",
                }),
                url: "https://fcm.googleapis.com/fcm/send",
                headers: {
                    Authorization: "key=AAAA6amStzE:APA91bGJEM79aWSNM7BYs81n8PLAoQR_TylQO1d1JqYI0EB8o3UJSviMsKL2tjrxniOGxfzYEsENUUIiT7_PcJu_mhyNkeiiysllXzY7MnZcACqU9CWUE3cv4zHsxtPmG-VSydg9Z_GW",
                    "Content-Type": "application/json",
                },
                data: data,
            };
            await axios(config);
        }
        catch (e) {
            console.log("error sending to firebase professor new", e);
        }


        try {
            let changeBody = [
                "Class on",
                DateOld,
                "from",
                startTimeOld,
                endTimeOld,
                "changed to ",
                professorChanged ? `of Professor ${professorNew}` : "",
            ];

            let data = JSON.stringify({
                to: professorTopicOld,
                data: {
                    title: title,
                    body: changeBody.join(" "),
                },
            });
            let config = {
                method: "post",
                content_available: 1,
                url: "https://fcm.googleapis.com/fcm/send",
                headers: {
                    Authorization: "key=AAAA6amStzE:APA91bGJEM79aWSNM7BYs81n8PLAoQR_TylQO1d1JqYI0EB8o3UJSviMsKL2tjrxniOGxfzYEsENUUIiT7_PcJu_mhyNkeiiysllXzY7MnZcACqU9CWUE3cv4zHsxtPmG-VSydg9Z_GW",
                    "Content-Type": "application/json",
                },
                data: data,
            };
            await axios(config);
        }
        catch (e) {
            console.log("error sending to firebase professor old", e);
        }

    }
    else {
        let professorTopicOld = `/topics/ProfessorTopic_${UniCode}_${professorOldUserCode}`;
        console.log("professor changed user Code Old", professorOldUserCode, professorTopicOld);
        let changeBodyOld = [
            "Class on",
            DateOld,
            "from",
            startTimeOld,
            endTimeOld,
            "changed to ",
            DataChanged ? `Class  on  ${DateNew}` : `Class  on  ${DateOld}`,
            newTime ? "from" : "",
            newTime ? (startTimeChanged ? startTimeNew : startTimeOld) : "",
            newTime ? (endTimeChanged ? endTimeNew : endTimeOld) : "",
            subjectChanged ? `to Subject ${subjectNew}` : "",
            professorChanged ? `of Professor ${professorNew}` : "",
        ];
        try {
            let data = JSON.stringify({
                to: professorTopicOld,
                data: {
                    type: "ScheduleChange",
                    title: title,
                    body: changeBodyOld.join(" "),
                },
            });
            let config = {
                method: "post",
                content_available: 1,
                aps: JSON.stringify({
                    "content-available": 1,
                    "mutable-content": 1,
                    alert: {
                        title: "New Class Scheduled",
                        "subject": detailsObject,
                        body: detailsObject,
                    },
                    badge: 1,
                    sound: "default",
                }),
                url: "https://fcm.googleapis.com/fcm/send",
                headers: {
                    Authorization: "key=AAAA6amStzE:APA91bGJEM79aWSNM7BYs81n8PLAoQR_TylQO1d1JqYI0EB8o3UJSviMsKL2tjrxniOGxfzYEsENUUIiT7_PcJu_mhyNkeiiysllXzY7MnZcACqU9CWUE3cv4zHsxtPmG-VSydg9Z_GW",
                    "Content-Type": "application/json",
                },
                data: data,
            };
            await axios(config);
        }
        catch (e) {
            console.log("error sending to firebase professor old", e);
        }
    }
    return new Promise((res) => res("resolve"));
};
