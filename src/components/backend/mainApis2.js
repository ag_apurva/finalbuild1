//GET,DELETE,PUT,POST,OPTIONS
const mysql = require('mysql');
const moment = require("moment");
const axios = require("axios");
const pool = mysql.createPool({
    // connectionLimit: 20,
    host: 'campuslive.czmocuynmaaq.us-east-2.rds.amazonaws.com',
    user: 'admin',
    password: 'Vishal#3',
    database: 'CampusLiveDev'
});
const AWS = require("aws-sdk");
const ses = new AWS.SES({ region: "us-east-2" });
const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
exports.handler = async(event) => {
    console.log("event", JSON.stringify(event));
    if (event.resource === "/contacts/getcontacts") {
        /*this API is extracting information form UniContacts table based on UniCode(universityCode)*/
        if (!event.queryStringParameters) {
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: " universityCode needs to be passed in  params " })
            };
            return res;
        }
        else {
            try {
                let universityCode;
                let lastUpdatedEpochTime = 0;
                if (!event.queryStringParameters.universityCode) {
                    universityCode = event.queryStringParameters.universityCode || null; /* University Code works same as UniCode in Table  */
                    let res = {
                        "statusCode": 422,
                        "headers": {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        "body": JSON.stringify({ error: " universityCode needs to be passed in  params " })
                    };
                    /* the response is same because uniCode needs to be a part of queryStringParameters */
                    if (!universityCode || universityCode == null || universityCode == undefined) {
                        return res;
                    }
                    else {
                        return res;
                    }
                }
                else {
                    universityCode = event.queryStringParameters.universityCode
                }


                if (!event.queryStringParameters.lastUpdatedEpochTime) {
                    lastUpdatedEpochTime = 0;
                }

                try {
                    lastUpdatedEpochTime = parseInt(event.queryStringParameters.lastUpdatedEpochTime)
                    if (isNaN(lastUpdatedEpochTime)) {
                        lastUpdatedEpochTime = 0;
                    }
                    console.log("lastUpdatedEpochTime111", lastUpdatedEpochTime);
                }
                catch (e) {
                    lastUpdatedEpochTime = 0;
                }
                console.log("lastUpdatedEpochTime", lastUpdatedEpochTime);

                let extractContactsSql = `Select * from UniContacts where UniCode = ? and ChangeId != 3 and LastUpdateEpochTime >= ?`;
                let contactsData = await getOrder(extractContactsSql, [universityCode, lastUpdatedEpochTime]);

                const contactData = [];
                contactsData.forEach(contact => {
                    const obj = {};
                    obj.idUniContacts = contact.idUniContacts;
                    obj.universityCode = contact.UniCode;
                    obj.contactCode = contact.ContactCode;
                    obj.name = contact.Name
                    obj.phoneNumber1 = contact.PhoneNumber1
                    obj.phoneNumber2 = contact.PhoneNumber2
                    obj.emailId = contact.EmailId
                    obj.designation = contact.Designation
                    obj.location = contact.Location
                    obj.changeId = contact.ChangeId
                    obj.category = contact.Category
                    obj.lastUpdateEpochTime = contact.LastUpdateEpochTime
                    contactData.push(obj);
                });

                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify(contactData)
                };
                return res;
            }
            catch (e) {
                console.log("ERROR OCCURED");
                console.log("Internal Server Error", e);
                let res = {
                    "statusCode": 500,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ type: "Internal Server Error", error: JSON.stringify(e), category: "/contacts/getContacts" })
                };
                return res;
            }
        }
    }
    if (event.resource === "/contacts/deletecontact") {
        /*this API is deleting the contact bases on idUniContact and UniCode basically it sets the ChnageId to 3*/
        if (!event.queryStringParameters) {
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,DELETE,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: " universityCode and idUniContact needs to be passed in  params " })
            };
            return res;
        }
        else {
            try {
                let universityCode;
                let idContact;
                if (!event.queryStringParameters.universityCode || !event.queryStringParameters.idContact) {
                    /* Modify the validation login in the ablut id statement
                    University Code works same as UniCode in Table  */
                    let res = {
                        "statusCode": 422,
                        "headers": {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        "body": JSON.stringify({ error: " universityCode and idUniContact needs to be passed in  param " })
                    };
                    return res;
                }
                else {
                    universityCode = event.queryStringParameters.universityCode;
                    idContact = event.queryStringParameters.idContact;
                }
                let deleteContactSql = `Update UniContacts set ChangeId = 3 where UniCode = ? and idUniContacts = ?`;
                let contactsData = await getOrder(deleteContactSql, [universityCode, idContact]);
                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,DELETE,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ data: "UniContact deleted successfully" })
                };
                return res;
            }
            catch (e) {
                console.log("ERROR OCCURED", e);
                console.log("Internal Server Error", e);
                console.log("UniContact Deletion Failed", e);
                let res = {
                    "statusCode": 500,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,DELETE,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ type: "Internal Server Error", error: JSON.stringify(e), category: "/contacts/getContacts" })
                };
                return res;
            }
        }
    }
    if (event.resource === "/contacts/createcontact") {
        /*   the api is create Contact which currently takes  Name,Location,Designation,PhoneNumber1,PhoneNumber2,UniCode as parameter */
        let inputBody = event.body ? JSON.parse(event.body) : {}; /* If event Body is not parsable if will be a empty object */
        if (inputBody.hasOwnProperty("universityCode") &&
            inputBody.hasOwnProperty("name") &&
            inputBody.hasOwnProperty("designation") &&
            inputBody.hasOwnProperty("category")) {
            let UniCode = inputBody["universityCode"]
            let createContactObject = {};
            createContactObject['UniCode'] = inputBody["universityCode"];
            createContactObject['Name'] = inputBody["name"];
            createContactObject['Designation'] = inputBody["designation"];
            createContactObject['Category'] = inputBody["category"];
            if (inputBody.hasOwnProperty("location") && inputBody["location"] && inputBody["location"] != "") {
                createContactObject['Location'] = inputBody["location"];
            }

            if (inputBody.hasOwnProperty("phoneNumber1") && inputBody["phoneNumber1"] && inputBody["phoneNumber1"] != "") {
                createContactObject['PhoneNumber1'] = inputBody["phoneNumber1"];
            }

            if (inputBody.hasOwnProperty("phoneNumber2") && inputBody["phoneNumber2"] && inputBody["phoneNumber2"] != "") {
                createContactObject['PhoneNumber2'] = inputBody["phoneNumber2"];
            }

            if (inputBody.hasOwnProperty("emailId") && inputBody["emailId"] && inputBody["emailId"] != "") {
                createContactObject['EmailId'] = inputBody["emailId"];
            }
            createContactObject["ChangeId"] = 1;
            createContactObject["LastUpdateEpochTime"] = Math.floor(Date.now() / 1000);
            try {
                let createContactSql = 'Insert into UniContacts set ?';
                await getOrder(createContactSql, [createContactObject])
                let newContact = await getOrder("select max(idUniContacts) as Value from UniContacts where UniCode=?", [UniCode]);
                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ data: newContact[0]["Value"] })
                };
                return res;
            }
            catch (e) {
                console.log("error in creating contact");
                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ type: "Internal Server Error", error: JSON.stringify(e), category: "/contacts/createcontact" })
                };
                return res;
            }




        }
        else {
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: " required paramater not present in body of POST Contact of Post" })
            };
            return res;
        }
    }
    if (event.resource === "/contacts/updatecontact") {
        /*   the api is to update Contact which currently takes  Name,Location,Designation,phoneNumber1,PhoneNumber2,UniCode,idUniContact as parameter */
        let inputBody = event.body ? JSON.parse(event.body) : {}; /* If event Body is not parsable if will be a empty object */
        if (inputBody.hasOwnProperty("universityCode") &&
            inputBody.hasOwnProperty("name") &&
            inputBody.hasOwnProperty("designation")) {
            let idUniContact = inputBody["idUniContact"]
            let UniCode = inputBody["universityCode"]
            let updateContactObject = {};
            updateContactObject['UniCode'] = inputBody["universityCode"];
            updateContactObject['Name'] = inputBody["name"];
            updateContactObject['Designation'] = inputBody["designation"];
            if (inputBody.hasOwnProperty("location") && inputBody["location"] != "") {
                updateContactObject['Location'] = inputBody["location"];
            }

            if (inputBody.hasOwnProperty("phoneNumber1") && inputBody["phoneNumber1"] != "") {
                updateContactObject['PhoneNumber1'] = inputBody["phoneNumber1"];
            }

            if (inputBody.hasOwnProperty("phoneNumber2") && inputBody["phoneNumber2"] != "") {
                updateContactObject['PhoneNumber2'] = inputBody["phoneNumber2"];
            }

            if (inputBody.hasOwnProperty("emailId") && inputBody["emailId"] != "") {
                updateContactObject['EmailId'] = inputBody["emailId"];
            }
            updateContactObject["ChangeId"] = 2;
            updateContactObject["LastUpdateEpochTime"] = Math.floor(Date.now() / 1000);

            try {
                let updateContactSql = 'update UniContacts set ? where UniCode = ? and idUniContacts = ?';
                let data = await getOrder(updateContactSql, [updateContactObject, UniCode, idUniContact])
                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ data: `Contact updated Successfully ${JSON.stringify(data)}  ${UniCode} ${idUniContact}` })
                };
                return res;
            }
            catch (e) {

                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,PUT,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ type: "Internal Server Error", error: JSON.stringify(e), category: "/contacts/updateContact" })
                };
                return res;
            }




        }
        else {
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,PUT,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: " required paramater not present in body of PUT Contact of Post" })
            };
            return res;
        }
    }
    
    else if (event.resource === "/students/getstudents") {
        if (!event.queryStringParameters) {
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: " universityCode needs to be passed in  params " })
            };
            return res;
        }
        else {
            try {
                let universityCode;
                if (!event.queryStringParameters.universityCode) {
                    universityCode = event.queryStringParameters.universityCode || null; /* University Code works same as UniCode in Table  */
                    let res = {
                        "statusCode": 422,
                        "headers": {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        "body": JSON.stringify({ error: " universityCode needs to be passed in  params " })
                    };
                    /* the response is same because uniCode needs to be a part of queryStringParameters */
                    if (!universityCode || universityCode == null || universityCode == undefined) {
                        return res;
                    }
                    else {
                        return res;
                    }
                }
                else {
                    universityCode = event.queryStringParameters.universityCode
                }

                let getStudentsSql = `SELECT UserInfo.UserCode,UserInfo.UserDOB,UserInfo.UserPinCode,UserInfo.UserEmail,UserInfo.UserName,UserInfo.UserPhone,UserInfo.UserRollNo,UserInfo.UserParentName,UserInfo.UserParentContactNo,UserInfo.UserAddress,UserInfo.UserPinCode,UserInfo.BloodGroup,            UserInfo.UserRegistrationNo, ClassStudent.ClassId, Class.SubYear, Class.Semester, Class.Division FROM UserInfo INNER JOIN ClassStudent
            INNER JOIN Class
            where ClassStudent.StudentCode = UserInfo.UserCode
            and ClassStudent.UniCode = UserInfo.UniCode
            and UserInfo.UniCode = ?
            and Class.idClass = ClassStudent.ClassId
            and UserInfo.UserRole = "Student"
            and UserInfo.ChangeId != 3`;



                let students = await getOrder(getStudentsSql, [universityCode]);


                let studentData = [];
                students.forEach(student => {
                    let obj = {};
                    obj.userCode = student.UserCode;
                    obj.userDOB = student.UserDOB;
                    obj.userPinCode = student.UserPinCode;
                    obj.userEmail = student.UserEmail;
                    obj.userName = student.UserName
                    obj.userPhoneNumber = student.UserPhone
                    obj.userRollNumber = student.UserRollNo
                    obj.userParentName = student.UserParentName
                    obj.userParentNumber = student.UserParentContactNo
                    obj.userAddress = student.UserAddress
                    obj.bloodGroup = student.BloodGroup
                    obj.userRegistrationNumber = student.UserRegistrationNo
                    obj.idClass = student.ClassId;
                    obj.subYear = student.SubYear;
                    obj.semester = student.Semester;
                    obj.division = student.Division;

                    studentData.push(obj);
                })
                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify(studentData)
                };
                return res;
            }
            catch (e) {
                console.log("ERROR OCCURED");
                console.log("Internal Server Error", e);
                let res = {
                    "statusCode": 500,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,POST,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ type: "Internal Server Error", error: JSON.stringify(e), category: "/students/getstudents" })
                };
                return res;
            }
        }
    }
    else if (event.resource === "/students/deletestudent") {
        if (!event.queryStringParameters) {
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: " universityCode needs to be passed in  params " })
            };
            return res;
        }
        else {
            try {
                let universityCode;
                let studentEmail;
                if (!event.queryStringParameters.universityCode || !event.queryStringParameters.studentEmail) {
                    universityCode = event.queryStringParameters.universityCode || null; /* University Code works same as UniCode in Table  */
                    let res = {
                        "statusCode": 422,
                        "headers": {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        "body": JSON.stringify({ error: " universityCode needs to be passed in  params " })
                    };

                    return res;
                }
                else {
                    universityCode = event.queryStringParameters.universityCode;
                    studentEmail = event.queryStringParameters.studentEmail
                }
                let studentDeleteObject = {}
                studentDeleteObject["ChangeId"] = 3;
                studentDeleteObject["LastUpdateEpochTime"] = Math.floor(Date.now() / 1000);

                var sqlDeleteStudent = 'Update UserInfo SET ? where UserEmail= ? and UniCode = ? and UserRole = ?'
                const cognito = new AWS.CognitoIdentityServiceProvider();

                let moreThanOneIdentity = true;
                // we are checking if there are not more users with this mail.
                await getOrder(sqlDeleteStudent, [studentDeleteObject, studentEmail, universityCode, "Student"]);
                let checkUser = 'Select * from UserInfo where UserEmail= ? and ChangeId!=?';
                const data = await getOrder(checkUser, [studentEmail, 3]);
                if (data.length < 1) {
                    moreThanOneIdentity = false; // only we get confirmation tha only 1 user exist in DB
                }
                console.log("moreThanOneIdentity", moreThanOneIdentity);
                if (!moreThanOneIdentity) {
                    await cognito.adminDeleteUser({
                        UserPoolId: "us-east-2_0Q8uWO7H9",
                        Username: studentEmail,
                    }).promise();
                }

                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ "data": "Student deleted sucessfully" })
                };
                return res;
            }
            catch (e) {
                console.log("ERROR OCCURED");
                console.log("Internal Server Error", e);
                let res = {
                    "statusCode": 500,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ type: "Internal Server Error", error: JSON.stringify(e), category: "/students/deletestudent" })
                };
                return res;
            }
        }
    }
    else if (event.resource === "/students/createstudent") {
        let inputBody = JSON.parse(event.body);
        let studentCode = ""; // this is declared outside because if somehting failed then we have to delete student from userinfo and class student 
        let studentEmail, studentName, universityCode, idClass;
        let phoneNumber, address, pincode, userRegistrationNo, userRollNo, userDOB, parentPhoneNumber, parentName;
        if (inputBody.hasOwnProperty("userEmail") &&
            inputBody.hasOwnProperty("userName") &&
            inputBody.hasOwnProperty("universityCode") && //
            inputBody.hasOwnProperty("userPhoneNumber") &&
            inputBody.hasOwnProperty("userAddress") &&
            inputBody.hasOwnProperty("userPinCode") &&
            inputBody.hasOwnProperty("userRegistrationNumber") &&
            inputBody.hasOwnProperty("userRollNumber") &&
            inputBody.hasOwnProperty("userDOB") &&
            inputBody.hasOwnProperty("userParentNumber") &&
            inputBody.hasOwnProperty("userParentName") &&
            inputBody.hasOwnProperty("idClass")
        ) {
            studentEmail = inputBody["userEmail"];
            studentName = inputBody["userName"];
            universityCode = inputBody["universityCode"];
            phoneNumber = inputBody["userPhoneNumber"];
            address = inputBody["userAddress"];
            pincode = inputBody["userPinCode"];
            userRegistrationNo = inputBody["userRegistrationNumber"];
            userRollNo = inputBody["userRollNumber"];
            userDOB = inputBody["userDOB"];
            parentPhoneNumber = inputBody["userParentNumber"];
            parentName = inputBody["userParentName"];
            idClass = inputBody["idClass"];
            
            console.log("create student",parentName,idClass,studentEmail,studentName,universityCode,phoneNumber,address,pincode,userRegistrationNo,userRollNo,userDOB,parentPhoneNumber)
            
            try {
                var userCheck = 'SELECT * FROM UserInfo where UserEmail=? and ChangeId!=?'
                var studentCheck = 'SELECT * FROM UserInfo where UserEmail=? and UniCode=? and ChangeId!=?'
                var userInfo = await getOrder(userCheck, [studentEmail, 3]);
                var studentInfo = await getOrder(studentCheck, [studentEmail, universityCode, 3]);
                console.log("userInfo studentInfo", userInfo, studentInfo, userInfo.length == 0, studentInfo.length == 0)

                if (userInfo.length < 1) { // this is checking whether email exist in system at all or not.
                    let params = {
                        UserPoolId: "us-east-2_0Q8uWO7H9", //us-east-2_0Q8uWO7H9
                        Username: studentEmail,
                        DesiredDeliveryMediums: ["EMAIL"],
                        ForceAliasCreation: false,
                        UserAttributes: [
                            { Name: "name", Value: studentName },
                            { Name: "email", Value: studentEmail },
                            { Name: "phone_number", Value: phoneNumber }
                        ],
                    };
                    let client = new AWS.CognitoIdentityServiceProvider();
                    var userDataCreated = await new Promise((resolve, reject) => {
                        client.adminCreateUser(params, function(err, data) {
                            if (err) {
                                reject(err);
                            }
                            else {
                                resolve(data);
                            }
                        })

                    })
                    console.log("userDataCreated", userDataCreated);

                    await new Promise((resolve, reject) => {
                        client.adminUpdateUserAttributes({
                            UserAttributes: [{
                                Name: 'email_verified',
                                Value: 'true'
                            }],
                            UserPoolId: "us-east-2_0Q8uWO7H9",
                            Username: studentEmail
                        }, function(err) {
                            if (err) {
                                reject(err);
                            }
                            else {
                                resolve()
                            }
                        })
                    })
                }
                //check if user exist for that particular univeristy
                if (studentInfo.length < 1) {
                    let tempValue = await getTempValueToInsertIntoTable(universityCode);
                    studentCode = "USR0" + tempValue;
                    let post_User = {
                        UserCode: "USR0" + tempValue,
                        UserName: studentName,
                        UserEmail: studentEmail,
                        UserPhone: phoneNumber,
                        UserRole: "Student",
                        UserActive: "True",
                        IsTrial: 0,
                        UniCode: universityCode,
                        UserRegistrationNo: userRegistrationNo,
                        UserRollNo: userRollNo,
                        UserDOB: userDOB,
                        UserParentName: parentName,
                        UserAddress: address,
                        UserPinCode: pincode,
                        UserParentContactNo: parentPhoneNumber,
                        LastUpdateEpochTime: Math.floor(Date.now() / 1000),
                        ChangeId: 1
                    }
                    let sqlUserInfo = 'INSERT INTO UserInfo SET ?';
                    let sqlData = await getOrder(sqlUserInfo, post_User);
                    let sqlInsertIntoClassStudent = 'INSERT INTO ClassStudent SET ?';
                    let sqlInsertIntoClassStudentData = await getOrder(sqlInsertIntoClassStudent, { ClassId: idClass, StudentCode: studentCode, UniCode: universityCode });
                    console.log("sqlInsertIntoClassStudentData", sqlInsertIntoClassStudentData);
                    try {
                        let sqlContactInsert = `Insert into UniContacts set ?`;
                        await getOrder(sqlContactInsert, [{
                            UniCode: universityCode,
                            ContactCode: studentCode,
                            Name: studentName,
                            PhoneNumber1: phoneNumber,
                            PhoneNumber2: parentPhoneNumber,
                            EmailId: studentEmail,
                            Designation: "Student",
                            Location: "",
                            ChangeId: 1,
                            Category: "Student"
                        }])
                    }
                    catch (e) {
                        console.log("ERROR while inserting into student", e);
                    }

                    let res = {
                        "statusCode": 200,
                        "headers": {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,POST,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        "body": JSON.stringify({ data: studentCode }),
                    };
                    return res;
                }
                else {
                    //here we know that students email address already exist for the university.

                    let res = {
                        "statusCode": 200,
                        "headers": {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,POST,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        "body": JSON.stringify({ error: "User Already Exists" }),
                    };
                    return res;
                }
            }
            catch (e) {
                try {
                    let sqlUserCode = "delete from UserInfo where UserCode=?";
                    await getOrder(sqlUserCode, studentCode);
                }
                catch (e) {
                    console.log("error student")
                }

                try {
                    let count = 2;
                    try { // we are checking if there are not more users with this mail.

                        let sql = 'Select * from UserInfo where UserEmail= ? and ChangeId!=?'
                        const data = await getOrder(sql, [studentEmail, 3]);
                        console.log("data", data.length);
                        if (data.length == 0) {
                            count = 1;
                        }
                    }
                    catch (e) {
                        console.log("error occured while checking count", e);
                    }

                    try {
                        if (count == 1) {
                            const cognito = new AWS.CognitoIdentityServiceProvider();
                            await cognito.adminDeleteUser({
                                UserPoolId: "us-east-2_0Q8uWO7H9",
                                Username: studentEmail,
                            }).promise();
                        }
                    }
                    catch (e) {}
                }
                catch (e) {}

                try {
                    let sqlstudentCode = "delete from ClassStudent where StudentCode=?"; //studentCode
                    await getOrder(sqlstudentCode, studentCode);
                }
                catch (e) {}

                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,POST,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ error: JSON.stringify(e) }),
                };
                return res;
            }
        }
        else {
            let obj = {
                error: "All keys not found to create student not found",
            };
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,POST,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: obj.error }),
            };
            return res;
        }
    }
    else if (event.resource === "/students/editstudent") {
        let inputBody = JSON.parse(event.body);
        if (inputBody.hasOwnProperty("userAddress") &&
            inputBody.hasOwnProperty("userParentName") &&
            inputBody.hasOwnProperty("userParentNumber") &&
            inputBody.hasOwnProperty("userPhoneNumber") &&
            inputBody.hasOwnProperty("userEmail") &&
            inputBody.hasOwnProperty("userName") &&
            inputBody.hasOwnProperty("universityCode") &&
            inputBody.hasOwnProperty("userPinCode") &&
            inputBody.hasOwnProperty("userDOB") &&
            inputBody.hasOwnProperty("userRegistrationNumber") &&
            inputBody.hasOwnProperty("userRollNumber")
        ) {
            let address = inputBody["userAddress"];
            let parentName = inputBody["userParentName"];
            let parentPhoneNumber = inputBody["userParentNumber"];
            let phoneNumber = inputBody["userPhoneNumber"];
            let studentEmail = inputBody["userEmail"];
            let studentName = inputBody["userName"];
            let universityCode = inputBody["universityCode"];
            let pincode = inputBody["userPinCode"];
            let userDOB = inputBody["userDOB"];
            let userRegistrationNo = inputBody["userRegistrationNumber"];
            let userRollNo = inputBody["userRollNumber"];
            
            //console.log("Ffff",userRegistrationNo,userRollNo,address,parentName,parentPhoneNumber,phoneNumber,studentEmail,studentName,universityCode,pincode,userDOB)
            
            var tableName = ""
            var studentAttributes = {
                UserPinCode: pincode,
                UserAddress: address,
                UserName: studentName,
                UserPhone: phoneNumber,
                UserRegistrationNo: userRegistrationNo,
                UserRollNo: userRollNo,
                UserDOB: userDOB,
                UserParentName: parentName,
                UserParentContactNo: parentPhoneNumber
            }
            studentAttributes["LastUpdateEpochTime"] = Math.floor(Date.now() / 1000);
            studentAttributes["ChangeId"] = 2;
            try {
                //updating the contacts table also.
                if (phoneNumber) {
                    let sqlUserInfo = `select * from UserInfo where UserEmail=? and UniCode=? and ChangeId!=3`
                    let userDataNew = await getOrder(sqlUserInfo, [studentEmail, universityCode]);
                    let sql = `update UniContacts set ? where ContactCode = ?  and UniCode= ?`;
                    await getOrder(sql, [{ PhoneNumber1: phoneNumber, PhoneNumber2: parentPhoneNumber || null }, userDataNew[0]['UserCode'], universityCode])
                }
            }
            catch (e) {
                console.log("Error updating UniContact after student is updated", e)
            }

            try {
                const sqlStudentUpdate = 'UPDATE UserInfo SET ? WHERE UserEmail = ? and UniCode = ? and ChangeId!= ?';
                const updatedStudent = await getOrder(sqlStudentUpdate, [studentAttributes, studentEmail, universityCode, 3])
                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,POST,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ data: "student edited updated" }),
                };
                return res;
            }
            catch (e) {

                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,POST,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ error: JSON.stringify(e) }),
                };
                return res;
            }

        }
        else {
            let obj = {
                error: "All keys not found to edit student not found",
            };
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,POST,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: obj.error }),
            };
            return res;
        }
    }
    else if (event.resource === "/authentication/adminmyprofile") {
        if (!event.queryStringParameters) {
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: " universityCode and student code needs to be passed in  params " })
            };
            return res;
        }
        else {
            try {
                let universityCode, userEmail
                if (!event.queryStringParameters.universityCode) {
                    universityCode = event.queryStringParameters.universityCode || null; /* University Code works same as UniCode in Table  */
                    let res = {
                        "statusCode": 422,
                        "headers": {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        "body": JSON.stringify({ error: " universityCode needs to be passed in  params " })
                    };
                    /* the response is same because uniCode needs to be a part of queryStringParameters */
                    if (!universityCode || universityCode == null || universityCode == undefined) {
                        return res;
                    }
                    else {
                        return res;
                    }
                }
                else {
                    universityCode = event.queryStringParameters.universityCode;
                    userEmail = event.queryStringParameters.userEmail;
                }

                let userSql = `SELECT UserInfo.*,UI.* FROM UserInfo 
                JOIN UniversityInfo as UI
                where UserInfo.UserEmail = ?
                and UserInfo.UniCode = ?
                and UserInfo.UserRole = "Admin"
                and UserInfo.ChangeId != ? and UserInfo.UniCode = UI.Uni_Code`;
                let codeAwait = await getOrder(userSql, [userEmail, universityCode, 3]);
                console.log("codeAwait", codeAwait);
                var bodyData = {};
                if (codeAwait.length !== 0) {
                    bodyData = codeAwait[0];
                }
                console.log("bodyData", JSON.stringify(bodyData))
                let statusCode = 200;
                if (Object.keys(bodyData) == 0) {
                    statusCode = 401;
                }
                let obj = {};
                obj.universityCode = bodyData.UniCode;
                obj.universityName = bodyData.Uni_Name;
                obj.universityCountry = bodyData.Uni_Country;
                obj.universityPostalCode = bodyData.Uni_PostalCode;
                obj.userCode = bodyData.UserCode;
                obj.userAddress = bodyData.UserAddress;
                obj.userPincode = bodyData.UserPinCode;
                obj.userEmail = bodyData.UserEmail;
                obj.userPhone = bodyData.UserPhone;
                obj.userName = bodyData.UserName;
                obj.userRole = bodyData.UserRole;
                obj.userDesignation = bodyData.UserDesignation;
                obj.userRollNumber = bodyData.UserRollNo; //
                obj.lastUpdateEpochTime = bodyData.LastUpdateEpochTime;


                var res = {
                    "statusCode": statusCode,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify(obj)
                };

                return res;
            }
            catch (e) {
                console.log("ERROR OCCURED");
                console.log("Internal Server Error", e);
                let res = {
                    "statusCode": 500,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ type: "Internal Server Error", error: JSON.stringify(e), category: "/authentication/adminmyprofile" })
                };
                return res;
            }
        }
    }
    else if (event.resource === "/authentication/adminsaveprofile") {
        /*   the api is create Contact which currently takes  Name,Location,Designation,PhoneNumber1,PhoneNumber2,UniCode as parameter */
        let inputBody = event.body ? JSON.parse(event.body) : {}; /* If event Body is not parsable if will be a empty object */
        if (inputBody.hasOwnProperty("universityCode") &&
            inputBody.hasOwnProperty("userCode") && inputBody["userCode"] &&
            inputBody.hasOwnProperty("userName") &&
            inputBody.hasOwnProperty("userDesignation") &&
            inputBody.hasOwnProperty("userPhone") &&
            inputBody.hasOwnProperty("userAddress") &&
            inputBody.hasOwnProperty("userPincode")) {

            let createUserObject = {};
            let UniCode = inputBody["universityCode"];
            let UserCode = inputBody["userCode"];

            var sqlCheck = 'SELECT * FROM UserInfo where UserCode = ? and UniCode = ? and ChangeId != 3 and UserRole="Admin"';
            let studentCheckData = await getOrder(sqlCheck, [UserCode, UniCode]);
            if (studentCheckData.length == 0) {
                let statusCode = 400;
                let res = {
                    "statusCode": statusCode,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ error: " User does not exist " })
                };

                return res;
            }

            createUserObject['UserName'] = inputBody["userName"];
            createUserObject['UserDesignation'] = inputBody["userDesignation"];
            createUserObject['UserPhone'] = inputBody["userPhone"];
            createUserObject['UserAddress'] = inputBody["userAddress"];
            createUserObject['UserPinCode'] = inputBody["userPincode"];

            createUserObject['LastUpdateEpochTime'] = Math.floor(Date.now() / 1000);
            createUserObject["ChangeId"] = 2;

            try {
                let updateProfile = 'Update UserInfo set ? where UniCode = ?  and UserCode=?  and UserRole="Admin" and ChangeId!=3';
                await getOrder(updateProfile, [createUserObject, UniCode, UserCode])

                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ data: "User Updated Successfully" })
                };
                return res;
            }
            catch (e) {
                console.log("error in creating contact");
                let res = {
                    "statusCode": 400,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ type: "Internal Server Error", error: JSON.stringify(e), category: "/authentication/adminsaveprofile" })
                };
                return res;
            }




        }
        else {
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: " required paramater not present in body of POST My Profile" })
            };
            return res;
        }
    }


    else if (event.resource === "/professors/createprofessor") {
        let inputBody = JSON.parse(event.body);
        let professorCode = ""; // this is declared outside because if somehting failed then we have to delete student from userinfo and class student 
        let professorEmail, professorName, universityCode;
        let phoneNumber, address, pincode, userRegistrationNo;
        if (inputBody.hasOwnProperty("professorEmail") &&
            inputBody.hasOwnProperty("professorName") &&
            inputBody.hasOwnProperty("universityCode") && //
            inputBody.hasOwnProperty("phoneNumber") &&
            inputBody.hasOwnProperty("address") &&
            inputBody.hasOwnProperty("pincode") &&
            inputBody.hasOwnProperty("userRegistrationNo") &&
            inputBody.hasOwnProperty("designation")
        ) {
            professorEmail = inputBody["professorEmail"];
            professorName = inputBody["professorName"];
            universityCode = inputBody["universityCode"];
            phoneNumber = inputBody["phoneNumber"];
            address = inputBody["address"];
            pincode = inputBody["pincode"];
            userRegistrationNo = inputBody["userRegistrationNo"];


            try {
                var userCheck = 'SELECT * FROM UserInfo where UserEmail=? and ChangeId!=?'
                var studentCheck = 'SELECT * FROM UserInfo where UserEmail=? and UniCode=? and ChangeId!=?'
                var userInfo = await getOrder(userCheck, [professorEmail, 3]);
                var studentInfo = await getOrder(studentCheck, [professorEmail, universityCode, 3]);
                console.log("userInfo studentInfo", userInfo, studentInfo, userInfo.length == 0, studentInfo.length == 0)

                if (userInfo.length < 1) { // this is checking whether email exist in system at all or not.
                    let params = {
                        UserPoolId: "us-east-2_0Q8uWO7H9", //us-east-2_0Q8uWO7H9
                        Username: professorEmail,
                        DesiredDeliveryMediums: ["EMAIL"],
                        ForceAliasCreation: false,
                        UserAttributes: [
                            { Name: "name", Value: professorName },
                            { Name: "email", Value: professorEmail },
                            { Name: "phone_number", Value: phoneNumber }
                        ],
                    };
                    let client = new AWS.CognitoIdentityServiceProvider();
                    var userDataCreated = await new Promise((resolve, reject) => {
                        client.adminCreateUser(params, function(err, data) {
                            if (err) {
                                reject(err);
                            }
                            else {
                                resolve(data);
                            }
                        })

                    })
                    console.log("userDataCreated", userDataCreated);

                    await new Promise((resolve, reject) => {
                        client.adminUpdateUserAttributes({
                            UserAttributes: [{
                                Name: 'email_verified',
                                Value: 'true'
                            }],
                            UserPoolId: "us-east-2_0Q8uWO7H9",
                            Username: professorEmail
                        }, function(err) {
                            if (err) {
                                reject(err);
                            }
                            else {
                                resolve()
                            }
                        })
                    })
                }
                //check if user exist for that particular univeristy
                if (studentInfo.length < 1) {
                    let tempValue = await getTempValueToInsertIntoTable(universityCode);
                    professorCode = "USR0" + tempValue;
                    let post_User = {
                        UserCode: "USR0" + tempValue,
                        UserName: professorName,
                        UserEmail: professorEmail,
                        UserPhone: phoneNumber,
                        UserRole: "Professor",
                        UserActive: "True",
                        IsTrial: 0,
                        UniCode: universityCode,
                        UserRegistrationNo: userRegistrationNo,
                        UserAddress: address,
                        UserPinCode: pincode,
                        LastUpdateEpochTime: Math.floor(Date.now() / 1000),
                        ChangeId: 1
                    }
                    let sqlUserInfo = 'INSERT INTO UserInfo SET ?';
                    let sqlData = await getOrder(sqlUserInfo, post_User);
                    try {
                        let sqlContactInsert = `Insert into UniContacts set ?`;
                        await getOrder(sqlContactInsert, [{
                            UniCode: universityCode,
                            ContactCode: professorCode,
                            Name: professorName,
                            PhoneNumber1: phoneNumber,
                            EmailId: professorEmail,
                            Designation: "Professor",
                            Location: "",
                            ChangeId: 1,
                            Category: "Student"
                        }])
                    }
                    catch (e) {
                        console.log("ERROR while inserting into student", e);
                    }

                    let res = {
                        "statusCode": 200,
                        "headers": {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,POST,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        "body": JSON.stringify({ data: professorCode }),
                    };
                    return res;
                }
                else {
                    //here we know that students email address already exist for the university.

                    let res = {
                        "statusCode": 200,
                        "headers": {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,POST,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        "body": JSON.stringify({ error: "User Already Exists" }),
                    };
                    return res;
                }
            }
            catch (e) {

                console.log("error create professor", e);
                try {
                    let sqlUserCode = "delete from UserInfo where UserCode=?";
                    await getOrder(sqlUserCode, professorCode);
                }
                catch (e) {
                    console.log("error student")
                }

                try {
                    let count = 2;
                    try { // we are checking if there are not more users with this mail.

                        let sql = 'Select * from UserInfo where UserEmail= ? and ChangeId!=?'
                        const data = await getOrder(sql, [professorEmail, 3]);
                        console.log("data", data.length);
                        if (data.length == 0) {
                            count = 1;
                        }
                    }
                    catch (e) {
                        console.log("error occured while checking count", e);
                    }

                    try {
                        if (count == 1) {
                            const cognito = new AWS.CognitoIdentityServiceProvider();
                            await cognito.adminDeleteUser({
                                UserPoolId: "us-east-2_0Q8uWO7H9",
                                Username: professorEmail,
                            }).promise();
                        }
                    }
                    catch (e) {}
                }
                catch (e) {}

                try {
                    let sqlstudentCode = "delete from ClassStudent where StudentCode=?"; //studentCode
                    await getOrder(sqlstudentCode, professorCode);
                }
                catch (e) {}

                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,POST,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ error: JSON.stringify(e) }),
                };
                return res;
            }
        }
        else {
            let obj = {
                error: "All keys not found to create student not found",
            };
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,POST,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: obj.error }),
            };
            return res;
        }
    }
    else if (event.resource === "/professors/deleteprofessor") {
        if (!event.queryStringParameters) {
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: " universityCode needs to be passed in  params " })
            };
            return res;
        }
        else {
            try {
                let universityCode;
                let professorEmail;
                if (!event.queryStringParameters.universityCode || !event.queryStringParameters.professorEmail) {
                    universityCode = event.queryStringParameters.universityCode || null; /* University Code works same as UniCode in Table  */
                    let res = {
                        "statusCode": 422,
                        "headers": {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,DELETE,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        "body": JSON.stringify({ error: " universityCode needs to be passed in  params " })
                    };

                    return res;
                }
                else {
                    universityCode = event.queryStringParameters.universityCode;
                    professorEmail = event.queryStringParameters.professorEmail
                }
                let professsorDeleteObject = {}
                professsorDeleteObject["ChangeId"] = 3;
                professsorDeleteObject["LastUpdateEpochTime"] = Math.floor(Date.now() / 1000);

                var sqlDeleteProfessor = 'Update UserInfo SET ? where UserEmail= ? and UniCode = ? and UserRole = ?'
                const cognito = new AWS.CognitoIdentityServiceProvider();

                let moreThanOneIdentity = true;
                // we are checking if there are not more users with this mail.
                await getOrder(sqlDeleteProfessor, [professsorDeleteObject, professorEmail, universityCode, "Professor"]);
                let checkUser = 'Select * from UserInfo where UserEmail= ? and ChangeId!=?';
                const data = await getOrder(checkUser, [professorEmail, 3]);
                if (data.length < 1) {
                    moreThanOneIdentity = false; // only we get confirmation tha only 1 user exist in DB
                }
                console.log("moreThanOneIdentity", moreThanOneIdentity);
                if (!moreThanOneIdentity) {
                    await cognito.adminDeleteUser({
                        UserPoolId: "us-east-2_0Q8uWO7H9",
                        Username: professorEmail,
                    }).promise();
                }

                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,DELETE,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ "data": "Student deleted sucessfully" })
                };
                return res;
            }
            catch (e) {
                console.log("ERROR OCCURED");
                console.log("Internal Server Error", e);
                let res = {
                    "statusCode": 500,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,DELETE,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ type: "Internal Server Error", error: JSON.stringify(e), category: "/students/deletestudent" })
                };
                return res;
            }
        }
    }
    
    
    if (event.resource === "/helps/gethelp") {
        if (!event.queryStringParameters) {
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: " universityCode and searchKeyword needs to be passed in  params " })
            };
            return res;
        }
        else {
            try {
                let universityCode;
                let searchKeyword;
                if (!event.queryStringParameters.universityCode || !event.queryStringParameters.searchKeyword) {
                    universityCode = event.queryStringParameters.universityCode || null; /* University Code works same as UniCode in Table  */
                    let res = {
                        "statusCode": 422,
                        "headers": {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        "body": JSON.stringify({ error: " universityCode and searchKeyword needs to be passed in  params " })
                    };
                    /* the response is same because uniCode needs to be a part of queryStringParameters */
                    if (!universityCode || universityCode == null || universityCode == undefined) {
                        return res;
                    }
                    else {
                        return res;
                    }
                }
                else {
                    universityCode = event.queryStringParameters.universityCode
                    searchKeyword = event.queryStringParameters.searchKeyword
                }
                let sqlFaq = 'Select * from UniFAQ where UniCode=?'
                let data = await getOrder(sqlFaq, [universityCode]);

                let outputArray = [];
                console.log('searchKeyword.toLowerCase()', searchKeyword.toLowerCase())
                data.forEach((item) => {
                    let obj = {};
                    obj.idFaq = item.idUniFAQ;
                    obj.universityCode = item.UniCode;
                    obj.category = item.Category;
                    obj.question = item.Question;
                    obj.answer = item.Answer;
                    outputArray.push(obj);

                });


                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify(outputArray)
                };
                return res;
            }
            catch (e) {
                console.log("ERROR OCCURED");
                console.log("Internal Server Error", e);
                let res = {
                    "statusCode": 500,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ type: "Internal Server Error", error: JSON.stringify(e), category: "/helps/gethelp" })
                };
                return res;

            }
        }


    }
    else if (event.resource === "/helps/deletehelp") {
        if (!event.queryStringParameters) {
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,DELETE,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: " universityCode and searchKeyword needs to be passed in  params " })
            };
            return res;
        }
        else {
            try {
                let universityCode;
                let idFaq;
                if (!event.queryStringParameters.universityCode || !event.queryStringParameters.idFaq) {
                    universityCode = event.queryStringParameters.universityCode || null; /* University Code works same as UniCode in Table  */
                    let res = {
                        "statusCode": 422,
                        "headers": {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        "body": JSON.stringify({ error: " universityCode and idFaq needs to be passed in  params " })
                    };
                    return res;
                }
                else {
                    universityCode = event.queryStringParameters.universityCode;
                    idFaq = event.queryStringParameters.idFaq;
                }

                let sqlDeleteFaq = 'Delete from UniFAQ where UniCode=? and idUniFAQ=?'
                let data = await getOrder(sqlDeleteFaq, [universityCode, idFaq]);

                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,DELETE,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ data: "deleted successfully " })
                };
                return res;
            }
            catch (e) {
                console.log("ERROR OCCURED");
                console.log("Internal Server Error", e);
                let res = {
                    "statusCode": 500,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,DELETE,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ type: "Internal Server Error", error: JSON.stringify(e), category: "/helps/deletehelp" })
                };
                return res;

            }
        }
    }
    else if (event.resource === "/helps/updatehelp") {
        let inputBody = JSON.parse(event.body);
        if (inputBody.hasOwnProperty("universityCode") &&
            inputBody.hasOwnProperty("idFaq") &&
            inputBody.hasOwnProperty("question") &&
            inputBody.hasOwnProperty("answer")
        ) {
            let universityCode = inputBody["universityCode"];
            let idFaq = inputBody["idFaq"];
            let question = inputBody["question"];
            let answer = inputBody["answer"];

            let updateFAQObject = {};
            updateFAQObject["Answer"] = answer
            updateFAQObject["Question"] = question;
            let sqlUpdateHelp = 'update UniFAQ set ? where  UniCode=? and idUniFAQ=?'
            let data = await getOrder(sqlUpdateHelp, [updateFAQObject, universityCode, idFaq]);

            let res = {
                "statusCode": 200,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ data: "Task Updated" })
            };

            return res;
        }
        else {
            let obj = {
                error: "All keys not found to update Help not available",
            };
            let res = {
                "statusCode": 500,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ type: "Internal Server Error", error: obj.error, category: "/tasks/deletetask" })
            };
            return res;
        }


    }
    else if (event.resource === "/helps/createhelp") {

        let inputBody = JSON.parse(event.body);
        if (inputBody.hasOwnProperty("universityCode") &&
            inputBody.hasOwnProperty("category") &&
            inputBody.hasOwnProperty("question") &&
            inputBody.hasOwnProperty("answer")
        ) {
            let universityCode = inputBody["universityCode"];
            let category = inputBody["category"];
            let question = inputBody["question"];
            let answer = inputBody["answer"];
            let createFaqObject = {};
            createFaqObject["UniCode"] = universityCode;
            createFaqObject["Category"] = category;
            createFaqObject["Answer"] = answer;
            createFaqObject["Question"] = question;

            let sqlFAQInsert = 'Insert UniFAQ SET ? ';
            await getOrder(sqlFAQInsert, [createFaqObject]);
            let data = await getOrder("select max(idUniFAQ) as Value from UniFAQ where UniCode=?", [universityCode]);

            let res = {
                "statusCode": 200,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ data: data[0]["Value"] })
            };

            return res;
        }
        else {
            let obj = {
                error: "All keys not found to update Help not available",
            };
            let res = {
                "statusCode": 500,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ type: "Internal Server Error", error: obj.error, category: "/helps/createhelp" })
            };
            return res;
        }
        // let tableName = "UniFAQ"
        // let inputBody = JSON.parse(event.body);
        // console.log("inputBody", inputBody);

        // try {
        //     if (inputBody.hasOwnProperty("UniCode") && inputBody["UniCode"] != "") {
        //         let faqObject = {}
        //         for (let [key, value] of Object.entries(inputBody)) {
        //             if (key == "UniCode") {
        //                 faqObject["UniCode"] = value;
        //             }
        //             else if (key == "Category") {
        //                 faqObject["Category"] = value;
        //             }
        //             else if (key == "Answer") {
        //                 faqObject["Answer"] = value;
        //             }
        //             else if (key == "Question") {
        //                 faqObject["Question"] = value;
        //             }
        //         }


        //         //faqObject["LastUpdateEpochTime"] = Math.floor(Date.now() / 1000);
        //         //faqObject["ChangeId"] = 1;
        //         let sqlFAQInsert = 'Insert ' + tableName + ' SET ? ';
        //         await getOrder(sqlFAQInsert, [faqObject]);
        //         let data = await getOrder("select max(idUniFAQ) as Value from UniFAQ where UniCode=?", [faqObject["UniCode"]]);
        //         let res = {
        //             statusCode: 200,
        //             headers: {
        //                 "Content-Type": "application/json",
        //                 "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
        //                 "Access-Control-Allow-Methods": "GET,OPTIONS",
        //                 "Access-Control-Allow-Origin": "*",
        //             },
        //             body: JSON.stringify({ data: data[0]["Value"] }),
        //         };
        //         return res;
        //     }
        //     else {
        //         let res = {
        //             statusCode: 400,
        //             headers: {
        //                 "Content-Type": "application/json",
        //                 "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
        //                 "Access-Control-Allow-Methods": "GET,OPTIONS",
        //                 "Access-Control-Allow-Origin": "*",
        //             },
        //             body: JSON.stringify({ error: "universityCode or userEmail in incorrect format" }),
        //         };
        //         return res;
        //     }
        // }
        // catch (e) {
        //     let res = {
        //         statusCode: 400,
        //         headers: {
        //             "Content-Type": "application/json",
        //             "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
        //             "Access-Control-Allow-Methods": "GET,OPTIONS",
        //             "Access-Control-Allow-Origin": "*",
        //         },
        //         body: JSON.stringify({ error: JSON.stringify(e) }),
        //     };
        //     return res;
        // }

    }

    else if (event.resource === "/admissions/userinfo") {
        console.log('Received event:', JSON.stringify(event, null, 2));
        console.log('Query String email:', event.queryStringParameters.email);
        console.log('Query String code:', event.queryStringParameters.code);
        let email = event.queryStringParameters.email
        let code = event.queryStringParameters.code
        //let email ="vishalg485@gmail.com"// event.queryStringParameters.email
        //let code = "TR002"  //event.queryStringParameters.code

        var sqlCheck = 'SELECT * FROM UserInfo where UserEmail = ?  and UniCode = ? and  ChangeId != ?';
        let studentCheckData = await getOrder(sqlCheck, [email, code, 3]);
        if (studentCheckData.length == 0) {
            let statusCode = 400;
            let res = {
                "statusCode": statusCode,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: " User does not exist " })
            };

            return res;
        }
        let sql4 = `SELECT AI.AdmissionYear,UserInfo.*,UI.* FROM UserInfo 
                JOIN UniversityInfo as UI
                JOIN ClassStudent as CS
                JOIN Class AS C
                JOIN AdmissionInfo as AI
                where UserInfo.UserEmail = ?
                and UserInfo.UniCode = ?
                and UserInfo.ChangeId != ? and UserInfo.UniCode = UI.Uni_Code and CS.StudentCode = UserInfo.UserCode
                and C.idClass = CS.ClassId and AI.AdmCode = C.AdmCode`;
        console.log(sql4);
        //console.log("query", await getOrder(sql4, ""));
        let codeAwait = await getOrder(sql4, [email, code, 3]);
        console.log("codeAwait", codeAwait);
        var bodyData = {};
        if (codeAwait.length !== 0) {
            bodyData = codeAwait[0];
        }
        console.log("bodyData", JSON.stringify(bodyData))


        let statusCode = 200;
        if (Object.keys(bodyData) == 0) {
            statusCode = 401;
        }

        var res = {
            "statusCode": statusCode,
            "headers": {
                "Content-Type": "application/json",
                "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                "Access-Control-Allow-Methods": "GET,OPTIONS",
                "Access-Control-Allow-Origin": "*",
            },
            "body": JSON.stringify(bodyData)
        };

        return res;

    }
    else if (event.resource === "/admissions/saveuserprofile") {
        console.log("event", event);

        let inputBody = JSON.parse(event.body);
        if (inputBody.hasOwnProperty("UserEmail") && inputBody.hasOwnProperty("UniCode") && inputBody["UserEmail"] != "" && inputBody["UniCode"] != "") {
            try {
                let obj = {};
                var sqlCheck = 'SELECT * FROM UserInfo where UserEmail = ?  and UniCode = ? and  ChangeId != ?';
                let studentCheckData = await getOrder(sqlCheck, [inputBody["UserEmail"], inputBody["UniCode"], 3]);
                if (studentCheckData.length == 0) {
                    let statusCode = 400;
                    let res = {
                        "statusCode": statusCode,
                        "headers": {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        "body": JSON.stringify({ error: " User does not exist " })
                    };

                    return res;
                }
                Object.keys(inputBody).forEach(item => {
                    if (!(item == "UserCode" || item == "UserRole" || item == "idUserInfo" || item == "UserRollNo" || item == "UserRegistrationNo" || item == "ChangeId" || item == "UserEmail" || item == "UniCode" || inputBody[item] == "")) {
                        obj[item] = inputBody[item];
                    }
                })

                var sqlStudentUpdate = 'UPDATE UserInfo SET ? WHERE UserEmail = ? and UniCode = ? and ChangeId!=3';
                console.log("Math.floor(Date.now() / 1000);", Math.floor(Date.now() / 1000));
                obj["LastUpdateEpochTime"] = Math.floor(Date.now() / 1000);
                obj["ChangeId"] = 2
                const updatedStudent = await getOrder(sqlStudentUpdate, [obj, inputBody["UserEmail"], inputBody["UniCode"]])
                let data = { response: "dataupdated" }
                res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify(data)
                };
                return res;
            }
            catch (e) {
                console.log("updated student", e);
                let obj = { error: e.sqlMessage };
                res = {
                    "statusCode": 401,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify(obj)
                };

                return res;
            }


        }
        else {
            let obj = { error: "All keys not found to query the db(UserEmail or UniCode)" };
            res = {
                "statusCode": 400,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify(obj)
            };
            return res;
        }

    }

    else if (event.resource === "/admissions/forgotcodes") {
        if (!event.queryStringParameters) {
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: " userEmail needs to be passed in  params 1" })
            };
            return res;
        }
        else {
            try {
                let userEmail = "";
                if (!event.queryStringParameters.userEmail) {
                    userEmail = event.queryStringParameters.userEmail || null; /* University Code works same as UniCode in Table  */
                    let res = {
                        "statusCode": 422,
                        "headers": {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        "body": JSON.stringify({ error: " userEmail and needs to be passed in  params 2" })
                    };
                    return res;
                }
                else {
                    userEmail = event.queryStringParameters.userEmail;

                }
                let sql = 'SELECT UniversityInfo.Uni_Name,UserInfo.UniCode,UserInfo.UserRole FROM UserInfo INNER JOIN UniversityInfo where UserInfo.UserEmail=? and ChangeId!=? and UserInfo.UniCode=UniversityInfo.Uni_Code';
                var data = await getOrder(sql, [userEmail, 3]);
                const userData = [];
                data.forEach((item) => {
                    let obj = {};
                    obj.universityName = item.Uni_Name;
                    obj.universityCode = item.UniCode;
                    obj.userRole = item.UserRole;
                    userData.push(obj);
                })
                console.log("data of user registered more than once", data)
                res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify(userData)
                };
                return res;

            }
            catch (e) {
                console.log("ERROR OCCURED");
                console.log("Internal Server Error", e);
                let res = {
                    "statusCode": 500,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ type: "Internal Server Error", error: JSON.stringify(e), category: "/tasks/gettasks" })
                };
                return res;
            }
        }
    }
    if (event.resource === "/tasks/gettasks") {
        if (!event.queryStringParameters) {
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: " universityCode needs to be passed in  params " })
            };
            return res;
        }
        else {
            try {


                let universityCode, email, lastUpdatedEpochTime;
                if (!event.queryStringParameters.universityCode || !event.queryStringParameters.email) {
                    universityCode = event.queryStringParameters.universityCode || null; /* University Code works same as UniCode in Table  */
                    let res = {
                        "statusCode": 422,
                        "headers": {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        "body": JSON.stringify({ error: " universityCode needs to be passed in  params " })
                    };
                    /* the response is same because uniCode needs to be a part of queryStringParameters */
                    if (!universityCode || universityCode == null || universityCode == undefined) {
                        return res;
                    }
                    else {
                        return res;
                    }
                }
                else {
                    universityCode = event.queryStringParameters.universityCode;
                    email = event.queryStringParameters.email;
                }
                if (!event.queryStringParameters.lastUpdatedEpochTime) {
                    lastUpdatedEpochTime = 0;
                }

                try {
                    lastUpdatedEpochTime = parseInt(event.queryStringParameters.lastUpdatedEpochTime)
                    if (isNaN(lastUpdatedEpochTime)) {
                        lastUpdatedEpochTime = 0;
                    }
                    console.log("lastUpdatedEpochTime111", lastUpdatedEpochTime);
                }
                catch (e) {
                    lastUpdatedEpochTime = 0;
                }
                var identitySql = 'SELECT * FROM UserInfo where UserEmail = ?  and UniCode = ? and  ChangeId != ?';
                let identityData = await getOrder(identitySql, [email, universityCode, 3]);
                if (identityData.length == 0) {
                    let statusCode = 400;
                    let res = {
                        "statusCode": statusCode,
                        "headers": {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        "body": JSON.stringify({ error: " User does not exist " })
                    };

                    return res;
                }
                let userCode = identityData[0].UserCode;
                let taskDetailsSql = `Select * from UserTask where UserCode=? and UniCode=? and ChangeId!=? and LastUpdatedEpochTime>=?`;
                let taskDetailsData = await getOrder(taskDetailsSql, [userCode, universityCode, 3, lastUpdatedEpochTime]);
                taskDetailsData.forEach((item, count) => {
                    if (taskDetailsData[count]["TaskActive"] == 0) {
                        taskDetailsData[count]["TaskActive"] = false;
                    }
                    else {
                        taskDetailsData[count]["TaskActive"] = true;
                    }

                    if (taskDetailsData[count]["Reminder"] == 0) {
                        taskDetailsData[count]["Reminder"] = false;
                    }
                    else {
                        taskDetailsData[count]["Reminder"] = true;
                    }

                })

                const taskDetailsDataFiltered = [];
                taskDetailsData.forEach(task => {
                    let obj = {};

                    obj.idTask = task.TaskId;
                    obj.userCode = task.UserCode;
                    obj.universityCode = task.UniCode;
                    obj.date = task.Date;
                    obj.startTime = task.StartTime;
                    obj.endTime = task.EndTime;
                    obj.taskTitle = task.TaskTitle;
                    obj.taskDescription = task.TaskDescription;
                    obj.category = task.Category;
                    obj.reminder = task.Reminder;
                    obj.professorCode = task.ProfCode;
                    obj.professorName = task.ProfName;
                    obj.idProfessor = task.ProfTaskId;
                    obj.minutesBefore = task.MinutesBefore;
                    obj.taskActive = task.TaskActive;
                    obj.idChange = task.ChangeId;
                    obj.idClass = task.ClassId;

                    taskDetailsDataFiltered.push(obj);
                })

                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify(taskDetailsDataFiltered)
                };
                return res;
            }
            catch (e) {
                console.log("ERROR OCCURED");
                console.log("Internal Server Error", e);
                let res = {
                    "statusCode": 500,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ type: "Internal Server Error", error: JSON.stringify(e), category: "/tasks/gettasks" })
                };
                return res;
            }
        }
    }
    if (event.resource === "/tasks/updatetasks") {
        let inputBody = JSON.parse(event.body);
        if (inputBody.hasOwnProperty("userEmail") &&
            inputBody.hasOwnProperty("universityCode") &&
            inputBody.hasOwnProperty("category") &&
            inputBody.hasOwnProperty("idTask") &&
            inputBody.hasOwnProperty("taskTitle") &&
            inputBody.hasOwnProperty("taskDescription") &&
            inputBody.hasOwnProperty("date") &&
            inputBody.hasOwnProperty("startTime") &&
            inputBody.hasOwnProperty("minutesBefore") &&
            inputBody.hasOwnProperty("taskActive") &&
            inputBody.hasOwnProperty("reminder")
        ) {
            let email = inputBody["userEmail"];
            let universityCode = inputBody["universityCode"];
            let category = inputBody["category"];
            let taskTitle = inputBody["taskTitle"];
            let taskDescription = inputBody["taskDescription"];
            let date = inputBody["date"];
            let startTime = inputBody["startTime"];
            let minutesBefore = inputBody["minutesBefore"];
            let taskActive = inputBody["taskActive"];
            let reminder = inputBody["reminder"];
            let idTask = inputBody["idTask"];
            // this is not a mandatory parameter if this is present its a professor updating task for a class of students
            let idClass = inputBody["idClass"]
            console.log("idClass", idClass);
            var identitySql = 'SELECT * FROM UserInfo where UserEmail = ?  and UniCode = ? and  ChangeId != ?';
            let identityData = await getOrder(identitySql, [email, universityCode, 3]);

            if (identityData.length == 0) {
                let statusCode = 400;
                let res = {
                    "statusCode": statusCode,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ error: " User does not exist " })
                };

                return res;
            }

            let userCode = identityData[0].UserCode;
            let userRole = identityData[0].UserRole;
            let userName = identityData[0].UserName;

            if (userRole == "Professor" && idClass != 0) {
                //id task will be task id of the professor
                let taskCheck = 'SELECT * FROM UserTask where UserCode = ?  and UniCode = ? and  ChangeId != ? and TaskId= ?';
                let taskCheckData = await getOrder(taskCheck, [userCode, universityCode, 3, idTask]);
                //checking whether task exist in database
                if (taskCheckData.length == 0) {
                    let statusCode = 400;
                    let res = {
                        "statusCode": statusCode,
                        "headers": {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        "body": JSON.stringify({ error: " Task does not exist " })
                    };
                    return res;
                }
                let taskUpdateObject = {};

                //taskUpdateObject["UserCode"] = userCode
                //taskUpdateObject["UniCode"] = universityCode
                taskUpdateObject["Reminder"] = reminder;
                taskUpdateObject["TaskActive"] = taskActive
                taskUpdateObject["Date"] = date
                taskUpdateObject["StartTime"] = startTime
                taskUpdateObject["MinutesBefore"] = minutesBefore
                taskUpdateObject["Category"] = category;
                taskUpdateObject["TaskTitle"] = taskTitle;
                taskUpdateObject["TaskDescription"] = taskDescription;
                taskUpdateObject["ChangeId"] = 2;
                taskUpdateObject["LastUpdatedEpochTime"] = Math.floor(Date.now() / 1000);

                // That particular professor getting updated
                let updateTask = `update UserTask Set ? where UniCode=? and UserCode=? and TaskId=? `

                let updateTaskData = await getOrder(updateTask, [taskUpdateObject, universityCode, userCode, idTask]);

                // all the students of the professor updated
                let updateTaskStudent = `update UserTask Set ? where UniCode=? and ProfCode=? and ProfTaskId=? and ClassId=?`;

                let updateTaskDataStudent = await getOrder(updateTaskStudent, [taskUpdateObject, universityCode, userCode, idTask, idClass]);

                try {
                    let classTopic = `/topics/ClassTopic_${universityCode}_${idClass}`;
                    let data = JSON.stringify({
                        to: classTopic,
                        priority: "high",
                        "content-available": true,
                        data: {
                            type: "UpdateTask",
                            title: "Task Updated",
                            body: "Task created by Professor updated",
                        }
                    });
                    let config = {
                        method: "post",
                        content_available: 1,
                        aps: JSON.stringify({
                            "content-available": 1,
                            "mutable-content": 1,
                            alert: {
                                type: "UpdateTask",
                                title: "Task Deleted",
                                body: "Task created by Professor updated",
                            },
                            badge: 1,
                            sound: "default",
                        }),
                        url: "https://fcm.googleapis.com/fcm/send",
                        headers: {
                            Authorization: "key=AAAA6amStzE:APA91bGJEM79aWSNM7BYs81n8PLAoQR_TylQO1d1JqYI0EB8o3UJSviMsKL2tjrxniOGxfzYEsENUUIiT7_PcJu_mhyNkeiiysllXzY7MnZcACqU9CWUE3cv4zHsxtPmG-VSydg9Z_GW",
                            "Content-Type": "application/json",
                        },
                        data: data,
                    };
                    await axios(config);
                    let res = {
                        "statusCode": 200,
                        "headers": {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        "body": JSON.stringify({ data: "all student whose proftaskid is given idTask are updated" })
                    }

                    return res;
                }
                catch (e) {

                }


                // let res = {
                //     "statusCode": 201,
                //     "headers": {
                //         "Content-Type": "application/json",
                //         "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                //         "Access-Control-Allow-Methods": "GET,OPTIONS",
                //         "Access-Control-Allow-Origin": "*",
                //     },
                //     "body": JSON.stringify({ taskId: "all student whose proftaskid is given idTask are updated" })
                // };
                // return res;
            }
            if (userRole == "Professor") {
                const userTaskUpdateObject = {};
                userTaskUpdateObject["UserCode"] = userCode
                userTaskUpdateObject["UniCode"] = universityCode
                userTaskUpdateObject["Reminder"] = reminder;
                userTaskUpdateObject["TaskActive"] = taskActive
                userTaskUpdateObject["Date"] = date
                userTaskUpdateObject["StartTime"] = startTime
                userTaskUpdateObject["MinutesBefore"] = minutesBefore
                userTaskUpdateObject["Category"] = category;
                userTaskUpdateObject["TaskTitle"] = taskTitle;
                userTaskUpdateObject["TaskDescription"] = taskDescription;


                userTaskUpdateObject["ChangeId"] = 2;
                userTaskUpdateObject["LastUpdatedEpochTime"] = Math.floor(Date.now() / 1000);
                let sqlInsert = `update UserTask Set ? where UniCode=? and UserCode=? and TaskId=? `
                let userTaskData = await getOrder(sqlInsert, [userTaskUpdateObject, universityCode, userCode, idTask]);

                let res = {
                    "statusCode": 201,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ data: "task updated" })
                };
                return res;

            }
            if (userRole == "Student") {
                const userTaskUpdateObject = {};
                userTaskUpdateObject["UserCode"] = userCode
                userTaskUpdateObject["UniCode"] = universityCode
                userTaskUpdateObject["Reminder"] = reminder;
                userTaskUpdateObject["TaskActive"] = taskActive
                userTaskUpdateObject["Date"] = date
                userTaskUpdateObject["StartTime"] = startTime
                userTaskUpdateObject["MinutesBefore"] = minutesBefore
                userTaskUpdateObject["Category"] = category;
                userTaskUpdateObject["TaskTitle"] = taskTitle;
                userTaskUpdateObject["TaskDescription"] = taskDescription;


                userTaskUpdateObject["ChangeId"] = 2;
                userTaskUpdateObject["LastUpdatedEpochTime"] = Math.floor(Date.now() / 1000);
                let sqlInsert = `update UserTask Set ? where UniCode=? and UserCode=? and TaskId=? `
                let userTaskData = await getOrder(sqlInsert, [userTaskUpdateObject, universityCode, userCode, idTask]);

                let res = {
                    "statusCode": 201,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ data: "task updated" })
                };
                return res;
            }



        }
        else {
            let obj = {
                error: "All keys not found to query the db(UserEmail or UniCode)",
            };
            let res = {
                statusCode: 400,
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                body: JSON.stringify({ error: obj.error }),
            };
            return res;
        }
    }
    if (event.resource === "/tasks/deletetask") {
        if (!event.queryStringParameters) {
            let res = {
                "statusCode": 422,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                "body": JSON.stringify({ error: " universityCode needs to be passed in  params " })
            };
            return res;
        }
        else {
            try {
                let universityCode, email, idTask, idClass;
                if (!event.queryStringParameters.universityCode || !event.queryStringParameters.email || !event.queryStringParameters.idTask) {
                    universityCode = event.queryStringParameters.universityCode || null; /* University Code works same as UniCode in Table  */
                    let res = {
                        "statusCode": 422,
                        "headers": {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        "body": JSON.stringify({ error: " universityCode and email and idTask needs to be passed in  params " })
                    };
                    return res;
                }
                else {
                    universityCode = event.queryStringParameters.universityCode;
                    email = event.queryStringParameters.email;
                    idTask = event.queryStringParameters.idTask;
                }
                try {
                    if (parseInt(event.queryStringParameters.idClass)) {
                        idClass = event.queryStringParameters.idClass;
                    }
                }
                catch (e) {}
                var identitySql = 'SELECT * FROM UserInfo where UserEmail = ?  and UniCode = ? and  ChangeId != ?';
                let identityData = await getOrder(identitySql, [email, universityCode, 3]);
                if (identityData.length == 0) {
                    let statusCode = 400;
                    let res = {
                        "statusCode": statusCode,
                        "headers": {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        "body": JSON.stringify({ error: " User does not exist " })
                    };

                    return res;
                }

                let userCode = identityData[0].UserCode;
                let userRole = identityData[0].UserRole;

                if (userRole == "Professor" && idClass != 0) {
                    let taskCheck = 'SELECT * FROM UserTask where UserCode = ?  and UniCode = ? and  ChangeId != ? and TaskId= ?';
                    let taskCheckData = await getOrder(taskCheck, [userCode, universityCode, 3, idTask]);
                    //checking whether task exist in database
                    if (taskCheckData.length == 0) {
                        let statusCode = 400;
                        let res = {
                            "statusCode": statusCode,
                            "headers": {
                                "Content-Type": "application/json",
                                "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                                "Access-Control-Allow-Methods": "GET,OPTIONS",
                                "Access-Control-Allow-Origin": "*",
                            },
                            "body": JSON.stringify({ error: " Task does not exist " })
                        };
                        return res;
                    }
                    let obj = {};
                    obj["ChangeId"] = 3;
                    obj["TaskActive"] = false;
                    obj["LastUpdatedEpochTime"] = Math.floor(Date.now() / 1000);

                    let deleteTask = `update UserTask Set ? where UniCode=? and UserCode=? and TaskId=? `;

                    let deleteTaskData = await getOrder(deleteTask, [obj, universityCode, userCode, idTask]);

                    /*ProfCode  ProfName ProfTaskId*/
                    let deleteTaskStudent = `update UserTask Set ? where UniCode=? and ProfCode=? and ProfTaskId=? and ClassId=?`
                    let deleteTaskDataStudent = await getOrder(deleteTaskStudent, [obj, universityCode, userCode, idTask, idClass]);

                    try {
                        let classTopic = `/topics/ClassTopic_${universityCode}_${idClass}`;
                        let data = JSON.stringify({
                            to: classTopic,
                            priority: "high",
                            "content-available": true,
                            data: {
                                type: "Delete Task",
                                title: "Task Deleted",
                                body: "Task created by Professor Deleted",
                            }
                        });
                        let config = {
                            method: "post",
                            content_available: 1,
                            aps: JSON.stringify({
                                "content-available": 1,
                                "mutable-content": 1,
                                alert: {
                                    type: "Delete Task",
                                    title: "Task Deleted",
                                    body: "Task created by Professor Deleted",
                                },
                                badge: 1,
                                sound: "default",
                            }),
                            url: "https://fcm.googleapis.com/fcm/send",
                            headers: {
                                Authorization: "key=AAAA6amStzE:APA91bGJEM79aWSNM7BYs81n8PLAoQR_TylQO1d1JqYI0EB8o3UJSviMsKL2tjrxniOGxfzYEsENUUIiT7_PcJu_mhyNkeiiysllXzY7MnZcACqU9CWUE3cv4zHsxtPmG-VSydg9Z_GW",
                                "Content-Type": "application/json",
                            },
                            data: data,
                        };
                        await axios(config);
                    }
                    catch (e) {

                    }

                    console.log("deleteTaskData", deleteTaskData);
                    let res = {
                        "statusCode": 201,
                        "headers": {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        "body": JSON.stringify({ taskId: idTask })
                    };
                    return res;
                }
                else if (userRole == "Professor") {
                    let taskCheck = 'SELECT * FROM UserTask where UserCode = ?  and UniCode = ? and  ChangeId != ? and TaskId= ?';
                    let taskCheckData = await getOrder(taskCheck, [userCode, universityCode, 3, idTask]);
                    //checking whether task exist in database
                    if (taskCheckData.length == 0) {
                        let statusCode = 400;
                        let res = {
                            "statusCode": statusCode,
                            "headers": {
                                "Content-Type": "application/json",
                                "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                                "Access-Control-Allow-Methods": "GET,OPTIONS",
                                "Access-Control-Allow-Origin": "*",
                            },
                            "body": JSON.stringify({ error: " Task does not exist " })
                        };
                        return res;
                    }
                    let classId = taskCheckData[0]["ClassId"];
                    console.log("classId");
                    let obj = {};
                    obj["ChangeId"] = 3;
                    obj["LastUpdatedEpochTime"] = Math.floor(Date.now() / 1000);

                    let deleteTask = `update UserTask Set ? where UniCode=? and UserCode=? and TaskId=? `
                    let deleteTaskData = await getOrder(deleteTask, [obj, universityCode, userCode, idTask]);
                    let res = {
                        "statusCode": 201,
                        "headers": {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        "body": JSON.stringify({ taskId: idTask })
                    };
                    return res;
                }
                else if (userRole == "Student") {
                    let taskCheck = 'SELECT * FROM UserTask where UserCode = ?  and UniCode = ? and  ChangeId != ? and TaskId= ?';
                    let taskCheckData = await getOrder(taskCheck, [userCode, universityCode, 3, idTask]);
                    //checking whether task exist in database
                    if (taskCheckData.length == 0) {
                        let statusCode = 400;
                        let res = {
                            "statusCode": statusCode,
                            "headers": {
                                "Content-Type": "application/json",
                                "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                                "Access-Control-Allow-Methods": "GET,OPTIONS",
                                "Access-Control-Allow-Origin": "*",
                            },
                            "body": JSON.stringify({ error: " Task does not exist " })
                        };
                        return res;
                    }

                    let obj = {};
                    obj["ChangeId"] = 3;
                    obj["LastUpdatedEpochTime"] = Math.floor(Date.now() / 1000);

                    let deleteTask = `update UserTask Set ? where UniCode=? and UserCode=? and TaskId=? `
                    let deleteTaskData = await getOrder(deleteTask, [obj, universityCode, userCode, idTask]);
                    console.log("deleteTaskData", deleteTaskData);
                    let res = {
                        "statusCode": 201,
                        "headers": {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                            "Access-Control-Allow-Methods": "GET,OPTIONS",
                            "Access-Control-Allow-Origin": "*",
                        },
                        "body": JSON.stringify({ taskId: idTask })
                    };
                    return res;
                }
            }
            catch (e) {
                console.log("ERROR OCCURED");
                console.log("Internal Server Error", e);
                let res = {
                    "statusCode": 500,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ type: "Internal Server Error", error: JSON.stringify(e), category: "/tasks/deletetask" })
                };
                return res;
            }
        }
    }
    if (event.resource === "/tasks/createtask") {
        let inputBody = JSON.parse(event.body);
        if (inputBody.hasOwnProperty("userEmail") &&
            inputBody.hasOwnProperty("universityCode") &&
            inputBody.hasOwnProperty("category") &&
            inputBody.hasOwnProperty("taskTitle") &&
            inputBody.hasOwnProperty("taskDescription") &&
            inputBody.hasOwnProperty("date") &&
            inputBody.hasOwnProperty("startTime") &&
            inputBody.hasOwnProperty("minutesBefore") &&
            inputBody.hasOwnProperty("taskActive") &&
            inputBody.hasOwnProperty("reminder")
        ) {
            let email = inputBody["userEmail"];
            let universityCode = inputBody["universityCode"];
            let category = inputBody["category"];
            let taskTitle = inputBody["taskTitle"];
            let taskDescription = inputBody["taskDescription"];
            let date = inputBody["date"];
            let startTime = inputBody["startTime"];
            let minutesBefore = inputBody["minutesBefore"];
            let taskActive = inputBody["taskActive"];
            let reminder = inputBody["reminder"];

            // this is not a mandatory parameter if this is present its a professor creating task for a class of students

            let idClass = inputBody["idClass"]

            var identitySql = 'SELECT * FROM UserInfo where UserEmail = ?  and UniCode = ? and  ChangeId != ?';
            let identityData = await getOrder(identitySql, [email, universityCode, 3]);
            if (identityData.length == 0) {
                let statusCode = 400;
                let res = {
                    "statusCode": statusCode,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ error: " User does not exist " })
                };

                return res;
            }


            var taskIdCountValuesSql = `Select * from CountValues where UniCode=?`;
            let taskIdData = await getOrder(taskIdCountValuesSql, [universityCode]);
            /* we are quering the count values table to get the value for the taskId */
            let taskIdValue = parseInt(taskIdData[0]['TaskId']);

            let userCode = identityData[0].UserCode;
            let userRole = identityData[0].UserRole;
            let userName = identityData[0].UserName;

            if (userRole == "Professor" && idClass) {
                const userTaskCreatingObject = {};
                userTaskCreatingObject["UserCode"] = userCode
                userTaskCreatingObject["UniCode"] = universityCode
                userTaskCreatingObject["Reminder"] = reminder;
                userTaskCreatingObject["TaskActive"] = taskActive
                userTaskCreatingObject["Date"] = date
                userTaskCreatingObject["StartTime"] = startTime
                userTaskCreatingObject["MinutesBefore"] = minutesBefore
                userTaskCreatingObject["Category"] = category;
                userTaskCreatingObject["TaskTitle"] = taskTitle;
                userTaskCreatingObject["TaskDescription"] = taskDescription;
                userTaskCreatingObject["ChangeId"] = 1;
                userTaskCreatingObject["LastUpdatedEpochTime"] = Math.floor(Date.now() / 1000);
                taskIdValue = taskIdValue + 1;
                userTaskCreatingObject["TaskId"] = taskIdValue;
                userTaskCreatingObject["ProfCode"] = userCode;
                let professorTaskId = taskIdValue;
                let sqlInsert = `Insert into UserTask Set  ?`
                let userTaskData = await getOrder(sqlInsert, [userTaskCreatingObject]);
                /*
                 we are inserting the task for the professor
                */

                let classId = idClass;

                let getStudentsSql = `SELECT UserInfo.UserCode,UserInfo.UserDOB,UserInfo.UserPinCode,UserInfo.UserEmail,UserInfo.UserName,UserInfo.UserPhone,UserInfo.UserRollNo,UserInfo.UserParentName,UserInfo.UserParentContactNo,UserInfo.UserAddress,UserInfo.UserPinCode,UserInfo.BloodGroup,            UserInfo.UserRegistrationNo, ClassStudent.ClassId, Class.SubYear, Class.Semester, Class.Division FROM UserInfo INNER JOIN ClassStudent
            INNER JOIN Class
            where ClassStudent.StudentCode = UserInfo.UserCode
            and ClassStudent.UniCode = UserInfo.UniCode
            and UserInfo.UniCode = ?
            and Class.idClass = ClassStudent.ClassId
            and UserInfo.UserRole = "Student"
            and UserInfo.ChangeId != 3 
            and Class.idClass = ?`;
                /* finding student of a particular class Id */
                let studentsData = await getOrder(getStudentsSql, [universityCode, classId]);
                let classTopic = `/topics/ClassTopic_${universityCode}_${classId}`;
                let profCodeId = taskIdValue;
                for (let i = 0; i < studentsData.length; i++) {
                    console.log("studentsData", studentsData)
                    /* we are inserting the task for the students */
                    let userCodeStudent = studentsData[i]["UserCode"]
                    userTaskCreatingObject["ProfCode"] = userCode; // its the professor Code
                    userTaskCreatingObject["UserCode"] = userCodeStudent;
                    userTaskCreatingObject["ProfName"] = userName;
                    userTaskCreatingObject["ProfTaskId"] = profCodeId;
                    userTaskCreatingObject["ClassId"] = classId;

                    userTaskCreatingObject["LastUpdatedEpochTime"] = Math.floor(Date.now() / 1000);
                    taskIdValue = taskIdValue + 1;
                    userTaskCreatingObject["TaskId"] = taskIdValue;

                    let sqlInsert = `Insert into UserTask Set  ?`
                    let userTaskData = await getOrder(sqlInsert, [userTaskCreatingObject]);
                    try {
                        let data = JSON.stringify({
                            to: classTopic,
                            priority: "high",
                            "content-available": true,
                            data: {
                                type: "AddTask",
                                title: 'Task_Notification',
                                body: "New Task Created"
                            }
                        });
                        let config = {
                            method: "post",
                            content_available: 1,
                            aps: JSON.stringify({
                                "content-available": 1,
                                "mutable-content": 1,
                                alert: {
                                    type: "AddTask",
                                    title: 'Task_Notification',
                                    body: "New Task Created"
                                },
                                badge: 1,
                                sound: "default",
                            }),
                            url: "https://fcm.googleapis.com/fcm/send",
                            headers: {
                                Authorization: "key=AAAA6amStzE:APA91bGJEM79aWSNM7BYs81n8PLAoQR_TylQO1d1JqYI0EB8o3UJSviMsKL2tjrxniOGxfzYEsENUUIiT7_PcJu_mhyNkeiiysllXzY7MnZcACqU9CWUE3cv4zHsxtPmG-VSydg9Z_GW",
                                "Content-Type": "application/json",
                            },
                            data: data,
                        };
                        console.log("config", config);
                        await axios(config);
                    }
                    catch (e) {
                        console.log("error sending to firebase professor old", e);
                    }
                };

                let countValuesUpdate = `Update CountValues  set TaskId=? where UniCode=?`;
                taskIdValue = taskIdValue + 1;
                taskIdValue = parseInt(taskIdValue)
                console.log('taskIdValue', taskIdValue);
                //we are upating the countValues with the new taskId
                let taskIdDataUpdate = await getOrder(countValuesUpdate, [taskIdValue, inputBody["universityCode"]]);
                let res = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ TaskId: professorTaskId })
                };

                return res;
            }
            else if (userRole == "Professor") {
                const userTaskCreatingObject = {};
                userTaskCreatingObject["UserCode"] = userCode
                userTaskCreatingObject["UniCode"] = universityCode
                userTaskCreatingObject["Reminder"] = reminder;
                userTaskCreatingObject["TaskActive"] = taskActive
                userTaskCreatingObject["Date"] = date
                userTaskCreatingObject["StartTime"] = startTime
                userTaskCreatingObject["MinutesBefore"] = minutesBefore
                userTaskCreatingObject["Category"] = category;
                userTaskCreatingObject["TaskTitle"] = taskTitle;
                userTaskCreatingObject["TaskDescription"] = taskDescription;


                userTaskCreatingObject["ChangeId"] = 1;
                userTaskCreatingObject["LastUpdatedEpochTime"] = Math.floor(Date.now() / 1000);
                taskIdValue = taskIdValue + 1;
                userTaskCreatingObject["TaskId"] = taskIdValue;

                let sqlInsert = `Insert into UserTask Set  ?`
                let userTaskData = await getOrder(sqlInsert, [userTaskCreatingObject]);
                let countValuesUpdate = `Update CountValues  set TaskId=? where UniCode=?`;
                //we are upating the countValues with the new taskId
                let taskIdDataUpdate = await getOrder(countValuesUpdate, [parseInt(taskIdValue) + 1, inputBody["universityCode"]]);
                let res = {
                    "statusCode": 201,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ TaskId: taskIdValue })
                };
                return res;

            }
            else if (userRole == "Student") {
                const userTaskCreatingObject = {};
                userTaskCreatingObject["UserCode"] = userCode
                userTaskCreatingObject["UniCode"] = universityCode
                userTaskCreatingObject["Reminder"] = reminder;
                userTaskCreatingObject["TaskActive"] = taskActive
                userTaskCreatingObject["Date"] = date
                userTaskCreatingObject["StartTime"] = startTime
                userTaskCreatingObject["MinutesBefore"] = minutesBefore
                userTaskCreatingObject["Category"] = category;
                userTaskCreatingObject["TaskTitle"] = taskTitle;
                userTaskCreatingObject["TaskDescription"] = taskDescription;


                userTaskCreatingObject["ChangeId"] = 1;
                userTaskCreatingObject["LastUpdatedEpochTime"] = Math.floor(Date.now() / 1000);
                taskIdValue = taskIdValue + 1;
                userTaskCreatingObject["TaskId"] = taskIdValue;

                let sqlInsert = `Insert into UserTask Set  ?`
                let userTaskData = await getOrder(sqlInsert, [userTaskCreatingObject]);
                let countValuesUpdate = `Update CountValues  set TaskId=? where UniCode=?`;
                taskIdValue = taskIdValue + 1;
                taskIdValue = parseInt(taskIdValue)
                console.log('taskIdValue', taskIdValue);
                //we are upating the countValues with the new taskId
                let taskIdDataUpdate = await getOrder(countValuesUpdate, [taskIdValue, inputBody["universityCode"]]);
                let res = {
                    "statusCode": 201,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    "body": JSON.stringify({ TaskId: taskIdValue })
                };
                return res;
            }



        }
        else {
            let obj = {
                error: "All keys not found to query the db(UserEmail or UniCode)",
            };
            let res = {
                statusCode: 400,
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                body: JSON.stringify({ error: obj.error }),
            };
            return res;
        }
    }

    if (event.resource === "/admissions/registeragain") {
        let inputBody = JSON.parse(event.body);
        if (inputBody.hasOwnProperty("college_name") &&
            inputBody.hasOwnProperty("country") &&
            inputBody.hasOwnProperty("pincode") &&
            inputBody.hasOwnProperty("email") &&
            inputBody.hasOwnProperty("name") &&
            inputBody.hasOwnProperty("phone") &&     
            inputBody.hasOwnProperty("address") &&
            inputBody.hasOwnProperty("state") &&
            inputBody.hasOwnProperty("designation") 
        ) {
            let UniveristyName = inputBody["college_name"];
            let UniveristyCountry = inputBody["country"];
            let UniveristyPostalCode = inputBody["pincode"];
            let UserEmail = inputBody["email"];
            let UserName = inputBody["name"];
            let UserPhone = "+"+inputBody["phone"];
            let Address = inputBody["address"];
            let State = inputBody["state"];
            let Designation = inputBody["designation"];
            
            console.log("2",UniveristyName,UniveristyCountry,UniveristyPostalCode,UserEmail,UserName,UserPhone,Address,State,Designation)
/*
address: "a12345"
​college_name: "abcd2  "
​confirmPassword: "Vishal@1"
​country: "c1"
​designation: "designation1"
​email: "vishalg9698@gmail.com"
​name: "check fail "
​password: "Vishal@1"
​phone: "1122334455"
​pincode: "123456"
​state: "s1"
*/
            try {


                let UserActive = "True";
                let UserRole = "Admin";
                let IsTrial = 1

                let value = "count";
                var sql1 = `Select count(*) as ${value} from CountValues`;
                let numberOfItems = await getOrder(sql1, 0);
                numberOfItems = parseInt(numberOfItems[0]['count']) + 1;
                console.log("numberOfItems", numberOfItems);
                /* we are counting number of rows in CountValues Table the find number for UniCode  */
                let universityCode = "TR"
                if (numberOfItems < 10) {
                    universityCode += "00" + numberOfItems
                }
                else if (numberOfItems > 10 && numberOfItems < 100) {
                    universityCode += "0" + numberOfItems
                }
                else {
                    universityCode += numberOfItems
                }
                console.log("universityCode", universityCode);

                const post_University = {
                    Uni_Code: universityCode,
                    Uni_Name: UniveristyName,
                    Uni_Country: UniveristyCountry,
                    Uni_PostalCode: UniveristyPostalCode,
                }
                console.log("post_University", post_University);
                const sqlUniversityInfo = 'INSERT INTO UniversityInfo SET ?'
                console.log("UniversityInfo", await getOrder(sqlUniversityInfo, post_University));

                const valuesCountValues = { UniCode: universityCode, UserCode: 50, UniDeptCode: 1, AdmCode: 1, SchCode: 1, SubjCode: 1, TaskId: 100, ContactCode: 1 };
                const sqlCountValues = 'INSERT INTO CountValues SET ?';
                console.log("CountValues", await getOrder(sqlCountValues, valuesCountValues));

                let userCodeItem = 50;
                let userCode = "USR0"
                userCode += userCodeItem
                console.log("userCode", userCode);

                var post_User = {
                    UserCode: userCode,
                    UserName: UserName,
                    UserEmail: UserEmail,
                    UserRole: UserRole,
                    UserActive: UserActive,
                    IsTrial: 1,
                    UniCode: universityCode,
                    UserDesignation:Designation,
                    UserPhone:UserPhone,
                    UserAddress:Address,
                    UserPinCode:UniveristyPostalCode,
                    ChangeId:1,
                    LastUpdateEpochTime: Math.floor(Date.now() / 1000)
                }
                console.log("post_User", post_User);
                var insertIntoUserInfo = 'INSERT INTO UserInfo SET ?'
                console.log("query", await getOrder(insertIntoUserInfo, post_User));

                try {
                    const university_details = {
                        "name": UniveristyName,
                        "code": universityCode
                    }
                    const paramsEmail = {
                        Destination: {
                            ToAddresses: [UserEmail],
                        },
                        Message: {
                            Body: {
                                Text: { Data: JSON.stringify(university_details) },
                            },

                            Subject: { Data: `University ${UniveristyName} SignUp Details` },
                        },
                        Source: "vishalg@innocurve.com",
                    };
                    await ses.sendEmail(paramsEmail).promise();
                }
                catch (e) {
                    console.log("error sending email", e);
                }

                let res = {
                    statusCode: 200,
                    headers: {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    body: JSON.stringify({ data: "Registered Again" }),
                };
                return res;
            }
            catch (e) {
                console.log("error post sign signup trigger", e);
                let res = {
                    statusCode: 200,
                    headers: {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                        "Access-Control-Allow-Methods": "GET,OPTIONS",
                        "Access-Control-Allow-Origin": "*",
                    },
                    body: JSON.stringify({ error: JSON.stringify(e) }),
                };
                return res;
            }
        }
        else {
            let obj = {
                error: "Error Registering Again",
            };
            let res = {
                statusCode: 200,
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Access-Control-Allow-Origin": "*",
                },
                body: JSON.stringify({ error: obj.error }),
            };
            return res;
        }



    }

};


/*
      let sql = 'SELECT UniversityInfo.Uni_Name,UserInfo.UniCode,UserInfo.UserRole FROM UserInfo INNER JOIN UniversityInfo where UserInfo.UserEmail=? and ChangeId!=? and UserInfo.UniCode=UniversityInfo.Uni_Code';
      var data = await getOrder(sql, [userEmail, 3]);
      console.log("data of user registered more than once", data)
      res = {
         "statusCode": 200,
         "headers": {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
            "Access-Control-Allow-Methods": "GET,OPTIONS",
            "Access-Control-Allow-Origin": "*",
         },
         "body": JSON.stringify(data)
      };
      return res;


*/

async function getTempValueToInsertIntoTable(universityCode) {
    return new Promise(async(resolve, reject) => {
        try {
            var sqlCountValues = 'select * from CountValues where UniCode=?';
            var data = await getOrder(sqlCountValues, [universityCode]);
            var sqlCountValues = 'update CountValues Set ? WHERE UniCode = ?';
            await getOrder(sqlCountValues, [{ UserCode: data[0]["UserCode"] + 1 }, data[0]["UniCode"]]);
            resolve(data[0]["UserCode"] + 1);
        }
        catch (e) {
            console.log("count values failed", e)
            reject(e)
        }
    })

    //await getOrder(sqlTempDelete,'');

}

/*

      if (inputBody.hasOwnProperty("UserEmail") && inputBody.hasOwnProperty("UniCode") && inputBody["UserEmail"] != "" && inputBody["UniCode"] != "" && inputBody.hasOwnProperty("TaskId") && inputBody["TaskId"] != "") {
         let sqlCheck = 'SELECT * FROM UserInfo where UserEmail = ?  and UniCode = ? and  ChangeId != ?';
         let studentCheckData = await getOrder(sqlCheck, [inputBody["UserEmail"], inputBody["UniCode"], 3]);
         console.log("studentCheckData", studentCheckData)
         //checking whether user exist in database
         if (studentCheckData.length == 0) {
            let statusCode = 400;
            let res = {
               "statusCode": statusCode,
               "headers": {
                  "Content-Type": "application/json",
                  "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                  "Access-Control-Allow-Methods": "GET,OPTIONS",
                  "Access-Control-Allow-Origin": "*",
               },
               "body": JSON.stringify({ error: " User does not exist " })
            };

            return res;
         }
         let userCode = studentCheckData[0].UserCode;
         let taskCheck = 'SELECT * FROM UserTask where UserCode = ?  and UniCode = ? and  ChangeId != ? and TaskId= ?';
         let taskCheckData = await getOrder(taskCheck, [userCode, inputBody["UniCode"], 3, inputBody['TaskId']]);
         //checking whether task exist in database
         if (taskCheckData.length == 0) {
            let statusCode = 400;
            let res = {
               "statusCode": statusCode,
               "headers": {
                  "Content-Type": "application/json",
                  "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
                  "Access-Control-Allow-Methods": "GET,OPTIONS",
                  "Access-Control-Allow-Origin": "*",
               },
               "body": JSON.stringify({ error: " Task does not exist " })
            };

            return res;
         }

         let obj = {};

         Object.keys(inputBody).forEach(item => {

            if (!(item == "UserEmail" || item == "UniCode" || item == "TaskId" || item == "LastUpdatedEpochTime" || item == "ChangeId")) {
               console.log("item", item);
               obj[item] = inputBody[item];
            }
         });
         obj["ChangeId"] = 2;
         obj["LastUpdatedEpochTime"] = Math.floor(Date.now() / 1000);
         let sqlInsert = `update UserTask Set ? where UniCode=? and UserCode=? and TaskId=? `
         let userTaskData = await getOrder(sqlInsert, [obj, inputBody["UniCode"], userCode, inputBody["TaskId"]]);
         console.log("userTaskDataInserted", userTaskData);
         let taskIdDatataskId = 'Select max(taskId) as value from UserTask';
         let taskIdData = await getOrder(taskIdDatataskId, '');
         let res = {
            "statusCode": 201,
            "headers": {
               "Content-Type": "application/json",
               "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
               "Access-Control-Allow-Methods": "GET,OPTIONS",
               "Access-Control-Allow-Origin": "*",
            },
            "body": JSON.stringify({ taskId: inputBody["TaskId"] })
         };
         return res;
      }
      else {
         let obj = {
            error: "All keys not found to query the db(UserEmail or UniCode or TaskId)",
         };
         let res = {
            statusCode: 400,
            headers: {
               "Content-Type": "application/json",
               "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
               "Access-Control-Allow-Methods": "GET,OPTIONS",
               "Access-Control-Allow-Origin": "*",
            },
            body: JSON.stringify({ error: obj.error }),
         };
         return res;
      }

*/

let getOrder = async(sql, params) => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if (err) {
                console.log("connection failed")
                reject(err);
            }
            else {
                connection.query(sql, params, (err, results) => {
                    if (err) {
                        console.log("query failed", err)
                        reject(err);
                    }
                    console.log("-----Query Done!");
                    connection.release();
                    //console.log("-----Data: ", results);
                    resolve(results);
                });
            }

        });
    });
};
