import React from "react";
import ReactDOM from "react-dom";
import "crypto-js/lib-typedarrays";
import Amplify from "aws-amplify";
import { Provider } from "react-redux";
import App from "./components";
import store from "./reducers";
Amplify.configure({
  Auth: {
    identityPoolId: "us-east-2:74ab8fa3-1d13-409b-885b-30f03796c657",
    region: "us-east-2",
    userPoolId: "us-east-2_0Q8uWO7H9",
    userPoolWebClientId: "4fm3gmveiomogobaacptr51d78",
    authenticationFlowType: "USER_SRP_AUTH",
  },
  Storage: {
    AWSS3: {
      bucket: "innocurveuniversity", //REQUIRED -  Amazon S3 bucket name
      region: "us-east-2", //OPTIONAL -  Amazon service region
    },
  },
});
ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.querySelector("#root")
);
